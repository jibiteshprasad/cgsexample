package cgs;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author root
 */
public class MainCGS
{
    private static TestServer cgs;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            cgs = new TestServer( Config.gluonServer, Config.gluonPort );
            cgs.gameId = Config.gameId;
            cgs.cgsId = Config.cgsId;
            cgs.connectBlocking();

            while( !cgs.finishedRunning() )
            {
                Thread.sleep( 100 );
                cgs.update();
            }
            
        }
        catch (URISyntaxException ex)
        {
            Logger.getLogger(MainCGS.class.getName()).log(Level.SEVERE, null, ex);
            System.exit( 1 );
        }
        catch( InterruptedException ex )
        {
            Logger.getLogger(MainCGS.class.getName()).log(Level.SEVERE, null, ex);
            System.exit( 1 );
        }

        System.exit( 0 );
    }
}
