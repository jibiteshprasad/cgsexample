package cgs.delegates;

import cgs.messages.CreateGroupRequest;
import cgs.messages.CreateGroupResponse;

public interface CreateGroupResponseDelegate extends GluonResponseDelegate {

    public void onCreateGroupResponse(CreateGroupRequest createGroupRequest, CreateGroupResponse createGroupResponse);

}