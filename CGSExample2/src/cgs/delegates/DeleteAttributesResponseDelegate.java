package cgs.delegates;

import cgs.messages.DeleteAttributesRequest;
import cgs.messages.DeleteAttributesResponse;

public interface DeleteAttributesResponseDelegate extends GluonResponseDelegate {

    public void onDeleteAttributesResponse(DeleteAttributesRequest deleteAttributesRequest, DeleteAttributesResponse deleteAttributesResponse);

}