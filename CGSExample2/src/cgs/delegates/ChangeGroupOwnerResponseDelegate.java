package cgs.delegates;

import cgs.messages.ChangeGroupOwnerRequest;
import cgs.messages.ChangeGroupOwnerResponse;

public interface ChangeGroupOwnerResponseDelegate extends GluonResponseDelegate {

    public void onChangeGroupOwnerResponse(ChangeGroupOwnerRequest changeGroupOwnerRequest, ChangeGroupOwnerResponse changeGroupOwnerResponse);

}