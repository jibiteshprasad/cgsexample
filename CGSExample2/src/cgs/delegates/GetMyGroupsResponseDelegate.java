package cgs.delegates;

import cgs.messages.GetMyGroupsRequest;
import cgs.messages.GetMyGroupsResponse;

public interface GetMyGroupsResponseDelegate extends GluonResponseDelegate {

    public void onGetMyGroupsResponse(GetMyGroupsRequest getMyGroupsRequest, GetMyGroupsResponse getMyGroupsResponse);

}