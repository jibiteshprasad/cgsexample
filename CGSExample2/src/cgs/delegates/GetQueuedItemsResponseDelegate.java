package cgs.delegates;

import cgs.messages.GetQueuedItemsRequest;
import cgs.messages.GetQueuedItemsResponse;

public interface GetQueuedItemsResponseDelegate extends GluonResponseDelegate {

    public void onGetQueuedItemsResponse(GetQueuedItemsRequest getQueuedItemsRequest, GetQueuedItemsResponse getQueuedItemsResponse);

}