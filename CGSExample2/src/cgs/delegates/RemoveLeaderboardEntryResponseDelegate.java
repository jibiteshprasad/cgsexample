package cgs.delegates;

import cgs.messages.RemoveLeaderboardEntryRequest;
import cgs.messages.RemoveLeaderboardEntryResponse;

public interface RemoveLeaderboardEntryResponseDelegate extends GluonResponseDelegate {

    public void onRemoveLeaderboardEntryResponse(RemoveLeaderboardEntryRequest removeLeaderboardEntryRequest, RemoveLeaderboardEntryResponse removeLeaderboardEntryResponse);

}