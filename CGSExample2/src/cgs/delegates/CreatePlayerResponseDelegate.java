package cgs.delegates;

import cgs.messages.CreatePlayerRequest;
import cgs.messages.CreatePlayerResponse;

public interface CreatePlayerResponseDelegate extends GluonResponseDelegate {

    public void onCreatePlayerResponse(CreatePlayerRequest createPlayerRequest, CreatePlayerResponse createPlayerResponse);

}