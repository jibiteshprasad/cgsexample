package cgs.delegates;

import cgs.messages.DeleteGroupMemberAttributesForAllMembersRequest;
import cgs.messages.DeleteGroupMemberAttributesForAllMembersResponse;

public interface DeleteGroupMemberAttributesForAllMembersResponseDelegate extends GluonResponseDelegate {

    public void onDeleteGroupMemberAttributesForAllMembersResponse(DeleteGroupMemberAttributesForAllMembersRequest deleteGroupMemberAttributesForAllMembersRequest, DeleteGroupMemberAttributesForAllMembersResponse deleteGroupMemberAttributesForAllMembersResponse);

}