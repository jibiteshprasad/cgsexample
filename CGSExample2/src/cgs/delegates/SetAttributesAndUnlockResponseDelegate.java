package cgs.delegates;

import cgs.messages.SetAttributesAndUnlockRequest;
import cgs.messages.SetAttributesAndUnlockResponse;

public interface SetAttributesAndUnlockResponseDelegate extends GluonResponseDelegate {

    public void onSetAttributesAndUnlockResponse(SetAttributesAndUnlockRequest setAttributesAndUnlockRequest, SetAttributesAndUnlockResponse setAttributesAndUnlockResponse);

}