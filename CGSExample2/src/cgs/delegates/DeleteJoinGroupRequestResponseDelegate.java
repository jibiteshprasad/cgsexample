package cgs.delegates;

import cgs.messages.DeleteJoinGroupRequestRequest;
import cgs.messages.DeleteJoinGroupRequestResponse;

public interface DeleteJoinGroupRequestResponseDelegate extends GluonResponseDelegate {

    public void onDeleteJoinGroupRequestResponse(DeleteJoinGroupRequestRequest deleteJoinGroupRequestRequest, DeleteJoinGroupRequestResponse deleteJoinGroupRequestResponse);

}