package cgs.delegates;

import cgs.messages.CheckGroupNameRequest;
import cgs.messages.CheckGroupNameResponse;

public interface CheckGroupNameResponseDelegate extends GluonResponseDelegate {

    public void onCheckGroupNameResponse(CheckGroupNameRequest checkGroupNameRequest, CheckGroupNameResponse checkGroupNameResponse);

}