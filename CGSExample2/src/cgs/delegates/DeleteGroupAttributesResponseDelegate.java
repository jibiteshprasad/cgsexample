package cgs.delegates;

import cgs.messages.DeleteGroupAttributesRequest;
import cgs.messages.DeleteGroupAttributesResponse;

public interface DeleteGroupAttributesResponseDelegate extends GluonResponseDelegate {

    public void onDeleteGroupAttributesResponse(DeleteGroupAttributesRequest deleteGroupAttributesRequest, DeleteGroupAttributesResponse deleteGroupAttributesResponse);

}