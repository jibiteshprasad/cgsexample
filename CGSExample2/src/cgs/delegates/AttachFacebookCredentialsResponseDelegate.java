package cgs.delegates;

import cgs.messages.AttachFacebookCredentialsRequest;
import cgs.messages.AttachFacebookCredentialsResponse;

public interface AttachFacebookCredentialsResponseDelegate extends GluonResponseDelegate {

    public void onAttachFacebookCredentialsResponse(AttachFacebookCredentialsRequest attachFacebookCredentialsRequest, AttachFacebookCredentialsResponse attachFacebookCredentialsResponse);

}