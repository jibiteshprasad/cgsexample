package cgs.delegates;

import cgs.messages.SetLeaderboardEntryRequest;
import cgs.messages.SetLeaderboardEntryResponse;

public interface SetLeaderboardEntryResponseDelegate extends GluonResponseDelegate {

    public void onSetLeaderboardEntryResponse(SetLeaderboardEntryRequest setLeaderboardEntryRequest, SetLeaderboardEntryResponse setLeaderboardEntryResponse);

}