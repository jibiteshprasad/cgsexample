package cgs.delegates;

import cgs.messages.RemovePlayerFromGroupRequest;
import cgs.messages.RemovePlayerFromGroupResponse;

public interface RemovePlayerFromGroupResponseDelegate extends GluonResponseDelegate {

    public void onRemovePlayerFromGroupResponse(RemovePlayerFromGroupRequest removePlayerFromGroupRequest, RemovePlayerFromGroupResponse removePlayerFromGroupResponse);

}