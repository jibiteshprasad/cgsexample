package cgs.delegates;

import cgs.messages.GetRandomGroupsRequest;
import cgs.messages.GetRandomGroupsResponse;

public interface GetRandomGroupsResponseDelegate extends GluonResponseDelegate {

    public void onGetRandomGroupsResponse(GetRandomGroupsRequest getRandomGroupsRequest, GetRandomGroupsResponse getRandomGroupsResponse);

}