package cgs.delegates;

import cgs.messages.SetGroupAttributesRequest;
import cgs.messages.SetGroupAttributesResponse;

public interface SetGroupAttributesResponseDelegate extends GluonResponseDelegate {

    public void onSetGroupAttributesResponse(SetGroupAttributesRequest setGroupAttributesRequest, SetGroupAttributesResponse setGroupAttributesResponse);

}