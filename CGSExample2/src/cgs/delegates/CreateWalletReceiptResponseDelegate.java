package cgs.delegates;

import cgs.messages.CreateWalletReceiptRequest;
import cgs.messages.CreateWalletReceiptResponse;

public interface CreateWalletReceiptResponseDelegate extends GluonResponseDelegate {

    public void onCreateWalletReceiptResponse(CreateWalletReceiptRequest createWalletReceiptRequest, CreateWalletReceiptResponse createWalletReceiptResponse);

}