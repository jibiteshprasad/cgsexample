package cgs.delegates;

import cgs.messages.SetDocumentRequest;
import cgs.messages.SetDocumentResponse;

public interface SetDocumentResponseDelegate extends GluonResponseDelegate {

    public void onSetDocumentResponse(SetDocumentRequest setDocumentRequest, SetDocumentResponse setDocumentResponse);

}