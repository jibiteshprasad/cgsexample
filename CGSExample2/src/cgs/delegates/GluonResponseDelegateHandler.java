package cgs.delegates;
import cgs.messages.GluonRequest;

import java.util.Date;


/**
 *
 * @author Trevor Dasch
 */
public class GluonResponseDelegateHandler {
    
    public static final long DEFAULT_TIMEOUT = 5000;
    
    public GluonRequest request;
    
    public GluonResponseDelegate delegate;
    
    Date timeoutDate;
    
    public GluonResponseDelegateHandler(GluonRequest request, GluonResponseDelegate delegate){
        this(request, delegate, DEFAULT_TIMEOUT);
    }
    
    public GluonResponseDelegateHandler(GluonRequest request, GluonResponseDelegate delegate, long timeout){
        this.request = request;
        this.delegate = delegate;
        
        if(timeout <= 0){
            timeout = DEFAULT_TIMEOUT;
        }
        
        Date sendTime = new Date();
        this.timeoutDate = new Date(sendTime.getTime() + timeout);        
    }
    
    public boolean isExpired(){
        return new Date().after(timeoutDate);
    }
}
