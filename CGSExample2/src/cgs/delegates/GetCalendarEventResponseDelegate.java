package cgs.delegates;

import cgs.messages.GetCalendarEventRequest;
import cgs.messages.GetCalendarEventResponse;

public interface GetCalendarEventResponseDelegate extends GluonResponseDelegate {

    public void onGetCalendarEventResponse(GetCalendarEventRequest getCalendarEventRequest, GetCalendarEventResponse getCalendarEventResponse);

}