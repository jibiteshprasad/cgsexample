package cgs.delegates;

import cgs.messages.GetDocumentsRequest;
import cgs.messages.GetDocumentsResponse;

public interface GetDocumentsResponseDelegate extends GluonResponseDelegate {

    public void onGetDocumentsResponse(GetDocumentsRequest getDocumentsRequest, GetDocumentsResponse getDocumentsResponse);

}