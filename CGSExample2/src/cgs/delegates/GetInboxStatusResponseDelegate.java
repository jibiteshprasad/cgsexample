package cgs.delegates;

import cgs.messages.GetInboxStatusRequest;
import cgs.messages.GetInboxStatusResponse;

public interface GetInboxStatusResponseDelegate extends GluonResponseDelegate {

    public void onGetInboxStatusResponse(GetInboxStatusRequest getInboxStatusRequest, GetInboxStatusResponse getInboxStatusResponse);

}