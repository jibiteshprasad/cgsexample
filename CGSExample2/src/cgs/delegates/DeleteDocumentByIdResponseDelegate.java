package cgs.delegates;

import cgs.messages.DeleteDocumentByIdRequest;
import cgs.messages.DeleteDocumentByIdResponse;

public interface DeleteDocumentByIdResponseDelegate extends GluonResponseDelegate {

    public void onDeleteDocumentByIdResponse(DeleteDocumentByIdRequest deleteDocumentByIdRequest, DeleteDocumentByIdResponse deleteDocumentByIdResponse);

}