package cgs.delegates;

import cgs.messages.DeleteJoinGroupInviteRequest;
import cgs.messages.DeleteJoinGroupInviteResponse;

public interface DeleteJoinGroupInviteResponseDelegate extends GluonResponseDelegate {

    public void onDeleteJoinGroupInviteResponse(DeleteJoinGroupInviteRequest deleteJoinGroupInviteRequest, DeleteJoinGroupInviteResponse deleteJoinGroupInviteResponse);

}