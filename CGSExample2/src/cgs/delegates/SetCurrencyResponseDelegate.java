package cgs.delegates;

import cgs.messages.SetCurrencyRequest;
import cgs.messages.SetCurrencyResponse;

public interface SetCurrencyResponseDelegate extends GluonResponseDelegate {

    public void onSetCurrencyResponse(SetCurrencyRequest setCurrencyRequest, SetCurrencyResponse setCurrencyResponse);

}