package cgs.delegates;

import cgs.messages.GetCalendarEventsRequest;
import cgs.messages.GetCalendarEventsResponse;

public interface GetCalendarEventsResponseDelegate extends GluonResponseDelegate {

    public void onGetCalendarEventsResponse(GetCalendarEventsRequest getCalendarEventsRequest, GetCalendarEventsResponse getCalendarEventsResponse);

}