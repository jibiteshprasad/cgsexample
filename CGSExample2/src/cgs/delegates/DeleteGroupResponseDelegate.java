package cgs.delegates;

import cgs.messages.DeleteGroupRequest;
import cgs.messages.DeleteGroupResponse;

public interface DeleteGroupResponseDelegate extends GluonResponseDelegate {

    public void onDeleteGroupResponse(DeleteGroupRequest deleteGroupRequest, DeleteGroupResponse deleteGroupResponse);

}