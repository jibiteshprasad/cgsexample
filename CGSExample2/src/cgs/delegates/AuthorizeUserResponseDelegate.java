package cgs.delegates;

import cgs.messages.AuthorizeUserRequest;
import cgs.messages.AuthorizeUserResponse;

public interface AuthorizeUserResponseDelegate extends GluonResponseDelegate {

    public void onAuthorizeUserResponse(AuthorizeUserRequest authorizeUserRequest, AuthorizeUserResponse authorizeUserResponse);

}