package cgs.delegates;

import cgs.messages.GetCustomGameServerIdRequest;
import cgs.messages.GetCustomGameServerIdResponse;

public interface GetCustomGameServerIdResponseDelegate extends GluonResponseDelegate {

    public void onGetCustomGameServerIdResponse(GetCustomGameServerIdRequest getCustomGameServerIdRequest, GetCustomGameServerIdResponse getCustomGameServerIdResponse);

}