package cgs.delegates;

import cgs.messages.GetGroupStatusRequest;
import cgs.messages.GetGroupStatusResponse;

public interface GetGroupStatusResponseDelegate extends GluonResponseDelegate {

    public void onGetGroupStatusResponse(GetGroupStatusRequest getGroupStatusRequest, GetGroupStatusResponse getGroupStatusResponse);

}