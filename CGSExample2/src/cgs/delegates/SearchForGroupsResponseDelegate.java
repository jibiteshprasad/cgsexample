package cgs.delegates;

import cgs.messages.SearchForGroupsRequest;
import cgs.messages.SearchForGroupsResponse;

public interface SearchForGroupsResponseDelegate extends GluonResponseDelegate {

    public void onSearchForGroupsResponse(SearchForGroupsRequest searchForGroupsRequest, SearchForGroupsResponse searchForGroupsResponse);

}