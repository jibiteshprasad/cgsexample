package cgs.delegates;

import cgs.messages.ClaimQueuedItemsRollbackRequest;
import cgs.messages.ClaimQueuedItemsRollbackResponse;

public interface ClaimQueuedItemsRollbackResponseDelegate extends GluonResponseDelegate {

    public void onClaimQueuedItemsRollbackResponse(ClaimQueuedItemsRollbackRequest claimQueuedItemsRollbackRequest, ClaimQueuedItemsRollbackResponse claimQueuedItemsRollbackResponse);

}