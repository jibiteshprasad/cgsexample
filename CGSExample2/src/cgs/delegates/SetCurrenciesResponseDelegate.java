package cgs.delegates;

import cgs.messages.SetCurrenciesRequest;
import cgs.messages.SetCurrenciesResponse;

public interface SetCurrenciesResponseDelegate extends GluonResponseDelegate {

    public void onSetCurrenciesResponse(SetCurrenciesRequest setCurrenciesRequest, SetCurrenciesResponse setCurrenciesResponse);

}