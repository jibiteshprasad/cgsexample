package cgs.delegates;

import cgs.messages.GetAttributeForPlayersRequest;
import cgs.messages.GetAttributeForPlayersResponse;

public interface GetAttributeForPlayersResponseDelegate extends GluonResponseDelegate {

    public void onGetAttributeForPlayersResponse(GetAttributeForPlayersRequest getAttributeForPlayersRequest, GetAttributeForPlayersResponse getAttributeForPlayersResponse);

}