package cgs.delegates;

import cgs.messages.ChangeGroupNameRequest;
import cgs.messages.ChangeGroupNameResponse;

public interface ChangeGroupNameResponseDelegate extends GluonResponseDelegate {

    public void onChangeGroupNameResponse(ChangeGroupNameRequest changeGroupNameRequest, ChangeGroupNameResponse changeGroupNameResponse);

}