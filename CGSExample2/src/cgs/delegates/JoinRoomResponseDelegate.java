package cgs.delegates;

import cgs.messages.JoinRoomRequest;
import cgs.messages.JoinRoomResponse;

public interface JoinRoomResponseDelegate extends GluonResponseDelegate {

    public void onJoinRoomResponse(JoinRoomRequest joinRoomRequest, JoinRoomResponse joinRoomResponse);

}