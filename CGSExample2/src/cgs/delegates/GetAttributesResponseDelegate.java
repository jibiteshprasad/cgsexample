package cgs.delegates;

import cgs.messages.GetAttributesRequest;
import cgs.messages.GetAttributesResponse;

public interface GetAttributesResponseDelegate extends GluonResponseDelegate {

    public void onGetAttributesResponse(GetAttributesRequest getAttributesRequest, GetAttributesResponse getAttributesResponse);

}