package cgs.delegates;

import cgs.messages.GetSentMailMessagesRequest;
import cgs.messages.GetSentMailMessagesResponse;

public interface GetSentMailMessagesResponseDelegate extends GluonResponseDelegate {

    public void onGetSentMailMessagesResponse(GetSentMailMessagesRequest getSentMailMessagesRequest, GetSentMailMessagesResponse getSentMailMessagesResponse);

}