package cgs.delegates;

import cgs.messages.MarkMailAsReadRequest;
import cgs.messages.MarkMailAsReadResponse;

public interface MarkMailAsReadResponseDelegate extends GluonResponseDelegate {

    public void onMarkMailAsReadResponse(MarkMailAsReadRequest markMailAsReadRequest, MarkMailAsReadResponse markMailAsReadResponse);

}