package cgs.delegates;

import cgs.messages.SendRoomMessageRequest;
import cgs.messages.SendRoomMessageResponse;

public interface SendRoomMessageResponseDelegate extends GluonResponseDelegate {

    public void onSendRoomMessageResponse(SendRoomMessageRequest sendRoomMessageRequest, SendRoomMessageResponse sendRoomMessageResponse);

}