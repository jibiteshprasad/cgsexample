package cgs.delegates;

import cgs.messages.LockAndGetAttributesRequest;
import cgs.messages.LockAndGetAttributesResponse;

public interface LockAndGetAttributesResponseDelegate extends GluonResponseDelegate {

    public void onLockAndGetAttributesResponse(LockAndGetAttributesRequest lockAndGetAttributesRequest, LockAndGetAttributesResponse lockAndGetAttributesResponse);

}