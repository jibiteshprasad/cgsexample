package cgs.delegates;

import cgs.messages.ConsumeIapReceiptRequest;
import cgs.messages.ConsumeIapReceiptResponse;

public interface ConsumeIapReceiptResponseDelegate extends GluonResponseDelegate {

    public void onConsumeIapReceiptResponse(ConsumeIapReceiptRequest consumeIapReceiptRequest, ConsumeIapReceiptResponse consumeIapReceiptResponse);

}