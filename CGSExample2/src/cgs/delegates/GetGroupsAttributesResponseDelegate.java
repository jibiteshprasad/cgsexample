package cgs.delegates;

import cgs.messages.GetGroupsAttributesRequest;
import cgs.messages.GetGroupsAttributesResponse;

public interface GetGroupsAttributesResponseDelegate extends GluonResponseDelegate {

    public void onGetGroupsAttributesResponse(GetGroupsAttributesRequest getGroupsAttributesRequest, GetGroupsAttributesResponse getGroupsAttributesResponse);

}