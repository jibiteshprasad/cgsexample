package cgs.delegates;

import cgs.messages.ServiceStatusRequest;
import cgs.messages.ServiceStatusResponse;

public interface ServiceStatusResponseDelegate extends GluonResponseDelegate {

    public void onServiceStatusResponse(ServiceStatusRequest serviceStatusRequest, ServiceStatusResponse serviceStatusResponse);

}