package cgs.delegates;

import cgs.messages.GetAllAttributesRequest;
import cgs.messages.GetAllAttributesResponse;

public interface GetAllAttributesResponseDelegate extends GluonResponseDelegate {

    public void onGetAllAttributesResponse(GetAllAttributesRequest getAllAttributesRequest, GetAllAttributesResponse getAllAttributesResponse);

}