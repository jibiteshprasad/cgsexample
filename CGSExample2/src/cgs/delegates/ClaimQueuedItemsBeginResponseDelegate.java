package cgs.delegates;

import cgs.messages.ClaimQueuedItemsBeginRequest;
import cgs.messages.ClaimQueuedItemsBeginResponse;

public interface ClaimQueuedItemsBeginResponseDelegate extends GluonResponseDelegate {

    public void onClaimQueuedItemsBeginResponse(ClaimQueuedItemsBeginRequest claimQueuedItemsBeginRequest, ClaimQueuedItemsBeginResponse claimQueuedItemsBeginResponse);

}