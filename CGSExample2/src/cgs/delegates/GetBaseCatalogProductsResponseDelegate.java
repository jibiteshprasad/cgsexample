package cgs.delegates;

import cgs.messages.GetBaseCatalogProductsRequest;
import cgs.messages.GetBaseCatalogProductsResponse;

public interface GetBaseCatalogProductsResponseDelegate extends GluonResponseDelegate {

    public void onGetBaseCatalogProductsResponse(GetBaseCatalogProductsRequest getBaseCatalogProductsRequest, GetBaseCatalogProductsResponse getBaseCatalogProductsResponse);

}