package cgs.delegates;

import cgs.messages.SendPrivateMessageRequest;
import cgs.messages.SendPrivateMessageResponse;

public interface SendPrivateMessageResponseDelegate extends GluonResponseDelegate {

    public void onSendPrivateMessageResponse(SendPrivateMessageRequest sendPrivateMessageRequest, SendPrivateMessageResponse sendPrivateMessageResponse);

}