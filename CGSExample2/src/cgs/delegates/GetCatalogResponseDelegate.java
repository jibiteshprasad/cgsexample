package cgs.delegates;

import cgs.messages.GetCatalogRequest;
import cgs.messages.GetCatalogResponse;

public interface GetCatalogResponseDelegate extends GluonResponseDelegate {

    public void onGetCatalogResponse(GetCatalogRequest getCatalogRequest, GetCatalogResponse getCatalogResponse);

}