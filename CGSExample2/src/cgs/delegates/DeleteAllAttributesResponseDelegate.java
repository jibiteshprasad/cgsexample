package cgs.delegates;

import cgs.messages.DeleteAllAttributesRequest;
import cgs.messages.DeleteAllAttributesResponse;

public interface DeleteAllAttributesResponseDelegate extends GluonResponseDelegate {

    public void onDeleteAllAttributesResponse(DeleteAllAttributesRequest deleteAllAttributesRequest, DeleteAllAttributesResponse deleteAllAttributesResponse);

}