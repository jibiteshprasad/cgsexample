package cgs.delegates;

import cgs.messages.SetDocumentByIdRequest;
import cgs.messages.SetDocumentByIdResponse;

public interface SetDocumentByIdResponseDelegate extends GluonResponseDelegate {

    public void onSetDocumentByIdResponse(SetDocumentByIdRequest setDocumentByIdRequest, SetDocumentByIdResponse setDocumentByIdResponse);

}