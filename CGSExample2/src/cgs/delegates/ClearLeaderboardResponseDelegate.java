package cgs.delegates;

import cgs.messages.ClearLeaderboardRequest;
import cgs.messages.ClearLeaderboardResponse;

public interface ClearLeaderboardResponseDelegate extends GluonResponseDelegate {

    public void onClearLeaderboardResponse(ClearLeaderboardRequest clearLeaderboardRequest, ClearLeaderboardResponse clearLeaderboardResponse);

}