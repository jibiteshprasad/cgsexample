package cgs.delegates;

import cgs.messages.GetMatchesRequest;
import cgs.messages.GetMatchesResponse;

public interface GetMatchesResponseDelegate extends GluonResponseDelegate {

    public void onGetMatchesResponse(GetMatchesRequest getMatchesRequest, GetMatchesResponse getMatchesResponse);

}