package cgs.delegates;

import cgs.messages.GetJoinedRoomsRequest;
import cgs.messages.GetJoinedRoomsResponse;

public interface GetJoinedRoomsResponseDelegate extends GluonResponseDelegate {

    public void onGetJoinedRoomsResponse(GetJoinedRoomsRequest getJoinedRoomsRequest, GetJoinedRoomsResponse getJoinedRoomsResponse);

}