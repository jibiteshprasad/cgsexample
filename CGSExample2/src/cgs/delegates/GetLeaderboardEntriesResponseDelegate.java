package cgs.delegates;

import cgs.messages.GetLeaderboardEntriesRequest;
import cgs.messages.GetLeaderboardEntriesResponse;

public interface GetLeaderboardEntriesResponseDelegate extends GluonResponseDelegate {

    public void onGetLeaderboardEntriesResponse(GetLeaderboardEntriesRequest getLeaderboardEntriesRequest, GetLeaderboardEntriesResponse getLeaderboardEntriesResponse);

}