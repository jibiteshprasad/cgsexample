package cgs.delegates;

import cgs.messages.LeaveRoomRequest;
import cgs.messages.LeaveRoomResponse;

public interface LeaveRoomResponseDelegate extends GluonResponseDelegate {

    public void onLeaveRoomResponse(LeaveRoomRequest leaveRoomRequest, LeaveRoomResponse leaveRoomResponse);

}