package cgs.delegates;

import cgs.messages.GetCurrenciesRequest;
import cgs.messages.GetCurrenciesResponse;

public interface GetCurrenciesResponseDelegate extends GluonResponseDelegate {

    public void onGetCurrenciesResponse(GetCurrenciesRequest getCurrenciesRequest, GetCurrenciesResponse getCurrenciesResponse);

}