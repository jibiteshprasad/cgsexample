package cgs.delegates;

import cgs.messages.SetAsPrimaryPlayerRequest;
import cgs.messages.SetAsPrimaryPlayerResponse;

public interface SetAsPrimaryPlayerResponseDelegate {

public void onSetAsPrimaryPlayerResponse(SetAsPrimaryPlayerRequest setAsPrimaryPlayerRequest, SetAsPrimaryPlayerResponse setAsPrimaryPlayerResponse);

}