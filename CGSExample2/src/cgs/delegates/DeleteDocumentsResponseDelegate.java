package cgs.delegates;

import cgs.messages.DeleteDocumentsRequest;
import cgs.messages.DeleteDocumentsResponse;

public interface DeleteDocumentsResponseDelegate extends GluonResponseDelegate {

    public void onDeleteDocumentsResponse(DeleteDocumentsRequest deleteDocumentsRequest, DeleteDocumentsResponse deleteDocumentsResponse);

}