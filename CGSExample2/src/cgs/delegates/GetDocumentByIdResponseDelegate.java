package cgs.delegates;

import cgs.messages.GetDocumentByIdRequest;
import cgs.messages.GetDocumentByIdResponse;

public interface GetDocumentByIdResponseDelegate extends GluonResponseDelegate {

    public void onGetDocumentByIdResponse(GetDocumentByIdRequest getDocumentByIdRequest, GetDocumentByIdResponse getDocumentByIdResponse);

}