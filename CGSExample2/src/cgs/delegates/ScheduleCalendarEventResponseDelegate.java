package cgs.delegates;

import cgs.messages.ScheduleCalendarEventRequest;
import cgs.messages.ScheduleCalendarEventResponse;

    public interface ScheduleCalendarEventResponseDelegate extends GluonResponseDelegate {

public void onScheduleCalendarEventResponse(ScheduleCalendarEventRequest scheduleCalendarEventRequest, ScheduleCalendarEventResponse scheduleCalendarEventResponse);

}