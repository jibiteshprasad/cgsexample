package cgs.delegates;

import cgs.messages.SearchForPlayersRequest;
import cgs.messages.SearchForPlayersResponse;

public interface SearchForPlayersResponseDelegate extends GluonResponseDelegate {

    public void onSearchForPlayersResponse(SearchForPlayersRequest searchForPlayersRequest, SearchForPlayersResponse searchForPlayersResponse);

}