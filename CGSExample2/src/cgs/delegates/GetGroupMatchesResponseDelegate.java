package cgs.delegates;

import cgs.messages.GetGroupMatchesRequest;
import cgs.messages.GetGroupMatchesResponse;

public interface GetGroupMatchesResponseDelegate extends GluonResponseDelegate {

    public void onGetGroupMatchesResponse(GetGroupMatchesRequest getGroupMatchesRequest, GetGroupMatchesResponse getGroupMatchesResponse);

}