package cgs.delegates;

import cgs.messages.LeaveGroupRequest;
import cgs.messages.LeaveGroupResponse;

public interface LeaveGroupResponseDelegate extends GluonResponseDelegate {

    public void onLeaveGroupResponse(LeaveGroupRequest leaveGroupRequest, LeaveGroupResponse leaveGroupResponse);

}