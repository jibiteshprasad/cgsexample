package cgs.delegates;

import cgs.messages.GetLeaderboardEntriesByRankRangeRequest;
import cgs.messages.GetLeaderboardEntriesByRankRangeResponse;

public interface GetLeaderboardEntriesByRankRangeResponseDelegate extends GluonResponseDelegate {

    public void onGetLeaderboardEntriesByRankRangeResponse(GetLeaderboardEntriesByRankRangeRequest getLeaderboardEntriesByRankRangeRequest, GetLeaderboardEntriesByRankRangeResponse getLeaderboardEntriesByRankRangeResponse);

}