package cgs.delegates;

import cgs.messages.GetCurrencyRequest;
import cgs.messages.GetCurrencyResponse;

public interface GetCurrencyResponseDelegate extends GluonResponseDelegate {

    public void onGetCurrencyResponse(GetCurrencyRequest getCurrencyRequest, GetCurrencyResponse getCurrencyResponse);

}