package cgs.delegates;

import cgs.messages.ClaimQueuedItemsCommitRequest;
import cgs.messages.ClaimQueuedItemsCommitResponse;

public interface ClaimQueuedItemsCommitResponseDelegate extends GluonResponseDelegate {

    public void onClaimQueuedItemsCommitResponse(ClaimQueuedItemsCommitRequest claimQueuedItemsCommitRequest, ClaimQueuedItemsCommitResponse claimQueuedItemsCommitResponse);

}