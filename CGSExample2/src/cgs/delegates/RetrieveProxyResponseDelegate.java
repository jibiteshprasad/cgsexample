package cgs.delegates;

import cgs.messages.RetrieveProxyRequest;
import cgs.messages.RetrieveProxyResponse;

public interface RetrieveProxyResponseDelegate {

public void onRetrieveProxyResponse(RetrieveProxyRequest retrieveProxyRequest, RetrieveProxyResponse retrieveProxyResponse);

}