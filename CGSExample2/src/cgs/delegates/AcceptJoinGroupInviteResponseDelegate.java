package cgs.delegates;

import cgs.messages.AcceptJoinGroupInviteRequest;
import cgs.messages.AcceptJoinGroupInviteResponse;

public interface AcceptJoinGroupInviteResponseDelegate extends GluonResponseDelegate {

    public void onAcceptJoinGroupInviteResponse(AcceptJoinGroupInviteRequest acceptJoinGroupInviteRequest, AcceptJoinGroupInviteResponse acceptJoinGroupInviteResponse);

}