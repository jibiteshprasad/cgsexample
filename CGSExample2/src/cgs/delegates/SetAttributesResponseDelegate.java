package cgs.delegates;

import cgs.messages.SetAttributesRequest;
import cgs.messages.SetAttributesResponse;

public interface SetAttributesResponseDelegate extends GluonResponseDelegate {

    public void onSetAttributesResponse(SetAttributesRequest setAttributesRequest, SetAttributesResponse setAttributesResponse);

}