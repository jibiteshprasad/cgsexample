package cgs.delegates;

import cgs.messages.ValidateIapReceiptRequest;
import cgs.messages.ValidateIapReceiptResponse;

public interface ValidateIapReceiptResponseDelegate extends GluonResponseDelegate {

    public void onValidateIapReceiptResponse(ValidateIapReceiptRequest validateIapReceiptRequest, ValidateIapReceiptResponse validateIapReceiptResponse);

}