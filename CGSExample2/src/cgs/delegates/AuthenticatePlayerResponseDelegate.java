package cgs.delegates;

import cgs.messages.AuthenticatePlayerRequest;
import cgs.messages.AuthenticatePlayerResponse;

public interface AuthenticatePlayerResponseDelegate extends GluonResponseDelegate {

    public void onAuthenticatePlayerResponse(AuthenticatePlayerRequest authenticatePlayerRequest, AuthenticatePlayerResponse authenticatePlayerResponse);

}