package cgs.delegates;

import cgs.messages.SetGroupStatusRequest;
import cgs.messages.SetGroupStatusResponse;

public interface SetGroupStatusResponseDelegate extends GluonResponseDelegate {

    public void onSetGroupStatusResponse(SetGroupStatusRequest setGroupStatusRequest, SetGroupStatusResponse setGroupStatusResponse);

}