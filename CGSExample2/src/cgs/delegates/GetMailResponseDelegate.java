package cgs.delegates;

import cgs.messages.GetMailRequest;
import cgs.messages.GetMailResponse;

public interface GetMailResponseDelegate extends GluonResponseDelegate {

    public void onGetMailResponse(GetMailRequest getMailRequest, GetMailResponse getMailResponse);

}