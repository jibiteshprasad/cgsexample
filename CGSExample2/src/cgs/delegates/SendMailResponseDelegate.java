package cgs.delegates;

import cgs.messages.SendMailRequest;
import cgs.messages.SendMailResponse;

public interface SendMailResponseDelegate extends GluonResponseDelegate {

    public void onSendMailResponse(SendMailRequest sendMailRequest, SendMailResponse sendMailResponse);

}