package cgs.delegates;

import cgs.messages.CheckUserStateRequest;
import cgs.messages.CheckUserStateResponse;

public interface CheckUserStateResponseDelegate extends GluonResponseDelegate {

    public void onCheckUserStateResponse(CheckUserStateRequest checkUserStateRequest, CheckUserStateResponse checkUserStateResponse);

}