package cgs.delegates;

import cgs.messages.GetFederatedTokenRequest;
import cgs.messages.GetFederatedTokenResponse;

public interface GetFederatedTokenResponseDelegate extends GluonResponseDelegate {

    public void onGetFederatedTokenResponse(GetFederatedTokenRequest getFederatedTokenRequest, GetFederatedTokenResponse getFederatedTokenResponse);

}