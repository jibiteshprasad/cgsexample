package cgs.delegates;

import cgs.messages.GetMySentGroupInvitesRequest;
import cgs.messages.GetMySentGroupInvitesResponse;

public interface GetMySentGroupInvitesResponseDelegate extends GluonResponseDelegate {

    public void onGetMySentGroupInvitesResponse(GetMySentGroupInvitesRequest getMySentGroupInvitesRequest, GetMySentGroupInvitesResponse getMySentGroupInvitesResponse);

}