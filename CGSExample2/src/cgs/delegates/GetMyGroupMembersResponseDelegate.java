package cgs.delegates;

import cgs.messages.GetMyGroupMembersRequest;
import cgs.messages.GetMyGroupMembersResponse;

public interface GetMyGroupMembersResponseDelegate extends GluonResponseDelegate {

    public void onGetMyGroupMembersResponse(GetMyGroupMembersRequest getMyGroupMembersRequest, GetMyGroupMembersResponse getMyGroupMembersResponse);

}