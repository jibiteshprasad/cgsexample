package cgs.delegates;

import cgs.messages.GetGroupAttributesRequest;
import cgs.messages.GetGroupAttributesResponse;

public interface GetGroupAttributesResponseDelegate extends GluonResponseDelegate {

    public void onGetGroupAttributesResponse(GetGroupAttributesRequest getGroupAttributesRequest, GetGroupAttributesResponse getGroupAttributesResponse);

}