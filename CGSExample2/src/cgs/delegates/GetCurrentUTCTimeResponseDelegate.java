package cgs.delegates;

import cgs.messages.GetCurrentUTCTimeRequest;
import cgs.messages.GetCurrentUTCTimeResponse;

public interface GetCurrentUTCTimeResponseDelegate extends GluonResponseDelegate {

    public void onGetCurrentUTCTimeResponse(GetCurrentUTCTimeRequest getCurrentUTCTimeRequest, GetCurrentUTCTimeResponse getCurrentUTCTimeResponse);

}