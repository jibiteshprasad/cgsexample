package cgs.delegates;

import cgs.messages.GetPlayersInRoomRequest;
import cgs.messages.GetPlayersInRoomResponse;

public interface GetPlayersInRoomResponseDelegate extends GluonResponseDelegate {

    public void onGetPlayersInRoomResponse(GetPlayersInRoomRequest getPlayersInRoomRequest, GetPlayersInRoomResponse getPlayersInRoomResponse);

}