package cgs.delegates;

import cgs.messages.SetBaseCatalogProductsRequest;
import cgs.messages.SetBaseCatalogProductsResponse;

public interface SetBaseCatalogProductsResponseDelegate extends GluonResponseDelegate {

    public void onSetBaseCatalogProductsResponse(SetBaseCatalogProductsRequest setBaseCatalogProductsRequest, SetBaseCatalogProductsResponse setBaseCatalogProductsResponse);

}