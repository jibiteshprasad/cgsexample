package cgs.delegates;

import cgs.messages.SendJoinGroupRequestRequest;
import cgs.messages.SendJoinGroupRequestResponse;

public interface SendJoinGroupRequestResponseDelegate extends GluonResponseDelegate {

    public void onSendJoinGroupRequestResponse(SendJoinGroupRequestRequest sendJoinGroupRequestRequest, SendJoinGroupRequestResponse sendJoinGroupRequestResponse);

}