package cgs.delegates;

import cgs.messages.GetAllGroupAttributesRequest;
import cgs.messages.GetAllGroupAttributesResponse;

public interface GetAllGroupAttributesResponseDelegate extends GluonResponseDelegate {

    public void onGetAllGroupAttributesResponse(GetAllGroupAttributesRequest getAllGroupAttributesRequest, GetAllGroupAttributesResponse getAllGroupAttributesResponse);

}