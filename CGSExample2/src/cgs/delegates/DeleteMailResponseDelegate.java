package cgs.delegates;

import cgs.messages.DeleteMailRequest;
import cgs.messages.DeleteMailResponse;

public interface DeleteMailResponseDelegate extends GluonResponseDelegate {

    public void onDeleteMailResponse(DeleteMailRequest deleteMailRequest, DeleteMailResponse deleteMailResponse);

}