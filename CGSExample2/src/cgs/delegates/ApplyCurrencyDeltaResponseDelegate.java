package cgs.delegates;

import cgs.messages.ApplyCurrencyDeltaRequest;
import cgs.messages.ApplyCurrencyDeltaResponse;

public interface ApplyCurrencyDeltaResponseDelegate extends GluonResponseDelegate {

    public void onApplyCurrencyDeltaResponse(ApplyCurrencyDeltaRequest applyCurrencyDeltaRequest, ApplyCurrencyDeltaResponse applyCurrencyDeltaResponse);

}