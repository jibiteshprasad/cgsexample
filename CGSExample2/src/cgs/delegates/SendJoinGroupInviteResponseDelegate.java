package cgs.delegates;

import cgs.messages.SendJoinGroupInviteRequest;
import cgs.messages.SendJoinGroupInviteResponse;

public interface SendJoinGroupInviteResponseDelegate extends GluonResponseDelegate {

    public void onSendJoinGroupInviteResponse(SendJoinGroupInviteRequest sendJoinGroupInviteRequest, SendJoinGroupInviteResponse sendJoinGroupInviteResponse);

}