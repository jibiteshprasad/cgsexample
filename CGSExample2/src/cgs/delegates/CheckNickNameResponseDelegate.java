package cgs.delegates;

import cgs.messages.CheckNickNameRequest;
import cgs.messages.CheckNickNameResponse;

public interface CheckNickNameResponseDelegate extends GluonResponseDelegate {

    public void onCheckNickNameResponse(CheckNickNameRequest checkNickNameRequest, CheckNickNameResponse checkNickNameResponse);

}