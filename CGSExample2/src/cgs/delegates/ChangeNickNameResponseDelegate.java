package cgs.delegates;

import cgs.messages.ChangeNickNameRequest;
import cgs.messages.ChangeNickNameResponse;

public interface ChangeNickNameResponseDelegate extends GluonResponseDelegate {

    public void onChangeNickNameResponse(ChangeNickNameRequest changeNickNameRequest, ChangeNickNameResponse changeNickNameResponse);

}