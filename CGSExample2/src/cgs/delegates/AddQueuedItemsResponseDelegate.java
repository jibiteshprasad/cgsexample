package cgs.delegates;

import cgs.messages.AddQueuedItemsRequest;
import cgs.messages.AddQueuedItemsResponse;

public interface AddQueuedItemsResponseDelegate extends GluonResponseDelegate {

    public void onAddQueuedItemsResponse(AddQueuedItemsRequest addQueuedItemsRequest, AddQueuedItemsResponse addQueuedItemsResponse);

}