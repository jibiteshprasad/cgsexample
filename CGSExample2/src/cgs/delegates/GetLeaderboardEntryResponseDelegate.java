package cgs.delegates;

import cgs.messages.GetLeaderboardEntryRequest;
import cgs.messages.GetLeaderboardEntryResponse;

public interface GetLeaderboardEntryResponseDelegate extends GluonResponseDelegate {

    public void onGetLeaderboardEntryResponse(GetLeaderboardEntryRequest getLeaderboardEntryRequest, GetLeaderboardEntryResponse getLeaderboardEntryResponse);

}