package cgs.delegates;

import cgs.messages.GetPlayersWhoLockedMeRequest;
import cgs.messages.GetPlayersWhoLockedMeResponse;

public interface GetPlayersWhoLockedMeResponseDelegate extends GluonResponseDelegate {

    public void onGetPlayersWhoLockedMeResponse(GetPlayersWhoLockedMeRequest getPlayersWhoLockedMeRequest, GetPlayersWhoLockedMeResponse getPlayersWhoLockedMeResponse);

}