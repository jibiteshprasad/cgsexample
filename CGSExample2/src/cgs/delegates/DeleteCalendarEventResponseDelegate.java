package cgs.delegates;

import cgs.messages.DeleteCalendarEventRequest;
import cgs.messages.DeleteCalendarEventResponse;

    public interface DeleteCalendarEventResponseDelegate extends GluonResponseDelegate {

public void onDeleteCalendarEventResponse(DeleteCalendarEventRequest deleteCalendarEventRequest, DeleteCalendarEventResponse deleteCalendarEventResponse);

}