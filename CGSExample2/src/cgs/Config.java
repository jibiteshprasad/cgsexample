package cgs;

/**
 * Created with IntelliJ IDEA.
 * User: stephen.diteljan
 * Date: 6/24/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class Config
{
    public static final String gameId = "com.glu.ios.herc";
    public static final String cgsId = "CGS1234";

    // Frequency of calendar polling in seconds. All server work is driven by this value.
    public static final int pollIntervalSeconds = 10;

    // Use this block of variables for stage 2
//    public static final String gluonServer = "ec2-54-242-92-43.compute-1.amazonaws.com"; //stage2
//    public static final int gluonPort = 54325; //stage2
//    public static final String portal_url = "http://ec2-54-234-159-234.compute-1.amazonaws.com:5000/"; //stage2
//    public static final String admin = "515b7631abe0e1ff89e64841"; //stage2
//    public static final String testPlayerStage2 = "testPlayerStage2"; //stage2

    // Use this block of variables for preprod
    public static final String gluonServer = "ec2-107-21-96-76.compute-1.amazonaws.com"; //preprod
    public static final int gluonPort = 4025; //preprod
    public static final String portal_url = "http://ec2-54-234-159-234.compute-1.amazonaws.com/"; //preprod
    public static final String admin = "5165ce5153c1346b42000001"; //preprod
    public static final String testPlayerPreprod = "testPlayerPreprod"; //preprod
}
