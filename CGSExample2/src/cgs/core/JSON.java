package cgs.core;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trevor Dasch
 */
public class JSON {
    
    public static ObjectMapper jsonObjectMapper= new ObjectMapper(new JsonFactory());
    
    public static void setClassProperty(String className){
        jsonObjectMapper.enableDefaultTypingAsProperty(ObjectMapper.DefaultTyping.OBJECT_AND_NON_CONCRETE, className);
    }
        
    public static <T extends Object> T Deserialize(String s, Class<T> c){
        try {
            return jsonObjectMapper.readValue(s, c);
        } catch (JsonParseException ex) {
            Logger.getLogger(JSON.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(JSON.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static String Serialize(Object o){
        try {
            return jsonObjectMapper.writeValueAsString(o);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(JSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
