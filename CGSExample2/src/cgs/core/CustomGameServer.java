
package cgs.core;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JEditorPane;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import cgs.delegates.GluonResponseDelegate;
import cgs.delegates.GluonResponseDelegateHandler;
import cgs.messages.CustomGameServerMessageRequest;
import cgs.messages.CustomGameServerMessageResponse;
import cgs.messages.GetCustomGameServerIdResponse;
import cgs.messages.GluonMessage;
import cgs.messages.GluonRequest;
import cgs.messages.GluonResponse;
import cgs.messages.JsonWrapper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Trevor Dasch
 */
public class CustomGameServer extends WebSocketClient {

    
    static final long DEFAULT_LOCK_TIMEOUT = 300;
    
    public String cgsId;
    
    public String gameId;
    public boolean enableStickySession;

    private boolean locking;

    private final ReentrantLock lock = new ReentrantLock();

    private final ExecutorService executor = Executors.newCachedThreadPool();

    private long lockTimeout;

    HashMap<String, GluonResponseDelegateHandler> delegates;


    private class TimeoutHandler implements Runnable{

        private CustomGameServer cgs;
        
        public TimeoutHandler(CustomGameServer cgs){
            this.cgs = cgs;
        }
        
        
        
        @Override
        public void run() {
            while(cgs.getReadyState() == WebSocket.READYSTATE.OPEN){
                try{
                    Thread.sleep(1000);
                }catch(Exception e){
                    
                }
                cgs.timeoutDelegates();
            }
        }
        
    }
    

    private class CGSRequestHandler implements Runnable{
        private CustomGameServer cgs;
        private CustomGameServerMessageRequest request;
        private String playerId;
        
        public CGSRequestHandler(CustomGameServer cgs, CustomGameServerMessageRequest request, String playerId){
            this.cgs = cgs;
            this.request = request;
            this.playerId = playerId;
        }
        
        @Override
        public void run() {
//        	This is the Entry point
            cgs.onCGSRequest(playerId, request);
        }
    }
    
    private class PlayerConnectedHandler implements Runnable{
        private CustomGameServer cgs;
        private String playerId;
        
        public PlayerConnectedHandler(CustomGameServer cgs,String playerId){
            this.cgs = cgs;
            this.playerId = playerId;
        }
        
        @Override
        public void run() {
            cgs.onPlayerConnected(playerId);
        }
    }    
    
    private class PlayerDisconnectedHandler implements Runnable{
        private CustomGameServer cgs;
        private String playerId;
        
        public PlayerDisconnectedHandler(CustomGameServer cgs,String playerId){
            this.cgs = cgs;
            this.playerId = playerId;
        }
        
        @Override
        public void run() {
            cgs.onPlayerDisconnected(playerId);
        }
    }  

    TimeoutHandler timeoutHandler;

    public Logger logger;

    public CustomGameServer(String host, int port, String gameId, String cgsId, boolean enableStickySession) throws URISyntaxException{
        this(host,port,gameId,cgsId,enableStickySession,false,DEFAULT_LOCK_TIMEOUT);
        
    }

    public CustomGameServer(String host, int port, String gameId, String cgsId, boolean enableStickySession, long lockTimeout) throws URISyntaxException{
        this(host,port,gameId,cgsId,enableStickySession,false,lockTimeout);
    }
    public CustomGameServer(String host, int port, String gameId, String cgsId, boolean enableStickySession, boolean locking) throws URISyntaxException{
        this(host,port,gameId,cgsId,enableStickySession,locking,DEFAULT_LOCK_TIMEOUT);

    }
    
    public CustomGameServer(String host, int port, String gameId, String cgsId, boolean enableStickySession, boolean locking,long lockTimeout) throws URISyntaxException{
        this ( host, port,locking,lockTimeout);
        
        this.cgsId = cgsId;
        this.gameId = gameId;

        this.enableStickySession = enableStickySession;

        
    }
    public CustomGameServer(String host, int port) throws URISyntaxException{
        this (host, port, false,DEFAULT_LOCK_TIMEOUT);
    }
    public CustomGameServer(String host, int port, long lockTimeout) throws URISyntaxException{
        this(host,port,false,lockTimeout);
    }

    public CustomGameServer(String host, int port, boolean locking) throws URISyntaxException{
        this(host,port,locking,DEFAULT_LOCK_TIMEOUT);
    }

    public CustomGameServer(String host, int port, boolean locking, long lockTimeout) throws URISyntaxException{
        this ( new URI("ws://"+host+":"+port+"/websocket"));

        this.locking = locking;        
        this.lockTimeout = lockTimeout;
        timeoutHandler = new TimeoutHandler(this);
    }

	private CustomGameServer( URI serverURI ) {
		super( serverURI , new Draft_17());
        JSON.jsonObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        this.logger = Logger.getLogger(this.getClass().getName());

        delegates = new HashMap<String, GluonResponseDelegateHandler>();
	}

	@Override
	public void onOpen( ServerHandshake handshakedata ) {
		logger.log(Level.INFO, "opened connection" );
		// if you pan to refuse connection based on ip or httpfields overload: onWebsocketHandshakeReceivedAsClient
        
	}

	@Override
	public void onMessage( String message ) {
		logger.log(Level.FINE, "received: {0}", message );
		// send( "you said: " + message );
        
        JsonWrapper jw = null;
        
        jw = JSON.Deserialize(message, JsonWrapper.class);
        
        if(jw == null || jw.body == null)
            return;
        
        
        if(jw.header.messageType.equals("GetCustomGameServerIdRequest")){
        
            jw.body = new GetCustomGameServerIdResponse(cgsId, enableStickySession);
            jw.header.gameId = gameId;
            

            this.send(JSON.Serialize(jw));

            executor.execute(timeoutHandler);
            onConnected();
            return;
        }
        
        if(jw.header.messageType.equals("CustomGameServerRequest")){
            if(!locking){
                executor.execute(new CGSRequestHandler(this,(CustomGameServerMessageRequest)jw.body, jw.header.playerId));
            }else{
               onCGSRequest(jw.header.playerId,(CustomGameServerMessageRequest)jw.body);
            }
            return;
        }

        if(jw.header.messageType.equals("PlayerLogin")){
            if(!locking){
                executor.execute(new PlayerConnectedHandler(this,jw.header.playerId));
            }else{
                onPlayerConnected(jw.header.playerId);
            }
            return;
        }
        
        if(jw.header.messageType.equals("PlayerLogout")){
            if(!locking){
                executor.execute(new PlayerDisconnectedHandler(this,jw.header.playerId));
            }else{
                onPlayerDisconnected(jw.header.playerId);
                
            }
            return;
        }
        try{
            if(lock.tryLock(lockTimeout, TimeUnit.MILLISECONDS)){
                if(delegates.containsKey(jw.body.passthrough)){
                    ResponseProcessor rp;
                    try{
                        rp = new ResponseProcessor(jw.body, delegates.get(jw.body.passthrough));
                        delegates.remove(jw.body.passthrough);
                    }finally{
                        lock.unlock();
                    }
                    if(locking){
                        rp.run();
                    }else{
                        executor.execute(rp);
                    }
                }else{
                    lock.unlock();
                }
            }else{
                onLockTimeout();
            }
        }catch(InterruptedException e){
            onLockInterrupted();
        }
    
    }

    private class ResponseProcessor implements Runnable{

        private GluonMessage response;
        private GluonResponseDelegateHandler handler;

        public ResponseProcessor(GluonMessage response, GluonResponseDelegateHandler handler){
            this.response = response;
            this.handler = handler;
        }

        @Override
        public void run() {
    
            if(response == null){
                handler.request.triggerDelegateTimeout(handler.delegate);
            }else{
                handler.request.triggerDelegate((GluonResponse)response, handler.delegate);
            }         
        }
    }

    
    @Override 
    public void send( String text){        
        super.send(text);
        logger.log(Level.FINE,"sent: {0}", text);
    }

    @Override
    public void onClose( int code, String reason, boolean remote ) {
        // The codecodes are documented in class org.java_websocket.framing.CloseFrame
        logger.log(Level.INFO, "Connection closed by {0}" , ( remote ? "remote peer" : "us" ) );
        if(remote)
            onDisconnected();
    }

    @Override
    public void onError( Exception ex ) {
        ex.printStackTrace();
        
    }

    /**
     * The extension point for CGS
     */
    public void onConnected(){
        
    }
    
    /**
     * The extension point for CGS
     */
    public void onDisconnected(){
        
    }
    
    /**
     * The extension point for CGS
     */    
    public void onLockTimeout(){
        
    }
    /**
     * The extension point for CGS
     */    
    public void onLockInterrupted(){
        
    }
    
    public String sendGluonRequest(String playerId, GluonRequest request) throws TimeoutException{
        return sendGluonRequest(playerId, request, null);
    }




    public String sendGluonRequest(String playerId, GluonRequest request, GluonResponseDelegate delegate) throws TimeoutException{
        return sendGluonRequest(playerId, request, delegate, -1);
    }

    public String sendGluonRequest(String playerId, GluonRequest request, GluonResponseDelegate delegate, long timeout) throws TimeoutException{

        JsonWrapper jw = new JsonWrapper();
        
        String tid = java.util.UUID.randomUUID().toString();

        if(delegate !=null){
            try{
                if(lock.tryLock(lockTimeout, TimeUnit.MILLISECONDS)){
                    try{
                        delegates.put(tid, new GluonResponseDelegateHandler(request, delegate,timeout));            
                    }finally{
                        lock.unlock();
                    }
                }else{
                    throw new TimeoutException("Acquire lock timed out before send operation");                
                }
            }catch(InterruptedException e){
                throw new TimeoutException("Thread interrupted before send operation");

            }
        }

        jw.protocolVersion = 1;
        jw.header = jw.new Header();
        
        jw.body = request;
        
        jw.header.messageId = tid;
               
        jw.header.messageType = "GluonRequest";
        
        jw.header.utc_timestamp = new Date().getTime();
        
        jw.header.gameId = gameId;
        
        jw.header.playerId = playerId;
        
        jw.body.passthrough = tid;

        this.send(JSON.Serialize(jw));
        
        return tid;
    }
        
    public void sendCGSResponse(String playerId, CustomGameServerMessageResponse response){
        JsonWrapper jw = new JsonWrapper();
        
        String tid = response.passthrough;
        jw.protocolVersion = 1;
        jw.header = jw.new Header();
        
        jw.body = response;
        
        jw.header.messageId = tid;
               
        jw.header.messageType = "CustomGameServerResponse";
        
        jw.header.utc_timestamp = new Date().getTime();
        
        jw.header.gameId = gameId;
        
        jw.header.playerId = playerId;
        
        jw.body.passthrough = tid;

        this.send(JSON.Serialize(jw));
        
        
    }
    
//    Code goes here. Do not forget!!!  @GB
    public void onCGSRequest(String playerId, CustomGameServerMessageRequest request){
    	Object payload = request.payload;
    	String pl = (String) payload;
    	String[] commasep = pl.split(",");
    	String herodetails = commasep[0];
    	String villian = commasep[1];
    	System.out.println("<=================== PAYLOAD ==================>");
    	System.out.println(pl);
    	System.out.println("\n\n");
    	
//    	<=================== PAYLOAD ==================>
//    	{"Hero":"Hercules","Villian":"JB and Rishi","Attack":"20"}
//    	{"Hero":"Hercules","Villian":"JB and Rishi","Attack":"21"}
    	
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			TestAction action = mapper.readValue(pl, TestAction.class);
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void onPlayerConnected(String playerId){}
    
    public void onPlayerDisconnected(String playerId){}


    public void timeoutDelegates(){

        ArrayList<GluonResponseDelegateHandler> timedOutDelegates = new ArrayList<GluonResponseDelegateHandler>();   
        
        try{
            if(lock.tryLock(lockTimeout, TimeUnit.MILLISECONDS)){
                 try{
                    Object[] keys = delegates.keySet().toArray();
                    for(Object o : keys){
                        String key = (String)o;
                        GluonResponseDelegateHandler del = delegates.get(key);

                        if(del.isExpired()){
                            delegates.remove(key);
                            timedOutDelegates.add(del);
                        }
                    }
                }finally{
                    lock.unlock();
                }
            }else{
                onLockTimeout();
            }
        }catch(InterruptedException e){
            onLockInterrupted();
        }
        
        for(GluonResponseDelegateHandler del : timedOutDelegates){
            if(del.delegate!=null){
                ResponseProcessor rp = new ResponseProcessor(null, del);
                if(locking){
                    rp.run();
                }else{
                    executor.execute(rp);
                }          
            }
        }
    }

}

