package cgs.core;

public class TestAction {
	private String Hero;
	private String Villian;
	private String Attack;
	
	public String getHero() {
		return Hero;
	}
	public void setHero(String hero) {
		this.Hero = hero;
	}
	public String getVillian() {
		return Villian;
	}
	public void setVillian(String villian) {
		this.Villian = villian;
	}
	public String getAttack() {
		return Attack;
	}
	public void setAttack(String attack) {
		this.Attack = attack;
	}
	
	

}
