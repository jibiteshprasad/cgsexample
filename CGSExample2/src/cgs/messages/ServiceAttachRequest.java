/**
 * @file ServiceAttachRequest.java
 * @page classServiceAttachRequest ServiceAttachRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ServiceAttachRequest: (playerId, gameId, requestorAddress, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request for a service to attach to a player.
*/
@JsonTypeName("ServiceAttachRequest")
public class ServiceAttachRequest extends GluonRequest{

	/**
	 *
	 */
	public String playerId;

	/**
	 *
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public String requestorAddress;

	/**
	 * Constructor for ServiceAttachRequest that requires all members
	 * @param playerId
	 * @param gameId
	 * @param requestorAddress
	 * @param passthrough
	 */
	public ServiceAttachRequest(String playerId, String gameId, 
			String requestorAddress, String passthrough){
		this.playerId = playerId;
		this.gameId = gameId;
		this.requestorAddress = requestorAddress;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ServiceAttachRequest without super class members
	 * @param playerId
	 * @param gameId
	 * @param requestorAddress
	 */
	public ServiceAttachRequest(String playerId, String gameId, 
			String requestorAddress){
		this.playerId = playerId;
		this.gameId = gameId;
		this.requestorAddress = requestorAddress;

	}

	/**
	 * Default Constructor for ServiceAttachRequest
	*/
	public ServiceAttachRequest(){

	}
}
/** @} */
