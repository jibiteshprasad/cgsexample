/**
 * @file SendPrivateMessageResponse.java
 * @page classSendPrivateMessageResponse SendPrivateMessageResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendPrivateMessageResponse: (status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Confirms whether the private message went through succesfully
 * 
*/
@JsonTypeName("SendPrivateMessageResponse")
public class SendPrivateMessageResponse extends GluonResponse{

	/**
	 * Constructor for SendPrivateMessageResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendPrivateMessageResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendPrivateMessageResponse without super class members
	 */
	public SendPrivateMessageResponse(){

	}
	/**
	 * Constructor for SendPrivateMessageResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendPrivateMessageResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
