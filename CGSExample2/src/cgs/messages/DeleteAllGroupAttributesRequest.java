/**
 * @file DeleteAllGroupAttributesRequest.java
 * @page classDeleteAllGroupAttributesRequest DeleteAllGroupAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllGroupAttributesRequest: (groupName, groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes all group attributes for a specified group
 * 
*/
@JsonTypeName("DeleteAllGroupAttributesRequest")
public class DeleteAllGroupAttributesRequest extends GluonRequest{

	/**
	 * Name of the group whose group attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteAllGroupAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteAllGroupAttributesResponseDelegate){
                ((DeleteAllGroupAttributesResponseDelegate)delegate).onDeleteAllGroupAttributesResponse(this, (DeleteAllGroupAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteAllGroupAttributesResponseDelegate){
            ((DeleteAllGroupAttributesResponseDelegate)delegate).onDeleteAllGroupAttributesResponse(this, new DeleteAllGroupAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteAllGroupAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public DeleteAllGroupAttributesRequest(String groupName, String groupType, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllGroupAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteAllGroupAttributesRequest(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for DeleteAllGroupAttributesRequest
	*/
	public DeleteAllGroupAttributesRequest(){

	}
}
/** @} */
