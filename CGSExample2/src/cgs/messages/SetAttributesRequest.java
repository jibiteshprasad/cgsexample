/**
 * @file SetAttributesRequest.java
 * @page classSetAttributesRequest SetAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAttributesRequest: (playerId, attributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Set multiple attributes for a specified player
 * 
*/
@JsonTypeName("SetAttributesRequest")
public class SetAttributesRequest extends GluonRequest{

	/**
	 * ID of the player whose attributes will be set
	 */
	public String playerId;

	/**
	 * A list of player attributes that will be set
	 */
	public List<PlayerAttribute> attributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetAttributesResponseDelegate){
                ((SetAttributesResponseDelegate)delegate).onSetAttributesResponse(this, (SetAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetAttributesResponseDelegate){
            ((SetAttributesResponseDelegate)delegate).onSetAttributesResponse(this, new SetAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetAttributesRequest that requires all members
	 * @param playerId
	 * @param attributes
	 * @param passthrough
	 */
	public SetAttributesRequest(String playerId, 
			List<PlayerAttribute> attributes, String passthrough){
		this.playerId = playerId;
		this.attributes = attributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAttributesRequest without super class members
	 * @param playerId
	 * @param attributes
	 */
	public SetAttributesRequest(String playerId, 
			List<PlayerAttribute> attributes){
		this.playerId = playerId;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for SetAttributesRequest
	*/
	public SetAttributesRequest(){

	}
}
/** @} */
