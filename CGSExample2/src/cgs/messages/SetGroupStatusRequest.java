/**
 * @file SetGroupStatusRequest.java
 * @page classSetGroupStatusRequest SetGroupStatusRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupStatusRequest: (groupName, groupType, isGroupOpen, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Changes the group status to either open or closed
 * 
*/
@JsonTypeName("SetGroupStatusRequest")
public class SetGroupStatusRequest extends GluonRequest{

	/**
	 * Name of the group whose status will change 
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Flag that indicates whether a group is open for anyone to join  
	 * (Optional)
	 */
	public Boolean isGroupOpen;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetGroupStatusResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetGroupStatusResponseDelegate){
                ((SetGroupStatusResponseDelegate)delegate).onSetGroupStatusResponse(this, (SetGroupStatusResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetGroupStatusResponseDelegate){
            ((SetGroupStatusResponseDelegate)delegate).onSetGroupStatusResponse(this, new SetGroupStatusResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetGroupStatusRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param isGroupOpen
	 * @param passthrough
	 */
	public SetGroupStatusRequest(String groupName, String groupType, 
			Boolean isGroupOpen, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.isGroupOpen = isGroupOpen;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupStatusRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param isGroupOpen
	 */
	public SetGroupStatusRequest(String groupName, String groupType, 
			Boolean isGroupOpen){
		this.groupName = groupName;
		this.groupType = groupType;
		this.isGroupOpen = isGroupOpen;

	}

	/**
	 * Default Constructor for SetGroupStatusRequest
	*/
	public SetGroupStatusRequest(){

	}
}
/** @} */
