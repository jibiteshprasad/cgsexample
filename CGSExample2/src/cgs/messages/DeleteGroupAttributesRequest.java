/**
 * @file DeleteGroupAttributesRequest.java
 * @page classDeleteGroupAttributesRequest DeleteGroupAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupAttributesRequest: (groupName, groupType, attributeNames, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes multiple group attributes for a specified group
 * 
*/
@JsonTypeName("DeleteGroupAttributesRequest")
public class DeleteGroupAttributesRequest extends GluonRequest{

	/**
	 * Name of the group whose attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * A list of group attribute names of the attributes to be deleted
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteGroupAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteGroupAttributesResponseDelegate){
                ((DeleteGroupAttributesResponseDelegate)delegate).onDeleteGroupAttributesResponse(this, (DeleteGroupAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteGroupAttributesResponseDelegate){
            ((DeleteGroupAttributesResponseDelegate)delegate).onDeleteGroupAttributesResponse(this, new DeleteGroupAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteGroupAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param passthrough
	 */
	public DeleteGroupAttributesRequest(String groupName, String groupType, 
			ArrayList<String> attributeNames, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 */
	public DeleteGroupAttributesRequest(String groupName, String groupType, 
			ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for DeleteGroupAttributesRequest
	*/
	public DeleteGroupAttributesRequest(){

	}
}
/** @} */
