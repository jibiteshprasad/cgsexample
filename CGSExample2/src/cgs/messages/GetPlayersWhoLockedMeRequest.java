/**
 * @file GetPlayersWhoLockedMeRequest.java
 * @page classGetPlayersWhoLockedMeRequest GetPlayersWhoLockedMeRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPlayersWhoLockedMeRequest: (maxResultsToReturn, firstResult, 
 * 		attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gathers a list of players who locked your attributes. Used for revenge 
 * matches.
*/
@JsonTypeName("GetPlayersWhoLockedMeRequest")
public class GetPlayersWhoLockedMeRequest extends GluonRequest{

	/**
	 * Used to limit the number of results returned
	 */
	public int maxResultsToReturn;

	/**
	 * (Currently not used) The first result to return; used for pagination. 
	 * (Optional)
	 */
	public int firstResult;

	/**
	 * The attributes by name of the player who locked me (Optional)
	 */
	public List<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetPlayersWhoLockedMeResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetPlayersWhoLockedMeResponseDelegate){
                ((GetPlayersWhoLockedMeResponseDelegate)delegate).onGetPlayersWhoLockedMeResponse(this, (GetPlayersWhoLockedMeResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetPlayersWhoLockedMeResponseDelegate){
            ((GetPlayersWhoLockedMeResponseDelegate)delegate).onGetPlayersWhoLockedMeResponse(this, new GetPlayersWhoLockedMeResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetPlayersWhoLockedMeRequest that requires all members
	 * @param maxResultsToReturn
	 * @param firstResult
	 * @param attributeNames
	 * @param passthrough
	 */
	public GetPlayersWhoLockedMeRequest(int maxResultsToReturn, int firstResult, 
			List<String> attributeNames, String passthrough){
		this.maxResultsToReturn = maxResultsToReturn;
		this.firstResult = firstResult;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPlayersWhoLockedMeRequest without super class members
	 * @param maxResultsToReturn
	 * @param firstResult
	 * @param attributeNames
	 */
	public GetPlayersWhoLockedMeRequest(int maxResultsToReturn, int firstResult, 
			List<String> attributeNames){
		this.maxResultsToReturn = maxResultsToReturn;
		this.firstResult = firstResult;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for GetPlayersWhoLockedMeRequest
	*/
	public GetPlayersWhoLockedMeRequest(){

	}
}
/** @} */
