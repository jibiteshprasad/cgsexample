/**
 * @file RemoveVariantCatalogProductsRequest.java
 * @page classRemoveVariantCatalogProductsRequest RemoveVariantCatalogProductsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemoveVariantCatalogProductsRequest: (baseCatalogName, 
 * 		variantCatalogName, productNames, userName, endDate, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Removes the variant products for a catalog.
*/
@JsonTypeName("RemoveVariantCatalogProductsRequest")
public class RemoveVariantCatalogProductsRequest extends GluonRequest{

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public String variantCatalogName;

	/**
	 *
	 */
	public List<String> productNames;

	/**
	 *
	 */
	public String userName;

	/**
	 * (Optional) (Optional)
	 */
	public Date endDate;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(RemoveVariantCatalogProductsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof RemoveVariantCatalogProductsResponseDelegate){
                ((RemoveVariantCatalogProductsResponseDelegate)delegate).onRemoveVariantCatalogProductsResponse(this, (RemoveVariantCatalogProductsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof RemoveVariantCatalogProductsResponseDelegate){
            ((RemoveVariantCatalogProductsResponseDelegate)delegate).onRemoveVariantCatalogProductsResponse(this, new RemoveVariantCatalogProductsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for RemoveVariantCatalogProductsRequest that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param productNames
	 * @param userName
	 * @param endDate
	 * @param passthrough
	 */
	public RemoveVariantCatalogProductsRequest(String baseCatalogName, 
			String variantCatalogName, List<String> productNames, 
			String userName, Date endDate, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.productNames = productNames;
		this.userName = userName;
		this.endDate = endDate;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemoveVariantCatalogProductsRequest without super class members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param productNames
	 * @param userName
	 * @param endDate
	 */
	public RemoveVariantCatalogProductsRequest(String baseCatalogName, 
			String variantCatalogName, List<String> productNames, 
			String userName, Date endDate){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.productNames = productNames;
		this.userName = userName;
		this.endDate = endDate;

	}

	/**
	 * Default Constructor for RemoveVariantCatalogProductsRequest
	*/
	public RemoveVariantCatalogProductsRequest(){

	}
}
/** @} */
