/**
 * @file CalendarEvent.java
 * @page classCalendarEvent CalendarEvent
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CalendarEvent: (calendarName, eventName, startTime, endTime, timezone, 
 * 		tags, state, data)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * An event on the calendar
 * 
*/
public class CalendarEvent {

	/**
	 * Name of the calendar where the event is held (Optional)
	 */
	public String calendarName;

	/**
	 * Name of the actual event (Optional)
	 */
	public String eventName;

	/**
	 * The start time of the event. Value is number of milliseconds since epoch 
	 * time (00:00:00 UTC on 1 January 1970) (Optional)
	 */
	public long startTime;

	/**
	 * The end time of the event. Value is number of milliseconds since epoch 
	 * time (00:00:00 UTC on 1 January 1970) (Optional)
	 */
	public long endTime;

	/**
	 * The timezone code (Value is between -12 and 12) (Optional)
	 */
	public int timezone;

	/**
	 * The tags relating to this event (Optional)
	 */
	public ArrayList<String> tags;

	/**
	 * Current state of the event (Optional)
	 */
	public CalendarEventState state;

	/**
	 * Any data associated to the event (Optional)
	 */
	public byte[] data;

	/**
	 * Constructor for CalendarEvent that requires all members
	 * @param calendarName
	 * @param eventName
	 * @param startTime
	 * @param endTime
	 * @param timezone
	 * @param tags
	 * @param state
	 * @param data
	 */
	public CalendarEvent(String calendarName, String eventName, long startTime, 
			long endTime, int timezone, ArrayList<String> tags, 
			CalendarEventState state, byte[] data){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timezone = timezone;
		this.tags = tags;
		this.state = state;
		this.data = data;

	}

	/**
	 * Default Constructor for CalendarEvent
	*/
	public CalendarEvent(){

	}
}
/** @} */
