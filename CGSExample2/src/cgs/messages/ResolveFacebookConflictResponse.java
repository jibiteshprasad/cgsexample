/**
 * @file ResolveFacebookConflictResponse.java
 * @page classResolveFacebookConflictResponse ResolveFacebookConflictResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ResolveFacebookConflictResponse: (playerInfo, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the specified player will become the only player 
 * account attached to the Facebook credential. All other player accounts 
 * attached will be retired. 
*/
@JsonTypeName("ResolveFacebookConflictResponse")
public class ResolveFacebookConflictResponse extends GluonResponse{

	/**
	 * The player now associated with the specified facebook credentials 
	 * (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for ResolveFacebookConflictResponse that requires all members
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ResolveFacebookConflictResponse(PlayerInfo playerInfo, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ResolveFacebookConflictResponse without super class members
	 * @param playerInfo
	 */
	public ResolveFacebookConflictResponse(PlayerInfo playerInfo){
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for ResolveFacebookConflictResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ResolveFacebookConflictResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ResolveFacebookConflictResponse
	*/
	public ResolveFacebookConflictResponse(){

	}
}
/** @} */
