/**
 * @file ExternalCredential.java
 * @page classExternalCredential ExternalCredential
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ExternalCredential: (credentialType, credentialId)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Encapsulates an external credential linked to a player account. Primary 
 * use-case is to match up players from external systems to %Gluon - for 
 * example, Friends in Facebook would be matched to their corresponding %Gluon 
 * accounts using this method in conjunction with the SearchForPlayers method.
*/
public class ExternalCredential {

	/**
	 * Enumeration of the possible external credential types (e.g. FACEBOOK) 
	 * (Optional)
	 */
	public PlayerCredentialType credentialType;

	/**
	 * If FACEBOOK, this will be the Facebook UUID. If EMAIL, this is the 
	 * email. And so on.... (Optional)
	 */
	public String credentialId;

	/**
	 * Constructor for ExternalCredential that requires all members
	 * @param credentialType
	 * @param credentialId
	 */
	public ExternalCredential(PlayerCredentialType credentialType, 
			String credentialId){
		this.credentialType = credentialType;
		this.credentialId = credentialId;

	}

	/**
	 * Default Constructor for ExternalCredential
	*/
	public ExternalCredential(){

	}
}
/** @} */
