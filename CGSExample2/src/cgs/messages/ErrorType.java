package cgs.messages;

public enum ErrorType
	{
	/* errors from backend */
	
	/// General: Unknown error
	UNKNOWN_ERROR,
	/// General: Request not processible at this time
	CANNOT_PROCESS_REQUEST,
	/// General: Request message not valid
	INVALID_REQUEST_MESSAGE,
	/// General: Invalid Game ID used
	INVALID_GAMEID,
	/// General: Invalid Player Name used
	INVALID_PLAYERNAME,
	/// General: Invalid Player ID used
	INVALID_PLAYERID,
	/// General: The request has been deprecated and is no longer available in this SDK version
	INVALID_REQUEST_DEPRECATED,
	/// General: There is an invalid parameter in the request
	INVALID_REQUEST_PARAMETERS,
	/// Edge: Session invalid
	INVALID_SESSION,
	/// Edge: Session exceeded expire time
	SESSION_EXPIRED,
	/// Edge: Player not found in database
	USER_NOT_FOUND,
	/// Player: Already exists in database
	PLAYER_ALREADY_EXISTS,
	/// Player: Not found in database
	PLAYER_NOT_FOUND,
	/// Player: Name already taken (could also mean FB account is already attached to different player account)
	ACCOUNT_NAME_TAKEN,
	/// Player: Authentication failed
	AUTHENTICATION_FAILURE,
	/// Player: The federated auth token has expired.
	AUTH_TOKEN_EXPIRED,
	/// Player: Credentials not found
	CREDENTIAL_NOT_FOUND,
	/// Player: Account type not supported
	UNSUPPORTED_ACCOUNT_TYPE,
	/// Player: Error when trying to create a new player
	CREATE_PLAYER_ERROR,
	/// Player: Error occurred when trying to retire a player
	RETIRE_PLAYER_ERROR,
	/// Matchmaking: If the alogrithm specified in getMatchesRequest is not in the backend database, this error will occur.
	INVALID_MATCH_MAKER_NAME,
	/// Player: The Facebook account is already attached to the player in question.
	CREDENTIAL_ALREADY_ATTACHED_TO_SAME_PLAYER,
	/// Player: Multiple players are associated with the facebook account, need to ResolveFacebookConflict to choose a primary if the current primary is incorrect.
	CREDENTIAL_ATTACHED_TO_DIFFERENT_PLAYER,
	/// Player: Error resolving Facebook conflict
	RESOLVE_CONFLICT_ERROR,
	/// Player: Error validating passed in Facebook credentials
	FACEBOOK_CREDENTIAL_VALIDATION_ERROR,
	/// Player: The nickname is invalid and needs to be resent properly
	CURRENT_NICKNAME_INVALID,
	/// Matchmaking: A lock exists on the attribute trying to be accessed.
	LOCK_EXISTS,
	/// Group: Already exists
	GROUP_ALREADY_EXISTS,
	/// Group: The group doesn't exist
	GROUP_DOES_NOT_EXIST,
	/// Mail: Returned when sending a message to an unknown recipient
	UNKNOWN_MAIL_RECEIPENT,
	/// Mail: Returned when using an invalid message ID
	INVALID_MAIL_ID,
	/// Chat: Returned when sending a message to an unknown recipient
	UNKNOWN_CHAT_RECEIPENT,
	/// Chat: Player has been muted by administrator
	PLAYER_MUTED,
	/// Chat: Player has not joined the room
	PLAYER_NOT_JOINED_ROOM,
	/// Leaderboard: Returned when leaderboard servers are down
	LEADERBOARD_SERVER_DOWN,
	/// Leaderboard: Returned when query returned no records
	LEADERBOARD_NO_DATA,
	/// IAP: content already consumed
	ALREADY_CONSUMED,
	/// IAP: receipt not found
	RECEIPT_NOT_FOUND,
	/// IAP: Player ID incorrect
	BAD_PLAYER_ID,
	/// IAP: content is unknown
	UNKNOWN_PRODUCT,
	/// IAP: Apple says not valid
	NOT_VALID,
	/// IAP: Unable to convert from base64 or into json (invalid ticket string)
	DECODE_FAILED,
	/// IAP: Request and ticket don't match
	BAD_BID,
	/// IAP: Retrieved version from database has mismatch
	RETRIEVED_BID_MISMATCH,
	/// IAP: Apple service threw an exception or was down or...something.
	VERIFY_EXCEPTION,
	/// IAP: Retrieved version from database has mismatch
	RETRIEVED_PLAYER_MISMATCH,
	/// Proxy: Error occurred while decoding JSON response
	PROXY_DECODE_ERROR,
	/// Proxy: Error occurred on the backend server
	PROXY_SERVER_ERROR,
	/// Proxy: Unknown error occurred
	PROXY_UNDEFINED_ERROR,
	// Document: Name needs to be unique when ExistingDocumentMode is set to Abort
	FILE_EXISTS,
	// Document: Name needs to be unique when ExistingDocumentMode is set to Abort
	NON_UNIQUE,
	// Document: Error retrieving from or uploading to file store
	FILE_TRANSFER_ERROR,
	// Document: There is no document with the documentId we are attempting to retrieve
	FILE_NOT_FOUND,
	/// Escrow: Queue contains max amounts, cannot add any more until emptied out
	QUEUE_FULL,
	/// Escrow: Item requested not available to claim
	ITEMS_NOT_AVAILABLE,
	/// Push Notification Error
	URBAN_AIRSHIP_KEY_OR_SECRET_NOT_AVAILABLE,
	
	/* errors from client */
	
	/// The value of a parameter in the request was not within an expected range, or was null where a value was needed.
	INVALID_PARAMETERS,
	/// A response was not received for the request within the specified timeout period.
	TIMEOUT,
	/// The request was cancelled via the Cancel method prior to receiving a response.
	CANCELLED,
	/// Client-side exception hit
	EXCEPTION,
	/// Error attempting to send out a request (such that the request never actually sent)
	SEND_ERROR
	
}
