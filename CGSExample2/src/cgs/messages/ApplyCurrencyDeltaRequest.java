/**
 * @file ApplyCurrencyDeltaRequest.java
 * @page classApplyCurrencyDeltaRequest ApplyCurrencyDeltaRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApplyCurrencyDeltaRequest: (currencyType, delta, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to apply a delta to a currency in the player's wallet. A negative 
 * delta will subtract currency  while a positive delta will add currency.
*/
@JsonTypeName("ApplyCurrencyDeltaRequest")
public class ApplyCurrencyDeltaRequest extends GluonRequest{

	/**
	 * The currency type (Optional)
	 */
	public int currencyType;

	/**
	 * The delta value to apply to the currency of currencyType (Optional)
	 */
	public int delta;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ApplyCurrencyDeltaResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ApplyCurrencyDeltaResponseDelegate){
                ((ApplyCurrencyDeltaResponseDelegate)delegate).onApplyCurrencyDeltaResponse(this, (ApplyCurrencyDeltaResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ApplyCurrencyDeltaResponseDelegate){
            ((ApplyCurrencyDeltaResponseDelegate)delegate).onApplyCurrencyDeltaResponse(this, new ApplyCurrencyDeltaResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ApplyCurrencyDeltaRequest that requires all members
	 * @param currencyType
	 * @param delta
	 * @param passthrough
	 */
	public ApplyCurrencyDeltaRequest(int currencyType, int delta, 
			String passthrough){
		this.currencyType = currencyType;
		this.delta = delta;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApplyCurrencyDeltaRequest without super class members
	 * @param currencyType
	 * @param delta
	 */
	public ApplyCurrencyDeltaRequest(int currencyType, int delta){
		this.currencyType = currencyType;
		this.delta = delta;

	}

	/**
	 * Default Constructor for ApplyCurrencyDeltaRequest
	*/
	public ApplyCurrencyDeltaRequest(){

	}
}
/** @} */
