/**
 * @file SendMailRequest.java
 * @page classSendMailRequest SendMailRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendMailRequest: (fromPlayerId, toPlayerId, subject, body, tags, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a message to another player
 * 
*/
@JsonTypeName("SendMailMessageRequest")
public class SendMailRequest extends GluonRequest{

	/**
	 * PlayerId of the sender
	 */
	public String fromPlayerId;

	/**
	 * PlayerId of recipient to send message to
	 */
	public String toPlayerId;

	/**
	 * Subject of mail message (Optional)
	 */
	public String subject;

	/**
	 * Body of mail message (Optional)
	 */
	public String body;

	/**
	 * Optional list of tags to associate to this mail message for easier 
	 * searching (Optional)
	 */
	public List<String> tags;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendMailResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendMailResponseDelegate){
                ((SendMailResponseDelegate)delegate).onSendMailResponse(this, (SendMailResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendMailResponseDelegate){
            ((SendMailResponseDelegate)delegate).onSendMailResponse(this, new SendMailResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendMailRequest that requires all members
	 * @param fromPlayerId
	 * @param toPlayerId
	 * @param subject
	 * @param body
	 * @param tags
	 * @param passthrough
	 */
	public SendMailRequest(String fromPlayerId, String toPlayerId, 
			String subject, String body, List<String> tags, String passthrough){
		this.fromPlayerId = fromPlayerId;
		this.toPlayerId = toPlayerId;
		this.subject = subject;
		this.body = body;
		this.tags = tags;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendMailRequest without super class members
	 * @param fromPlayerId
	 * @param toPlayerId
	 * @param subject
	 * @param body
	 * @param tags
	 */
	public SendMailRequest(String fromPlayerId, String toPlayerId, 
			String subject, String body, List<String> tags){
		this.fromPlayerId = fromPlayerId;
		this.toPlayerId = toPlayerId;
		this.subject = subject;
		this.body = body;
		this.tags = tags;

	}

	/**
	 * Default Constructor for SendMailRequest
	*/
	public SendMailRequest(){

	}
}
/** @} */
