/**
 * @file GetMyGroupsResponse.java
 * @page classGetMyGroupsResponse GetMyGroupsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyGroupsResponse: (groups, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all the groups the authenticated player has joined
 * 
*/
@JsonTypeName("GetMyGroupsResponse")
public class GetMyGroupsResponse extends GluonResponse{

	/**
	 * Detailed information (such as owner name, group size, attributes, etc.) 
	 * for all groups the authenticated player has joined  (Optional)
	 */
	public List<GroupSummary> groups;

	/**
	 * Constructor for GetMyGroupsResponse that requires all members
	 * @param groups
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMyGroupsResponse(List<GroupSummary> groups, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groups = groups;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyGroupsResponse without super class members
	 * @param groups
	 */
	public GetMyGroupsResponse(List<GroupSummary> groups){
		this.groups = groups;

	}
	/**
	 * Constructor for GetMyGroupsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMyGroupsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMyGroupsResponse
	*/
	public GetMyGroupsResponse(){

	}
}
/** @} */
