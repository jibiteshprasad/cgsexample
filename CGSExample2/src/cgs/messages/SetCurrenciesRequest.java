/**
 * @file SetCurrenciesRequest.java
 * @page classSetCurrenciesRequest SetCurrenciesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetCurrenciesRequest: (currencies, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to set multiple currency values in a player's wallet.
 * 
*/
@JsonTypeName("SetCurrenciesRequest")
public class SetCurrenciesRequest extends GluonRequest{

	/**
	 * The currencies to set
	 */
	public HashSet<Currency> currencies;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetCurrenciesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetCurrenciesResponseDelegate){
                ((SetCurrenciesResponseDelegate)delegate).onSetCurrenciesResponse(this, (SetCurrenciesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetCurrenciesResponseDelegate){
            ((SetCurrenciesResponseDelegate)delegate).onSetCurrenciesResponse(this, new SetCurrenciesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetCurrenciesRequest that requires all members
	 * @param currencies
	 * @param passthrough
	 */
	public SetCurrenciesRequest(HashSet<Currency> currencies, String passthrough){
		this.currencies = currencies;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetCurrenciesRequest without super class members
	 * @param currencies
	 */
	public SetCurrenciesRequest(HashSet<Currency> currencies){
		this.currencies = currencies;

	}

	/**
	 * Default Constructor for SetCurrenciesRequest
	*/
	public SetCurrenciesRequest(){

	}
}
/** @} */
