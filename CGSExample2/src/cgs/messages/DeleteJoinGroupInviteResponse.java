/**
 * @file DeleteJoinGroupInviteResponse.java
 * @page classDeleteJoinGroupInviteResponse DeleteJoinGroupInviteResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteJoinGroupInviteResponse: (invitationId, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether the invite was removed 
 * properly
*/
@JsonTypeName("DeleteJoinGroupInviteResponse")
public class DeleteJoinGroupInviteResponse extends GluonResponse{

	/**
	 * Unique invitation ID of invitation that was deleted
	 */
	public String invitationId;

	/**
	 * Constructor for DeleteJoinGroupInviteResponse that requires all members
	 * @param invitationId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteJoinGroupInviteResponse(String invitationId, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.invitationId = invitationId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteJoinGroupInviteResponse without super class members
	 * @param invitationId
	 */
	public DeleteJoinGroupInviteResponse(String invitationId){
		this.invitationId = invitationId;

	}
	/**
	 * Constructor for DeleteJoinGroupInviteResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteJoinGroupInviteResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteJoinGroupInviteResponse
	*/
	public DeleteJoinGroupInviteResponse(){

	}
}
/** @} */
