/**
 * @file GetSentJoinGroupRequestsResponse.java
 * @page classGetSentJoinGroupRequestsResponse GetSentJoinGroupRequestsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetSentJoinGroupRequestsResponse: (requests, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all the join group requests that were sent by the authenticated 
 * player
*/
@JsonTypeName("GetSentJoinGroupRequestsResponse")
public class GetSentJoinGroupRequestsResponse extends GluonResponse{

	/**
	 * Requests that were sent by authenticated player to join groups mentioned 
	 * in request (Optional)
	 */
	public List<SentJoinGroupRequest> requests;

	/**
	 * Constructor for GetSentJoinGroupRequestsResponse that requires all members
	 * @param requests
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetSentJoinGroupRequestsResponse(List<SentJoinGroupRequest> requests, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.requests = requests;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetSentJoinGroupRequestsResponse without super class members
	 * @param requests
	 */
	public GetSentJoinGroupRequestsResponse(List<SentJoinGroupRequest> requests){
		this.requests = requests;

	}
	/**
	 * Constructor for GetSentJoinGroupRequestsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetSentJoinGroupRequestsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetSentJoinGroupRequestsResponse
	*/
	public GetSentJoinGroupRequestsResponse(){

	}
}
/** @} */
