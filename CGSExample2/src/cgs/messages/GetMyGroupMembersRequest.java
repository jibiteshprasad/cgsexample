/**
 * @file GetMyGroupMembersRequest.java
 * @page classGetMyGroupMembersRequest GetMyGroupMembersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyGroupMembersRequest: (groupName, groupType, pageIndex, memberSize, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all the members of the group specified for authenticated player
 * 
*/
@JsonTypeName("GetMyGroupMembersRequest")
public class GetMyGroupMembersRequest extends GluonRequest{

	/**
	 * Name of the group the members returned are associated with
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Page index for members list (starts at 0) (Optional)
	 */
	public int pageIndex;

	/**
	 * Number of members per page (Optional)
	 */
	public int memberSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMyGroupMembersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMyGroupMembersResponseDelegate){
                ((GetMyGroupMembersResponseDelegate)delegate).onGetMyGroupMembersResponse(this, (GetMyGroupMembersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMyGroupMembersResponseDelegate){
            ((GetMyGroupMembersResponseDelegate)delegate).onGetMyGroupMembersResponse(this, new GetMyGroupMembersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMyGroupMembersRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param memberSize
	 * @param passthrough
	 */
	public GetMyGroupMembersRequest(String groupName, String groupType, 
			int pageIndex, int memberSize, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyGroupMembersRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param memberSize
	 */
	public GetMyGroupMembersRequest(String groupName, String groupType, 
			int pageIndex, int memberSize){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;

	}

	/**
	 * Default Constructor for GetMyGroupMembersRequest
	*/
	public GetMyGroupMembersRequest(){

	}
}
/** @} */
