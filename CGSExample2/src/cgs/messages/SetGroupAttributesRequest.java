/**
 * @file SetGroupAttributesRequest.java
 * @page classSetGroupAttributesRequest SetGroupAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupAttributesRequest: (groupName, groupType, attributes, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Set multiple group attributes for a specified group
 * 
*/
@JsonTypeName("SetGroupAttributesRequest")
public class SetGroupAttributesRequest extends GluonRequest{

	/**
	 * Name of the group whose group attributes will be set
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * A list of group attributes to be set for specified group
	 */
	public ArrayList<GroupAttribute> attributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetGroupAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetGroupAttributesResponseDelegate){
                ((SetGroupAttributesResponseDelegate)delegate).onSetGroupAttributesResponse(this, (SetGroupAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetGroupAttributesResponseDelegate){
            ((SetGroupAttributesResponseDelegate)delegate).onSetGroupAttributesResponse(this, new SetGroupAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetGroupAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param passthrough
	 */
	public SetGroupAttributesRequest(String groupName, String groupType, 
			ArrayList<GroupAttribute> attributes, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 */
	public SetGroupAttributesRequest(String groupName, String groupType, 
			ArrayList<GroupAttribute> attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for SetGroupAttributesRequest
	*/
	public SetGroupAttributesRequest(){

	}
}
/** @} */
