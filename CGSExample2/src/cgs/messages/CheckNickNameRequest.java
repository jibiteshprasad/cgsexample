/**
 * @file CheckNickNameRequest.java
 * @page classCheckNickNameRequest CheckNickNameRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CheckNickNameRequest: (nickName, gameId, protocolVersion, 
 * 		clientSdkVersion, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Verify whether the nickname is available for use (since nicknames must be 
 * unique per game).
*/
@JsonTypeName("CheckNickNameRequest")
public class CheckNickNameRequest extends GluonRequest{

	/**
	 * The name to be checked for availability
	 */
	public String nickName;

	/**
	 * The game identifier string provided by the %Gluon support team
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public int protocolVersion;

	/**
	 *
	 */
	public String clientSdkVersion;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CheckNickNameResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CheckNickNameResponseDelegate){
                ((CheckNickNameResponseDelegate)delegate).onCheckNickNameResponse(this, (CheckNickNameResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CheckNickNameResponseDelegate){
            ((CheckNickNameResponseDelegate)delegate).onCheckNickNameResponse(this, new CheckNickNameResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CheckNickNameRequest that requires all members
	 * @param nickName
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 * @param passthrough
	 */
	public CheckNickNameRequest(String nickName, String gameId, 
			int protocolVersion, String clientSdkVersion, String passthrough){
		this.nickName = nickName;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CheckNickNameRequest without super class members
	 * @param nickName
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 */
	public CheckNickNameRequest(String nickName, String gameId, 
			int protocolVersion, String clientSdkVersion){
		this.nickName = nickName;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;

	}

	/**
	 * Default Constructor for CheckNickNameRequest
	*/
	public CheckNickNameRequest(){

	}
}
/** @} */
