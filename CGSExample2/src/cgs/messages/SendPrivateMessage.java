/**
 * @file SendPrivateMessage.java
 * @page classSendPrivateMessage SendPrivateMessage
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendPrivateMessage: (to, message, timestamp, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;

/**
 * Sends a private message to a specific player. This will trigger a 
 * ReceivePrivateMessageNotification as its corresponding response.
*/
@JsonTypeName("SendPrivateMessageRequest")
public class SendPrivateMessage extends GluonRequest{

	/**
	 * PlayerId of the recipient of the message
	 */
	public String to;

	/**
	 * Message that will be sent to the named player (Optional)
	 */
	public String message;

	/**
	 * (Optional)
	 */
	public long timestamp;

	/**
	 * Constructor for SendPrivateMessage that requires all members
	 * @param to
	 * @param message
	 * @param timestamp
	 * @param passthrough
	 */
	public SendPrivateMessage(String to, String message, long timestamp, 
			String passthrough){
		this.to = to;
		this.message = message;
		this.timestamp = timestamp;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendPrivateMessage without super class members
	 * @param to
	 * @param message
	 * @param timestamp
	 */
	public SendPrivateMessage(String to, String message, long timestamp){
		this.to = to;
		this.message = message;
		this.timestamp = timestamp;

	}

	/**
	 * Default Constructor for SendPrivateMessage
	*/
	public SendPrivateMessage(){

	}
}
/** @} */
