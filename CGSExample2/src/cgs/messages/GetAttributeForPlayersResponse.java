/**
 * @file GetAttributeForPlayersResponse.java
 * @page classGetAttributeForPlayersResponse GetAttributeForPlayersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAttributeForPlayersResponse: (playerAndAttributesList, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all players and the players' attribute that was requested
 * 
*/
@JsonTypeName("GetAttributeForPlayersResponse")
public class GetAttributeForPlayersResponse extends GluonResponse{

	/**
	 * A list of players and their attributes that was requested (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributesList;

	/**
	 * Constructor for GetAttributeForPlayersResponse that requires all members
	 * @param playerAndAttributesList
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetAttributeForPlayersResponse(List<PlayerAndAttributes> playerAndAttributesList, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.playerAndAttributesList = playerAndAttributesList;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAttributeForPlayersResponse without super class members
	 * @param playerAndAttributesList
	 */
	public GetAttributeForPlayersResponse(List<PlayerAndAttributes> playerAndAttributesList){
		this.playerAndAttributesList = playerAndAttributesList;

	}
	/**
	 * Constructor for GetAttributeForPlayersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetAttributeForPlayersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetAttributeForPlayersResponse
	*/
	public GetAttributeForPlayersResponse(){

	}
}
/** @} */
