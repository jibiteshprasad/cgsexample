/**
 * @file Currency.java
 * @page classCurrency Currency
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class Currency: (type, value)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * The state of a currency in a player's wallet.
 * 
*/
public class Currency {

	/**
	 * The currency type (Optional)
	 */
	public int type;

	/**
	 * The currenct value of the currency (Optional)
	 */
	public long value;

	/**
	 * Constructor for Currency that requires all members
	 * @param type
	 * @param value
	 */
	public Currency(int type, long value){
		this.type = type;
		this.value = value;

	}

	/**
	 * Default Constructor for Currency
	*/
	public Currency(){

	}
}
/** @} */
