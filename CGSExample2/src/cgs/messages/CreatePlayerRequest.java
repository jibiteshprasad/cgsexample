/**
 * @file CreatePlayerRequest.java
 * @page classCreatePlayerRequest CreatePlayerRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreatePlayerRequest: (loginId, password, gameId, protocolVersion, 
 * 		clientSdkVersion, email, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Create a new player, whose credentials can then be used to authenticate for 
 * this game (based on the gameId).  * The loginId and email must be unique for 
 * the game. If this is successful, you will also be 'logged in' to use %Gluon 
 * services (and thus do not need to call AuthenticatePlayer for this session). 
*/
@JsonTypeName("CreatePlayerRequest")
public class CreatePlayerRequest extends GluonRequest{

	/**
	 * The name to be associated with the player - this should be human 
	 * readable if the game is not completely anonymous 
	 */
	public String loginId;

	/**
	 * Either a password chosen by the loginId, or another automatically 
	 * generated string to associat with the device (Optional)
	 */
	public String password;

	/**
	 * The game identifier string provided by the %Gluon support team
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public int protocolVersion;

	/**
	 *
	 */
	public String clientSdkVersion;

	/**
	 * (Optional)
	 */
	public String email;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CreatePlayerResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CreatePlayerResponseDelegate){
                ((CreatePlayerResponseDelegate)delegate).onCreatePlayerResponse(this, (CreatePlayerResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CreatePlayerResponseDelegate){
            ((CreatePlayerResponseDelegate)delegate).onCreatePlayerResponse(this, new CreatePlayerResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CreatePlayerRequest that requires all members
	 * @param loginId
	 * @param password
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 * @param email
	 * @param passthrough
	 */
	public CreatePlayerRequest(String loginId, String password, String gameId, 
			int protocolVersion, String clientSdkVersion, String email, 
			String passthrough){
		this.loginId = loginId;
		this.password = password;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;
		this.email = email;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreatePlayerRequest without super class members
	 * @param loginId
	 * @param password
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 * @param email
	 */
	public CreatePlayerRequest(String loginId, String password, String gameId, 
			int protocolVersion, String clientSdkVersion, String email){
		this.loginId = loginId;
		this.password = password;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;
		this.email = email;

	}

	/**
	 * Default Constructor for CreatePlayerRequest
	*/
	public CreatePlayerRequest(){

	}
}
/** @} */
