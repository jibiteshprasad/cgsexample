/**
 * @file GetAllAttributesRequest.java
 * @page classGetAllAttributesRequest GetAllAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllAttributesRequest: (playerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve all attributes for a specified player
 * 
*/
@JsonTypeName("GetAllAttributesRequest")
public class GetAllAttributesRequest extends GluonRequest{

	/**
	 * ID of the player whose attributes will be received
	 */
	public String playerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetAllAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetAllAttributesResponseDelegate){
                ((GetAllAttributesResponseDelegate)delegate).onGetAllAttributesResponse(this, (GetAllAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetAllAttributesResponseDelegate){
            ((GetAllAttributesResponseDelegate)delegate).onGetAllAttributesResponse(this, new GetAllAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetAllAttributesRequest that requires all members
	 * @param playerId
	 * @param passthrough
	 */
	public GetAllAttributesRequest(String playerId, String passthrough){
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllAttributesRequest without super class members
	 * @param playerId
	 */
	public GetAllAttributesRequest(String playerId){
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for GetAllAttributesRequest
	*/
	public GetAllAttributesRequest(){

	}
}
/** @} */
