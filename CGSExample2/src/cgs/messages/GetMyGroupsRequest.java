/**
 * @file GetMyGroupsRequest.java
 * @page classGetMyGroupsRequest GetMyGroupsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyGroupsRequest: (groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all the groups that the authenticated player has joined
 * 
*/
@JsonTypeName("GetMyGroupsRequest")
public class GetMyGroupsRequest extends GluonRequest{

	/**
	 * Group type specifed here will only retrieve groups of that type for 
	 * authenticated player 
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMyGroupsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMyGroupsResponseDelegate){
                ((GetMyGroupsResponseDelegate)delegate).onGetMyGroupsResponse(this, (GetMyGroupsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMyGroupsResponseDelegate){
            ((GetMyGroupsResponseDelegate)delegate).onGetMyGroupsResponse(this, new GetMyGroupsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMyGroupsRequest that requires all members
	 * @param groupType
	 * @param passthrough
	 */
	public GetMyGroupsRequest(String groupType, String passthrough){
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyGroupsRequest without super class members
	 * @param groupType
	 */
	public GetMyGroupsRequest(String groupType){
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetMyGroupsRequest
	*/
	public GetMyGroupsRequest(){

	}
}
/** @} */
