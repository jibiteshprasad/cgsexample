/**
 * @file GetLeaderboardEntryRequest.java
 * @page classGetLeaderboardEntryRequest GetLeaderboardEntryRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetLeaderboardEntryRequest: (leaderboardName, ownerId, type, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to get the value for a leaderboard entry.
*/
@JsonTypeName("GetLeaderboardEntryRequest")
public class GetLeaderboardEntryRequest extends GluonRequest{

	/**
	 *
	 */
	public String leaderboardName;

	/**
	 *
	 */
	public String ownerId;

	/**
	 * (Optional)
	 */
	public EntityType type;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetLeaderboardEntryResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetLeaderboardEntryResponseDelegate){
                ((GetLeaderboardEntryResponseDelegate)delegate).onGetLeaderboardEntryResponse(this, (GetLeaderboardEntryResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetLeaderboardEntryResponseDelegate){
            ((GetLeaderboardEntryResponseDelegate)delegate).onGetLeaderboardEntryResponse(this, new GetLeaderboardEntryResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetLeaderboardEntryRequest that requires all members
	 * @param leaderboardName
	 * @param ownerId
	 * @param type
	 * @param passthrough
	 */
	public GetLeaderboardEntryRequest(String leaderboardName, String ownerId, 
			EntityType type, String passthrough){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;
		this.type = type;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetLeaderboardEntryRequest without super class members
	 * @param leaderboardName
	 * @param ownerId
	 * @param type
	 */
	public GetLeaderboardEntryRequest(String leaderboardName, String ownerId, 
			EntityType type){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;
		this.type = type;

	}

	/**
	 * Default Constructor for GetLeaderboardEntryRequest
	*/
	public GetLeaderboardEntryRequest(){

	}
}
/** @} */
