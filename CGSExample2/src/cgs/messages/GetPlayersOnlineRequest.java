/**
 * @file GetPlayersOnlineRequest.java
 * @page classGetPlayersOnlineRequest GetPlayersOnlineRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPlayersOnlineRequest: (playerIds, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Searches for a list of players in the gluon system matching the specified 
 * identifers.  Currently only supports searching for FACEBOOK or NICKNAME.
*/
@JsonTypeName("")
public class GetPlayersOnlineRequest extends GluonRequest{

	/**
	 * List of identifiers to search for players on. This will differ based on 
	 * type. For FACEBOOK, facebook UUIDs are provided. For Nickname, %Gluon 
	 * nickname strings are provided. (Optional)
	 */
	public ArrayList<String> playerIds;

	/**
	 * Constructor for GetPlayersOnlineRequest that requires all members
	 * @param playerIds
	 * @param passthrough
	 */
	public GetPlayersOnlineRequest(ArrayList<String> playerIds, 
			String passthrough){
		this.playerIds = playerIds;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPlayersOnlineRequest without super class members
	 * @param playerIds
	 */
	public GetPlayersOnlineRequest(ArrayList<String> playerIds){
		this.playerIds = playerIds;

	}

	/**
	 * Default Constructor for GetPlayersOnlineRequest
	*/
	public GetPlayersOnlineRequest(){

	}
}
/** @} */
