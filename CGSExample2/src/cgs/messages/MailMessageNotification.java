/**
 * @file MailMessageNotification.java
 * @page classMailMessageNotification MailMessageNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class MailMessageNotification: (message, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returned from the server to authenticated player while the player is 
 * connected to notify the player that a new mail message has arrived 
*/
@JsonTypeName("MailMessageNotification")
public class MailMessageNotification extends GluonRequest{

	/**
	 * Mail message sent to authenticated player
	 */
	public MailMessage message;

	/**
	 * Constructor for MailMessageNotification that requires all members
	 * @param message
	 * @param passthrough
	 */
	public MailMessageNotification(MailMessage message, String passthrough){
		this.message = message;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for MailMessageNotification without super class members
	 * @param message
	 */
	public MailMessageNotification(MailMessage message){
		this.message = message;

	}

	/**
	 * Default Constructor for MailMessageNotification
	*/
	public MailMessageNotification(){

	}
}
/** @} */
