/**
 * @file LockDetail.java
 * @page classLockDetail LockDetail
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LockDetail: (playerInfo, whenLocked, attributesModifiedWhenLocked, 
 * 		lockerAttributes, reverseLockTime)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Data structure used to encapsulate a player who have locked another player.
 * 
*/
public class LockDetail {

	/**
	 * The player who locked me (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * When the lock was initiated
	 */
	public Date whenLocked;

	/**
	 * The list of attributes modified when under lock.
	 */
	public List<PlayerAttribute> attributesModifiedWhenLocked;

	/**
	 * The list of attributes of the locker player
	 */
	public List<PlayerAttribute> lockerAttributes;

	/**
	 * If we ever locked the other player, the last time of that lock. This can 
	 * be used to determine if revenge match should be allowed. (Optional)
	 */
	public Date reverseLockTime;

	/**
	 * Constructor for LockDetail that requires all members
	 * @param playerInfo
	 * @param whenLocked
	 * @param attributesModifiedWhenLocked
	 * @param lockerAttributes
	 * @param reverseLockTime
	 */
	public LockDetail(PlayerInfo playerInfo, Date whenLocked, 
			List<PlayerAttribute> attributesModifiedWhenLocked, 
			List<PlayerAttribute> lockerAttributes, Date reverseLockTime){
		this.playerInfo = playerInfo;
		this.whenLocked = whenLocked;
		this.attributesModifiedWhenLocked = attributesModifiedWhenLocked;
		this.lockerAttributes = lockerAttributes;
		this.reverseLockTime = reverseLockTime;

	}

	/**
	 * Default Constructor for LockDetail
	*/
	public LockDetail(){

	}
}
/** @} */
