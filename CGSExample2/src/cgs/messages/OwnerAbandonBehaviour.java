package cgs.messages;

public enum OwnerAbandonBehaviour
	{
	/// The group will automatically be removed and all players are disbanded when the owner leaves the group.
	DELETE_GROUP,
	/// The group member who has been part of the group the longest will automatically be promoted to owner when the group owner leaves the group.
	PROMOTE_ELDEST,
	/// The group will still exist even after the owner leaves the group.
	FORBID
	
}
