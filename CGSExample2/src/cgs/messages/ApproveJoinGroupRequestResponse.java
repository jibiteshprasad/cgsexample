/**
 * @file ApproveJoinGroupRequestResponse.java
 * @page classApproveJoinGroupRequestResponse ApproveJoinGroupRequestResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApproveJoinGroupRequestResponse: (groupName, groupType, requestId, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, the player you approved has successfully joined the 
 * group. 
*/
@JsonTypeName("ApproveJoinGroupRequestResponse")
public class ApproveJoinGroupRequestResponse extends GluonResponse{

	/**
	 * Name of the group the approved player has just joined - this name is 
	 * unique (Optional)
	 */
	public String groupName;

	/**
	 * Name of the group's type the approved player has just joined (Optional)
	 */
	public String groupType;

	/**
	 * The requestId associated with the original ReceivedJoinGroupRequest that 
	 * was approved
	 */
	public String requestId;

	/**
	 * Constructor for ApproveJoinGroupRequestResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param requestId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ApproveJoinGroupRequestResponse(String groupName, String groupType, 
			String requestId, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.requestId = requestId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApproveJoinGroupRequestResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param requestId
	 */
	public ApproveJoinGroupRequestResponse(String groupName, String groupType, 
			String requestId){
		this.groupName = groupName;
		this.groupType = groupType;
		this.requestId = requestId;

	}
	/**
	 * Constructor for ApproveJoinGroupRequestResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ApproveJoinGroupRequestResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ApproveJoinGroupRequestResponse
	*/
	public ApproveJoinGroupRequestResponse(){

	}
}
/** @} */
