/**
 * @file SetCurrencyResponse.java
 * @page classSetCurrencyResponse SetCurrencyResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetCurrencyResponse: (playerInfo, currency, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response for setting a currency value in a player's wallet.
 * 
*/
@JsonTypeName("SetCurrencyResponse")
public class SetCurrencyResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * The current currency state
	 */
	public Currency currency;

	/**
	 * Constructor for SetCurrencyResponse that requires all members
	 * @param playerInfo
	 * @param currency
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetCurrencyResponse(PlayerInfo playerInfo, Currency currency, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.currency = currency;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetCurrencyResponse without super class members
	 * @param playerInfo
	 * @param currency
	 */
	public SetCurrencyResponse(PlayerInfo playerInfo, Currency currency){
		this.playerInfo = playerInfo;
		this.currency = currency;

	}
	/**
	 * Constructor for SetCurrencyResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetCurrencyResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetCurrencyResponse
	*/
	public SetCurrencyResponse(){

	}
}
/** @} */
