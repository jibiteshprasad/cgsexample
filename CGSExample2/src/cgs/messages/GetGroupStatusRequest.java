/**
 * @file GetGroupStatusRequest.java
 * @page classGetGroupStatusRequest GetGroupStatusRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupStatusRequest: (groupName, groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves the current status of specified group
 * 
*/
@JsonTypeName("GetGroupStatusRequest")
public class GetGroupStatusRequest extends GluonRequest{

	/**
	 * Name of the group whose status will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetGroupStatusResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetGroupStatusResponseDelegate){
                ((GetGroupStatusResponseDelegate)delegate).onGetGroupStatusResponse(this, (GetGroupStatusResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetGroupStatusResponseDelegate){
            ((GetGroupStatusResponseDelegate)delegate).onGetGroupStatusResponse(this, new GetGroupStatusResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetGroupStatusRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public GetGroupStatusRequest(String groupName, String groupType, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupStatusRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public GetGroupStatusRequest(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetGroupStatusRequest
	*/
	public GetGroupStatusRequest(){

	}
}
/** @} */
