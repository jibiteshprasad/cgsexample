/**
 * @file SetDocumentByIdResponse.java
 * @page classSetDocumentByIdResponse SetDocumentByIdResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetDocumentByIdResponse: (documentId, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the document was uploaded and stored, and can be 
 * identified with the  documentId returned.
*/
@JsonTypeName("SetDocumentByIdResponse")
public class SetDocumentByIdResponse extends GluonResponse{

	/**
	 * The unique identifier for the uploaded document, to be used for 
	 * retrieval (GetDocumentById)  or overwriting (SetDocumentById)
	 */
	public String documentId;

	/**
	 * Constructor for SetDocumentByIdResponse that requires all members
	 * @param documentId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetDocumentByIdResponse(String documentId, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.documentId = documentId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetDocumentByIdResponse without super class members
	 * @param documentId
	 */
	public SetDocumentByIdResponse(String documentId){
		this.documentId = documentId;

	}
	/**
	 * Constructor for SetDocumentByIdResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetDocumentByIdResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetDocumentByIdResponse
	*/
	public SetDocumentByIdResponse(){

	}
}
/** @} */
