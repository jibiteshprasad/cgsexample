/**
 * @file GetBaseCatalogProductsRequest.java
 * @page classGetBaseCatalogProductsRequest GetBaseCatalogProductsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetBaseCatalogProductsRequest: (baseCatalogName, sorting, pageIndex, 
 * 		productsPerPage, showActiveProductsOnly, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gets the base products for a catalog.
*/
@JsonTypeName("GetBaseCatalogProductsRequest")
public class GetBaseCatalogProductsRequest extends GluonRequest{

	/**
	 * (Optional) (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional) (Optional)
	 */
	public List<CatalogSort> sorting;

	/**
	 * (Optional) (Optional) (Optional) (Optional)
	 */
	public int pageIndex;

	/**
	 * (Optional) (Optional)
	 */
	public int productsPerPage;

	/**
	 * (Optional) (Optional)
	 */
	public boolean showActiveProductsOnly;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetBaseCatalogProductsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetBaseCatalogProductsResponseDelegate){
                ((GetBaseCatalogProductsResponseDelegate)delegate).onGetBaseCatalogProductsResponse(this, (GetBaseCatalogProductsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetBaseCatalogProductsResponseDelegate){
            ((GetBaseCatalogProductsResponseDelegate)delegate).onGetBaseCatalogProductsResponse(this, new GetBaseCatalogProductsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetBaseCatalogProductsRequest that requires all members
	 * @param baseCatalogName
	 * @param sorting
	 * @param pageIndex
	 * @param productsPerPage
	 * @param showActiveProductsOnly
	 * @param passthrough
	 */
	public GetBaseCatalogProductsRequest(String baseCatalogName, 
			List<CatalogSort> sorting, int pageIndex, int productsPerPage, 
			boolean showActiveProductsOnly, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.sorting = sorting;
		this.pageIndex = pageIndex;
		this.productsPerPage = productsPerPage;
		this.showActiveProductsOnly = showActiveProductsOnly;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetBaseCatalogProductsRequest without super class members
	 * @param baseCatalogName
	 * @param sorting
	 * @param pageIndex
	 * @param productsPerPage
	 * @param showActiveProductsOnly
	 */
	public GetBaseCatalogProductsRequest(String baseCatalogName, 
			List<CatalogSort> sorting, int pageIndex, int productsPerPage, 
			boolean showActiveProductsOnly){
		this.baseCatalogName = baseCatalogName;
		this.sorting = sorting;
		this.pageIndex = pageIndex;
		this.productsPerPage = productsPerPage;
		this.showActiveProductsOnly = showActiveProductsOnly;

	}

	/**
	 * Default Constructor for GetBaseCatalogProductsRequest
	*/
	public GetBaseCatalogProductsRequest(){

	}
}
/** @} */
