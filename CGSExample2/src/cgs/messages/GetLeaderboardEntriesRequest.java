/**
 * @file GetLeaderboardEntriesRequest.java
 * @page classGetLeaderboardEntriesRequest GetLeaderboardEntriesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetLeaderboardEntriesRequest: (leaderboardName, ownerIds, type, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve the values for specific entries in a leaderboard
 * 
*/
@JsonTypeName("GetLeaderboardEntriesRequest")
public class GetLeaderboardEntriesRequest extends GluonRequest{

	/**
	 * The name of the leaderboard from which the entries will be retrieved
	 */
	public String leaderboardName;

	/**
	 * The name of each entry that will be retrieved (each playerId for a 
	 * player-based leaderboard)
	 */
	public List<String> ownerIds;

	/**
	 * The Entity Type PLAYER or GROUP (Optional)
	 */
	public EntityType type;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetLeaderboardEntriesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetLeaderboardEntriesResponseDelegate){
                ((GetLeaderboardEntriesResponseDelegate)delegate).onGetLeaderboardEntriesResponse(this, (GetLeaderboardEntriesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetLeaderboardEntriesResponseDelegate){
            ((GetLeaderboardEntriesResponseDelegate)delegate).onGetLeaderboardEntriesResponse(this, new GetLeaderboardEntriesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetLeaderboardEntriesRequest that requires all members
	 * @param leaderboardName
	 * @param ownerIds
	 * @param type
	 * @param passthrough
	 */
	public GetLeaderboardEntriesRequest(String leaderboardName, 
			List<String> ownerIds, EntityType type, String passthrough){
		this.leaderboardName = leaderboardName;
		this.ownerIds = ownerIds;
		this.type = type;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetLeaderboardEntriesRequest without super class members
	 * @param leaderboardName
	 * @param ownerIds
	 * @param type
	 */
	public GetLeaderboardEntriesRequest(String leaderboardName, 
			List<String> ownerIds, EntityType type){
		this.leaderboardName = leaderboardName;
		this.ownerIds = ownerIds;
		this.type = type;

	}

	/**
	 * Default Constructor for GetLeaderboardEntriesRequest
	*/
	public GetLeaderboardEntriesRequest(){

	}
}
/** @} */
