package cgs.messages;

public enum JoinGroupRequestStatus
	{
	/// Player is pending approval from a group owner. Group owner needs to send a ApproveJoinGroupRequestRequest to complete.
	PENDING,
	/// Player has successfully been accepted and joined the group specified.
	ACCEPTED,
	/// Player has been rejected to join the specified group.
	REJECTED,
	/// An error occurred trying to join the specified group.
	ERROR
	
}
