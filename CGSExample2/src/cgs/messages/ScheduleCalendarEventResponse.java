/**
 * @file ScheduleCalendarEventResponse.java
 * @page classScheduleCalendarEventResponse ScheduleCalendarEventResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ScheduleCalendarEventResponse: (status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Schedules an event for a calendar.
*/
@JsonTypeName("ScheduleCalendarEventResponse")
public class ScheduleCalendarEventResponse extends GluonResponse{

	/**
	 * Constructor for ScheduleCalendarEventResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ScheduleCalendarEventResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ScheduleCalendarEventResponse without super class members
	 */
	public ScheduleCalendarEventResponse(){

	}
	/**
	 * Constructor for ScheduleCalendarEventResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ScheduleCalendarEventResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
