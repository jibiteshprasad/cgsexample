/**
 * @file DeleteAllGroupMemberAttributesForAllMembersRequest.java
 * @page classDeleteAllGroupMemberAttributesForAllMembersRequest DeleteAllGroupMemberAttributesForAllMembersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllGroupMemberAttributesForAllMembersRequest: (groupName, 
 * 		groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to delete all group-member attributes for all the members.
*/
@JsonTypeName("DeleteAllGroupMemberAttributesForAllMembersRequest")
public class DeleteAllGroupMemberAttributesForAllMembersRequest extends GluonRequest{

	/**
	 *
	 */
	public String groupName;

	/**
	 * (Optional)
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteAllGroupMemberAttributesForAllMembersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteAllGroupMemberAttributesForAllMembersResponseDelegate){
                ((DeleteAllGroupMemberAttributesForAllMembersResponseDelegate)delegate).onDeleteAllGroupMemberAttributesForAllMembersResponse(this, (DeleteAllGroupMemberAttributesForAllMembersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteAllGroupMemberAttributesForAllMembersResponseDelegate){
            ((DeleteAllGroupMemberAttributesForAllMembersResponseDelegate)delegate).onDeleteAllGroupMemberAttributesForAllMembersResponse(this, new DeleteAllGroupMemberAttributesForAllMembersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteAllGroupMemberAttributesForAllMembersRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public DeleteAllGroupMemberAttributesForAllMembersRequest(String groupName, 
			String groupType, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllGroupMemberAttributesForAllMembersRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteAllGroupMemberAttributesForAllMembersRequest(String groupName, 
			String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for DeleteAllGroupMemberAttributesForAllMembersRequest
	*/
	public DeleteAllGroupMemberAttributesForAllMembersRequest(){

	}
}
/** @} */
