/**
 * @file UnsubscribeFromGroupNotificationsRequest.java
 * @page classUnsubscribeFromGroupNotificationsRequest UnsubscribeFromGroupNotificationsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class UnsubscribeFromGroupNotificationsRequest: (groups, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Unsubscribe from one or more groups to stop receiving notifications when an 
 * attribute's value is updated.
*/
@JsonTypeName("UnsubscribeFromGroupNotificationsRequest")
public class UnsubscribeFromGroupNotificationsRequest extends GluonRequest{

	/**
	 * Collection of groups to unsubscribe from (Optional)
	 */
	public List<GroupDescriptor> groups;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(UnsubscribeFromGroupNotificationsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof UnsubscribeFromGroupNotificationsResponseDelegate){
                ((UnsubscribeFromGroupNotificationsResponseDelegate)delegate).onUnsubscribeFromGroupNotificationsResponse(this, (UnsubscribeFromGroupNotificationsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof UnsubscribeFromGroupNotificationsResponseDelegate){
            ((UnsubscribeFromGroupNotificationsResponseDelegate)delegate).onUnsubscribeFromGroupNotificationsResponse(this, new UnsubscribeFromGroupNotificationsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for UnsubscribeFromGroupNotificationsRequest that requires all members
	 * @param groups
	 * @param passthrough
	 */
	public UnsubscribeFromGroupNotificationsRequest(List<GroupDescriptor> groups, 
			String passthrough){
		this.groups = groups;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for UnsubscribeFromGroupNotificationsRequest without super class members
	 * @param groups
	 */
	public UnsubscribeFromGroupNotificationsRequest(List<GroupDescriptor> groups){
		this.groups = groups;

	}

	/**
	 * Default Constructor for UnsubscribeFromGroupNotificationsRequest
	*/
	public UnsubscribeFromGroupNotificationsRequest(){

	}
}
/** @} */
