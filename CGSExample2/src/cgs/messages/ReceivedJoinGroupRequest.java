/**
 * @file ReceivedJoinGroupRequest.java
 * @page classReceivedJoinGroupRequest ReceivedJoinGroupRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ReceivedJoinGroupRequest: (requestId, playerInfo, groupName, groupType, 
 * 		createDate)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Request that was received by a non-member player to join a group
 * 
*/
public class ReceivedJoinGroupRequest {

	/**
	 * Unique request ID of the join group request
	 */
	public String requestId;

	/**
	 * The player that wants to join the group
	 */
	public PlayerInfo playerInfo;

	/**
	 * Name of the group that the non-member player wants to join   
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Date and time the request was originally sent (Optional)
	 */
	public Date createDate;

	/**
	 * Constructor for ReceivedJoinGroupRequest that requires all members
	 * @param requestId
	 * @param playerInfo
	 * @param groupName
	 * @param groupType
	 * @param createDate
	 */
	public ReceivedJoinGroupRequest(String requestId, PlayerInfo playerInfo, 
			String groupName, String groupType, Date createDate){
		this.requestId = requestId;
		this.playerInfo = playerInfo;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createDate = createDate;

	}

	/**
	 * Default Constructor for ReceivedJoinGroupRequest
	*/
	public ReceivedJoinGroupRequest(){

	}
}
/** @} */
