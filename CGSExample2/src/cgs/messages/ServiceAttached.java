/**
 * @file ServiceAttached.java
 * @page classServiceAttached ServiceAttached
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ServiceAttached: (serviceType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * 
*/
@JsonTypeName("ServiceAttached")
public class ServiceAttached extends GluonRequest{

	/**
	 *
	 */
	public ServiceType serviceType;

	/**
	 * Constructor for ServiceAttached that requires all members
	 * @param serviceType
	 * @param passthrough
	 */
	public ServiceAttached(ServiceType serviceType, String passthrough){
		this.serviceType = serviceType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ServiceAttached without super class members
	 * @param serviceType
	 */
	public ServiceAttached(ServiceType serviceType){
		this.serviceType = serviceType;

	}

	/**
	 * Default Constructor for ServiceAttached
	*/
	public ServiceAttached(){

	}
}
/** @} */
