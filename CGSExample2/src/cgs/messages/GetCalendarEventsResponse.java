/**
 * @file GetCalendarEventsResponse.java
 * @page classGetCalendarEventsResponse GetCalendarEventsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCalendarEventsResponse: (events, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of calandar event objects as specified in the request.
 * 
*/
@JsonTypeName("GetCalendarEventsResponse")
public class GetCalendarEventsResponse extends GluonResponse{

	/**
	 * A list of calender events specified in the query (Optional)
	 */
	public List<CalendarEvent> events;

	/**
	 * Constructor for GetCalendarEventsResponse that requires all members
	 * @param events
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCalendarEventsResponse(List<CalendarEvent> events, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.events = events;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCalendarEventsResponse without super class members
	 * @param events
	 */
	public GetCalendarEventsResponse(List<CalendarEvent> events){
		this.events = events;

	}
	/**
	 * Constructor for GetCalendarEventsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCalendarEventsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCalendarEventsResponse
	*/
	public GetCalendarEventsResponse(){

	}
}
/** @} */
