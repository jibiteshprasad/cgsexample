/**
 * @file SetBaseCatalogProductsResponse.java
 * @page classSetBaseCatalogProductsResponse SetBaseCatalogProductsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetBaseCatalogProductsResponse: (baseCatalogName, 
 * 		totalNumberOfProductsAdded, totalNumberOfProductsUpdated, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets the base products for a catalog.
*/
@JsonTypeName("SetBaseCatalogProductsResponse")
public class SetBaseCatalogProductsResponse extends GluonResponse{

	/**
	 * (Optional) (Optional) (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public int totalNumberOfProductsAdded;

	/**
	 * (Optional)
	 */
	public int totalNumberOfProductsUpdated;

	/**
	 * Constructor for SetBaseCatalogProductsResponse that requires all members
	 * @param baseCatalogName
	 * @param totalNumberOfProductsAdded
	 * @param totalNumberOfProductsUpdated
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetBaseCatalogProductsResponse(String baseCatalogName, 
			int totalNumberOfProductsAdded, int totalNumberOfProductsUpdated, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.totalNumberOfProductsAdded = totalNumberOfProductsAdded;
		this.totalNumberOfProductsUpdated = totalNumberOfProductsUpdated;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetBaseCatalogProductsResponse without super class members
	 * @param baseCatalogName
	 * @param totalNumberOfProductsAdded
	 * @param totalNumberOfProductsUpdated
	 */
	public SetBaseCatalogProductsResponse(String baseCatalogName, 
			int totalNumberOfProductsAdded, int totalNumberOfProductsUpdated){
		this.baseCatalogName = baseCatalogName;
		this.totalNumberOfProductsAdded = totalNumberOfProductsAdded;
		this.totalNumberOfProductsUpdated = totalNumberOfProductsUpdated;

	}
	/**
	 * Constructor for SetBaseCatalogProductsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetBaseCatalogProductsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetBaseCatalogProductsResponse
	*/
	public SetBaseCatalogProductsResponse(){

	}
}
/** @} */
