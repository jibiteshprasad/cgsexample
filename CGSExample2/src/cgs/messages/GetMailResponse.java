/**
 * @file GetMailResponse.java
 * @page classGetMailResponse GetMailResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMailResponse: (page, size, totalSize, totalPages, messages, 
 * 		unreadMessagesOnly, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all messages that were sent to authenticated player
 * 
*/
@JsonTypeName("GetMailMessagesResponse")
public class GetMailResponse extends GluonResponse{

	/**
	 * Current page that was returned (Optional)
	 */
	public int page;

	/**
	 * Number of messages returned for current page (Optional)
	 */
	public int size;

	/**
	 * Total number of messages sent to this player (Optional)
	 */
	public long totalSize;

	/**
	 * Total number of pages according to proposed size (Optional)
	 */
	public int totalPages;

	/**
	 * List of mail messages that was returned (Optional)
	 */
	public ArrayList<MailMessage> messages;

	/**
	 * (Optional)
	 */
	public boolean unreadMessagesOnly;

	/**
	 * Constructor for GetMailResponse that requires all members
	 * @param page
	 * @param size
	 * @param totalSize
	 * @param totalPages
	 * @param messages
	 * @param unreadMessagesOnly
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMailResponse(int page, int size, long totalSize, int totalPages, 
			ArrayList<MailMessage> messages, boolean unreadMessagesOnly, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.page = page;
		this.size = size;
		this.totalSize = totalSize;
		this.totalPages = totalPages;
		this.messages = messages;
		this.unreadMessagesOnly = unreadMessagesOnly;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMailResponse without super class members
	 * @param page
	 * @param size
	 * @param totalSize
	 * @param totalPages
	 * @param messages
	 * @param unreadMessagesOnly
	 */
	public GetMailResponse(int page, int size, long totalSize, int totalPages, 
			ArrayList<MailMessage> messages, boolean unreadMessagesOnly){
		this.page = page;
		this.size = size;
		this.totalSize = totalSize;
		this.totalPages = totalPages;
		this.messages = messages;
		this.unreadMessagesOnly = unreadMessagesOnly;

	}
	/**
	 * Constructor for GetMailResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMailResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMailResponse
	*/
	public GetMailResponse(){

	}
}
/** @} */
