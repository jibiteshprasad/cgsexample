package cgs.messages;

public enum ErrorAction
	{
	/// Request not valid and will never be processed by the backend server
	DISCARD,
	/// Resend the request at a later time (same request might work at a later time)
	HOLD_AND_RESEND,
	/// Resend the request after fixing the problem with the request that was sent
	FIX_AND_RESEND
	
}
