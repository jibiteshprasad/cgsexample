/**
 * @file GetMyGroupMemberAttributesResponse.java
 * @page classGetMyGroupMemberAttributesResponse GetMyGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyGroupMemberAttributesResponse: (groupName, groupType, attributes, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information and group member attributes for the 
 * authenticated player will be returned
*/
@JsonTypeName("GetMyGroupMemberAttributesResponse")
public class GetMyGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Contains player information and a list of the member attributes that was 
	 * requested (Optional)
	 */
	public PlayerAndAttributes attributes;

	/**
	 * Constructor for GetMyGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMyGroupMemberAttributesResponse(String groupName, String groupType, 
			PlayerAndAttributes attributes, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 */
	public GetMyGroupMemberAttributesResponse(String groupName, String groupType, 
			PlayerAndAttributes attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetMyGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMyGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMyGroupMemberAttributesResponse
	*/
	public GetMyGroupMemberAttributesResponse(){

	}
}
/** @} */
