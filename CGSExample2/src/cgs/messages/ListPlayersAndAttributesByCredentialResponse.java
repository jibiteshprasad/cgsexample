/**
 * @file ListPlayersAndAttributesByCredentialResponse.java
 * @page classListPlayersAndAttributesByCredentialResponse ListPlayersAndAttributesByCredentialResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ListPlayersAndAttributesByCredentialResponse: (playerAndAttributesList, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the playerAndAttributeList will be populated with the 
 * specified attributes for each player  associated with the (Facebook) 
 * credential passed. At this point you can let the user decide which player 
 * data to keep, and remove the other (using SetAsPrimaryPlayer message). 
*/
@JsonTypeName("ListPlayersAndAttributesByCredentialResponse")
public class ListPlayersAndAttributesByCredentialResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributesList;

	/**
	 * Constructor for ListPlayersAndAttributesByCredentialResponse that requires all members
	 * @param playerAndAttributesList
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ListPlayersAndAttributesByCredentialResponse(List<PlayerAndAttributes> playerAndAttributesList, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.playerAndAttributesList = playerAndAttributesList;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ListPlayersAndAttributesByCredentialResponse without super class members
	 * @param playerAndAttributesList
	 */
	public ListPlayersAndAttributesByCredentialResponse(List<PlayerAndAttributes> playerAndAttributesList){
		this.playerAndAttributesList = playerAndAttributesList;

	}
	/**
	 * Constructor for ListPlayersAndAttributesByCredentialResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ListPlayersAndAttributesByCredentialResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ListPlayersAndAttributesByCredentialResponse
	*/
	public ListPlayersAndAttributesByCredentialResponse(){

	}
}
/** @} */
