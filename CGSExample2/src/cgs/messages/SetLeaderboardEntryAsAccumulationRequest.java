/**
 * @file SetLeaderboardEntryAsAccumulationRequest.java
 * @page classSetLeaderboardEntryAsAccumulationRequest SetLeaderboardEntryAsAccumulationRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetLeaderboardEntryAsAccumulationRequest: (sourceLeaderboardName, 
 * 		sourceOwnerIds, targetLeaderboardName, targetOwnerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Add up the values of multiple entries in a player-based leaderboard, and 
 * write it to a single entry in a separate group-based leaderboard. For 
 * instance this might be used to sum up the score for all players in a Group 
 * (in a PlayerScore leaderboard) and write the total to that Group's entry in a 
 * GroupScore leaderboard.
*/
@JsonTypeName("SetLeaderboardEntryAsAccumulationRequest")
public class SetLeaderboardEntryAsAccumulationRequest extends GluonRequest{

	/**
	 * Leaderboard from which to sum up values of certain entries
	 */
	public String sourceLeaderboardName;

	/**
	 * The entries whose values we want to sum up, typically each player in a 
	 * certain Group
	 */
	public List<String> sourceOwnerIds;

	/**
	 * The Group leaderboard we wish to write the aggregate to
	 */
	public String targetLeaderboardName;

	/**
	 * The name of the Group whose players' scores (or other stat) will be 
	 * tallied up into a single Group entry
	 */
	public String targetOwnerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetLeaderboardEntryAsAccumulationResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetLeaderboardEntryAsAccumulationResponseDelegate){
                ((SetLeaderboardEntryAsAccumulationResponseDelegate)delegate).onSetLeaderboardEntryAsAccumulationResponse(this, (SetLeaderboardEntryAsAccumulationResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetLeaderboardEntryAsAccumulationResponseDelegate){
            ((SetLeaderboardEntryAsAccumulationResponseDelegate)delegate).onSetLeaderboardEntryAsAccumulationResponse(this, new SetLeaderboardEntryAsAccumulationResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetLeaderboardEntryAsAccumulationRequest that requires all members
	 * @param sourceLeaderboardName
	 * @param sourceOwnerIds
	 * @param targetLeaderboardName
	 * @param targetOwnerId
	 * @param passthrough
	 */
	public SetLeaderboardEntryAsAccumulationRequest(String sourceLeaderboardName, 
			List<String> sourceOwnerIds, String targetLeaderboardName, 
			String targetOwnerId, String passthrough){
		this.sourceLeaderboardName = sourceLeaderboardName;
		this.sourceOwnerIds = sourceOwnerIds;
		this.targetLeaderboardName = targetLeaderboardName;
		this.targetOwnerId = targetOwnerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetLeaderboardEntryAsAccumulationRequest without super class members
	 * @param sourceLeaderboardName
	 * @param sourceOwnerIds
	 * @param targetLeaderboardName
	 * @param targetOwnerId
	 */
	public SetLeaderboardEntryAsAccumulationRequest(String sourceLeaderboardName, 
			List<String> sourceOwnerIds, String targetLeaderboardName, 
			String targetOwnerId){
		this.sourceLeaderboardName = sourceLeaderboardName;
		this.sourceOwnerIds = sourceOwnerIds;
		this.targetLeaderboardName = targetLeaderboardName;
		this.targetOwnerId = targetOwnerId;

	}

	/**
	 * Default Constructor for SetLeaderboardEntryAsAccumulationRequest
	*/
	public SetLeaderboardEntryAsAccumulationRequest(){

	}
}
/** @} */
