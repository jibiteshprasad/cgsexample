/**
 * @file MyGroupMember.java
 * @page classMyGroupMember MyGroupMember
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class MyGroupMember: (playerInfo, role, permissions)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A group member and his detailed information
 * 
*/
public class MyGroupMember {

	/**
	 * The player who is a group member    
	 */
	public PlayerInfo playerInfo;

	/**
	 * Role the member has been assigned (e.g. officer, leader, etc.)
	 */
	public String role;

	/**
	 * Permissions given to the member (e.g. moderator, etc.) (Optional)
	 */
	public Set<String> permissions;

	/**
	 * Constructor for MyGroupMember that requires all members
	 * @param playerInfo
	 * @param role
	 * @param permissions
	 */
	public MyGroupMember(PlayerInfo playerInfo, String role, 
			Set<String> permissions){
		this.playerInfo = playerInfo;
		this.role = role;
		this.permissions = permissions;

	}

	/**
	 * Default Constructor for MyGroupMember
	*/
	public MyGroupMember(){

	}
}
/** @} */
