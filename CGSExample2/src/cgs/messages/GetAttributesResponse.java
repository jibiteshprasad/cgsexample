/**
 * @file GetAttributesResponse.java
 * @page classGetAttributesResponse GetAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAttributesResponse: (playerInfo, attributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of the attributes requested
 * 
*/
@JsonTypeName("GetAttributesResponse")
public class GetAttributesResponse extends GluonResponse{

	/**
	 * The player associated with the attributes received (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * A list of attributes requested for specified player (Optional)
	 */
	public List<PlayerAttribute> attributes;

	/**
	 * Constructor for GetAttributesResponse that requires all members
	 * @param playerInfo
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetAttributesResponse(PlayerInfo playerInfo, 
			List<PlayerAttribute> attributes, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.playerInfo = playerInfo;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAttributesResponse without super class members
	 * @param playerInfo
	 * @param attributes
	 */
	public GetAttributesResponse(PlayerInfo playerInfo, 
			List<PlayerAttribute> attributes){
		this.playerInfo = playerInfo;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetAttributesResponse
	*/
	public GetAttributesResponse(){

	}
}
/** @} */
