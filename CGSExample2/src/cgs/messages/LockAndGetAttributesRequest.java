/**
 * @file LockAndGetAttributesRequest.java
 * @page classLockAndGetAttributesRequest LockAndGetAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LockAndGetAttributesRequest: (attributeNames, lockDurationSeconds, 
 * 		optimisticLocking, playerIds, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retreives a list of players and their attributes while simultaneously locking 
 * their player attributes records.  * No changes can be made to a player's 
 * attributes until the lock expires or the locking player calls the  
 * corresponding SetAttributesAndUnlockRequest. This call is used in 
 * asynchronous matchmaking to prevent race conditions while a  match is in 
 * progress. * It's recommended to only ever request a single lock (player) at a 
 * time in this request, and to pass false to optimisticLocking.
*/
@JsonTypeName("LockAndGetAttributesRequest")
public class LockAndGetAttributesRequest extends GluonRequest{

	/**
	 * Collection of player attributes you wish to retrieve from the players. 
	 * Note that the lock will be kept on the entire player record, not on the 
	 * specific attributes requested.
	 */
	public List<String> attributeNames;

	/**
	 * Amount of time (in seconds) to hold the lock
	 */
	public int lockDurationSeconds;

	/**
	 * Using true won't guarantee all locks for users will occur, false will 
	 * cause this request to fail if any lock for a player is unavailable.
	 */
	public boolean optimisticLocking;

	/**
	 * Collection of players you wish to retreive attributes for and lock
	 */
	public List<String> playerIds;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(LockAndGetAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof LockAndGetAttributesResponseDelegate){
                ((LockAndGetAttributesResponseDelegate)delegate).onLockAndGetAttributesResponse(this, (LockAndGetAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof LockAndGetAttributesResponseDelegate){
            ((LockAndGetAttributesResponseDelegate)delegate).onLockAndGetAttributesResponse(this, new LockAndGetAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for LockAndGetAttributesRequest that requires all members
	 * @param attributeNames
	 * @param lockDurationSeconds
	 * @param optimisticLocking
	 * @param playerIds
	 * @param passthrough
	 */
	public LockAndGetAttributesRequest(List<String> attributeNames, 
			int lockDurationSeconds, boolean optimisticLocking, 
			List<String> playerIds, String passthrough){
		this.attributeNames = attributeNames;
		this.lockDurationSeconds = lockDurationSeconds;
		this.optimisticLocking = optimisticLocking;
		this.playerIds = playerIds;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for LockAndGetAttributesRequest without super class members
	 * @param attributeNames
	 * @param lockDurationSeconds
	 * @param optimisticLocking
	 * @param playerIds
	 */
	public LockAndGetAttributesRequest(List<String> attributeNames, 
			int lockDurationSeconds, boolean optimisticLocking, 
			List<String> playerIds){
		this.attributeNames = attributeNames;
		this.lockDurationSeconds = lockDurationSeconds;
		this.optimisticLocking = optimisticLocking;
		this.playerIds = playerIds;

	}

	/**
	 * Default Constructor for LockAndGetAttributesRequest
	*/
	public LockAndGetAttributesRequest(){

	}
}
/** @} */
