/**
 * @file AddQueuedItemsRequest.java
 * @page classAddQueuedItemsRequest AddQueuedItemsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AddQueuedItemsRequest: (ownerId, targetId, items, queueName, 
 * 		queueMaxWeight, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Adds a collection of items to the specified escrow queue to be given to the 
 * target account
*/
@JsonTypeName("AddQueuedItemsRequest")
public class AddQueuedItemsRequest extends GluonRequest{

	/**
	 * The original owner of the items (Optional)
	 */
	public EscrowAccount ownerId;

	/**
	 * The new owner of the items
	 */
	public EscrowAccount targetId;

	/**
	 * Collection of items to give to the target account
	 */
	public List<EscrowItem> items;

	/**
	 * Name of the queue to hold the items (can be any arbitrary name)
	 */
	public String queueName;

	/**
	 * The maximum combined weight of the items allowed in the specified queue 
	 * (Optional)
	 */
	public int queueMaxWeight;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(AddQueuedItemsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof AddQueuedItemsResponseDelegate){
                ((AddQueuedItemsResponseDelegate)delegate).onAddQueuedItemsResponse(this, (AddQueuedItemsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof AddQueuedItemsResponseDelegate){
            ((AddQueuedItemsResponseDelegate)delegate).onAddQueuedItemsResponse(this, new AddQueuedItemsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for AddQueuedItemsRequest that requires all members
	 * @param ownerId
	 * @param targetId
	 * @param items
	 * @param queueName
	 * @param queueMaxWeight
	 * @param passthrough
	 */
	public AddQueuedItemsRequest(EscrowAccount ownerId, EscrowAccount targetId, 
			List<EscrowItem> items, String queueName, int queueMaxWeight, 
			String passthrough){
		this.ownerId = ownerId;
		this.targetId = targetId;
		this.items = items;
		this.queueName = queueName;
		this.queueMaxWeight = queueMaxWeight;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AddQueuedItemsRequest without super class members
	 * @param ownerId
	 * @param targetId
	 * @param items
	 * @param queueName
	 * @param queueMaxWeight
	 */
	public AddQueuedItemsRequest(EscrowAccount ownerId, EscrowAccount targetId, 
			List<EscrowItem> items, String queueName, int queueMaxWeight){
		this.ownerId = ownerId;
		this.targetId = targetId;
		this.items = items;
		this.queueName = queueName;
		this.queueMaxWeight = queueMaxWeight;

	}

	/**
	 * Default Constructor for AddQueuedItemsRequest
	*/
	public AddQueuedItemsRequest(){

	}
}
/** @} */
