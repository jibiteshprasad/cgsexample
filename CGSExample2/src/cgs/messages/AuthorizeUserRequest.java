/**
 * @file AuthorizeUserRequest.java
 * @page classAuthorizeUserRequest AuthorizeUserRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AuthorizeUserRequest: (playerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request from a game server to authorize a user.
*/
@JsonTypeName("AuthorizeUserRequest")
public class AuthorizeUserRequest extends GluonRequest{

	/**
	 *
	 */
	public String playerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(AuthorizeUserResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof AuthorizeUserResponseDelegate){
                ((AuthorizeUserResponseDelegate)delegate).onAuthorizeUserResponse(this, (AuthorizeUserResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof AuthorizeUserResponseDelegate){
            ((AuthorizeUserResponseDelegate)delegate).onAuthorizeUserResponse(this, new AuthorizeUserResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for AuthorizeUserRequest that requires all members
	 * @param playerId
	 * @param passthrough
	 */
	public AuthorizeUserRequest(String playerId, String passthrough){
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AuthorizeUserRequest without super class members
	 * @param playerId
	 */
	public AuthorizeUserRequest(String playerId){
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for AuthorizeUserRequest
	*/
	public AuthorizeUserRequest(){

	}
}
/** @} */
