/**
 * @file EscrowItem.java
 * @page classEscrowItem EscrowItem
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class EscrowItem: (id, weight, type, count, expirationDate, attributes)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * An Escrow Item abstraction
 * 
*/
public class EscrowItem {

	/**
	 * Unique identifier to distinguish this item, set this as "null" when 
	 * adding a new item to a queue. (Optional)
	 */
	public String id;

	/**
	 * Weight associated with this item (used to limit how many items can be 
	 * kept in escrow at a time, such as "clan castle" troop donations) 
	 * (Optional)
	 */
	public int weight;

	/**
	 * Game specific identifier indicating the type of item
	 */
	public String type;

	/**
	 *  Count for stacked items, e.g. if type = "gold" then count would 
	 * indicate the amount of gold. (Optional)
	 */
	public int count;

	/**
	 * Expiration for item. If item is not claimed by expiration date, it will 
	 * be returned to the owner. (Optional)
	 */
	public Date expirationDate;

	/**
	 * Attributes associated to this item (Optional)
	 */
	public List<EscrowAttribute> attributes;

	/**
	 * Constructor for EscrowItem that requires all members
	 * @param id
	 * @param weight
	 * @param type
	 * @param count
	 * @param expirationDate
	 * @param attributes
	 */
	public EscrowItem(String id, int weight, String type, int count, 
			Date expirationDate, List<EscrowAttribute> attributes){
		this.id = id;
		this.weight = weight;
		this.type = type;
		this.count = count;
		this.expirationDate = expirationDate;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for EscrowItem
	*/
	public EscrowItem(){

	}
}
/** @} */
