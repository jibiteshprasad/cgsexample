/**
 * @file SetDocumentByIdRequest.java
 * @page classSetDocumentByIdRequest SetDocumentByIdRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetDocumentByIdRequest: (tags, document, documentId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upload a document (byte array) to %Gluon file storage
 * 
*/
@JsonTypeName("SetDocumentByIdRequest")
public class SetDocumentByIdRequest extends GluonRequest{

	/**
	 * Categorize the document as you see fit (e.g. something like 
	 * "DeerScreenshot" or "AttackReplay") (Optional)
	 */
	public List<String> tags;

	/**
	 * The file or in memory data to be uploaded
	 */
	public byte[] document;

	/**
	 * Replace the document with this documentId
	 */
	public String documentId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetDocumentByIdResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetDocumentByIdResponseDelegate){
                ((SetDocumentByIdResponseDelegate)delegate).onSetDocumentByIdResponse(this, (SetDocumentByIdResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetDocumentByIdResponseDelegate){
            ((SetDocumentByIdResponseDelegate)delegate).onSetDocumentByIdResponse(this, new SetDocumentByIdResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetDocumentByIdRequest that requires all members
	 * @param tags
	 * @param document
	 * @param documentId
	 * @param passthrough
	 */
	public SetDocumentByIdRequest(List<String> tags, byte[] document, 
			String documentId, String passthrough){
		this.tags = tags;
		this.document = document;
		this.documentId = documentId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetDocumentByIdRequest without super class members
	 * @param tags
	 * @param document
	 * @param documentId
	 */
	public SetDocumentByIdRequest(List<String> tags, byte[] document, 
			String documentId){
		this.tags = tags;
		this.document = document;
		this.documentId = documentId;

	}

	/**
	 * Default Constructor for SetDocumentByIdRequest
	*/
	public SetDocumentByIdRequest(){

	}
}
/** @} */
