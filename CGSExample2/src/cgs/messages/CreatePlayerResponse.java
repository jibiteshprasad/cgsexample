/**
 * @file CreatePlayerResponse.java
 * @page classCreatePlayerResponse CreatePlayerResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreatePlayerResponse: (session, playerInfo, expireTime, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the player account was created and you are now 
 * authorized to use %Gluon services
*/
@JsonTypeName("CreatePlayerResponse")
public class CreatePlayerResponse extends GluonResponse{

	/**
	 * Guid passed to future requests  (Optional)
	 */
	public String session;

	/**
	 * The created player (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * This is unused currently, so you can ignore. (Optional)
	 */
	public Calendar expireTime;

	/**
	 * Constructor for CreatePlayerResponse that requires all members
	 * @param session
	 * @param playerInfo
	 * @param expireTime
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CreatePlayerResponse(String session, PlayerInfo playerInfo, 
			Calendar expireTime, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.session = session;
		this.playerInfo = playerInfo;
		this.expireTime = expireTime;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreatePlayerResponse without super class members
	 * @param session
	 * @param playerInfo
	 * @param expireTime
	 */
	public CreatePlayerResponse(String session, PlayerInfo playerInfo, 
			Calendar expireTime){
		this.session = session;
		this.playerInfo = playerInfo;
		this.expireTime = expireTime;

	}
	/**
	 * Constructor for CreatePlayerResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CreatePlayerResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CreatePlayerResponse
	*/
	public CreatePlayerResponse(){

	}
}
/** @} */
