/**
 * @file LeaveGroupResponse.java
 * @page classLeaveGroupResponse LeaveGroupResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LeaveGroupResponse: (groupName, groupType, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the authenticated player confirming whether the group 
 * was left successfully
*/
@JsonTypeName("LeaveGroupResponse")
public class LeaveGroupResponse extends GluonResponse{

	/**
	 * Name of the group that was left
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Constructor for LeaveGroupResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public LeaveGroupResponse(String groupName, String groupType, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for LeaveGroupResponse without super class members
	 * @param groupName
	 * @param groupType
	 */
	public LeaveGroupResponse(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}
	/**
	 * Constructor for LeaveGroupResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public LeaveGroupResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for LeaveGroupResponse
	*/
	public LeaveGroupResponse(){

	}
}
/** @} */
