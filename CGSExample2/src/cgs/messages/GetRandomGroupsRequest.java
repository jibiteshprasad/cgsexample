/**
 * @file GetRandomGroupsRequest.java
 * @page classGetRandomGroupsRequest GetRandomGroupsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetRandomGroupsRequest: (groupType, groupSizeMin, groupSizeMax, 
 * 		maxGroupsReturned, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve random groups according to specified parameters
 * 
*/
@JsonTypeName("GetRandomGroupsRequest")
public class GetRandomGroupsRequest extends GluonRequest{

	/**
	 * Type associated to the group that will be retrieved
	 */
	public String groupType;

	/**
	 * Minimum number of members currently in groups that will be retrieved in 
	 * the response (Optional)
	 */
	public int groupSizeMin;

	/**
	 * Maximum number of members currently in groups that will be retrieved in 
	 * the response (Optional)
	 */
	public int groupSizeMax;

	/**
	 * Maximum number of groups that will be retrieved in the response 
	 * (Optional)
	 */
	public int maxGroupsReturned;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetRandomGroupsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetRandomGroupsResponseDelegate){
                ((GetRandomGroupsResponseDelegate)delegate).onGetRandomGroupsResponse(this, (GetRandomGroupsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetRandomGroupsResponseDelegate){
            ((GetRandomGroupsResponseDelegate)delegate).onGetRandomGroupsResponse(this, new GetRandomGroupsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetRandomGroupsRequest that requires all members
	 * @param groupType
	 * @param groupSizeMin
	 * @param groupSizeMax
	 * @param maxGroupsReturned
	 * @param passthrough
	 */
	public GetRandomGroupsRequest(String groupType, int groupSizeMin, 
			int groupSizeMax, int maxGroupsReturned, String passthrough){
		this.groupType = groupType;
		this.groupSizeMin = groupSizeMin;
		this.groupSizeMax = groupSizeMax;
		this.maxGroupsReturned = maxGroupsReturned;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetRandomGroupsRequest without super class members
	 * @param groupType
	 * @param groupSizeMin
	 * @param groupSizeMax
	 * @param maxGroupsReturned
	 */
	public GetRandomGroupsRequest(String groupType, int groupSizeMin, 
			int groupSizeMax, int maxGroupsReturned){
		this.groupType = groupType;
		this.groupSizeMin = groupSizeMin;
		this.groupSizeMax = groupSizeMax;
		this.maxGroupsReturned = maxGroupsReturned;

	}

	/**
	 * Default Constructor for GetRandomGroupsRequest
	*/
	public GetRandomGroupsRequest(){

	}
}
/** @} */
