/**
 * @file DeleteAllGroupMemberAttributesForAllMembersResponse.java
 * @page classDeleteAllGroupMemberAttributesForAllMembersResponse DeleteAllGroupMemberAttributesForAllMembersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllGroupMemberAttributesForAllMembersResponse: (groupName, 
 * 		groupType, members, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to delete all group-member attributes for all the members.
*/
@JsonTypeName("DeleteAllGroupMemberAttributesForAllMembersResponse")
public class DeleteAllGroupMemberAttributesForAllMembersResponse extends GluonResponse{

	/**
	 *
	 */
	public String groupName;

	/**
	 * (Optional)
	 */
	public String groupType;

	/**
	 * (Optional)
	 */
	public ArrayList<PlayerInfo> members;

	/**
	 * Constructor for DeleteAllGroupMemberAttributesForAllMembersResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteAllGroupMemberAttributesForAllMembersResponse(String groupName, 
			String groupType, ArrayList<PlayerInfo> members, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllGroupMemberAttributesForAllMembersResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param members
	 */
	public DeleteAllGroupMemberAttributesForAllMembersResponse(String groupName, 
			String groupType, ArrayList<PlayerInfo> members){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;

	}
	/**
	 * Constructor for DeleteAllGroupMemberAttributesForAllMembersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteAllGroupMemberAttributesForAllMembersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteAllGroupMemberAttributesForAllMembersResponse
	*/
	public DeleteAllGroupMemberAttributesForAllMembersResponse(){

	}
}
/** @} */
