/**
 * @file SendJoinGroupInviteRequest.java
 * @page classSendJoinGroupInviteRequest SendJoinGroupInviteRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendJoinGroupInviteRequest: (groupName, groupType, playerId, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Send an invite to a specified player to join a group
 * 
*/
@JsonTypeName("SendJoinGroupInviteRequest")
public class SendJoinGroupInviteRequest extends GluonRequest{

	/**
	 * Name of the group to be joined	
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * ID of the player that will be invited to the group
	 */
	public String playerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendJoinGroupInviteResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendJoinGroupInviteResponseDelegate){
                ((SendJoinGroupInviteResponseDelegate)delegate).onSendJoinGroupInviteResponse(this, (SendJoinGroupInviteResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendJoinGroupInviteResponseDelegate){
            ((SendJoinGroupInviteResponseDelegate)delegate).onSendJoinGroupInviteResponse(this, new SendJoinGroupInviteResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendJoinGroupInviteRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerId
	 * @param passthrough
	 */
	public SendJoinGroupInviteRequest(String groupName, String groupType, 
			String playerId, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendJoinGroupInviteRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerId
	 */
	public SendJoinGroupInviteRequest(String groupName, String groupType, 
			String playerId){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for SendJoinGroupInviteRequest
	*/
	public SendJoinGroupInviteRequest(){

	}
}
/** @} */
