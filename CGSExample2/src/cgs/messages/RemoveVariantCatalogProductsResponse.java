/**
 * @file RemoveVariantCatalogProductsResponse.java
 * @page classRemoveVariantCatalogProductsResponse RemoveVariantCatalogProductsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemoveVariantCatalogProductsResponse: (baseCatalogName, 
 * 		variantCatalogName, totalNumberOfProductsRemoved, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Removes the variant products for a catalog.
*/
@JsonTypeName("RemoveVariantCatalogProductsResponse")
public class RemoveVariantCatalogProductsResponse extends GluonResponse{

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public String variantCatalogName;

	/**
	 * (Optional) (Optional)
	 */
	public int totalNumberOfProductsRemoved;

	/**
	 * Constructor for RemoveVariantCatalogProductsResponse that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param totalNumberOfProductsRemoved
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public RemoveVariantCatalogProductsResponse(String baseCatalogName, 
			String variantCatalogName, int totalNumberOfProductsRemoved, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.totalNumberOfProductsRemoved = totalNumberOfProductsRemoved;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemoveVariantCatalogProductsResponse without super class members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param totalNumberOfProductsRemoved
	 */
	public RemoveVariantCatalogProductsResponse(String baseCatalogName, 
			String variantCatalogName, int totalNumberOfProductsRemoved){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.totalNumberOfProductsRemoved = totalNumberOfProductsRemoved;

	}
	/**
	 * Constructor for RemoveVariantCatalogProductsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public RemoveVariantCatalogProductsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for RemoveVariantCatalogProductsResponse
	*/
	public RemoveVariantCatalogProductsResponse(){

	}
}
/** @} */
