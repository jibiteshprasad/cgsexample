/**
 * @file GetGroupStatusResponse.java
 * @page classGetGroupStatusResponse GetGroupStatusResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupStatusResponse: (groupName, groupType, isGroupOpen, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns whether the specified group is open or closed
 * 
*/
@JsonTypeName("GetGroupStatusResponse")
public class GetGroupStatusResponse extends GluonResponse{

	/**
	 * Name of the group associated to the status 
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Flag which determines whether group is open or closed (Optional)
	 */
	public Boolean isGroupOpen;

	/**
	 * Constructor for GetGroupStatusResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param isGroupOpen
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetGroupStatusResponse(String groupName, String groupType, 
			Boolean isGroupOpen, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.isGroupOpen = isGroupOpen;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupStatusResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param isGroupOpen
	 */
	public GetGroupStatusResponse(String groupName, String groupType, 
			Boolean isGroupOpen){
		this.groupName = groupName;
		this.groupType = groupType;
		this.isGroupOpen = isGroupOpen;

	}
	/**
	 * Constructor for GetGroupStatusResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetGroupStatusResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetGroupStatusResponse
	*/
	public GetGroupStatusResponse(){

	}
}
/** @} */
