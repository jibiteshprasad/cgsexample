/**
 * @file EscrowAttribute.java
 * @page classEscrowAttribute EscrowAttribute
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class EscrowAttribute: (name, value)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * An Escrow Attribute abstraction
 * 
*/
public class EscrowAttribute {

	/**
	 * Attribute name
	 */
	public String name;

	/**
	 * Value of the attribute
	 */
	public String value;

	/**
	 * Constructor for EscrowAttribute that requires all members
	 * @param name
	 * @param value
	 */
	public EscrowAttribute(String name, String value){
		this.name = name;
		this.value = value;

	}

	/**
	 * Default Constructor for EscrowAttribute
	*/
	public EscrowAttribute(){

	}
}
/** @} */
