/**
 * @file GetCurrenciesRequest.java
 * @page classGetCurrenciesRequest GetCurrenciesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCurrenciesRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to get all the currencies in a player's wallet and their values for 
 * the player.
*/
@JsonTypeName("GetCurrenciesRequest")
public class GetCurrenciesRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCurrenciesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCurrenciesResponseDelegate){
                ((GetCurrenciesResponseDelegate)delegate).onGetCurrenciesResponse(this, (GetCurrenciesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCurrenciesResponseDelegate){
            ((GetCurrenciesResponseDelegate)delegate).onGetCurrenciesResponse(this, new GetCurrenciesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCurrenciesRequest that requires all members
	 * @param passthrough
	 */
	public GetCurrenciesRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCurrenciesRequest without super class members
	 */
	public GetCurrenciesRequest(){

	}
}
/** @} */
