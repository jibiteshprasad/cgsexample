/**
 * @file GetGroupsAttributesResponse.java
 * @page classGetGroupsAttributesResponse GetGroupsAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupsAttributesResponse: (attributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all group attributes for the multiple groups that were requested
 * 
*/
@JsonTypeName("GetGroupsAttributesResponse")
public class GetGroupsAttributesResponse extends GluonResponse{

	/**
	 * A list of group attributes for specified group (Optional)
	 */
	public List<GroupAttributes> attributes;

	/**
	 * Constructor for GetGroupsAttributesResponse that requires all members
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetGroupsAttributesResponse(List<GroupAttributes> attributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupsAttributesResponse without super class members
	 * @param attributes
	 */
	public GetGroupsAttributesResponse(List<GroupAttributes> attributes){
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetGroupsAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetGroupsAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetGroupsAttributesResponse
	*/
	public GetGroupsAttributesResponse(){

	}
}
/** @} */
