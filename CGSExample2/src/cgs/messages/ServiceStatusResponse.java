/**
 * @file ServiceStatusResponse.java
 * @page classServiceStatusResponse ServiceStatusResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ServiceStatusResponse: (services, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to a player indicating whether they were able to reconnect.
*/
@JsonTypeName("ServiceStatusResponse")
public class ServiceStatusResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public List<String> services;

	/**
	 * Constructor for ServiceStatusResponse that requires all members
	 * @param services
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ServiceStatusResponse(List<String> services, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.services = services;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ServiceStatusResponse without super class members
	 * @param services
	 */
	public ServiceStatusResponse(List<String> services){
		this.services = services;

	}
	/**
	 * Constructor for ServiceStatusResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ServiceStatusResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ServiceStatusResponse
	*/
	public ServiceStatusResponse(){

	}
}
/** @} */
