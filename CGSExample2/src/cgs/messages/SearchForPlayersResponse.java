/**
 * @file SearchForPlayersResponse.java
 * @page classSearchForPlayersResponse SearchForPlayersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SearchForPlayersResponse: (players, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of players in the gluon system matching the identifiers 
 * requested
*/
@JsonTypeName("SearchForPlayersResponse")
public class SearchForPlayersResponse extends GluonResponse{

	/**
	 * A list of players that match the corresponding search criteria 
	 * (Optional)
	 */
	public List<DetailedPlayerInfo> players;

	/**
	 * Constructor for SearchForPlayersResponse that requires all members
	 * @param players
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SearchForPlayersResponse(List<DetailedPlayerInfo> players, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.players = players;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SearchForPlayersResponse without super class members
	 * @param players
	 */
	public SearchForPlayersResponse(List<DetailedPlayerInfo> players){
		this.players = players;

	}
	/**
	 * Constructor for SearchForPlayersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SearchForPlayersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SearchForPlayersResponse
	*/
	public SearchForPlayersResponse(){

	}
}
/** @} */
