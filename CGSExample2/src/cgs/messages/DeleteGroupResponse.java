/**
 * @file DeleteGroupResponse.java
 * @page classDeleteGroupResponse DeleteGroupResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupResponse: (groupName, groupType, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether the group was deleted 
 * successfully
*/
@JsonTypeName("DeleteGroupResponse")
public class DeleteGroupResponse extends GluonResponse{

	/**
	 * Name of the group that was deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Constructor for DeleteGroupResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteGroupResponse(String groupName, String groupType, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupResponse without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteGroupResponse(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}
	/**
	 * Constructor for DeleteGroupResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteGroupResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteGroupResponse
	*/
	public DeleteGroupResponse(){

	}
}
/** @} */
