/**
 * @file SetLeaderboardEntryRequest.java
 * @page classSetLeaderboardEntryRequest SetLeaderboardEntryRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetLeaderboardEntryRequest: (entry, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Add a new key/value entry to a leaderboard (where 'key' is the playerId for a 
 * player-based leaderboard), or update the value of an existing leaderboard 
 * entry
*/
@JsonTypeName("SetLeaderboardEntryRequest")
public class SetLeaderboardEntryRequest extends GluonRequest{

	/**
	 * Either the new entry to add to the leaderboard, or the existing entry 
	 * whose value will be updated
	 */
	public LeaderboardEntry entry;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetLeaderboardEntryResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetLeaderboardEntryResponseDelegate){
                ((SetLeaderboardEntryResponseDelegate)delegate).onSetLeaderboardEntryResponse(this, (SetLeaderboardEntryResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetLeaderboardEntryResponseDelegate){
            ((SetLeaderboardEntryResponseDelegate)delegate).onSetLeaderboardEntryResponse(this, new SetLeaderboardEntryResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetLeaderboardEntryRequest that requires all members
	 * @param entry
	 * @param passthrough
	 */
	public SetLeaderboardEntryRequest(LeaderboardEntry entry, String passthrough){
		this.entry = entry;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetLeaderboardEntryRequest without super class members
	 * @param entry
	 */
	public SetLeaderboardEntryRequest(LeaderboardEntry entry){
		this.entry = entry;

	}

	/**
	 * Default Constructor for SetLeaderboardEntryRequest
	*/
	public SetLeaderboardEntryRequest(){

	}
}
/** @} */
