/**
 * @file SetLeaderboardEntryAsAccumulationResponse.java
 * @page classSetLeaderboardEntryAsAccumulationResponse SetLeaderboardEntryAsAccumulationResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetLeaderboardEntryAsAccumulationResponse: (leaderboardName, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the entries in the Player-based leaderboard for all 
 * members of a certain Group have been summed up into a single entry of the 
 * Group-based leaderboard
*/
@JsonTypeName("SetLeaderboardEntryAsAccumulationResponse")
public class SetLeaderboardEntryAsAccumulationResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * Constructor for SetLeaderboardEntryAsAccumulationResponse that requires all members
	 * @param leaderboardName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetLeaderboardEntryAsAccumulationResponse(String leaderboardName, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.leaderboardName = leaderboardName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetLeaderboardEntryAsAccumulationResponse without super class members
	 * @param leaderboardName
	 */
	public SetLeaderboardEntryAsAccumulationResponse(String leaderboardName){
		this.leaderboardName = leaderboardName;

	}
	/**
	 * Constructor for SetLeaderboardEntryAsAccumulationResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetLeaderboardEntryAsAccumulationResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetLeaderboardEntryAsAccumulationResponse
	*/
	public SetLeaderboardEntryAsAccumulationResponse(){

	}
}
/** @} */
