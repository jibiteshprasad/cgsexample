/**
 * @file SentGroupInvite.java
 * @page classSentGroupInvite SentGroupInvite
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SentGroupInvite: (invitationId, recipientPlayerInfo, groupName, 
 * 		groupType, createDate)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * An invite that was sent by a group player (Retrieved by the group player)
 * 
*/
public class SentGroupInvite {

	/**
	 * Unique invitiation ID
	 */
	public String invitationId;

	/**
	 * The player who was invited
	 */
	public PlayerInfo recipientPlayerInfo;

	/**
	 * Name of the group associated to the invitation
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Date and time invite was originally sent (Optional)
	 */
	public Date createDate;

	/**
	 * Constructor for SentGroupInvite that requires all members
	 * @param invitationId
	 * @param recipientPlayerInfo
	 * @param groupName
	 * @param groupType
	 * @param createDate
	 */
	public SentGroupInvite(String invitationId, PlayerInfo recipientPlayerInfo, 
			String groupName, String groupType, Date createDate){
		this.invitationId = invitationId;
		this.recipientPlayerInfo = recipientPlayerInfo;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createDate = createDate;

	}

	/**
	 * Default Constructor for SentGroupInvite
	*/
	public SentGroupInvite(){

	}
}
/** @} */
