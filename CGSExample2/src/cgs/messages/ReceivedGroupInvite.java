/**
 * @file ReceivedGroupInvite.java
 * @page classReceivedGroupInvite ReceivedGroupInvite
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ReceivedGroupInvite: (invitationId, senderPlayerInfo, groupName, 
 * 		groupType, createDate)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A received group invite, which a member sent to a non-member player (Received 
 * by the non-member player)
*/
public class ReceivedGroupInvite {

	/**
	 * Unique Invitation ID	
	 */
	public String invitationId;

	/**
	 * The player who sent the invite   
	 */
	public PlayerInfo senderPlayerInfo;

	/**
	 * Name of the group associated to the invitation
	 */
	public String groupName;

	/**
	 * Name the group's type
	 */
	public String groupType;

	/**
	 * (Optional)
	 */
	public Date createDate;

	/**
	 * Constructor for ReceivedGroupInvite that requires all members
	 * @param invitationId
	 * @param senderPlayerInfo
	 * @param groupName
	 * @param groupType
	 * @param createDate
	 */
	public ReceivedGroupInvite(String invitationId, PlayerInfo senderPlayerInfo, 
			String groupName, String groupType, Date createDate){
		this.invitationId = invitationId;
		this.senderPlayerInfo = senderPlayerInfo;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createDate = createDate;

	}

	/**
	 * Default Constructor for ReceivedGroupInvite
	*/
	public ReceivedGroupInvite(){

	}
}
/** @} */
