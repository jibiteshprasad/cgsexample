/**
 * @file GetAttributesRequest.java
 * @page classGetAttributesRequest GetAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAttributesRequest: (playerId, attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves multiple attributes for a specified player
 * 
*/
@JsonTypeName("GetAttributesRequest")
public class GetAttributesRequest extends GluonRequest{

	/**
	 * ID of the player whose attributes will be received
	 */
	public String playerId;

	/**
	 * A list of attribute names of the attributes to be received
	 */
	public List<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetAttributesResponseDelegate){
                ((GetAttributesResponseDelegate)delegate).onGetAttributesResponse(this, (GetAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetAttributesResponseDelegate){
            ((GetAttributesResponseDelegate)delegate).onGetAttributesResponse(this, new GetAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetAttributesRequest that requires all members
	 * @param playerId
	 * @param attributeNames
	 * @param passthrough
	 */
	public GetAttributesRequest(String playerId, List<String> attributeNames, 
			String passthrough){
		this.playerId = playerId;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAttributesRequest without super class members
	 * @param playerId
	 * @param attributeNames
	 */
	public GetAttributesRequest(String playerId, List<String> attributeNames){
		this.playerId = playerId;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for GetAttributesRequest
	*/
	public GetAttributesRequest(){

	}
}
/** @} */
