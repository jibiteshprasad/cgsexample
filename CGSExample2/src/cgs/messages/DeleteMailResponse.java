/**
 * @file DeleteMailResponse.java
 * @page classDeleteMailResponse DeleteMailResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteMailResponse: (id, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether a mail message was 
 * deleted
*/
@JsonTypeName("DeleteMailMessageResponse")
public class DeleteMailResponse extends GluonResponse{

	/**
	 * Unique mesaage ID of mail that was deleted
	 */
	public String id;

	/**
	 * Constructor for DeleteMailResponse that requires all members
	 * @param id
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteMailResponse(String id, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.id = id;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteMailResponse without super class members
	 * @param id
	 */
	public DeleteMailResponse(String id){
		this.id = id;

	}
	/**
	 * Constructor for DeleteMailResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteMailResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteMailResponse
	*/
	public DeleteMailResponse(){

	}
}
/** @} */
