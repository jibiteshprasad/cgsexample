/**
 * @file ChangeGroupOwnerRequest.java
 * @page classChangeGroupOwnerRequest ChangeGroupOwnerRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ChangeGroupOwnerRequest: (groupName, groupType, playerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Changes the owner of the specified group to the player designated in the 
 * message
*/
@JsonTypeName("ChangeGroupOwnerRequest")
public class ChangeGroupOwnerRequest extends GluonRequest{

	/**
	 * Name of the group that will have its owner changed
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * ID of the player who will be the new owner
	 */
	public String playerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ChangeGroupOwnerResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ChangeGroupOwnerResponseDelegate){
                ((ChangeGroupOwnerResponseDelegate)delegate).onChangeGroupOwnerResponse(this, (ChangeGroupOwnerResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ChangeGroupOwnerResponseDelegate){
            ((ChangeGroupOwnerResponseDelegate)delegate).onChangeGroupOwnerResponse(this, new ChangeGroupOwnerResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ChangeGroupOwnerRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerId
	 * @param passthrough
	 */
	public ChangeGroupOwnerRequest(String groupName, String groupType, 
			String playerId, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ChangeGroupOwnerRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerId
	 */
	public ChangeGroupOwnerRequest(String groupName, String groupType, 
			String playerId){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for ChangeGroupOwnerRequest
	*/
	public ChangeGroupOwnerRequest(){

	}
}
/** @} */
