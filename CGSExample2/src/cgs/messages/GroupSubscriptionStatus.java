package cgs.messages;

public enum GroupSubscriptionStatus
	{
	/// You have successfully subscribed to this group
	SUBSCRIBED,
	/// You could not subscribe to this group
	NOT_SUBSCRIBED,
	/// You are unsubscribed from this group
	UNSUBSCRIBED,
	/// You cannot unsubscribe from this gruop
	CANNOT_UNSUBSCRIBE
	
}
