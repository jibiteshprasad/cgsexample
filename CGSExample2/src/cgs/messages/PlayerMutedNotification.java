/**
 * @file PlayerMutedNotification.java
 * @page classPlayerMutedNotification PlayerMutedNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class PlayerMutedNotification: (playerInfo, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Notification from the server that the player has been muted by the admin
 * 
*/
@JsonTypeName("PlayerMuted")
public class PlayerMutedNotification extends GluonResponse{

	/**
	 * The player who was muted (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for PlayerMutedNotification that requires all members
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public PlayerMutedNotification(PlayerInfo playerInfo, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for PlayerMutedNotification without super class members
	 * @param playerInfo
	 */
	public PlayerMutedNotification(PlayerInfo playerInfo){
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for PlayerMutedNotification with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public PlayerMutedNotification(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for PlayerMutedNotification
	*/
	public PlayerMutedNotification(){

	}
}
/** @} */
