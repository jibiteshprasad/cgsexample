/**
 * @file LeaveGroupRequest.java
 * @page classLeaveGroupRequest LeaveGroupRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LeaveGroupRequest: (groupName, groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a request for the authenticated player to leave the specified group
 * 
*/
@JsonTypeName("LeaveGroupRequest")
public class LeaveGroupRequest extends GluonRequest{

	/**
	 * Name of the group that will be left
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(LeaveGroupResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof LeaveGroupResponseDelegate){
                ((LeaveGroupResponseDelegate)delegate).onLeaveGroupResponse(this, (LeaveGroupResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof LeaveGroupResponseDelegate){
            ((LeaveGroupResponseDelegate)delegate).onLeaveGroupResponse(this, new LeaveGroupResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for LeaveGroupRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public LeaveGroupRequest(String groupName, String groupType, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for LeaveGroupRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public LeaveGroupRequest(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for LeaveGroupRequest
	*/
	public LeaveGroupRequest(){

	}
}
/** @} */
