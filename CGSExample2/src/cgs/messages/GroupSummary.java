/**
 * @file GroupSummary.java
 * @page classGroupSummary GroupSummary
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupSummary: (name, ownerPlayerInfo, type, groupSize, isOpen, 
 * 		attributes, members)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Detailed information about a group
 * 
*/
public class GroupSummary {

	/**
	 * Name of the group (Optional)
	 */
	public String name;

	/**
	 * The player who is the owner of the group (Optional)
	 */
	public PlayerInfo ownerPlayerInfo;

	/**
	 * Name of the group's type (Optional)
	 */
	public String type;

	/**
	 * Current size of the group (Optional)
	 */
	public int groupSize;

	/**
	 * Flag to determine whether the group is open to the public (Optional)
	 */
	public boolean isOpen;

	/**
	 * All the attributes associated to the group (Optional)
	 */
	public List<GroupAttribute> attributes;

	/**
	 * A list of group members (Optional)
	 */
	public Set<PlayerInfo> members;

	/**
	 * Constructor for GroupSummary that requires all members
	 * @param name
	 * @param ownerPlayerInfo
	 * @param type
	 * @param groupSize
	 * @param isOpen
	 * @param attributes
	 * @param members
	 */
	public GroupSummary(String name, PlayerInfo ownerPlayerInfo, String type, 
			int groupSize, boolean isOpen, List<GroupAttribute> attributes, 
			Set<PlayerInfo> members){
		this.name = name;
		this.ownerPlayerInfo = ownerPlayerInfo;
		this.type = type;
		this.groupSize = groupSize;
		this.isOpen = isOpen;
		this.attributes = attributes;
		this.members = members;

	}

	/**
	 * Default Constructor for GroupSummary
	*/
	public GroupSummary(){

	}
}
/** @} */
