/**
 * @file RetrieveProxyRequest.java
 * @page classRetrieveProxyRequest RetrieveProxyRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RetrieveProxyRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve IP/port information for a proxy server used for multiplayer 
 * communication
*/
@JsonTypeName("")
public class RetrieveProxyRequest extends GluonRequest{

	/**
	 * Constructor for RetrieveProxyRequest that requires all members
	 * @param passthrough
	 */
	public RetrieveProxyRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RetrieveProxyRequest without super class members
	 */
	public RetrieveProxyRequest(){

	}
}
/** @} */
