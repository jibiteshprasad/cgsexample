/**
 * @file SetMyGroupMemberAttributesRequest.java
 * @page classSetMyGroupMemberAttributesRequest SetMyGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetMyGroupMemberAttributesRequest: (groupName, groupType, attributes, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets group member attributes, which are specified in the attributes list, for 
 * the authenticated player
*/
@JsonTypeName("SetMyGroupMemberAttributesRequest")
public class SetMyGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where member attributes will be set
	 */
	public String groupName;

	/**
	 * Name of the group's type  (Optional)
	 */
	public String groupType;

	/**
	 * List of player attributes (both name and value required) that will be 
	 * set
	 */
	public ArrayList<PlayerAttribute> attributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetMyGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetMyGroupMemberAttributesResponseDelegate){
                ((SetMyGroupMemberAttributesResponseDelegate)delegate).onSetMyGroupMemberAttributesResponse(this, (SetMyGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetMyGroupMemberAttributesResponseDelegate){
            ((SetMyGroupMemberAttributesResponseDelegate)delegate).onSetMyGroupMemberAttributesResponse(this, new SetMyGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetMyGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param passthrough
	 */
	public SetMyGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<PlayerAttribute> attributes, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetMyGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 */
	public SetMyGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<PlayerAttribute> attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for SetMyGroupMemberAttributesRequest
	*/
	public SetMyGroupMemberAttributesRequest(){

	}
}
/** @} */
