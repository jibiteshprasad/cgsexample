/**
 * @file ValidateIapReceiptResponse.java
 * @page classValidateIapReceiptResponse ValidateIapReceiptResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ValidateIapReceiptResponse: (playerId, wasValid, wasConfirmed, 
 * 		transactionId, status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response from validating an IAP (in-app purchase) receipt.
 * 
*/
@JsonTypeName("ValidateIapReceiptResponse")
public class ValidateIapReceiptResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public String playerId;

	/**
	 * Set to true if the receipt is valid (Optional)
	 */
	public boolean wasValid;

	/**
	 * Set to true if the receipt was already consumed (Optional)
	 */
	public boolean wasConfirmed;

	/**
	 * The transaction identifier to use for consumption of this receipt 
	 * (Optional)
	 */
	public String transactionId;

	/**
	 * Constructor for ValidateIapReceiptResponse that requires all members
	 * @param playerId
	 * @param wasValid
	 * @param wasConfirmed
	 * @param transactionId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ValidateIapReceiptResponse(String playerId, boolean wasValid, 
			boolean wasConfirmed, String transactionId, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerId = playerId;
		this.wasValid = wasValid;
		this.wasConfirmed = wasConfirmed;
		this.transactionId = transactionId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ValidateIapReceiptResponse without super class members
	 * @param playerId
	 * @param wasValid
	 * @param wasConfirmed
	 * @param transactionId
	 */
	public ValidateIapReceiptResponse(String playerId, boolean wasValid, 
			boolean wasConfirmed, String transactionId){
		this.playerId = playerId;
		this.wasValid = wasValid;
		this.wasConfirmed = wasConfirmed;
		this.transactionId = transactionId;

	}
	/**
	 * Constructor for ValidateIapReceiptResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ValidateIapReceiptResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ValidateIapReceiptResponse
	*/
	public ValidateIapReceiptResponse(){

	}
}
/** @} */
