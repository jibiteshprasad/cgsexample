/**
 * @file GetInboxStatusRequest.java
 * @page classGetInboxStatusRequest GetInboxStatusRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetInboxStatusRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves the current status of authenticated player's inbox
 * 
*/
@JsonTypeName("GetInboxStatusRequest")
public class GetInboxStatusRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetInboxStatusResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetInboxStatusResponseDelegate){
                ((GetInboxStatusResponseDelegate)delegate).onGetInboxStatusResponse(this, (GetInboxStatusResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetInboxStatusResponseDelegate){
            ((GetInboxStatusResponseDelegate)delegate).onGetInboxStatusResponse(this, new GetInboxStatusResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetInboxStatusRequest that requires all members
	 * @param passthrough
	 */
	public GetInboxStatusRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetInboxStatusRequest without super class members
	 */
	public GetInboxStatusRequest(){

	}
}
/** @} */
