/**
 * @file SetGroupAttributesResponse.java
 * @page classSetGroupAttributesResponse SetGroupAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupAttributesResponse: (groupName, groupType, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether attributes were set 
 * properly
*/
@JsonTypeName("SetGroupAttributesResponse")
public class SetGroupAttributesResponse extends GluonResponse{

	/**
	 * Name of the group whose attributes were set
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Constructor for SetGroupAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetGroupAttributesResponse(String groupName, String groupType, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 */
	public SetGroupAttributesResponse(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}
	/**
	 * Constructor for SetGroupAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetGroupAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetGroupAttributesResponse
	*/
	public SetGroupAttributesResponse(){

	}
}
/** @} */
