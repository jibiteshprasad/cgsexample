/**
 * @file DeleteGroupMemberAttributesForAllMembersRequest.java
 * @page classDeleteGroupMemberAttributesForAllMembersRequest DeleteGroupMemberAttributesForAllMembersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupMemberAttributesForAllMembersRequest: (groupName, groupType, 
 * 		attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes group member attributes, which are specified in the attributeNames 
 * list, for ALL members who are in the specified group
*/
@JsonTypeName("DeleteGroupMemberAttributesForAllMembersRequest")
public class DeleteGroupMemberAttributesForAllMembersRequest extends GluonRequest{

	/**
	 * Name of the group where group attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of attribute names for the attributes that will be deleted
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteGroupMemberAttributesForAllMembersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteGroupMemberAttributesForAllMembersResponseDelegate){
                ((DeleteGroupMemberAttributesForAllMembersResponseDelegate)delegate).onDeleteGroupMemberAttributesForAllMembersResponse(this, (DeleteGroupMemberAttributesForAllMembersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteGroupMemberAttributesForAllMembersResponseDelegate){
            ((DeleteGroupMemberAttributesForAllMembersResponseDelegate)delegate).onDeleteGroupMemberAttributesForAllMembersResponse(this, new DeleteGroupMemberAttributesForAllMembersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteGroupMemberAttributesForAllMembersRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param passthrough
	 */
	public DeleteGroupMemberAttributesForAllMembersRequest(String groupName, 
			String groupType, ArrayList<String> attributeNames, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupMemberAttributesForAllMembersRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 */
	public DeleteGroupMemberAttributesForAllMembersRequest(String groupName, 
			String groupType, ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for DeleteGroupMemberAttributesForAllMembersRequest
	*/
	public DeleteGroupMemberAttributesForAllMembersRequest(){

	}
}
/** @} */
