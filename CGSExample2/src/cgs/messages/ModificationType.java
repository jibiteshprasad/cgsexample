package cgs.messages;

public enum ModificationType
	{
	/// Name of the group has been changed.
	NAME_CHANGED,
	/// An attribute of the group has been changed.
	ATTRIBUTE_CHANGED,
	/// The current status of the group has changed (open/closed). Only currentValue will be set indicating new status.
	STATE_CHANGED,
	/// List of members for the group has changed. Only currentValue will be set indicating which member was added/removed.
	MEMBER_LIST_CHANGED
	
}
