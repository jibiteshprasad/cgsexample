/**
 * @file GetAllGroupAttributesRequest.java
 * @page classGetAllGroupAttributesRequest GetAllGroupAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllGroupAttributesRequest: (groupName, groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all the group attributes for a specified group
 * 
*/
@JsonTypeName("GetAllGroupAttributesRequest")
public class GetAllGroupAttributesRequest extends GluonRequest{

	/**
	 * Name of the specified group
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetAllGroupAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetAllGroupAttributesResponseDelegate){
                ((GetAllGroupAttributesResponseDelegate)delegate).onGetAllGroupAttributesResponse(this, (GetAllGroupAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetAllGroupAttributesResponseDelegate){
            ((GetAllGroupAttributesResponseDelegate)delegate).onGetAllGroupAttributesResponse(this, new GetAllGroupAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetAllGroupAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public GetAllGroupAttributesRequest(String groupName, String groupType, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllGroupAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public GetAllGroupAttributesRequest(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetAllGroupAttributesRequest
	*/
	public GetAllGroupAttributesRequest(){

	}
}
/** @} */
