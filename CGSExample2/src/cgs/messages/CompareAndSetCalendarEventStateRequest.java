/**
 * @file CompareAndSetCalendarEventStateRequest.java
 * @page classCompareAndSetCalendarEventStateRequest CompareAndSetCalendarEventStateRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CompareAndSetCalendarEventStateRequest: (calendarName, eventName, 
 * 		expectedState, newState, userName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Compares and sets a calendar event state if the comparison is valid.
*/
@JsonTypeName("CompareAndSetCalendarEventStateRequest")
public class CompareAndSetCalendarEventStateRequest extends GluonRequest{

	/**
	 *
	 */
	public String calendarName;

	/**
	 *
	 */
	public String eventName;

	/**
	 *
	 */
	public CalendarEventState expectedState;

	/**
	 *
	 */
	public CalendarEventState newState;

	/**
	 *
	 */
	public String userName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CompareAndSetCalendarEventStateResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CompareAndSetCalendarEventStateResponseDelegate){
                ((CompareAndSetCalendarEventStateResponseDelegate)delegate).onCompareAndSetCalendarEventStateResponse(this, (CompareAndSetCalendarEventStateResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CompareAndSetCalendarEventStateResponseDelegate){
            ((CompareAndSetCalendarEventStateResponseDelegate)delegate).onCompareAndSetCalendarEventStateResponse(this, new CompareAndSetCalendarEventStateResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CompareAndSetCalendarEventStateRequest that requires all members
	 * @param calendarName
	 * @param eventName
	 * @param expectedState
	 * @param newState
	 * @param userName
	 * @param passthrough
	 */
	public CompareAndSetCalendarEventStateRequest(String calendarName, 
			String eventName, CalendarEventState expectedState, 
			CalendarEventState newState, String userName, String passthrough){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.expectedState = expectedState;
		this.newState = newState;
		this.userName = userName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CompareAndSetCalendarEventStateRequest without super class members
	 * @param calendarName
	 * @param eventName
	 * @param expectedState
	 * @param newState
	 * @param userName
	 */
	public CompareAndSetCalendarEventStateRequest(String calendarName, 
			String eventName, CalendarEventState expectedState, 
			CalendarEventState newState, String userName){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.expectedState = expectedState;
		this.newState = newState;
		this.userName = userName;

	}

	/**
	 * Default Constructor for CompareAndSetCalendarEventStateRequest
	*/
	public CompareAndSetCalendarEventStateRequest(){

	}
}
/** @} */
