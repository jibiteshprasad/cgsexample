/**
 * @file ApplyCurrencyDeltaResponse.java
 * @page classApplyCurrencyDeltaResponse ApplyCurrencyDeltaResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApplyCurrencyDeltaResponse: (playerInfo, currency, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to applying a delta to a currency in the player's wallet.
 * 
*/
@JsonTypeName("ApplyCurrencyDeltaResponse")
public class ApplyCurrencyDeltaResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * The current currency state
	 */
	public Currency currency;

	/**
	 * Constructor for ApplyCurrencyDeltaResponse that requires all members
	 * @param playerInfo
	 * @param currency
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ApplyCurrencyDeltaResponse(PlayerInfo playerInfo, Currency currency, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.currency = currency;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApplyCurrencyDeltaResponse without super class members
	 * @param playerInfo
	 * @param currency
	 */
	public ApplyCurrencyDeltaResponse(PlayerInfo playerInfo, Currency currency){
		this.playerInfo = playerInfo;
		this.currency = currency;

	}
	/**
	 * Constructor for ApplyCurrencyDeltaResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ApplyCurrencyDeltaResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ApplyCurrencyDeltaResponse
	*/
	public ApplyCurrencyDeltaResponse(){

	}
}
/** @} */
