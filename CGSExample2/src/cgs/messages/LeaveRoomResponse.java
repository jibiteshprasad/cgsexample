/**
 * @file LeaveRoomResponse.java
 * @page classLeaveRoomResponse LeaveRoomResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LeaveRoomResponse: (roomName, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message confirming that the player has left the chat room 
 * successfully
*/
@JsonTypeName("LeftRoom")
public class LeaveRoomResponse extends GluonResponse{

	/**
	 * Name of the room that was left
	 */
	public String roomName;

	/**
	 * Constructor for LeaveRoomResponse that requires all members
	 * @param roomName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public LeaveRoomResponse(String roomName, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.roomName = roomName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for LeaveRoomResponse without super class members
	 * @param roomName
	 */
	public LeaveRoomResponse(String roomName){
		this.roomName = roomName;

	}
	/**
	 * Constructor for LeaveRoomResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public LeaveRoomResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for LeaveRoomResponse
	*/
	public LeaveRoomResponse(){

	}
}
/** @} */
