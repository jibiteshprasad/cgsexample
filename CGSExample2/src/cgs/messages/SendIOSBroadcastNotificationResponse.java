/**
 * @file SendIOSBroadcastNotificationResponse.java
 * @page classSendIOSBroadcastNotificationResponse SendIOSBroadcastNotificationResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendIOSBroadcastNotificationResponse: (status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to send IOS broadcast notifications.
*/
@JsonTypeName("SendIOSBroadcastNotificationResponse")
public class SendIOSBroadcastNotificationResponse extends GluonResponse{

	/**
	 * Constructor for SendIOSBroadcastNotificationResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendIOSBroadcastNotificationResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendIOSBroadcastNotificationResponse without super class members
	 */
	public SendIOSBroadcastNotificationResponse(){

	}
	/**
	 * Constructor for SendIOSBroadcastNotificationResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendIOSBroadcastNotificationResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
