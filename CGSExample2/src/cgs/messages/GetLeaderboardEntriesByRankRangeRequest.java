/**
 * @file GetLeaderboardEntriesByRankRangeRequest.java
 * @page classGetLeaderboardEntriesByRankRangeRequest GetLeaderboardEntriesByRankRangeRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetLeaderboardEntriesByRankRangeRequest: (leaderboardName, minRank, 
 * 		maxRank, type, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve entries (name, value, and rank) whose rank is within the min/max 
 * range provided. 1-10 for instance could be the front page of a leaderboard - 
 * top 10 entries based on the value. 
*/
@JsonTypeName("GetLeaderboardEntriesByRankRangeRequest")
public class GetLeaderboardEntriesByRankRangeRequest extends GluonRequest{

	/**
	 * The name of the leaderboard from which the entries will be retrieved
	 */
	public String leaderboardName;

	/**
	 * The lower end of the rank range - 1 in the top 10 example (must be 
	 * greater than 0) (Optional)
	 */
	public long minRank;

	/**
	 * The upper end of the rank range - 10 in the top 10 example (must be 
	 * greater than minRank) (Optional)
	 */
	public long maxRank;

	/**
	 * The Entity Type PLAYER or GROUP (Optional)
	 */
	public EntityType type;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetLeaderboardEntriesByRankRangeResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetLeaderboardEntriesByRankRangeResponseDelegate){
                ((GetLeaderboardEntriesByRankRangeResponseDelegate)delegate).onGetLeaderboardEntriesByRankRangeResponse(this, (GetLeaderboardEntriesByRankRangeResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetLeaderboardEntriesByRankRangeResponseDelegate){
            ((GetLeaderboardEntriesByRankRangeResponseDelegate)delegate).onGetLeaderboardEntriesByRankRangeResponse(this, new GetLeaderboardEntriesByRankRangeResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetLeaderboardEntriesByRankRangeRequest that requires all members
	 * @param leaderboardName
	 * @param minRank
	 * @param maxRank
	 * @param type
	 * @param passthrough
	 */
	public GetLeaderboardEntriesByRankRangeRequest(String leaderboardName, 
			long minRank, long maxRank, EntityType type, String passthrough){
		this.leaderboardName = leaderboardName;
		this.minRank = minRank;
		this.maxRank = maxRank;
		this.type = type;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetLeaderboardEntriesByRankRangeRequest without super class members
	 * @param leaderboardName
	 * @param minRank
	 * @param maxRank
	 * @param type
	 */
	public GetLeaderboardEntriesByRankRangeRequest(String leaderboardName, 
			long minRank, long maxRank, EntityType type){
		this.leaderboardName = leaderboardName;
		this.minRank = minRank;
		this.maxRank = maxRank;
		this.type = type;

	}

	/**
	 * Default Constructor for GetLeaderboardEntriesByRankRangeRequest
	*/
	public GetLeaderboardEntriesByRankRangeRequest(){

	}
}
/** @} */
