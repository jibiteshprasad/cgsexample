/**
 * @file DeleteDocumentByIdResponse.java
 * @page classDeleteDocumentByIdResponse DeleteDocumentByIdResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteDocumentByIdResponse: (status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the document was deleted.
 * 
*/
@JsonTypeName("DeleteDocumentByIdResponse")
public class DeleteDocumentByIdResponse extends GluonResponse{

	/**
	 * Constructor for DeleteDocumentByIdResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteDocumentByIdResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteDocumentByIdResponse without super class members
	 */
	public DeleteDocumentByIdResponse(){

	}
	/**
	 * Constructor for DeleteDocumentByIdResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteDocumentByIdResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
