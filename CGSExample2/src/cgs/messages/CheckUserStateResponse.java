/**
 * @file CheckUserStateResponse.java
 * @page classCheckUserStateResponse CheckUserStateResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CheckUserStateResponse: (playerId, time, isConnected, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A message from a service to get user's state.
*/
@JsonTypeName("CheckUserStateResponse")
public class CheckUserStateResponse extends GluonResponse{

	/**
	 *
	 */
	public String playerId;

	/**
	 * (Optional)
	 */
	public long time;

	/**
	 * (Optional)
	 */
	public boolean isConnected;

	/**
	 * Constructor for CheckUserStateResponse that requires all members
	 * @param playerId
	 * @param time
	 * @param isConnected
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CheckUserStateResponse(String playerId, long time, 
			boolean isConnected, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.playerId = playerId;
		this.time = time;
		this.isConnected = isConnected;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CheckUserStateResponse without super class members
	 * @param playerId
	 * @param time
	 * @param isConnected
	 */
	public CheckUserStateResponse(String playerId, long time, 
			boolean isConnected){
		this.playerId = playerId;
		this.time = time;
		this.isConnected = isConnected;

	}
	/**
	 * Constructor for CheckUserStateResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CheckUserStateResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CheckUserStateResponse
	*/
	public CheckUserStateResponse(){

	}
}
/** @} */
