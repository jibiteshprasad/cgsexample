/**
 * @file ReceivePrivateMessageNotification.java
 * @page classReceivePrivateMessageNotification ReceivePrivateMessageNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ReceivePrivateMessageNotification: (fromPlayerInfo, message, timestamp, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returned to an authenticated player when a private message is received from 
 * another player.  If the recipient is not connected, the private message will 
 * not be received.
*/
@JsonTypeName("ReceivePrivateMessage")
public class ReceivePrivateMessageNotification extends GluonRequest{

	/**
	 * Player that sent the message (Optional)
	 */
	public PlayerInfo fromPlayerInfo;

	/**
	 * Message that was sent to the player (Optional)
	 */
	public String message;

	/**
	 * Date and Time from when this message was originally sent (Optional)
	 */
	public long timestamp;

	/**
	 * Constructor for ReceivePrivateMessageNotification that requires all members
	 * @param fromPlayerInfo
	 * @param message
	 * @param timestamp
	 * @param passthrough
	 */
	public ReceivePrivateMessageNotification(PlayerInfo fromPlayerInfo, 
			String message, long timestamp, String passthrough){
		this.fromPlayerInfo = fromPlayerInfo;
		this.message = message;
		this.timestamp = timestamp;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ReceivePrivateMessageNotification without super class members
	 * @param fromPlayerInfo
	 * @param message
	 * @param timestamp
	 */
	public ReceivePrivateMessageNotification(PlayerInfo fromPlayerInfo, 
			String message, long timestamp){
		this.fromPlayerInfo = fromPlayerInfo;
		this.message = message;
		this.timestamp = timestamp;

	}

	/**
	 * Default Constructor for ReceivePrivateMessageNotification
	*/
	public ReceivePrivateMessageNotification(){

	}
}
/** @} */
