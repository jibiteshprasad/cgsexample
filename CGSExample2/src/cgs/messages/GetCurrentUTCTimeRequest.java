/**
 * @file GetCurrentUTCTimeRequest.java
 * @page classGetCurrentUTCTimeRequest GetCurrentUTCTimeRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCurrentUTCTimeRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Get the current UTC time from the Server.
 * 
*/
@JsonTypeName("GetCurrentUTCTimeRequest")
public class GetCurrentUTCTimeRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCurrentUTCTimeResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCurrentUTCTimeResponseDelegate){
                ((GetCurrentUTCTimeResponseDelegate)delegate).onGetCurrentUTCTimeResponse(this, (GetCurrentUTCTimeResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCurrentUTCTimeResponseDelegate){
            ((GetCurrentUTCTimeResponseDelegate)delegate).onGetCurrentUTCTimeResponse(this, new GetCurrentUTCTimeResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCurrentUTCTimeRequest that requires all members
	 * @param passthrough
	 */
	public GetCurrentUTCTimeRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCurrentUTCTimeRequest without super class members
	 */
	public GetCurrentUTCTimeRequest(){

	}
}
/** @} */
