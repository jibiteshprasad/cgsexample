/**
 * @file UnsubscribeFromGroupNotificationsResponse.java
 * @page classUnsubscribeFromGroupNotificationsResponse UnsubscribeFromGroupNotificationsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class UnsubscribeFromGroupNotificationsResponse: (groupsWithStatus, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns list of specified groups with status indicating whether or not you 
 * were properly unsubscribed to them.
*/
@JsonTypeName("UnsubscribeFromGroupNotificationsResponse")
public class UnsubscribeFromGroupNotificationsResponse extends GluonResponse{

	/**
	 * Collection of groups and their subscription status (UNSUBSCRIBED or 
	 * CANNOT_UNSUBSCRIBE) (Optional)
	 */
	public List<GroupDescriptorWithStatus> groupsWithStatus;

	/**
	 * Constructor for UnsubscribeFromGroupNotificationsResponse that requires all members
	 * @param groupsWithStatus
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public UnsubscribeFromGroupNotificationsResponse(List<GroupDescriptorWithStatus> groupsWithStatus, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.groupsWithStatus = groupsWithStatus;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for UnsubscribeFromGroupNotificationsResponse without super class members
	 * @param groupsWithStatus
	 */
	public UnsubscribeFromGroupNotificationsResponse(List<GroupDescriptorWithStatus> groupsWithStatus){
		this.groupsWithStatus = groupsWithStatus;

	}
	/**
	 * Constructor for UnsubscribeFromGroupNotificationsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public UnsubscribeFromGroupNotificationsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for UnsubscribeFromGroupNotificationsResponse
	*/
	public UnsubscribeFromGroupNotificationsResponse(){

	}
}
/** @} */
