/**
 * @file DeleteMyGroupMemberAttributesRequest.java
 * @page classDeleteMyGroupMemberAttributesRequest DeleteMyGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteMyGroupMemberAttributesRequest: (groupName, groupType, 
 * 		attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes group member attributes, which are specified in a list, for the 
 * authenticated player
*/
@JsonTypeName("DeleteMyGroupMemberAttributesRequest")
public class DeleteMyGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where group attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of attribute names for the attributes that will be deleted
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteMyGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteMyGroupMemberAttributesResponseDelegate){
                ((DeleteMyGroupMemberAttributesResponseDelegate)delegate).onDeleteMyGroupMemberAttributesResponse(this, (DeleteMyGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteMyGroupMemberAttributesResponseDelegate){
            ((DeleteMyGroupMemberAttributesResponseDelegate)delegate).onDeleteMyGroupMemberAttributesResponse(this, new DeleteMyGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteMyGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param passthrough
	 */
	public DeleteMyGroupMemberAttributesRequest(String groupName, 
			String groupType, ArrayList<String> attributeNames, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteMyGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 */
	public DeleteMyGroupMemberAttributesRequest(String groupName, 
			String groupType, ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for DeleteMyGroupMemberAttributesRequest
	*/
	public DeleteMyGroupMemberAttributesRequest(){

	}
}
/** @} */
