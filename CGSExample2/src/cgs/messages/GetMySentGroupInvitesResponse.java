/**
 * @file GetMySentGroupInvitesResponse.java
 * @page classGetMySentGroupInvitesResponse GetMySentGroupInvitesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMySentGroupInvitesResponse: (sentGroupInvites, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of all invites that the authenticated player sent out
 * 
*/
@JsonTypeName("GetMySentGroupInvitesResponse")
public class GetMySentGroupInvitesResponse extends GluonResponse{

	/**
	 * A list of invites that a group player sent (Optional)
	 */
	public List<SentGroupInvite> sentGroupInvites;

	/**
	 * Constructor for GetMySentGroupInvitesResponse that requires all members
	 * @param sentGroupInvites
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMySentGroupInvitesResponse(List<SentGroupInvite> sentGroupInvites, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.sentGroupInvites = sentGroupInvites;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMySentGroupInvitesResponse without super class members
	 * @param sentGroupInvites
	 */
	public GetMySentGroupInvitesResponse(List<SentGroupInvite> sentGroupInvites){
		this.sentGroupInvites = sentGroupInvites;

	}
	/**
	 * Constructor for GetMySentGroupInvitesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMySentGroupInvitesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMySentGroupInvitesResponse
	*/
	public GetMySentGroupInvitesResponse(){

	}
}
/** @} */
