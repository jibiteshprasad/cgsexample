/**
 * @file ListPlayersAndAttributesByCredentialRequest.java
 * @page classListPlayersAndAttributesByCredentialRequest ListPlayersAndAttributesByCredentialRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ListPlayersAndAttributesByCredentialRequest: (loginId, accountType, 
 * 		attributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves attributes for each player associated with the specified 
 * credential.  * This is meant to be used when a Facebook account has been 
 * attached on multiple devices -  the intention here is to retrieve game 
 * information that the user has saved on each device, where each  is associated 
 * with a different 'player' (identified by OpenUDID credential e.g.) Then the 
 * user can choose  which player to keep (using the SetAsPrimaryPlayer message). 
 * (See AttachFacebookCredentialRequest description for further details on this 
 * flow.)
*/
@JsonTypeName("ListPlayersAndAttributesByCredentialRequest")
public class ListPlayersAndAttributesByCredentialRequest extends GluonRequest{

	/**
	 * Currently, this should be the Facebook userID (in future iterations 
	 * there may be other kinds of credentials that can get attached to a player 
	 * on multiple devices).
	 */
	public String loginId;

	/**
	 * Currently, this should be FACEBOOK (in future iterations there may be 
	 * other kinds of credentials that can get attached to a player on multiple 
	 * devices).
	 */
	public String accountType;

	/**
	 * The name of each attribute to retrieve for each player.  (Optional)
	 */
	public List<String> attributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ListPlayersAndAttributesByCredentialResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ListPlayersAndAttributesByCredentialResponseDelegate){
                ((ListPlayersAndAttributesByCredentialResponseDelegate)delegate).onListPlayersAndAttributesByCredentialResponse(this, (ListPlayersAndAttributesByCredentialResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ListPlayersAndAttributesByCredentialResponseDelegate){
            ((ListPlayersAndAttributesByCredentialResponseDelegate)delegate).onListPlayersAndAttributesByCredentialResponse(this, new ListPlayersAndAttributesByCredentialResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ListPlayersAndAttributesByCredentialRequest that requires all members
	 * @param loginId
	 * @param accountType
	 * @param attributes
	 * @param passthrough
	 */
	public ListPlayersAndAttributesByCredentialRequest(String loginId, 
			String accountType, List<String> attributes, String passthrough){
		this.loginId = loginId;
		this.accountType = accountType;
		this.attributes = attributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ListPlayersAndAttributesByCredentialRequest without super class members
	 * @param loginId
	 * @param accountType
	 * @param attributes
	 */
	public ListPlayersAndAttributesByCredentialRequest(String loginId, 
			String accountType, List<String> attributes){
		this.loginId = loginId;
		this.accountType = accountType;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for ListPlayersAndAttributesByCredentialRequest
	*/
	public ListPlayersAndAttributesByCredentialRequest(){

	}
}
/** @} */
