/**
 * @file SetLeaderboardEntryResponse.java
 * @page classSetLeaderboardEntryResponse SetLeaderboardEntryResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetLeaderboardEntryResponse: (leaderboardName, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the leaderboard entry has been added or updated.
 * 
*/
@JsonTypeName("SetLeaderboardEntryResponse")
public class SetLeaderboardEntryResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * Constructor for SetLeaderboardEntryResponse that requires all members
	 * @param leaderboardName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetLeaderboardEntryResponse(String leaderboardName, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.leaderboardName = leaderboardName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetLeaderboardEntryResponse without super class members
	 * @param leaderboardName
	 */
	public SetLeaderboardEntryResponse(String leaderboardName){
		this.leaderboardName = leaderboardName;

	}
	/**
	 * Constructor for SetLeaderboardEntryResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetLeaderboardEntryResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetLeaderboardEntryResponse
	*/
	public SetLeaderboardEntryResponse(){

	}
}
/** @} */
