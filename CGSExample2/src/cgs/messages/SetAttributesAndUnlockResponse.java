/**
 * @file SetAttributesAndUnlockResponse.java
 * @page classSetAttributesAndUnlockResponse SetAttributesAndUnlockResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAttributesAndUnlockResponse: (playerAndAttributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, returns a list of players and their attributes that 
 * have been successfully set and unlocked.
*/
@JsonTypeName("SetAttributesAndUnlockResponse")
public class SetAttributesAndUnlockResponse extends GluonResponse{

	/**
	 * List of players and their attributes that have been successfully set and 
	 * unlocked (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributes;

	/**
	 * Constructor for SetAttributesAndUnlockResponse that requires all members
	 * @param playerAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetAttributesAndUnlockResponse(List<PlayerAndAttributes> playerAndAttributes, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.playerAndAttributes = playerAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAttributesAndUnlockResponse without super class members
	 * @param playerAndAttributes
	 */
	public SetAttributesAndUnlockResponse(List<PlayerAndAttributes> playerAndAttributes){
		this.playerAndAttributes = playerAndAttributes;

	}
	/**
	 * Constructor for SetAttributesAndUnlockResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetAttributesAndUnlockResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetAttributesAndUnlockResponse
	*/
	public SetAttributesAndUnlockResponse(){

	}
}
/** @} */
