/**
 * @file DeleteGroupMemberAttributesRequest.java
 * @page classDeleteGroupMemberAttributesRequest DeleteGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupMemberAttributesRequest: (groupName, groupType, playerIds, 
 * 		attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes group member attributes, which are specified in the attributeNames 
 * list, for the group members identified in the playerIds list
*/
@JsonTypeName("DeleteGroupMemberAttributesRequest")
public class DeleteGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where group attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of playerIds for group members whose attributes will be deleted
	 */
	public ArrayList<String> playerIds;

	/**
	 * A list of attribute names for the attributes that will be deleted
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteGroupMemberAttributesResponseDelegate){
                ((DeleteGroupMemberAttributesResponseDelegate)delegate).onDeleteGroupMemberAttributesResponse(this, (DeleteGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteGroupMemberAttributesResponseDelegate){
            ((DeleteGroupMemberAttributesResponseDelegate)delegate).onDeleteGroupMemberAttributesResponse(this, new DeleteGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributeNames
	 * @param passthrough
	 */
	public DeleteGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, ArrayList<String> attributeNames, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributeNames
	 */
	public DeleteGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for DeleteGroupMemberAttributesRequest
	*/
	public DeleteGroupMemberAttributesRequest(){

	}
}
/** @} */
