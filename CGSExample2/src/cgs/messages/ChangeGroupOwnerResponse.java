/**
 * @file ChangeGroupOwnerResponse.java
 * @page classChangeGroupOwnerResponse ChangeGroupOwnerResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ChangeGroupOwnerResponse: (groupName, groupType, playerInfo, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player who made the request confirming whether the 
 * player specified is now the owner of the group
*/
@JsonTypeName("ChangeGroupOwnerResponse")
public class ChangeGroupOwnerResponse extends GluonResponse{

	/**
	 * Name of the group whose owner has been changed
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * The player who has been made the new owner of the group (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for ChangeGroupOwnerResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ChangeGroupOwnerResponse(String groupName, String groupType, 
			PlayerInfo playerInfo, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ChangeGroupOwnerResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerInfo
	 */
	public ChangeGroupOwnerResponse(String groupName, String groupType, 
			PlayerInfo playerInfo){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for ChangeGroupOwnerResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ChangeGroupOwnerResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ChangeGroupOwnerResponse
	*/
	public ChangeGroupOwnerResponse(){

	}
}
/** @} */
