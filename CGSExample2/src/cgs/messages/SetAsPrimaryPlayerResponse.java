/**
 * @file SetAsPrimaryPlayerResponse.java
 * @page classSetAsPrimaryPlayerResponse SetAsPrimaryPlayerResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAsPrimaryPlayerResponse: (playerInfo, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the specified player will become the only player 
 * account attached to the Facebook credential. All other player accounts 
 * attached will be retired. 
*/
@JsonTypeName("")
public class SetAsPrimaryPlayerResponse extends GluonResponse{

	/**
	 * The player to keep attached to a facebook account (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for SetAsPrimaryPlayerResponse that requires all members
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetAsPrimaryPlayerResponse(PlayerInfo playerInfo, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAsPrimaryPlayerResponse without super class members
	 * @param playerInfo
	 */
	public SetAsPrimaryPlayerResponse(PlayerInfo playerInfo){
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for SetAsPrimaryPlayerResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetAsPrimaryPlayerResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetAsPrimaryPlayerResponse
	*/
	public SetAsPrimaryPlayerResponse(){

	}
}
/** @} */
