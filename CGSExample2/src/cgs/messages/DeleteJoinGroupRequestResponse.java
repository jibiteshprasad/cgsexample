/**
 * @file DeleteJoinGroupRequestResponse.java
 * @page classDeleteJoinGroupRequestResponse DeleteJoinGroupRequestResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteJoinGroupRequestResponse: (requestId, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether a join group request was 
 * deleted successfully
*/
@JsonTypeName("DeleteJoinGroupRequestResponse")
public class DeleteJoinGroupRequestResponse extends GluonResponse{

	/**
	 * Unique request ID for the join group request that was deleted
	 */
	public String requestId;

	/**
	 * Constructor for DeleteJoinGroupRequestResponse that requires all members
	 * @param requestId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteJoinGroupRequestResponse(String requestId, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.requestId = requestId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteJoinGroupRequestResponse without super class members
	 * @param requestId
	 */
	public DeleteJoinGroupRequestResponse(String requestId){
		this.requestId = requestId;

	}
	/**
	 * Constructor for DeleteJoinGroupRequestResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteJoinGroupRequestResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteJoinGroupRequestResponse
	*/
	public DeleteJoinGroupRequestResponse(){

	}
}
/** @} */
