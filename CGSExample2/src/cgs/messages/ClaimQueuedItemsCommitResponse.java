/**
 * @file ClaimQueuedItemsCommitResponse.java
 * @page classClaimQueuedItemsCommitResponse ClaimQueuedItemsCommitResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClaimQueuedItemsCommitResponse: (status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, Items have been successfully claimed and removed from 
 * the escrow service.
*/
@JsonTypeName("ClaimQueuedItemsCommitResponse")
public class ClaimQueuedItemsCommitResponse extends GluonResponse{

	/**
	 * Constructor for ClaimQueuedItemsCommitResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ClaimQueuedItemsCommitResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClaimQueuedItemsCommitResponse without super class members
	 */
	public ClaimQueuedItemsCommitResponse(){

	}
	/**
	 * Constructor for ClaimQueuedItemsCommitResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ClaimQueuedItemsCommitResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
