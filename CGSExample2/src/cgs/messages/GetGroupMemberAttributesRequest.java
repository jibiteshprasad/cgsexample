/**
 * @file GetGroupMemberAttributesRequest.java
 * @page classGetGroupMemberAttributesRequest GetGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupMemberAttributesRequest: (groupName, groupType, playerIds, 
 * 		attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves group member attributes, which are specified in the attributeNames 
 * list, for members specified in the playerIds list
*/
@JsonTypeName("GetGroupMemberAttributesRequest")
public class GetGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where member attributes will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of playerIds for the members whose attributes will be retrieved
	 */
	public ArrayList<String> playerIds;

	/**
	 * A list of the attribute names for the attributes that will be retrieved
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetGroupMemberAttributesResponseDelegate){
                ((GetGroupMemberAttributesResponseDelegate)delegate).onGetGroupMemberAttributesResponse(this, (GetGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetGroupMemberAttributesResponseDelegate){
            ((GetGroupMemberAttributesResponseDelegate)delegate).onGetGroupMemberAttributesResponse(this, new GetGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributeNames
	 * @param passthrough
	 */
	public GetGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, ArrayList<String> attributeNames, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributeNames
	 */
	public GetGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for GetGroupMemberAttributesRequest
	*/
	public GetGroupMemberAttributesRequest(){

	}
}
/** @} */
