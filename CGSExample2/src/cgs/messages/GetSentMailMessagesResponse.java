/**
 * @file GetSentMailMessagesResponse.java
 * @page classGetSentMailMessagesResponse GetSentMailMessagesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetSentMailMessagesResponse: (page, size, totalPages, totalSize, 
 * 		messages, status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns the requested number of sent mail messages sent by the authenticated 
 * player
*/
@JsonTypeName("GetSentMailMessagesResponse")
public class GetSentMailMessagesResponse extends GluonResponse{

	/**
	 * Current page that was returned (Optional)
	 */
	public int page;

	/**
	 * Number of messages returned for current page (Optional)
	 */
	public int size;

	/**
	 * Total number of pages according to proposed size (Optional)
	 */
	public int totalPages;

	/**
	 * Total number of messages sent from the authenticated player (Optional)
	 */
	public long totalSize;

	/**
	 * List of sent mail messages that was returned (Optional)
	 */
	public ArrayList<MailMessage> messages;

	/**
	 * Constructor for GetSentMailMessagesResponse that requires all members
	 * @param page
	 * @param size
	 * @param totalPages
	 * @param totalSize
	 * @param messages
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetSentMailMessagesResponse(int page, int size, int totalPages, 
			long totalSize, ArrayList<MailMessage> messages, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.page = page;
		this.size = size;
		this.totalPages = totalPages;
		this.totalSize = totalSize;
		this.messages = messages;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetSentMailMessagesResponse without super class members
	 * @param page
	 * @param size
	 * @param totalPages
	 * @param totalSize
	 * @param messages
	 */
	public GetSentMailMessagesResponse(int page, int size, int totalPages, 
			long totalSize, ArrayList<MailMessage> messages){
		this.page = page;
		this.size = size;
		this.totalPages = totalPages;
		this.totalSize = totalSize;
		this.messages = messages;

	}
	/**
	 * Constructor for GetSentMailMessagesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetSentMailMessagesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetSentMailMessagesResponse
	*/
	public GetSentMailMessagesResponse(){

	}
}
/** @} */
