/**
 * @file GetMyReceivedGroupInvitesResponse.java
 * @page classGetMyReceivedGroupInvitesResponse GetMyReceivedGroupInvitesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyReceivedGroupInvitesResponse: (receivedGroupInvites, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all incoming invites that are intended for authenticated player's 
 * group
*/
@JsonTypeName("GetMyReceivedGroupInvitesResponse")
public class GetMyReceivedGroupInvitesResponse extends GluonResponse{

	/**
	 * A list of invites intended for authenticated player (Optional)
	 */
	public List<ReceivedGroupInvite> receivedGroupInvites;

	/**
	 * Constructor for GetMyReceivedGroupInvitesResponse that requires all members
	 * @param receivedGroupInvites
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMyReceivedGroupInvitesResponse(List<ReceivedGroupInvite> receivedGroupInvites, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.receivedGroupInvites = receivedGroupInvites;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyReceivedGroupInvitesResponse without super class members
	 * @param receivedGroupInvites
	 */
	public GetMyReceivedGroupInvitesResponse(List<ReceivedGroupInvite> receivedGroupInvites){
		this.receivedGroupInvites = receivedGroupInvites;

	}
	/**
	 * Constructor for GetMyReceivedGroupInvitesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMyReceivedGroupInvitesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMyReceivedGroupInvitesResponse
	*/
	public GetMyReceivedGroupInvitesResponse(){

	}
}
/** @} */
