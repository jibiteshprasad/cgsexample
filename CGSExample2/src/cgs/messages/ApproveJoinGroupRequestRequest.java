/**
 * @file ApproveJoinGroupRequestRequest.java
 * @page classApproveJoinGroupRequestRequest ApproveJoinGroupRequestRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApproveJoinGroupRequestRequest: (requestId, maxGroupSize, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Approve a JoinGroupRequest from a player who wants to join a group.  Used in 
 * conjunction with GetReceivedJoinGroupRequestsRequest to get a list of 
 * requests and their corresponding requestIds.
*/
@JsonTypeName("ApproveJoinGroupRequestRequest")
public class ApproveJoinGroupRequestRequest extends GluonRequest{

	/**
	 * Name of the group to join - this name is unique
	 */
	public String requestId;

	/**
	 *Approval will only succeed if the current group size is less than the 
	 * maxGroupSize specified (Optional)
	 */
	public int maxGroupSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ApproveJoinGroupRequestResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ApproveJoinGroupRequestResponseDelegate){
                ((ApproveJoinGroupRequestResponseDelegate)delegate).onApproveJoinGroupRequestResponse(this, (ApproveJoinGroupRequestResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ApproveJoinGroupRequestResponseDelegate){
            ((ApproveJoinGroupRequestResponseDelegate)delegate).onApproveJoinGroupRequestResponse(this, new ApproveJoinGroupRequestResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ApproveJoinGroupRequestRequest that requires all members
	 * @param requestId
	 * @param maxGroupSize
	 * @param passthrough
	 */
	public ApproveJoinGroupRequestRequest(String requestId, int maxGroupSize, 
			String passthrough){
		this.requestId = requestId;
		this.maxGroupSize = maxGroupSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApproveJoinGroupRequestRequest without super class members
	 * @param requestId
	 * @param maxGroupSize
	 */
	public ApproveJoinGroupRequestRequest(String requestId, int maxGroupSize){
		this.requestId = requestId;
		this.maxGroupSize = maxGroupSize;

	}

	/**
	 * Default Constructor for ApproveJoinGroupRequestRequest
	*/
	public ApproveJoinGroupRequestRequest(){

	}
}
/** @} */
