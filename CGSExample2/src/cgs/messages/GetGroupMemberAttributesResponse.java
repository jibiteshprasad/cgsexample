/**
 * @file GetGroupMemberAttributesResponse.java
 * @page classGetGroupMemberAttributesResponse GetGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupMemberAttributesResponse: (groupName, groupType, attributes, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information and chosen group member attributes for 
 * each member requested will be returned
*/
@JsonTypeName("GetGroupMemberAttributesResponse")
public class GetGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Contains a list of members' player information as well as a list of 
	 * member attributes requested for each member specified in the request 
	 * (Optional)
	 */
	public ArrayList<PlayerAndAttributes> attributes;

	/**
	 * Constructor for GetGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetGroupMemberAttributesResponse(String groupName, String groupType, 
			ArrayList<PlayerAndAttributes> attributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 */
	public GetGroupMemberAttributesResponse(String groupName, String groupType, 
			ArrayList<PlayerAndAttributes> attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetGroupMemberAttributesResponse
	*/
	public GetGroupMemberAttributesResponse(){

	}
}
/** @} */
