/**
 * @file GroupDescriptor.java
 * @page classGroupDescriptor GroupDescriptor
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupDescriptor: (groupName, groupType)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Description of a group which consists of name and type
 * 
*/
public class GroupDescriptor {

	/**
	 * Name of the group
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Constructor for GroupDescriptor that requires all members
	 * @param groupName
	 * @param groupType
	 */
	public GroupDescriptor(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GroupDescriptor
	*/
	public GroupDescriptor(){

	}
}
/** @} */
