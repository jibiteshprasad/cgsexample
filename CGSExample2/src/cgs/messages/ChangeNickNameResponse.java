/**
 * @file ChangeNickNameResponse.java
 * @page classChangeNickNameResponse ChangeNickNameResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ChangeNickNameResponse: (suggestedNames, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the nickName will be changed to proposed name. If not 
 * available, user will receive a list of suggested nickNames.
*/
@JsonTypeName("ChangeNickNameResponse")
public class ChangeNickNameResponse extends GluonResponse{

	/**
	 * This will be populated if NickName is already taken, it will return a 
	 * list of suggested NickNames to use instead (Optional)
	 */
	public List<String> suggestedNames;

	/**
	 * Constructor for ChangeNickNameResponse that requires all members
	 * @param suggestedNames
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ChangeNickNameResponse(List<String> suggestedNames, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.suggestedNames = suggestedNames;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ChangeNickNameResponse without super class members
	 * @param suggestedNames
	 */
	public ChangeNickNameResponse(List<String> suggestedNames){
		this.suggestedNames = suggestedNames;

	}
	/**
	 * Constructor for ChangeNickNameResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ChangeNickNameResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ChangeNickNameResponse
	*/
	public ChangeNickNameResponse(){

	}
}
/** @} */
