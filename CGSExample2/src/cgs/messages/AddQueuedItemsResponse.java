/**
 * @file AddQueuedItemsResponse.java
 * @page classAddQueuedItemsResponse AddQueuedItemsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AddQueuedItemsResponse: (status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, items have been added to the escrow queue
 * 
*/
@JsonTypeName("AddQueuedItemsResponse")
public class AddQueuedItemsResponse extends GluonResponse{

	/**
	 * Constructor for AddQueuedItemsResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public AddQueuedItemsResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AddQueuedItemsResponse without super class members
	 */
	public AddQueuedItemsResponse(){

	}
	/**
	 * Constructor for AddQueuedItemsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public AddQueuedItemsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
