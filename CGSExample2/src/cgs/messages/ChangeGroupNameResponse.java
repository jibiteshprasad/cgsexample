/**
 * @file ChangeGroupNameResponse.java
 * @page classChangeGroupNameResponse ChangeGroupNameResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ChangeGroupNameResponse: (oldGroupName, groupType, groupName, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, the group name has been changed.
 * 
*/
@JsonTypeName("ChangeGroupNameResponse")
public class ChangeGroupNameResponse extends GluonResponse{

	/**
	 * Old name of the group who's name was just changed
	 */
	public String oldGroupName;

	/**
	 * Name of the group's type who's name was just changed
	 */
	public String groupType;

	/**
	 * New name of the group who's name was just changed
	 */
	public String groupName;

	/**
	 * Constructor for ChangeGroupNameResponse that requires all members
	 * @param oldGroupName
	 * @param groupType
	 * @param groupName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ChangeGroupNameResponse(String oldGroupName, String groupType, 
			String groupName, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.oldGroupName = oldGroupName;
		this.groupType = groupType;
		this.groupName = groupName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ChangeGroupNameResponse without super class members
	 * @param oldGroupName
	 * @param groupType
	 * @param groupName
	 */
	public ChangeGroupNameResponse(String oldGroupName, String groupType, 
			String groupName){
		this.oldGroupName = oldGroupName;
		this.groupType = groupType;
		this.groupName = groupName;

	}
	/**
	 * Constructor for ChangeGroupNameResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ChangeGroupNameResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ChangeGroupNameResponse
	*/
	public ChangeGroupNameResponse(){

	}
}
/** @} */
