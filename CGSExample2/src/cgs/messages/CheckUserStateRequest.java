/**
 * @file CheckUserStateRequest.java
 * @page classCheckUserStateRequest CheckUserStateRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CheckUserStateRequest: (serviceAddress, playerId, time, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A message from a service to get user's state.
*/
@JsonTypeName("CheckUserStateRequest")
public class CheckUserStateRequest extends GluonRequest{

	/**
	 *
	 */
	public String serviceAddress;

	/**
	 *
	 */
	public String playerId;

	/**
	 * (Optional) (Optional)
	 */
	public long time;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CheckUserStateResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CheckUserStateResponseDelegate){
                ((CheckUserStateResponseDelegate)delegate).onCheckUserStateResponse(this, (CheckUserStateResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CheckUserStateResponseDelegate){
            ((CheckUserStateResponseDelegate)delegate).onCheckUserStateResponse(this, new CheckUserStateResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CheckUserStateRequest that requires all members
	 * @param serviceAddress
	 * @param playerId
	 * @param time
	 * @param passthrough
	 */
	public CheckUserStateRequest(String serviceAddress, String playerId, 
			long time, String passthrough){
		this.serviceAddress = serviceAddress;
		this.playerId = playerId;
		this.time = time;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CheckUserStateRequest without super class members
	 * @param serviceAddress
	 * @param playerId
	 * @param time
	 */
	public CheckUserStateRequest(String serviceAddress, String playerId, 
			long time){
		this.serviceAddress = serviceAddress;
		this.playerId = playerId;
		this.time = time;

	}

	/**
	 * Default Constructor for CheckUserStateRequest
	*/
	public CheckUserStateRequest(){

	}
}
/** @} */
