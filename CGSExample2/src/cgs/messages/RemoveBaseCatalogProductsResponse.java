/**
 * @file RemoveBaseCatalogProductsResponse.java
 * @page classRemoveBaseCatalogProductsResponse RemoveBaseCatalogProductsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemoveBaseCatalogProductsResponse: (baseCatalogName, 
 * 		totalNumberOfProductsRemoved, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Removes the base products for a catalog.
*/
@JsonTypeName("RemoveBaseCatalogProductsResponse")
public class RemoveBaseCatalogProductsResponse extends GluonResponse{

	/**
	 * (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public int totalNumberOfProductsRemoved;

	/**
	 * Constructor for RemoveBaseCatalogProductsResponse that requires all members
	 * @param baseCatalogName
	 * @param totalNumberOfProductsRemoved
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public RemoveBaseCatalogProductsResponse(String baseCatalogName, 
			int totalNumberOfProductsRemoved, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.totalNumberOfProductsRemoved = totalNumberOfProductsRemoved;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemoveBaseCatalogProductsResponse without super class members
	 * @param baseCatalogName
	 * @param totalNumberOfProductsRemoved
	 */
	public RemoveBaseCatalogProductsResponse(String baseCatalogName, 
			int totalNumberOfProductsRemoved){
		this.baseCatalogName = baseCatalogName;
		this.totalNumberOfProductsRemoved = totalNumberOfProductsRemoved;

	}
	/**
	 * Constructor for RemoveBaseCatalogProductsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public RemoveBaseCatalogProductsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for RemoveBaseCatalogProductsResponse
	*/
	public RemoveBaseCatalogProductsResponse(){

	}
}
/** @} */
