package cgs.messages;

public enum SearchType
	{
	/// Will return any group names that begins the with string specified in searchString
	STARTS_WITH,
	/// Will return any group names that contains what is specified in searchString
	CONTAINS
	
}
