/**
 * @file PlayerInfo.java
 * @page classPlayerInfo PlayerInfo
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class PlayerInfo: (playerId, nickName)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A representation of a player as exposed to other %Gluon services.
 * 
*/
public class PlayerInfo {

	/**
	 * Player's Unique Identifier - used to reference a player in requests to 
	 * %Gluon services. (Optional)
	 */
	public String playerId;

	/**
	 * Player's publically exposed name displayed in-game to other players 
	 * (Optional)
	 */
	public String nickName;

	/**
	 * Constructor for PlayerInfo that requires all members
	 * @param playerId
	 * @param nickName
	 */
	public PlayerInfo(String playerId, String nickName){
		this.playerId = playerId;
		this.nickName = nickName;

	}

	/**
	 * Default Constructor for PlayerInfo
	*/
	public PlayerInfo(){

	}
}
/** @} */
