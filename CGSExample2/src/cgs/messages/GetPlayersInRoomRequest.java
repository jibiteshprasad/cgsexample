/**
 * @file GetPlayersInRoomRequest.java
 * @page classGetPlayersInRoomRequest GetPlayersInRoomRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPlayersInRoomRequest: (roomName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves a list of players who are in the specified chat room
 * 
*/
@JsonTypeName("GetPlayersInRoomRequest")
public class GetPlayersInRoomRequest extends GluonRequest{

	/**
	 * Name of the room in question
	 */
	public String roomName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetPlayersInRoomResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetPlayersInRoomResponseDelegate){
                ((GetPlayersInRoomResponseDelegate)delegate).onGetPlayersInRoomResponse(this, (GetPlayersInRoomResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetPlayersInRoomResponseDelegate){
            ((GetPlayersInRoomResponseDelegate)delegate).onGetPlayersInRoomResponse(this, new GetPlayersInRoomResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetPlayersInRoomRequest that requires all members
	 * @param roomName
	 * @param passthrough
	 */
	public GetPlayersInRoomRequest(String roomName, String passthrough){
		this.roomName = roomName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPlayersInRoomRequest without super class members
	 * @param roomName
	 */
	public GetPlayersInRoomRequest(String roomName){
		this.roomName = roomName;

	}

	/**
	 * Default Constructor for GetPlayersInRoomRequest
	*/
	public GetPlayersInRoomRequest(){

	}
}
/** @} */
