/**
 * @file CreateWalletReceiptResponse.java
 * @page classCreateWalletReceiptResponse CreateWalletReceiptResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreateWalletReceiptResponse: (playerInfo, currencyType, providerType, 
 * 		receiptId, initialState, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response for a player wallet creation.
 * 
*/
@JsonTypeName("CreateWalletReceiptResponse")
public class CreateWalletReceiptResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * The currency type (Optional)
	 */
	public int currencyType;

	/**
	 * The type of provider. Such as APPLE_IAP, COUPON, etc.
	 */
	public String providerType;

	/**
	 * The identifier for the created receipt (Optional)
	 */
	public String receiptId;

	/**
	 * The initial state of the receipt (Optional)
	 */
	public ReceiptState initialState;

	/**
	 * Constructor for CreateWalletReceiptResponse that requires all members
	 * @param playerInfo
	 * @param currencyType
	 * @param providerType
	 * @param receiptId
	 * @param initialState
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CreateWalletReceiptResponse(PlayerInfo playerInfo, int currencyType, 
			String providerType, String receiptId, ReceiptState initialState, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.currencyType = currencyType;
		this.providerType = providerType;
		this.receiptId = receiptId;
		this.initialState = initialState;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreateWalletReceiptResponse without super class members
	 * @param playerInfo
	 * @param currencyType
	 * @param providerType
	 * @param receiptId
	 * @param initialState
	 */
	public CreateWalletReceiptResponse(PlayerInfo playerInfo, int currencyType, 
			String providerType, String receiptId, ReceiptState initialState){
		this.playerInfo = playerInfo;
		this.currencyType = currencyType;
		this.providerType = providerType;
		this.receiptId = receiptId;
		this.initialState = initialState;

	}
	/**
	 * Constructor for CreateWalletReceiptResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CreateWalletReceiptResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CreateWalletReceiptResponse
	*/
	public CreateWalletReceiptResponse(){

	}
}
/** @} */
