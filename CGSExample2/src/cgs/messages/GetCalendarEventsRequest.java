/**
 * @file GetCalendarEventsRequest.java
 * @page classGetCalendarEventsRequest GetCalendarEventsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCalendarEventsRequest: (calendarNames, startTime, endTime, timezone, 
 * 		tagsFilter, stateFilter, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Request for calender events according to the search criteria specified.
 * 
*/
@JsonTypeName("GetCalendarEventsRequest")
public class GetCalendarEventsRequest extends GluonRequest{

	/**
	 * Name of the calendars to be used.
	 */
	public Set<String> calendarNames;

	/**
	 * The start time of the event. Value is number of milliseconds since epoch 
	 * time (00:00:00 UTC on 1 January 1970) (Optional)
	 */
	public long startTime;

	/**
	 * The end time of the event. Value is number of milliseconds since epoch 
	 * time (00:00:00 UTC on 1 January 1970) (Optional)
	 */
	public long endTime;

	/**
	 * The timezone code (Value is between -12 and 12) (Optional)
	 */
	public int timezone;

	/**
	 * The tags of the events to include in the response (multiple tags can be 
	 * specified) (Optional)
	 */
	public Set<String> tagsFilter;

	/**
	 * The states of the events to include in the response (multiple states can 
	 * be specified) (Optional)
	 */
	public Set<CalendarEventState> stateFilter;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCalendarEventsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCalendarEventsResponseDelegate){
                ((GetCalendarEventsResponseDelegate)delegate).onGetCalendarEventsResponse(this, (GetCalendarEventsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCalendarEventsResponseDelegate){
            ((GetCalendarEventsResponseDelegate)delegate).onGetCalendarEventsResponse(this, new GetCalendarEventsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCalendarEventsRequest that requires all members
	 * @param calendarNames
	 * @param startTime
	 * @param endTime
	 * @param timezone
	 * @param tagsFilter
	 * @param stateFilter
	 * @param passthrough
	 */
	public GetCalendarEventsRequest(Set<String> calendarNames, long startTime, 
			long endTime, int timezone, Set<String> tagsFilter, 
			Set<CalendarEventState> stateFilter, String passthrough){
		this.calendarNames = calendarNames;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timezone = timezone;
		this.tagsFilter = tagsFilter;
		this.stateFilter = stateFilter;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCalendarEventsRequest without super class members
	 * @param calendarNames
	 * @param startTime
	 * @param endTime
	 * @param timezone
	 * @param tagsFilter
	 * @param stateFilter
	 */
	public GetCalendarEventsRequest(Set<String> calendarNames, long startTime, 
			long endTime, int timezone, Set<String> tagsFilter, 
			Set<CalendarEventState> stateFilter){
		this.calendarNames = calendarNames;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timezone = timezone;
		this.tagsFilter = tagsFilter;
		this.stateFilter = stateFilter;

	}

	/**
	 * Default Constructor for GetCalendarEventsRequest
	*/
	public GetCalendarEventsRequest(){

	}
}
/** @} */
