/**
 * @file GroupAttributes.java
 * @page classGroupAttributes GroupAttributes
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupAttributes: (group, attributes)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Description of the group and a list of a groups attributes
 * 
*/
public class GroupAttributes {

	/**
	 * Description of the group which consists of group name and type
	 */
	public GroupDescriptor group;

	/**
	 * attributes A list of group attributes for the specified group (Optional)
	 */
	public List<GroupAttribute> attributes;

	/**
	 * Constructor for GroupAttributes that requires all members
	 * @param group
	 * @param attributes
	 */
	public GroupAttributes(GroupDescriptor group, 
			List<GroupAttribute> attributes){
		this.group = group;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for GroupAttributes
	*/
	public GroupAttributes(){

	}
}
/** @} */
