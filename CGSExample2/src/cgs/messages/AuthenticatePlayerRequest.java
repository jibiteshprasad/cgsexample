/**
 * @file AuthenticatePlayerRequest.java
 * @page classAuthenticatePlayerRequest AuthenticatePlayerRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AuthenticatePlayerRequest: (loginId, password, gameId, protocolVersion, 
 * 		clientSdkVersion, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Login to the %Gluon system using 'Player' credentials, which can either be 
 * loginId-supplied or automatically generated based on something like the 
 * OpenUDID of the device
*/
@JsonTypeName("AuthenticatePlayerRequest")
public class AuthenticatePlayerRequest extends GluonRequest{

	/**
	 * The name associated with the player - this should be human readable if 
	 * the game is not completely anonymous 
	 */
	public String loginId;

	/**
	 * Either a password chosen by the loginId, or another automatically 
	 * generated string associated with the device (Optional)
	 */
	public String password;

	/**
	 * The game identifier string provided by the %Gluon support team
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public int protocolVersion;

	/**
	 *
	 */
	public String clientSdkVersion;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(AuthenticatePlayerResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof AuthenticatePlayerResponseDelegate){
                ((AuthenticatePlayerResponseDelegate)delegate).onAuthenticatePlayerResponse(this, (AuthenticatePlayerResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof AuthenticatePlayerResponseDelegate){
            ((AuthenticatePlayerResponseDelegate)delegate).onAuthenticatePlayerResponse(this, new AuthenticatePlayerResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for AuthenticatePlayerRequest that requires all members
	 * @param loginId
	 * @param password
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 * @param passthrough
	 */
	public AuthenticatePlayerRequest(String loginId, String password, 
			String gameId, int protocolVersion, String clientSdkVersion, 
			String passthrough){
		this.loginId = loginId;
		this.password = password;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AuthenticatePlayerRequest without super class members
	 * @param loginId
	 * @param password
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 */
	public AuthenticatePlayerRequest(String loginId, String password, 
			String gameId, int protocolVersion, String clientSdkVersion){
		this.loginId = loginId;
		this.password = password;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;

	}

	/**
	 * Default Constructor for AuthenticatePlayerRequest
	*/
	public AuthenticatePlayerRequest(){

	}
}
/** @} */
