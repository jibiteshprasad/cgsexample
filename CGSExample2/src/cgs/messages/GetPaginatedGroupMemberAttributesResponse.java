/**
 * @file GetPaginatedGroupMemberAttributesResponse.java
 * @page classGetPaginatedGroupMemberAttributesResponse GetPaginatedGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPaginatedGroupMemberAttributesResponse: (groupName, groupType, 
 * 		totalSize, totalPages, playersAndAttributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information and group member attributes is returned 
 * according to the pageNumber/pageSize requested
*/
@JsonTypeName("GetPaginatedGroupMemberAttributesResponse")
public class GetPaginatedGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * The total number of members in the group (Optional)
	 */
	public int totalSize;

	/**
	 * The total number of pages in relation to the pageSize given in the 
	 * request. (Optional)
	 */
	public int totalPages;

	/**
	 * Contains player information and a list of the member attributes that was 
	 * requested (Optional)
	 */
	public List<PlayerAndAttributes> playersAndAttributes;

	/**
	 * Constructor for GetPaginatedGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param totalSize
	 * @param totalPages
	 * @param playersAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetPaginatedGroupMemberAttributesResponse(String groupName, 
			String groupType, int totalSize, int totalPages, 
			List<PlayerAndAttributes> playersAndAttributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.totalSize = totalSize;
		this.totalPages = totalPages;
		this.playersAndAttributes = playersAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPaginatedGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param totalSize
	 * @param totalPages
	 * @param playersAndAttributes
	 */
	public GetPaginatedGroupMemberAttributesResponse(String groupName, 
			String groupType, int totalSize, int totalPages, 
			List<PlayerAndAttributes> playersAndAttributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.totalSize = totalSize;
		this.totalPages = totalPages;
		this.playersAndAttributes = playersAndAttributes;

	}
	/**
	 * Constructor for GetPaginatedGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetPaginatedGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetPaginatedGroupMemberAttributesResponse
	*/
	public GetPaginatedGroupMemberAttributesResponse(){

	}
}
/** @} */
