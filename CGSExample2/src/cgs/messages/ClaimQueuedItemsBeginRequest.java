/**
 * @file ClaimQueuedItemsBeginRequest.java
 * @page classClaimQueuedItemsBeginRequest ClaimQueuedItemsBeginRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClaimQueuedItemsBeginRequest: (accountId, itemIds, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Begins a claim request for queued items. Will transition items to a pending 
 * state.
*/
@JsonTypeName("ClaimQueuedItemsBeginRequest")
public class ClaimQueuedItemsBeginRequest extends GluonRequest{

	/**
	 * Account of the player who intends receive the item
	 */
	public EscrowAccount accountId;

	/**
	 * List of item ids (EscrowItem.id) to be claimed
	 */
	public List<String> itemIds;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ClaimQueuedItemsBeginResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ClaimQueuedItemsBeginResponseDelegate){
                ((ClaimQueuedItemsBeginResponseDelegate)delegate).onClaimQueuedItemsBeginResponse(this, (ClaimQueuedItemsBeginResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ClaimQueuedItemsBeginResponseDelegate){
            ((ClaimQueuedItemsBeginResponseDelegate)delegate).onClaimQueuedItemsBeginResponse(this, new ClaimQueuedItemsBeginResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ClaimQueuedItemsBeginRequest that requires all members
	 * @param accountId
	 * @param itemIds
	 * @param passthrough
	 */
	public ClaimQueuedItemsBeginRequest(EscrowAccount accountId, 
			List<String> itemIds, String passthrough){
		this.accountId = accountId;
		this.itemIds = itemIds;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClaimQueuedItemsBeginRequest without super class members
	 * @param accountId
	 * @param itemIds
	 */
	public ClaimQueuedItemsBeginRequest(EscrowAccount accountId, 
			List<String> itemIds){
		this.accountId = accountId;
		this.itemIds = itemIds;

	}

	/**
	 * Default Constructor for ClaimQueuedItemsBeginRequest
	*/
	public ClaimQueuedItemsBeginRequest(){

	}
}
/** @} */
