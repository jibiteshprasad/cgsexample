/**
 * @file RemoveLeaderboardEntryResponse.java
 * @page classRemoveLeaderboardEntryResponse RemoveLeaderboardEntryResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemoveLeaderboardEntryResponse: (leaderboardName, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the entry has been removed from the leaderboard.
 * 
*/
@JsonTypeName("RemoveLeaderboardEntryResponse")
public class RemoveLeaderboardEntryResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * Constructor for RemoveLeaderboardEntryResponse that requires all members
	 * @param leaderboardName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public RemoveLeaderboardEntryResponse(String leaderboardName, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.leaderboardName = leaderboardName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemoveLeaderboardEntryResponse without super class members
	 * @param leaderboardName
	 */
	public RemoveLeaderboardEntryResponse(String leaderboardName){
		this.leaderboardName = leaderboardName;

	}
	/**
	 * Constructor for RemoveLeaderboardEntryResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public RemoveLeaderboardEntryResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for RemoveLeaderboardEntryResponse
	*/
	public RemoveLeaderboardEntryResponse(){

	}
}
/** @} */
