/**
 * @file CreateFacebookPlayerResponse.java
 * @page classCreateFacebookPlayerResponse CreateFacebookPlayerResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreateFacebookPlayerResponse: (session, playerInfo, loginId, expireTime, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the player account with Facebook credentials was 
 * created and you are now authorized to use  %Gluon services.
*/
@JsonTypeName("")
public class CreateFacebookPlayerResponse extends GluonResponse{

	/**
	 * Guid passed to future requests  (Optional)
	 */
	public String session;

	/**
	 * The created player (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Unique player identifier passed to future requests (Optional)
	 */
	public String loginId;

	/**
	 * This is unused currently, so you can ignore. (Optional)
	 */
	public Date expireTime;

	/**
	 * Constructor for CreateFacebookPlayerResponse that requires all members
	 * @param session
	 * @param playerInfo
	 * @param loginId
	 * @param expireTime
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CreateFacebookPlayerResponse(String session, PlayerInfo playerInfo, 
			String loginId, Date expireTime, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.session = session;
		this.playerInfo = playerInfo;
		this.loginId = loginId;
		this.expireTime = expireTime;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreateFacebookPlayerResponse without super class members
	 * @param session
	 * @param playerInfo
	 * @param loginId
	 * @param expireTime
	 */
	public CreateFacebookPlayerResponse(String session, PlayerInfo playerInfo, 
			String loginId, Date expireTime){
		this.session = session;
		this.playerInfo = playerInfo;
		this.loginId = loginId;
		this.expireTime = expireTime;

	}
	/**
	 * Constructor for CreateFacebookPlayerResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CreateFacebookPlayerResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CreateFacebookPlayerResponse
	*/
	public CreateFacebookPlayerResponse(){

	}
}
/** @} */
