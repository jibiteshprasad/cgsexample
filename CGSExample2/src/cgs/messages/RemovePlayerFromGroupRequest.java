/**
 * @file RemovePlayerFromGroupRequest.java
 * @page classRemovePlayerFromGroupRequest RemovePlayerFromGroupRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemovePlayerFromGroupRequest: (groupName, groupType, playerId, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Removes a specified player from the group specified
 * 
*/
@JsonTypeName("RemovePlayerFromGroupRequest")
public class RemovePlayerFromGroupRequest extends GluonRequest{

	/**
	 * Name of the group that the player specified will be removed from
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * ID of the player who will be removed from the group
	 */
	public String playerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(RemovePlayerFromGroupResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof RemovePlayerFromGroupResponseDelegate){
                ((RemovePlayerFromGroupResponseDelegate)delegate).onRemovePlayerFromGroupResponse(this, (RemovePlayerFromGroupResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof RemovePlayerFromGroupResponseDelegate){
            ((RemovePlayerFromGroupResponseDelegate)delegate).onRemovePlayerFromGroupResponse(this, new RemovePlayerFromGroupResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for RemovePlayerFromGroupRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerId
	 * @param passthrough
	 */
	public RemovePlayerFromGroupRequest(String groupName, String groupType, 
			String playerId, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemovePlayerFromGroupRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerId
	 */
	public RemovePlayerFromGroupRequest(String groupName, String groupType, 
			String playerId){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for RemovePlayerFromGroupRequest
	*/
	public RemovePlayerFromGroupRequest(){

	}
}
/** @} */
