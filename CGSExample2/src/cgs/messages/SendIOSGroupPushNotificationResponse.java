/**
 * @file SendIOSGroupPushNotificationResponse.java
 * @page classSendIOSGroupPushNotificationResponse SendIOSGroupPushNotificationResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendIOSGroupPushNotificationResponse: (status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to send IOS group push notifications.
*/
@JsonTypeName("SendIOSGroupPushNotificationResponse")
public class SendIOSGroupPushNotificationResponse extends GluonResponse{

	/**
	 * Constructor for SendIOSGroupPushNotificationResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendIOSGroupPushNotificationResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendIOSGroupPushNotificationResponse without super class members
	 */
	public SendIOSGroupPushNotificationResponse(){

	}
	/**
	 * Constructor for SendIOSGroupPushNotificationResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendIOSGroupPushNotificationResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
