/**
 * @file GetGroupAttributesResponse.java
 * @page classGetGroupAttributesResponse GetGroupAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupAttributesResponse: (groupName, groupType, attributes, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns requested group attributes for a specified group
 * 
*/
@JsonTypeName("GetGroupAttributesResponse")
public class GetGroupAttributesResponse extends GluonResponse{

	/**
	 * Name of the group whose attributes were received
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * A list of group attributes for the specified group (Optional)
	 */
	public List<GroupAttribute> attributes;

	/**
	 * Constructor for GetGroupAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetGroupAttributesResponse(String groupName, String groupType, 
			List<GroupAttribute> attributes, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 */
	public GetGroupAttributesResponse(String groupName, String groupType, 
			List<GroupAttribute> attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetGroupAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetGroupAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetGroupAttributesResponse
	*/
	public GetGroupAttributesResponse(){

	}
}
/** @} */
