/**
 * @file SetGroupAttributesForGroupsResponse.java
 * @page classSetGroupAttributesForGroupsResponse SetGroupAttributesForGroupsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupAttributesForGroupsResponse: (groupsAndAttributes, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, returns a List of groups and the attributes that have 
 * been set for each
*/
@JsonTypeName("SetGroupAttributesForGroupsResponse")
public class SetGroupAttributesForGroupsResponse extends GluonResponse{

	/**
	 * List of groups and the collection of attributes set for each group 
	 * (Optional)
	 */
	public List<GroupAttributes> groupsAndAttributes;

	/**
	 * Constructor for SetGroupAttributesForGroupsResponse that requires all members
	 * @param groupsAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetGroupAttributesForGroupsResponse(List<GroupAttributes> groupsAndAttributes, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.groupsAndAttributes = groupsAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupAttributesForGroupsResponse without super class members
	 * @param groupsAndAttributes
	 */
	public SetGroupAttributesForGroupsResponse(List<GroupAttributes> groupsAndAttributes){
		this.groupsAndAttributes = groupsAndAttributes;

	}
	/**
	 * Constructor for SetGroupAttributesForGroupsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetGroupAttributesForGroupsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetGroupAttributesForGroupsResponse
	*/
	public SetGroupAttributesForGroupsResponse(){

	}
}
/** @} */
