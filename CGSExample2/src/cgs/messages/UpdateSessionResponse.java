/**
 * @file UpdateSessionResponse.java
 * @page classUpdateSessionResponse UpdateSessionResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class UpdateSessionResponse: (wasSuccessful, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to an update session request.
*/
@JsonTypeName("UpdateSessionResponse")
public class UpdateSessionResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public boolean wasSuccessful;

	/**
	 * Constructor for UpdateSessionResponse that requires all members
	 * @param wasSuccessful
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public UpdateSessionResponse(boolean wasSuccessful, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.wasSuccessful = wasSuccessful;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for UpdateSessionResponse without super class members
	 * @param wasSuccessful
	 */
	public UpdateSessionResponse(boolean wasSuccessful){
		this.wasSuccessful = wasSuccessful;

	}
	/**
	 * Constructor for UpdateSessionResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public UpdateSessionResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for UpdateSessionResponse
	*/
	public UpdateSessionResponse(){

	}
}
/** @} */
