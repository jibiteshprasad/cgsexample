package cgs.messages;

public enum Filter
	{
	/// Retrieves all items in a request
	ALL,
	/// Retrieves only unclaimed items in a request
	UNCLAIMED,
	/// Retrieves only pending claimed items in a request
	PENDING
	
}
