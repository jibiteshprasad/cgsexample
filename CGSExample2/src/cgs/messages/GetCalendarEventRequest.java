/**
 * @file GetCalendarEventRequest.java
 * @page classGetCalendarEventRequest GetCalendarEventRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCalendarEventRequest: (calendarName, eventName, timezone, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Request for a calender event specified
 * 
*/
@JsonTypeName("GetCalendarEventRequest")
public class GetCalendarEventRequest extends GluonRequest{

	/**
	 * Name of the calendar in question
	 */
	public String calendarName;

	/**
	 * Name of the event in question
	 */
	public String eventName;

	/**
	 * The timezone code (Value is between -12 and 12) (Optional)
	 */
	public int timezone;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCalendarEventResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCalendarEventResponseDelegate){
                ((GetCalendarEventResponseDelegate)delegate).onGetCalendarEventResponse(this, (GetCalendarEventResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCalendarEventResponseDelegate){
            ((GetCalendarEventResponseDelegate)delegate).onGetCalendarEventResponse(this, new GetCalendarEventResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCalendarEventRequest that requires all members
	 * @param calendarName
	 * @param eventName
	 * @param timezone
	 * @param passthrough
	 */
	public GetCalendarEventRequest(String calendarName, String eventName, 
			int timezone, String passthrough){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.timezone = timezone;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCalendarEventRequest without super class members
	 * @param calendarName
	 * @param eventName
	 * @param timezone
	 */
	public GetCalendarEventRequest(String calendarName, String eventName, 
			int timezone){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.timezone = timezone;

	}

	/**
	 * Default Constructor for GetCalendarEventRequest
	*/
	public GetCalendarEventRequest(){

	}
}
/** @} */
