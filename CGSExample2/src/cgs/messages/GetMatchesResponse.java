/**
 * @file GetMatchesResponse.java
 * @page classGetMatchesResponse GetMatchesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMatchesResponse: (playerAndAttributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, returns a list of available players to match with based 
 * upon the original query.
*/
@JsonTypeName("GetMatchesResponse")
public class GetMatchesResponse extends GluonResponse{

	/**
	 * List of available players to match with and their respective attributes 
	 * (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributes;

	/**
	 * Constructor for GetMatchesResponse that requires all members
	 * @param playerAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMatchesResponse(List<PlayerAndAttributes> playerAndAttributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerAndAttributes = playerAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMatchesResponse without super class members
	 * @param playerAndAttributes
	 */
	public GetMatchesResponse(List<PlayerAndAttributes> playerAndAttributes){
		this.playerAndAttributes = playerAndAttributes;

	}
	/**
	 * Constructor for GetMatchesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMatchesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMatchesResponse
	*/
	public GetMatchesResponse(){

	}
}
/** @} */
