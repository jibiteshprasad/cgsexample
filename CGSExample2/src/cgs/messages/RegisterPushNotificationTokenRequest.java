/**
 * @file RegisterPushNotificationTokenRequest.java
 * @page classRegisterPushNotificationTokenRequest RegisterPushNotificationTokenRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RegisterPushNotificationTokenRequest: (deviceUDID, 
 * 		pushNotificationDeviceToken, tags, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Associates the device token from Urban Airship (for now) to the currently 
 * logged in %Gluon player account. This allows for offline access to send push 
 * notifications to players using the %Gluon system.
*/
@JsonTypeName("RegisterPushNotificationTokenRequest")
public class RegisterPushNotificationTokenRequest extends GluonRequest{

	/**
	 * The unique device ID that the device token to be associated with.
	 */
	public String deviceUDID;

	/**
	 * The device token received from Urban Airship used for sending Push 
	 * Notifications to a device.
	 */
	public String pushNotificationDeviceToken;

	/**
	 * Collection of arbitrary tags to associate to Urban Airship for the 
	 * supplied Push Notification Token. (Optional)
	 */
	public List<String> tags;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(RegisterPushNotificationTokenResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof RegisterPushNotificationTokenResponseDelegate){
                ((RegisterPushNotificationTokenResponseDelegate)delegate).onRegisterPushNotificationTokenResponse(this, (RegisterPushNotificationTokenResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof RegisterPushNotificationTokenResponseDelegate){
            ((RegisterPushNotificationTokenResponseDelegate)delegate).onRegisterPushNotificationTokenResponse(this, new RegisterPushNotificationTokenResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for RegisterPushNotificationTokenRequest that requires all members
	 * @param deviceUDID
	 * @param pushNotificationDeviceToken
	 * @param tags
	 * @param passthrough
	 */
	public RegisterPushNotificationTokenRequest(String deviceUDID, 
			String pushNotificationDeviceToken, List<String> tags, 
			String passthrough){
		this.deviceUDID = deviceUDID;
		this.pushNotificationDeviceToken = pushNotificationDeviceToken;
		this.tags = tags;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RegisterPushNotificationTokenRequest without super class members
	 * @param deviceUDID
	 * @param pushNotificationDeviceToken
	 * @param tags
	 */
	public RegisterPushNotificationTokenRequest(String deviceUDID, 
			String pushNotificationDeviceToken, List<String> tags){
		this.deviceUDID = deviceUDID;
		this.pushNotificationDeviceToken = pushNotificationDeviceToken;
		this.tags = tags;

	}

	/**
	 * Default Constructor for RegisterPushNotificationTokenRequest
	*/
	public RegisterPushNotificationTokenRequest(){

	}
}
/** @} */
