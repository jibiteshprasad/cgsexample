/**
 * @file DeleteGroupMemberAttributesForAllMembersResponse.java
 * @page classDeleteGroupMemberAttributesForAllMembersResponse DeleteGroupMemberAttributesForAllMembersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupMemberAttributesForAllMembersResponse: (groupName, groupType, 
 * 		members, status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon successful deletion, the player information for each member whose 
 * attributes were deleted will be returned
*/
@JsonTypeName("DeleteGroupMemberAttributesForAllMembersResponse")
public class DeleteGroupMemberAttributesForAllMembersResponse extends GluonResponse{

	/**
	 * Name of the group where group attributes were deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Player information for the members whose attributes were deleted 
	 * (Optional)
	 */
	public ArrayList<PlayerInfo> members;

	/**
	 * Constructor for DeleteGroupMemberAttributesForAllMembersResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteGroupMemberAttributesForAllMembersResponse(String groupName, 
			String groupType, ArrayList<PlayerInfo> members, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupMemberAttributesForAllMembersResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param members
	 */
	public DeleteGroupMemberAttributesForAllMembersResponse(String groupName, 
			String groupType, ArrayList<PlayerInfo> members){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;

	}
	/**
	 * Constructor for DeleteGroupMemberAttributesForAllMembersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteGroupMemberAttributesForAllMembersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteGroupMemberAttributesForAllMembersResponse
	*/
	public DeleteGroupMemberAttributesForAllMembersResponse(){

	}
}
/** @} */
