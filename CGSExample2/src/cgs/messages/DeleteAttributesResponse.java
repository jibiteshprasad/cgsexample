/**
 * @file DeleteAttributesResponse.java
 * @page classDeleteAttributesResponse DeleteAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAttributesResponse: (playerInfo, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether attributes were deleted 
 * successfully
*/
@JsonTypeName("DeleteAttributesResponse")
public class DeleteAttributesResponse extends GluonResponse{

	/**
	 * The player whose attributes were deleted (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for DeleteAttributesResponse that requires all members
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteAttributesResponse(PlayerInfo playerInfo, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAttributesResponse without super class members
	 * @param playerInfo
	 */
	public DeleteAttributesResponse(PlayerInfo playerInfo){
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for DeleteAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteAttributesResponse
	*/
	public DeleteAttributesResponse(){

	}
}
/** @} */
