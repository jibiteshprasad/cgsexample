/**
 * @file SetDocumentResponse.java
 * @page classSetDocumentResponse SetDocumentResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetDocumentResponse: (documentId, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the document was uploaded and stored, and can be 
 * identified with the  documentId returned.
*/
@JsonTypeName("SetDocumentResponse")
public class SetDocumentResponse extends GluonResponse{

	/**
	 * The unique identifier for the uploaded document, to be used for 
	 * retrieval (GetDocumentById)  or overwriting (SetDocumentById)
	 */
	public String documentId;

	/**
	 * Constructor for SetDocumentResponse that requires all members
	 * @param documentId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetDocumentResponse(String documentId, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.documentId = documentId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetDocumentResponse without super class members
	 * @param documentId
	 */
	public SetDocumentResponse(String documentId){
		this.documentId = documentId;

	}
	/**
	 * Constructor for SetDocumentResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetDocumentResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetDocumentResponse
	*/
	public SetDocumentResponse(){

	}
}
/** @} */
