/**
 * @file ApplyDeltaToLeaderboardEntryRequest.java
 * @page classApplyDeltaToLeaderboardEntryRequest ApplyDeltaToLeaderboardEntryRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApplyDeltaToLeaderboardEntryRequest: (leaderboardName, ownerId, delta, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Adds the 'delta' value to the value of an existing entry in a leaderboard; 
 * this can be used to avoid the need to first retrieve the existing value 
 * before updating.
*/
@JsonTypeName("ApplyDeltaToLeaderboardEntryRequest")
public class ApplyDeltaToLeaderboardEntryRequest extends GluonRequest{

	/**
	 * The name of the leaderboard that contains the entry to update
	 */
	public String leaderboardName;

	/**
	 * The name of the entry that will be updated (the playerId for a 
	 * player-based leaderboard)
	 */
	public String ownerId;

	/**
	 * This will get added to the current value of the entry (Optional)
	 */
	public long delta;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ApplyDeltaToLeaderboardEntryResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ApplyDeltaToLeaderboardEntryResponseDelegate){
                ((ApplyDeltaToLeaderboardEntryResponseDelegate)delegate).onApplyDeltaToLeaderboardEntryResponse(this, (ApplyDeltaToLeaderboardEntryResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ApplyDeltaToLeaderboardEntryResponseDelegate){
            ((ApplyDeltaToLeaderboardEntryResponseDelegate)delegate).onApplyDeltaToLeaderboardEntryResponse(this, new ApplyDeltaToLeaderboardEntryResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ApplyDeltaToLeaderboardEntryRequest that requires all members
	 * @param leaderboardName
	 * @param ownerId
	 * @param delta
	 * @param passthrough
	 */
	public ApplyDeltaToLeaderboardEntryRequest(String leaderboardName, 
			String ownerId, long delta, String passthrough){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;
		this.delta = delta;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApplyDeltaToLeaderboardEntryRequest without super class members
	 * @param leaderboardName
	 * @param ownerId
	 * @param delta
	 */
	public ApplyDeltaToLeaderboardEntryRequest(String leaderboardName, 
			String ownerId, long delta){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;
		this.delta = delta;

	}

	/**
	 * Default Constructor for ApplyDeltaToLeaderboardEntryRequest
	*/
	public ApplyDeltaToLeaderboardEntryRequest(){

	}
}
/** @} */
