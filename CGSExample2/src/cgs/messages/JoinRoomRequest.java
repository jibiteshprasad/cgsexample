/**
 * @file JoinRoomRequest.java
 * @page classJoinRoomRequest JoinRoomRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class JoinRoomRequest: (roomName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a request to join the specified chat room. Upon joining the room, the 
 * player will receive the last 5 chat room messages sent to the room via 
 * ReceiveRoomMessageNotification.
*/
@JsonTypeName("JoinRoom")
public class JoinRoomRequest extends GluonRequest{

	/**
	 * Name of the room that the player wants to join
	 */
	public String roomName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(JoinRoomResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof JoinRoomResponseDelegate){
                ((JoinRoomResponseDelegate)delegate).onJoinRoomResponse(this, (JoinRoomResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof JoinRoomResponseDelegate){
            ((JoinRoomResponseDelegate)delegate).onJoinRoomResponse(this, new JoinRoomResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for JoinRoomRequest that requires all members
	 * @param roomName
	 * @param passthrough
	 */
	public JoinRoomRequest(String roomName, String passthrough){
		this.roomName = roomName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for JoinRoomRequest without super class members
	 * @param roomName
	 */
	public JoinRoomRequest(String roomName){
		this.roomName = roomName;

	}

	/**
	 * Default Constructor for JoinRoomRequest
	*/
	public JoinRoomRequest(){

	}
}
/** @} */
