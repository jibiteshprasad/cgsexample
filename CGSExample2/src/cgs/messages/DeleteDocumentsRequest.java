/**
 * @file DeleteDocumentsRequest.java
 * @page classDeleteDocumentsRequest DeleteDocumentsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteDocumentsRequest: (tags, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Delete all documents (previously stored by this player) labeled with any of 
 * the tags passed
*/
@JsonTypeName("DeleteDocumentsRequest")
public class DeleteDocumentsRequest extends GluonRequest{

	/**
	 * Strings that categorize each document (Optional)
	 */
	public List<String> tags;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteDocumentsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteDocumentsResponseDelegate){
                ((DeleteDocumentsResponseDelegate)delegate).onDeleteDocumentsResponse(this, (DeleteDocumentsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteDocumentsResponseDelegate){
            ((DeleteDocumentsResponseDelegate)delegate).onDeleteDocumentsResponse(this, new DeleteDocumentsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteDocumentsRequest that requires all members
	 * @param tags
	 * @param passthrough
	 */
	public DeleteDocumentsRequest(List<String> tags, String passthrough){
		this.tags = tags;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteDocumentsRequest without super class members
	 * @param tags
	 */
	public DeleteDocumentsRequest(List<String> tags){
		this.tags = tags;

	}

	/**
	 * Default Constructor for DeleteDocumentsRequest
	*/
	public DeleteDocumentsRequest(){

	}
}
/** @} */
