/**
 * @file CustomGameServerMessageResponse.java
 * @page classCustomGameServerMessageResponse CustomGameServerMessageResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CustomGameServerMessageResponse: (payload, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response message from a Custom Game Server (CGS) - corresponding to a CGS 
 * request message
*/
@JsonTypeName("CustomGameServerResponse")
public class CustomGameServerMessageResponse extends GluonResponse{

	/**
	 * The response data payload containing the data for the game (Optional)
	 */
	public Object payload;

	/**
	 * Constructor for CustomGameServerMessageResponse that requires all members
	 * @param payload
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CustomGameServerMessageResponse(Object payload, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.payload = payload;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CustomGameServerMessageResponse without super class members
	 * @param payload
	 */
	public CustomGameServerMessageResponse(Object payload){
		this.payload = payload;

	}
	/**
	 * Constructor for CustomGameServerMessageResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CustomGameServerMessageResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CustomGameServerMessageResponse
	*/
	public CustomGameServerMessageResponse(){

	}
}
/** @} */
