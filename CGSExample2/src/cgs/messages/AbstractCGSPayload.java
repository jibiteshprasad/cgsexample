/**
 * @file AbstractCGSPayload.java
 * @page classAbstractCGSPayload AbstractCGSPayload
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AbstractCGSPayload: (protocolVersion, cgsHeader, body)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * 
*/
public class AbstractCGSPayload {

	/**
	 * (Optional)
	 */
	public int protocolVersion;

	/**
	 * A header containing meta information about the CGS message (Optional)
	 */
	public CGSHeader cgsHeader;

	/**
	 * (Optional)
	 */
	public Object body;

	/**
	 * Constructor for AbstractCGSPayload that requires all members
	 * @param protocolVersion
	 * @param cgsHeader
	 * @param body
	 */
	public AbstractCGSPayload(int protocolVersion, CGSHeader cgsHeader, 
			Object body){
		this.protocolVersion = protocolVersion;
		this.cgsHeader = cgsHeader;
		this.body = body;

	}

	/**
	 * Default Constructor for AbstractCGSPayload
	*/
	public AbstractCGSPayload(){

	}
}
/** @} */
