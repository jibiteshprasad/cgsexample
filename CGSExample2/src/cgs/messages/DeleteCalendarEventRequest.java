/**
 * @file DeleteCalendarEventRequest.java
 * @page classDeleteCalendarEventRequest DeleteCalendarEventRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteCalendarEventRequest: (calendarName, eventName, userName, endDate, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes an event from a calendar.
*/
@JsonTypeName("DeleteCalendarEventRequest")
public class DeleteCalendarEventRequest extends GluonRequest{

	/**
	 *
	 */
	public String calendarName;

	/**
	 *
	 */
	public String eventName;

	/**
	 *
	 */
	public String userName;

	/**
	 * (Optional) (Optional) (Optional) (Optional) (Optional)
	 */
	public Date endDate;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteCalendarEventResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteCalendarEventResponseDelegate){
                ((DeleteCalendarEventResponseDelegate)delegate).onDeleteCalendarEventResponse(this, (DeleteCalendarEventResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteCalendarEventResponseDelegate){
            ((DeleteCalendarEventResponseDelegate)delegate).onDeleteCalendarEventResponse(this, new DeleteCalendarEventResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteCalendarEventRequest that requires all members
	 * @param calendarName
	 * @param eventName
	 * @param userName
	 * @param endDate
	 * @param passthrough
	 */
	public DeleteCalendarEventRequest(String calendarName, String eventName, 
			String userName, Date endDate, String passthrough){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.userName = userName;
		this.endDate = endDate;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteCalendarEventRequest without super class members
	 * @param calendarName
	 * @param eventName
	 * @param userName
	 * @param endDate
	 */
	public DeleteCalendarEventRequest(String calendarName, String eventName, 
			String userName, Date endDate){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.userName = userName;
		this.endDate = endDate;

	}

	/**
	 * Default Constructor for DeleteCalendarEventRequest
	*/
	public DeleteCalendarEventRequest(){

	}
}
/** @} */
