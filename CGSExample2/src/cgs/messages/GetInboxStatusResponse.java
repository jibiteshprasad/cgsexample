/**
 * @file GetInboxStatusResponse.java
 * @page classGetInboxStatusResponse GetInboxStatusResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetInboxStatusResponse: (totalMailCount, unreadMailCount, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns the current inbox status of the authenticated player such as the 
 * total number of messages and number of unread messages
*/
@JsonTypeName("GetInboxStatusResponse")
public class GetInboxStatusResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public long totalMailCount;

	/**
	 * (Optional)
	 */
	public long unreadMailCount;

	/**
	 * Constructor for GetInboxStatusResponse that requires all members
	 * @param totalMailCount
	 * @param unreadMailCount
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetInboxStatusResponse(long totalMailCount, long unreadMailCount, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.totalMailCount = totalMailCount;
		this.unreadMailCount = unreadMailCount;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetInboxStatusResponse without super class members
	 * @param totalMailCount
	 * @param unreadMailCount
	 */
	public GetInboxStatusResponse(long totalMailCount, long unreadMailCount){
		this.totalMailCount = totalMailCount;
		this.unreadMailCount = unreadMailCount;

	}
	/**
	 * Constructor for GetInboxStatusResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetInboxStatusResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetInboxStatusResponse
	*/
	public GetInboxStatusResponse(){

	}
}
/** @} */
