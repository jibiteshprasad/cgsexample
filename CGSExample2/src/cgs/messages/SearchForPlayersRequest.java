/**
 * @file SearchForPlayersRequest.java
 * @page classSearchForPlayersRequest SearchForPlayersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SearchForPlayersRequest: (playerCredentialType, identifiers, 
 * 		maxPlayersToReturn, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Searches for a list of players in the gluon system matching the specified 
 * identifers.  Currently only supports searching for FACEBOOK or NICKNAME.
*/
@JsonTypeName("SearchForPlayersRequest")
public class SearchForPlayersRequest extends GluonRequest{

	/**
	 * Type of credential being searched for (e.g. FACEBOOK, NICKNAME, etc.)
	 */
	public PlayerCredentialType playerCredentialType;

	/**
	 * List of identifiers to search for players on. This will differ based on 
	 * type. For FACEBOOK, facebook UUIDs are provided. For Nickname, %Gluon 
	 * nickname strings are provided.
	 */
	public List<String> identifiers;

	/**
	 * Limits the number of results returned. 0 means unlimited.
	 */
	public int maxPlayersToReturn;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SearchForPlayersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SearchForPlayersResponseDelegate){
                ((SearchForPlayersResponseDelegate)delegate).onSearchForPlayersResponse(this, (SearchForPlayersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SearchForPlayersResponseDelegate){
            ((SearchForPlayersResponseDelegate)delegate).onSearchForPlayersResponse(this, new SearchForPlayersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SearchForPlayersRequest that requires all members
	 * @param playerCredentialType
	 * @param identifiers
	 * @param maxPlayersToReturn
	 * @param passthrough
	 */
	public SearchForPlayersRequest(PlayerCredentialType playerCredentialType, 
			List<String> identifiers, int maxPlayersToReturn, String passthrough){
		this.playerCredentialType = playerCredentialType;
		this.identifiers = identifiers;
		this.maxPlayersToReturn = maxPlayersToReturn;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SearchForPlayersRequest without super class members
	 * @param playerCredentialType
	 * @param identifiers
	 * @param maxPlayersToReturn
	 */
	public SearchForPlayersRequest(PlayerCredentialType playerCredentialType, 
			List<String> identifiers, int maxPlayersToReturn){
		this.playerCredentialType = playerCredentialType;
		this.identifiers = identifiers;
		this.maxPlayersToReturn = maxPlayersToReturn;

	}

	/**
	 * Default Constructor for SearchForPlayersRequest
	*/
	public SearchForPlayersRequest(){

	}
}
/** @} */
