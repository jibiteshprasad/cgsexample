/**
 * @file IOSNotification.java
 * @page classIOSNotification IOSNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class IOSNotification: (message, localizedViewButtonKey, localizedMessageKey, 
 * 		localizedMessageKeyArgs, launchImage)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A player/group notification message
*/
public class IOSNotification {

	/**
	 * (Optional)
	 */
	public String message;

	/**
	 * (Optional)
	 */
	public String localizedViewButtonKey;

	/**
	 * (Optional)
	 */
	public String localizedMessageKey;

	/**
	 * (Optional)
	 */
	public String[] localizedMessageKeyArgs;

	/**
	 * (Optional)
	 */
	public String launchImage;

	/**
	 * Constructor for IOSNotification that requires all members
	 * @param message
	 * @param localizedViewButtonKey
	 * @param localizedMessageKey
	 * @param localizedMessageKeyArgs
	 * @param launchImage
	 */
	public IOSNotification(String message, String localizedViewButtonKey, 
			String localizedMessageKey, String[] localizedMessageKeyArgs, 
			String launchImage){
		this.message = message;
		this.localizedViewButtonKey = localizedViewButtonKey;
		this.localizedMessageKey = localizedMessageKey;
		this.localizedMessageKeyArgs = localizedMessageKeyArgs;
		this.launchImage = launchImage;

	}

	/**
	 * Default Constructor for IOSNotification
	*/
	public IOSNotification(){

	}
}
/** @} */
