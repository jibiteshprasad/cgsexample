/**
 * @file ClaimQueuedItemsRollbackRequest.java
 * @page classClaimQueuedItemsRollbackRequest ClaimQueuedItemsRollbackRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClaimQueuedItemsRollbackRequest: (accountId, itemIds, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Rolls back a claim transaction for queued items. This resets their state to 
 * unclaimed.
*/
@JsonTypeName("ClaimQueuedItemsRollbackRequest")
public class ClaimQueuedItemsRollbackRequest extends GluonRequest{

	/**
	 * Account of the player who set the item to a pending state.
	 */
	public EscrowAccount accountId;

	/**
	 * List of item ids to rollback out the claim transaction. This resets 
	 * their state to unclaimed.
	 */
	public List<String> itemIds;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ClaimQueuedItemsRollbackResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ClaimQueuedItemsRollbackResponseDelegate){
                ((ClaimQueuedItemsRollbackResponseDelegate)delegate).onClaimQueuedItemsRollbackResponse(this, (ClaimQueuedItemsRollbackResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ClaimQueuedItemsRollbackResponseDelegate){
            ((ClaimQueuedItemsRollbackResponseDelegate)delegate).onClaimQueuedItemsRollbackResponse(this, new ClaimQueuedItemsRollbackResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ClaimQueuedItemsRollbackRequest that requires all members
	 * @param accountId
	 * @param itemIds
	 * @param passthrough
	 */
	public ClaimQueuedItemsRollbackRequest(EscrowAccount accountId, 
			List<String> itemIds, String passthrough){
		this.accountId = accountId;
		this.itemIds = itemIds;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClaimQueuedItemsRollbackRequest without super class members
	 * @param accountId
	 * @param itemIds
	 */
	public ClaimQueuedItemsRollbackRequest(EscrowAccount accountId, 
			List<String> itemIds){
		this.accountId = accountId;
		this.itemIds = itemIds;

	}

	/**
	 * Default Constructor for ClaimQueuedItemsRollbackRequest
	*/
	public ClaimQueuedItemsRollbackRequest(){

	}
}
/** @} */
