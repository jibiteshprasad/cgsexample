/**
 * @file CreateWalletReceiptRequest.java
 * @page classCreateWalletReceiptRequest CreateWalletReceiptRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreateWalletReceiptRequest: (currencyType, providerType, receiptData, 
 * 		initialState, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to create a receipt for a player's wallet.
 * 
*/
@JsonTypeName("CreateWalletReceiptRequest")
public class CreateWalletReceiptRequest extends GluonRequest{

	/**
	 * The currency type (Optional)
	 */
	public int currencyType;

	/**
	 * The type of provider. Such as APPLE_IAP, COUPON, etc.
	 */
	public String providerType;

	/**
	 * The data blob to associate with this receipt (Optional)
	 */
	public String receiptData;

	/**
	 * The initial state of the receipt
	 */
	public ReceiptState initialState;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CreateWalletReceiptResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CreateWalletReceiptResponseDelegate){
                ((CreateWalletReceiptResponseDelegate)delegate).onCreateWalletReceiptResponse(this, (CreateWalletReceiptResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CreateWalletReceiptResponseDelegate){
            ((CreateWalletReceiptResponseDelegate)delegate).onCreateWalletReceiptResponse(this, new CreateWalletReceiptResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CreateWalletReceiptRequest that requires all members
	 * @param currencyType
	 * @param providerType
	 * @param receiptData
	 * @param initialState
	 * @param passthrough
	 */
	public CreateWalletReceiptRequest(int currencyType, String providerType, 
			String receiptData, ReceiptState initialState, String passthrough){
		this.currencyType = currencyType;
		this.providerType = providerType;
		this.receiptData = receiptData;
		this.initialState = initialState;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreateWalletReceiptRequest without super class members
	 * @param currencyType
	 * @param providerType
	 * @param receiptData
	 * @param initialState
	 */
	public CreateWalletReceiptRequest(int currencyType, String providerType, 
			String receiptData, ReceiptState initialState){
		this.currencyType = currencyType;
		this.providerType = providerType;
		this.receiptData = receiptData;
		this.initialState = initialState;

	}

	/**
	 * Default Constructor for CreateWalletReceiptRequest
	*/
	public CreateWalletReceiptRequest(){

	}
}
/** @} */
