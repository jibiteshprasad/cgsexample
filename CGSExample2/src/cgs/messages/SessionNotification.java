/**
 * @file SessionNotification.java
 * @page classSessionNotification SessionNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SessionNotification: (session, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * 
*/
@JsonTypeName("SessionNotification")
public class SessionNotification extends GluonRequest{

	/**
	 *
	 */
	public String session;

	/**
	 * Constructor for SessionNotification that requires all members
	 * @param session
	 * @param passthrough
	 */
	public SessionNotification(String session, String passthrough){
		this.session = session;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SessionNotification without super class members
	 * @param session
	 */
	public SessionNotification(String session){
		this.session = session;

	}

	/**
	 * Default Constructor for SessionNotification
	*/
	public SessionNotification(){

	}
}
/** @} */
