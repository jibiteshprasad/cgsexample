/**
 * @file GetPaginatedGroupMemberAttributesRequest.java
 * @page classGetPaginatedGroupMemberAttributesRequest GetPaginatedGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPaginatedGroupMemberAttributesRequest: (groupName, groupType, 
 * 		attributeNames, pageNumber, pageSize, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves a list of attributes for members of a group. The members are 
 * paginated by mongo id.
*/
@JsonTypeName("GetPaginatedGroupMemberAttributesRequest")
public class GetPaginatedGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where member attributes will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * A list of the attribute names for the attributes that will be retrieved 
	 * (Optional)
	 */
	public ArrayList<String> attributeNames;

	/**
	 * The current page index (index starts at 0) (Optional)
	 */
	public int pageNumber;

	/**
	 * The number of results to return for this page (Optional)
	 */
	public int pageSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetPaginatedGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetPaginatedGroupMemberAttributesResponseDelegate){
                ((GetPaginatedGroupMemberAttributesResponseDelegate)delegate).onGetPaginatedGroupMemberAttributesResponse(this, (GetPaginatedGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetPaginatedGroupMemberAttributesResponseDelegate){
            ((GetPaginatedGroupMemberAttributesResponseDelegate)delegate).onGetPaginatedGroupMemberAttributesResponse(this, new GetPaginatedGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetPaginatedGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param pageNumber
	 * @param pageSize
	 * @param passthrough
	 */
	public GetPaginatedGroupMemberAttributesRequest(String groupName, 
			String groupType, ArrayList<String> attributeNames, int pageNumber, 
			int pageSize, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPaginatedGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param pageNumber
	 * @param pageSize
	 */
	public GetPaginatedGroupMemberAttributesRequest(String groupName, 
			String groupType, ArrayList<String> attributeNames, int pageNumber, 
			int pageSize){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;

	}

	/**
	 * Default Constructor for GetPaginatedGroupMemberAttributesRequest
	*/
	public GetPaginatedGroupMemberAttributesRequest(){

	}
}
/** @} */
