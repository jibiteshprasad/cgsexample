/**
 * @file CGSHeader.java
 * @page classCGSHeader CGSHeader
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CGSHeader: (utc_timestamp, messageId, playerId, gameId, messageType)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * 
*/
public class CGSHeader {

	/**
	 * (Optional)
	 */
	public long utc_timestamp;

	/**
	 * (Optional)
	 */
	public String messageId;

	/**
	 * (Optional)
	 */
	public String playerId;

	/**
	 * (Optional)
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public String messageType;

	/**
	 * Constructor for CGSHeader that requires all members
	 * @param utc_timestamp
	 * @param messageId
	 * @param playerId
	 * @param gameId
	 * @param messageType
	 */
	public CGSHeader(long utc_timestamp, String messageId, String playerId, 
			String gameId, String messageType){
		this.utc_timestamp = utc_timestamp;
		this.messageId = messageId;
		this.playerId = playerId;
		this.gameId = gameId;
		this.messageType = messageType;

	}

	/**
	 * Default Constructor for CGSHeader
	*/
	public CGSHeader(){

	}
}
/** @} */
