/**
 * @file GroupAttribute.java
 * @page classGroupAttribute GroupAttribute
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupAttribute: (name, value, type)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A key name/value pair used for group attributes
 * 
*/
public class GroupAttribute {

	/**
	 * Name description of the group attribute (e.g. Team Color)
	 */
	public String name;

	/**
	 * Value of the group attribute (e.g. Red) (Optional)
	 */
	public Object value;

	/**
	 * (Optional)
	 */
	public String type;

	/**
	 * Constructor for GroupAttribute that requires all members
	 * @param name
	 * @param value
	 * @param type
	 */
	public GroupAttribute(String name, Object value, String type){
		this.name = name;
		this.value = value;
		this.type = type;

	}

	/**
	 * Default Constructor for GroupAttribute
	*/
	public GroupAttribute(){

	}
}
/** @} */
