/**
 * @file CompareAndSetCalendarEventStateResponse.java
 * @page classCompareAndSetCalendarEventStateResponse CompareAndSetCalendarEventStateResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CompareAndSetCalendarEventStateResponse: (status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Compares and sets a calendar event state if the comparison is valid.
*/
@JsonTypeName("CompareAndSetCalendarEventStateResponse")
public class CompareAndSetCalendarEventStateResponse extends GluonResponse{

	/**
	 * Constructor for CompareAndSetCalendarEventStateResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CompareAndSetCalendarEventStateResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CompareAndSetCalendarEventStateResponse without super class members
	 */
	public CompareAndSetCalendarEventStateResponse(){

	}
	/**
	 * Constructor for CompareAndSetCalendarEventStateResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CompareAndSetCalendarEventStateResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
