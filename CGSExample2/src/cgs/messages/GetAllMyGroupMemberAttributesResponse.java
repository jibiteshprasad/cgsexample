/**
 * @file GetAllMyGroupMemberAttributesResponse.java
 * @page classGetAllMyGroupMemberAttributesResponse GetAllMyGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllMyGroupMemberAttributesResponse: (groupName, groupType, pageIndex, 
 * 		attributeSize, totalPages, totalAttributeSize, attributes, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information and all group member attributes for 
 * authenticated player will be returned
*/
@JsonTypeName("GetAllMyGroupMemberAttributesResponse")
public class GetAllMyGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Page index for attributes to be retrieved (starts at 0) (Optional)
	 */
	public int pageIndex;

	/**
	 * Attribute size of the current page (Optional)
	 */
	public int attributeSize;

	/**
	 * Total Number of pages (Optional)
	 */
	public int totalPages;

	/**
	 * Total Number of attributes (Optional)
	 */
	public long totalAttributeSize;

	/**
	 * Contains the authenticated player's information and a list of all member 
	 * attributes (Optional)
	 */
	public PlayerAndAttributes attributes;

	/**
	 * Constructor for GetAllMyGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param attributeSize
	 * @param totalPages
	 * @param totalAttributeSize
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetAllMyGroupMemberAttributesResponse(String groupName, 
			String groupType, int pageIndex, int attributeSize, int totalPages, 
			long totalAttributeSize, PlayerAndAttributes attributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.attributeSize = attributeSize;
		this.totalPages = totalPages;
		this.totalAttributeSize = totalAttributeSize;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllMyGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param attributeSize
	 * @param totalPages
	 * @param totalAttributeSize
	 * @param attributes
	 */
	public GetAllMyGroupMemberAttributesResponse(String groupName, 
			String groupType, int pageIndex, int attributeSize, int totalPages, 
			long totalAttributeSize, PlayerAndAttributes attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.attributeSize = attributeSize;
		this.totalPages = totalPages;
		this.totalAttributeSize = totalAttributeSize;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetAllMyGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetAllMyGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetAllMyGroupMemberAttributesResponse
	*/
	public GetAllMyGroupMemberAttributesResponse(){

	}
}
/** @} */
