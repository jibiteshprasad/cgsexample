/**
 * @file SentJoinGroupRequest.java
 * @page classSentJoinGroupRequest SentJoinGroupRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SentJoinGroupRequest: (requestId, groupName, groupType, createDate)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Contains join request information for the player who wants to join the 
 * specified group
*/
public class SentJoinGroupRequest {

	/**
	 * Unique request ID for the request instance   
	 */
	public String requestId;

	/**
	 * Name of the group that the player wants to join   
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Date and time when the request was originally sent (Optional)
	 */
	public Date createDate;

	/**
	 * Constructor for SentJoinGroupRequest that requires all members
	 * @param requestId
	 * @param groupName
	 * @param groupType
	 * @param createDate
	 */
	public SentJoinGroupRequest(String requestId, String groupName, 
			String groupType, Date createDate){
		this.requestId = requestId;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createDate = createDate;

	}

	/**
	 * Default Constructor for SentJoinGroupRequest
	*/
	public SentJoinGroupRequest(){

	}
}
/** @} */
