/**
 * @file PlayerAndAttributes.java
 * @page classPlayerAndAttributes PlayerAndAttributes
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class PlayerAndAttributes: (playerInfo, attributes)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all the attributes for a specified player
 * 
*/
public class PlayerAndAttributes {

	/**
	 * The player whose attributes were received (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * A list of all the player's attributes (Optional)
	 */
	public List<PlayerAttribute> attributes;

	/**
	 * Constructor for PlayerAndAttributes that requires all members
	 * @param playerInfo
	 * @param attributes
	 */
	public PlayerAndAttributes(PlayerInfo playerInfo, 
			List<PlayerAttribute> attributes){
		this.playerInfo = playerInfo;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for PlayerAndAttributes
	*/
	public PlayerAndAttributes(){

	}
}
/** @} */
