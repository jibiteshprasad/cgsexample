/**
 * @file DeleteJoinGroupInviteRequest.java
 * @page classDeleteJoinGroupInviteRequest DeleteJoinGroupInviteRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteJoinGroupInviteRequest: (invitiationId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes an invite that was sent by the authenticated player
 * 
*/
@JsonTypeName("DeleteJoinGroupInviteRequest")
public class DeleteJoinGroupInviteRequest extends GluonRequest{

	/**
	 * Unique Invitation ID for the invite that will be deleted
	 */
	public String invitiationId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteJoinGroupInviteResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteJoinGroupInviteResponseDelegate){
                ((DeleteJoinGroupInviteResponseDelegate)delegate).onDeleteJoinGroupInviteResponse(this, (DeleteJoinGroupInviteResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteJoinGroupInviteResponseDelegate){
            ((DeleteJoinGroupInviteResponseDelegate)delegate).onDeleteJoinGroupInviteResponse(this, new DeleteJoinGroupInviteResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteJoinGroupInviteRequest that requires all members
	 * @param invitiationId
	 * @param passthrough
	 */
	public DeleteJoinGroupInviteRequest(String invitiationId, String passthrough){
		this.invitiationId = invitiationId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteJoinGroupInviteRequest without super class members
	 * @param invitiationId
	 */
	public DeleteJoinGroupInviteRequest(String invitiationId){
		this.invitiationId = invitiationId;

	}

	/**
	 * Default Constructor for DeleteJoinGroupInviteRequest
	*/
	public DeleteJoinGroupInviteRequest(){

	}
}
/** @} */
