/**
 * @file GetAllAttributesResponse.java
 * @page classGetAllAttributesResponse GetAllAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllAttributesResponse: (playerInfo, attributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all the attributes for a specified player
 * 
*/
@JsonTypeName("GetAllAttributesResponse")
public class GetAllAttributesResponse extends GluonResponse{

	/**
	 * The player whose attributes were received (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * A list of all the player's attributes (Optional)
	 */
	public List<PlayerAttribute> attributes;

	/**
	 * Constructor for GetAllAttributesResponse that requires all members
	 * @param playerInfo
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetAllAttributesResponse(PlayerInfo playerInfo, 
			List<PlayerAttribute> attributes, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.playerInfo = playerInfo;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllAttributesResponse without super class members
	 * @param playerInfo
	 * @param attributes
	 */
	public GetAllAttributesResponse(PlayerInfo playerInfo, 
			List<PlayerAttribute> attributes){
		this.playerInfo = playerInfo;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetAllAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetAllAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetAllAttributesResponse
	*/
	public GetAllAttributesResponse(){

	}
}
/** @} */
