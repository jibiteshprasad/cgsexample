/**
 * @file DeleteMyGroupMemberAttributesResponse.java
 * @page classDeleteMyGroupMemberAttributesResponse DeleteMyGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteMyGroupMemberAttributesResponse: (groupName, groupType, 
 * 		memberInfo, status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon successful deletion, the authenticated player's information will be 
 * returned
*/
@JsonTypeName("DeleteMyGroupMemberAttributesResponse")
public class DeleteMyGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where group attributes were deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Player information for the member whose attributes were deleted 
	 * (Optional)
	 */
	public PlayerInfo memberInfo;

	/**
	 * Constructor for DeleteMyGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param memberInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteMyGroupMemberAttributesResponse(String groupName, 
			String groupType, PlayerInfo memberInfo, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.memberInfo = memberInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteMyGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param memberInfo
	 */
	public DeleteMyGroupMemberAttributesResponse(String groupName, 
			String groupType, PlayerInfo memberInfo){
		this.groupName = groupName;
		this.groupType = groupType;
		this.memberInfo = memberInfo;

	}
	/**
	 * Constructor for DeleteMyGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteMyGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteMyGroupMemberAttributesResponse
	*/
	public DeleteMyGroupMemberAttributesResponse(){

	}
}
/** @} */
