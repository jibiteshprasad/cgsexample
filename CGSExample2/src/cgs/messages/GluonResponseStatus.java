package cgs.messages;

public enum GluonResponseStatus
	{
	/// Request was processed properly by the backend server
	SUCCESS,
	/// Request failed while being processed by the backend
	FAILURE
	
}
