/**
 * @file DeleteAttributesRequest.java
 * @page classDeleteAttributesRequest DeleteAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAttributesRequest: (playerId, attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes multiple attributes for a specified player
 * 
*/
@JsonTypeName("DeleteAttributesRequest")
public class DeleteAttributesRequest extends GluonRequest{

	/**
	 * ID of the player whose attributes will be deleted
	 */
	public String playerId;

	/**
	 * A list of attribute names of the attributes to be deleted
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteAttributesResponseDelegate){
                ((DeleteAttributesResponseDelegate)delegate).onDeleteAttributesResponse(this, (DeleteAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteAttributesResponseDelegate){
            ((DeleteAttributesResponseDelegate)delegate).onDeleteAttributesResponse(this, new DeleteAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteAttributesRequest that requires all members
	 * @param playerId
	 * @param attributeNames
	 * @param passthrough
	 */
	public DeleteAttributesRequest(String playerId, 
			ArrayList<String> attributeNames, String passthrough){
		this.playerId = playerId;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAttributesRequest without super class members
	 * @param playerId
	 * @param attributeNames
	 */
	public DeleteAttributesRequest(String playerId, 
			ArrayList<String> attributeNames){
		this.playerId = playerId;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for DeleteAttributesRequest
	*/
	public DeleteAttributesRequest(){

	}
}
/** @} */
