/**
 * @file ApplyDeltaToGroupAttributeResponse.java
 * @page classApplyDeltaToGroupAttributeResponse ApplyDeltaToGroupAttributeResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApplyDeltaToGroupAttributeResponse: (groupName, groupType, attribute, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the group attribute's value has been updated with the 
 * added delta.
*/
@JsonTypeName("ApplyDeltaToGroupAttributeResponse")
public class ApplyDeltaToGroupAttributeResponse extends GluonResponse{

	/**
	 * Name of the group whose attribute was just updated
	 */
	public String groupName;

	/**
	 * Type of the group whose attribute was just updated
	 */
	public String groupType;

	/**
	 * Updated attribute after applying delta value
	 */
	public GroupAttribute attribute;

	/**
	 * Constructor for ApplyDeltaToGroupAttributeResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attribute
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ApplyDeltaToGroupAttributeResponse(String groupName, String groupType, 
			GroupAttribute attribute, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attribute = attribute;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApplyDeltaToGroupAttributeResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param attribute
	 */
	public ApplyDeltaToGroupAttributeResponse(String groupName, String groupType, 
			GroupAttribute attribute){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attribute = attribute;

	}
	/**
	 * Constructor for ApplyDeltaToGroupAttributeResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ApplyDeltaToGroupAttributeResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ApplyDeltaToGroupAttributeResponse
	*/
	public ApplyDeltaToGroupAttributeResponse(){

	}
}
/** @} */
