/**
 * @file SetGroupMemberAttributesResponse.java
 * @page classSetGroupMemberAttributesResponse SetGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupMemberAttributesResponse: (groupName, groupType, members, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information for all members whose attributes were 
 * set will be returned
*/
@JsonTypeName("SetGroupMemberAttributesResponse")
public class SetGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were set
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Player information for the members whose attributes were set (Optional)
	 */
	public ArrayList<PlayerInfo> members;

	/**
	 * Constructor for SetGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetGroupMemberAttributesResponse(String groupName, String groupType, 
			ArrayList<PlayerInfo> members, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param members
	 */
	public SetGroupMemberAttributesResponse(String groupName, String groupType, 
			ArrayList<PlayerInfo> members){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;

	}
	/**
	 * Constructor for SetGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetGroupMemberAttributesResponse
	*/
	public SetGroupMemberAttributesResponse(){

	}
}
/** @} */
