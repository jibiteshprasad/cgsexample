/**
 * @file SetAttributesResponse.java
 * @page classSetAttributesResponse SetAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAttributesResponse: (playerInfo, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether attributes have been set
 * 
*/
@JsonTypeName("SetAttributesResponse")
public class SetAttributesResponse extends GluonResponse{

	/**
	 *	The player that had attributes set (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for SetAttributesResponse that requires all members
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetAttributesResponse(PlayerInfo playerInfo, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAttributesResponse without super class members
	 * @param playerInfo
	 */
	public SetAttributesResponse(PlayerInfo playerInfo){
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for SetAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetAttributesResponse
	*/
	public SetAttributesResponse(){

	}
}
/** @} */
