package cgs.messages;

public enum DisconnectReason
	{
	/// Client SDK triggered Disconnect() or GluonClient is being shutdown.
	ClientInitiated,
	/// Client connection to the Server was lost. Attempt to reconnect.
	ServerDisconnected,
	/// Disconnection happened due to a Network Exception. Check network connectivity before proceeding.
	NetworkError
	
}
