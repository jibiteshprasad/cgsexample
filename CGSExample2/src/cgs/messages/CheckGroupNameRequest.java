/**
 * @file CheckGroupNameRequest.java
 * @page classCheckGroupNameRequest CheckGroupNameRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CheckGroupNameRequest: (groupName, groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Confirms whether a group name is available for use. If the name is available, 
 * GluonResponseStatus will return SUCCESS. If the name is not available, 
 * GluonResponseStatus will return FAILURE and the ErrorType will be 
 * GROUP_ALREADY_EXISTS.
*/
@JsonTypeName("CheckGroupNameRequest")
public class CheckGroupNameRequest extends GluonRequest{

	/**
	 * Group name to be confirmed on the backend server
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CheckGroupNameResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CheckGroupNameResponseDelegate){
                ((CheckGroupNameResponseDelegate)delegate).onCheckGroupNameResponse(this, (CheckGroupNameResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CheckGroupNameResponseDelegate){
            ((CheckGroupNameResponseDelegate)delegate).onCheckGroupNameResponse(this, new CheckGroupNameResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CheckGroupNameRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public CheckGroupNameRequest(String groupName, String groupType, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CheckGroupNameRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public CheckGroupNameRequest(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for CheckGroupNameRequest
	*/
	public CheckGroupNameRequest(){

	}
}
/** @} */
