/**
 * @file NotificationType.java
 * @page classNotificationType NotificationType
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class NotificationType: (STARTED, ENDED)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Notification state
*/
public class NotificationType {

	/**
	 * (Optional)
	 */
	public NotificationType STARTED;

	/**
	 * (Optional)
	 */
	public NotificationType ENDED;

	/**
	 * Constructor for NotificationType that requires all members
	 * @param STARTED
	 * @param ENDED
	 */
	public NotificationType(NotificationType STARTED, NotificationType ENDED){
		this.STARTED = STARTED;
		this.ENDED = ENDED;

	}

	/**
	 * Default Constructor for NotificationType
	*/
	public NotificationType(){

	}
}
/** @} */
