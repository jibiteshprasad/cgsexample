/**
 * @file RemovePlayerFromGroupResponse.java
 * @page classRemovePlayerFromGroupResponse RemovePlayerFromGroupResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemovePlayerFromGroupResponse: (groupName, groupType, playerInfo, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player making the request confirming whether the 
 * player specified was removed successfully
*/
@JsonTypeName("RemovePlayerFromGroupResponse")
public class RemovePlayerFromGroupResponse extends GluonResponse{

	/**
	 * Name of the group the specified player was removed from   
	 */
	public String groupName;

	/**
	 * Name of the group's type   
	 */
	public String groupType;

	/**
	 * The player that was removed from the group (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for RemovePlayerFromGroupResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public RemovePlayerFromGroupResponse(String groupName, String groupType, 
			PlayerInfo playerInfo, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemovePlayerFromGroupResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerInfo
	 */
	public RemovePlayerFromGroupResponse(String groupName, String groupType, 
			PlayerInfo playerInfo){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for RemovePlayerFromGroupResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public RemovePlayerFromGroupResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for RemovePlayerFromGroupResponse
	*/
	public RemovePlayerFromGroupResponse(){

	}
}
/** @} */
