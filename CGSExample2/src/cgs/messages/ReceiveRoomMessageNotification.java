/**
 * @file ReceiveRoomMessageNotification.java
 * @page classReceiveRoomMessageNotification ReceiveRoomMessageNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ReceiveRoomMessageNotification: (fromPlayerInfo, room, message, 
 * 		timestamp, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returned to all players who are currently in the chat room when a room 
 * message is sent
*/
@JsonTypeName("ReceiveRoomMessage")
public class ReceiveRoomMessageNotification extends GluonRequest{

	/**
	 * The player who sent the message to the room (Optional)
	 */
	public PlayerInfo fromPlayerInfo;

	/**
	 * Name of the room where the message was sent
	 */
	public String room;

	/**
	 * Message that was sent to the room (Optional)
	 */
	public String message;

	/**
	 * Date and Time from when this message was originally sent (Optional)
	 */
	public long timestamp;

	/**
	 * Constructor for ReceiveRoomMessageNotification that requires all members
	 * @param fromPlayerInfo
	 * @param room
	 * @param message
	 * @param timestamp
	 * @param passthrough
	 */
	public ReceiveRoomMessageNotification(PlayerInfo fromPlayerInfo, String room, 
			String message, long timestamp, String passthrough){
		this.fromPlayerInfo = fromPlayerInfo;
		this.room = room;
		this.message = message;
		this.timestamp = timestamp;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ReceiveRoomMessageNotification without super class members
	 * @param fromPlayerInfo
	 * @param room
	 * @param message
	 * @param timestamp
	 */
	public ReceiveRoomMessageNotification(PlayerInfo fromPlayerInfo, String room, 
			String message, long timestamp){
		this.fromPlayerInfo = fromPlayerInfo;
		this.room = room;
		this.message = message;
		this.timestamp = timestamp;

	}

	/**
	 * Default Constructor for ReceiveRoomMessageNotification
	*/
	public ReceiveRoomMessageNotification(){

	}
}
/** @} */
