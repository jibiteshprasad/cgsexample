/**
 * @file SetGroupMemberAttributesRequest.java
 * @page classSetGroupMemberAttributesRequest SetGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupMemberAttributesRequest: (groupName, groupType, playerIds, 
 * 		attributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets group member attributes, which are specified in the attributes list, for 
 * members specifed in the playerIds list
*/
@JsonTypeName("SetGroupMemberAttributesRequest")
public class SetGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where member attributes will be set
	 */
	public String groupName;

	/**
	 * Name of the group's type  (Optional)
	 */
	public String groupType;

	/**
	 * List of playerIds for group members whose attributes will be set
	 */
	public ArrayList<String> playerIds;

	/**
	 * List of player attributes (both name and value) that will be set for 
	 * above group members
	 */
	public ArrayList<PlayerAttribute> attributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetGroupMemberAttributesResponseDelegate){
                ((SetGroupMemberAttributesResponseDelegate)delegate).onSetGroupMemberAttributesResponse(this, (SetGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetGroupMemberAttributesResponseDelegate){
            ((SetGroupMemberAttributesResponseDelegate)delegate).onSetGroupMemberAttributesResponse(this, new SetGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributes
	 * @param passthrough
	 */
	public SetGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, ArrayList<PlayerAttribute> attributes, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributes = attributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributes
	 */
	public SetGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, ArrayList<PlayerAttribute> attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for SetGroupMemberAttributesRequest
	*/
	public SetGroupMemberAttributesRequest(){

	}
}
/** @} */
