/**
 * @file SetDocumentRequest.java
 * @page classSetDocumentRequest SetDocumentRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetDocumentRequest: (tags, document, mode, ownerType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upload a document (byte array) to %Gluon file storage
 * 
*/
@JsonTypeName("SetDocumentRequest")
public class SetDocumentRequest extends GluonRequest{

	/**
	 * Categorize the document as you see fit (e.g. something like 
	 * "DeerScreenshot" or "AttackReplay") (Optional)
	 */
	public List<String> tags;

	/**
	 * The file or in memory data to be uploaded
	 */
	public byte[] document;

	/**
	 * Specifies whether we are replacing/overwriting an existing file 
	 * (Optional)
	 */
	public ExistingDocumentMode mode;

	/**
	 * (Optional)
	 */
	public DocumentOwnerType ownerType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetDocumentResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetDocumentResponseDelegate){
                ((SetDocumentResponseDelegate)delegate).onSetDocumentResponse(this, (SetDocumentResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetDocumentResponseDelegate){
            ((SetDocumentResponseDelegate)delegate).onSetDocumentResponse(this, new SetDocumentResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetDocumentRequest that requires all members
	 * @param tags
	 * @param document
	 * @param mode
	 * @param ownerType
	 * @param passthrough
	 */
	public SetDocumentRequest(List<String> tags, byte[] document, 
			ExistingDocumentMode mode, DocumentOwnerType ownerType, 
			String passthrough){
		this.tags = tags;
		this.document = document;
		this.mode = mode;
		this.ownerType = ownerType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetDocumentRequest without super class members
	 * @param tags
	 * @param document
	 * @param mode
	 * @param ownerType
	 */
	public SetDocumentRequest(List<String> tags, byte[] document, 
			ExistingDocumentMode mode, DocumentOwnerType ownerType){
		this.tags = tags;
		this.document = document;
		this.mode = mode;
		this.ownerType = ownerType;

	}

	/**
	 * Default Constructor for SetDocumentRequest
	*/
	public SetDocumentRequest(){

	}
}
/** @} */
