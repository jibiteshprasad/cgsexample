/**
 * @file CustomGameServerMessageRequest.java
 * @page classCustomGameServerMessageRequest CustomGameServerMessageRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CustomGameServerMessageRequest: (payload, cgsId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to the custom game server
 * 
*/
@JsonTypeName("CustomGameServerRequest")
public class CustomGameServerMessageRequest extends GluonRequest{

	/**
	 * The payload identifying the request type and parameters
	 */
	public Object payload;

	/**
	 * The id of the custom game server
	 */
	public String cgsId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CustomGameServerMessageResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CustomGameServerMessageResponseDelegate){
                ((CustomGameServerMessageResponseDelegate)delegate).onCustomGameServerMessageResponse(this, (CustomGameServerMessageResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CustomGameServerMessageResponseDelegate){
            ((CustomGameServerMessageResponseDelegate)delegate).onCustomGameServerMessageResponse(this, new CustomGameServerMessageResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CustomGameServerMessageRequest that requires all members
	 * @param payload
	 * @param cgsId
	 * @param passthrough
	 */
	public CustomGameServerMessageRequest(Object payload, String cgsId, 
			String passthrough){
		this.payload = payload;
		this.cgsId = cgsId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CustomGameServerMessageRequest without super class members
	 * @param payload
	 * @param cgsId
	 */
	public CustomGameServerMessageRequest(Object payload, String cgsId){
		this.payload = payload;
		this.cgsId = cgsId;

	}

	/**
	 * Default Constructor for CustomGameServerMessageRequest
	*/
	public CustomGameServerMessageRequest(){

	}
}
/** @} */
