/**
 * @file GetLeaderboardEntriesResponse.java
 * @page classGetLeaderboardEntriesResponse GetLeaderboardEntriesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetLeaderboardEntriesResponse: (leaderboardName, entries, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the specified entries of the leaderboard were retrieved 
 * (and thus each RankedLeaderboardEntry in 'entries' will be populated with the 
 * name and value, as well as the rank on the overall leaderboard). The List of 
 * entries returned will also be sorted, in descending order.
*/
@JsonTypeName("GetLeaderboardEntriesResponse")
public class GetLeaderboardEntriesResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * The entries retrieved, each populated with its value and overall rank 
	 * (Optional)
	 */
	public List<RankedLeaderboardEntry> entries;

	/**
	 * Constructor for GetLeaderboardEntriesResponse that requires all members
	 * @param leaderboardName
	 * @param entries
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetLeaderboardEntriesResponse(String leaderboardName, 
			List<RankedLeaderboardEntry> entries, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.leaderboardName = leaderboardName;
		this.entries = entries;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetLeaderboardEntriesResponse without super class members
	 * @param leaderboardName
	 * @param entries
	 */
	public GetLeaderboardEntriesResponse(String leaderboardName, 
			List<RankedLeaderboardEntry> entries){
		this.leaderboardName = leaderboardName;
		this.entries = entries;

	}
	/**
	 * Constructor for GetLeaderboardEntriesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetLeaderboardEntriesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetLeaderboardEntriesResponse
	*/
	public GetLeaderboardEntriesResponse(){

	}
}
/** @} */
