/**
 * @file Document.java
 * @page classDocument Document
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class Document: (ownerType, ownerId, id, tags, data, expires, startDate, 
 * 		endDate)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Encapsulates the raw data of a document retrieved from storage, and includes 
 * some associated info
*/
public class Document {

	/**
	 * Distinguish whether this was a player-uploaded document or backend/admin 
	 * managed
	 */
	public String ownerType;

	/**
	 * Either playerId of the player who uploaded the document, or an 
	 * identifier for the administrator  who uploaded the document 
	 */
	public String ownerId;

	/**
	 * documentId of the document
	 */
	public String id;

	/**
	 * Categorical identifier strings to describe the document (Optional)
	 */
	public List<String> tags;

	/**
	 * The raw document data (Optional)
	 */
	public byte[] data;

	/**
	 * Only applicable for backend-owned documents - this is when the document 
	 * will no longer be valid (Optional)
	 */
	public Date expires;

	/**
	 * Only applicable for backend-owned documents - this is when the document 
	 * becomes valid (Optional)
	 */
	public Date startDate;

	/**
	 * Only applicable for backend-owned documents - this is when the document 
	 * will no longer be valid (Optional)
	 */
	public Date endDate;

	/**
	 * Constructor for Document that requires all members
	 * @param ownerType
	 * @param ownerId
	 * @param id
	 * @param tags
	 * @param data
	 * @param expires
	 * @param startDate
	 * @param endDate
	 */
	public Document(String ownerType, String ownerId, String id, 
			List<String> tags, byte[] data, Date expires, Date startDate, 
			Date endDate){
		this.ownerType = ownerType;
		this.ownerId = ownerId;
		this.id = id;
		this.tags = tags;
		this.data = data;
		this.expires = expires;
		this.startDate = startDate;
		this.endDate = endDate;

	}

	/**
	 * Default Constructor for Document
	*/
	public Document(){

	}
}
/** @} */
