/**
 * @file ClearLeaderboardResponse.java
 * @page classClearLeaderboardResponse ClearLeaderboardResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClearLeaderboardResponse: (leaderboardName, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, then all entries in the leaderboard were removed.
 * 
*/
@JsonTypeName("ClearLeaderboardResponse")
public class ClearLeaderboardResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * Constructor for ClearLeaderboardResponse that requires all members
	 * @param leaderboardName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ClearLeaderboardResponse(String leaderboardName, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.leaderboardName = leaderboardName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClearLeaderboardResponse without super class members
	 * @param leaderboardName
	 */
	public ClearLeaderboardResponse(String leaderboardName){
		this.leaderboardName = leaderboardName;

	}
	/**
	 * Constructor for ClearLeaderboardResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ClearLeaderboardResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ClearLeaderboardResponse
	*/
	public ClearLeaderboardResponse(){

	}
}
/** @} */
