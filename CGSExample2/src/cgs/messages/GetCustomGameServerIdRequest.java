/**
 * @file GetCustomGameServerIdRequest.java
 * @page classGetCustomGameServerIdRequest GetCustomGameServerIdRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCustomGameServerIdRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A message sent to a game server requesting identification.
*/
@JsonTypeName("GetCustomGameServerIdRequest")
public class GetCustomGameServerIdRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCustomGameServerIdResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCustomGameServerIdResponseDelegate){
                ((GetCustomGameServerIdResponseDelegate)delegate).onGetCustomGameServerIdResponse(this, (GetCustomGameServerIdResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCustomGameServerIdResponseDelegate){
            ((GetCustomGameServerIdResponseDelegate)delegate).onGetCustomGameServerIdResponse(this, new GetCustomGameServerIdResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCustomGameServerIdRequest that requires all members
	 * @param passthrough
	 */
	public GetCustomGameServerIdRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCustomGameServerIdRequest without super class members
	 */
	public GetCustomGameServerIdRequest(){

	}
}
/** @} */
