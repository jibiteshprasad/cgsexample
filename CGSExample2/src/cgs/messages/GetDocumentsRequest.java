/**
 * @file GetDocumentsRequest.java
 * @page classGetDocumentsRequest GetDocumentsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetDocumentsRequest: (ownerType, ownerId, tags, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve/download all documents (previously stored by this player) labeled 
 * with any of the tags passed
*/
@JsonTypeName("GetDocumentsRequest")
public class GetDocumentsRequest extends GluonRequest{

	/**
	 * specify whether the document belongs to a player, or is game-wide 
	 * (Optional)
	 */
	public DocumentOwnerType ownerType;

	/**
	 * the playerId of the player who uploaded the document; if ownerType is 
	 * GAME, this should be null; when ownerType is PLAYER, this should be the 
	 * playerId of the player who Set the document (Optional)
	 */
	public String ownerId;

	/**
	 * Strings that categorize each document (Optional)
	 */
	public List<String> tags;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetDocumentsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetDocumentsResponseDelegate){
                ((GetDocumentsResponseDelegate)delegate).onGetDocumentsResponse(this, (GetDocumentsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetDocumentsResponseDelegate){
            ((GetDocumentsResponseDelegate)delegate).onGetDocumentsResponse(this, new GetDocumentsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetDocumentsRequest that requires all members
	 * @param ownerType
	 * @param ownerId
	 * @param tags
	 * @param passthrough
	 */
	public GetDocumentsRequest(DocumentOwnerType ownerType, String ownerId, 
			List<String> tags, String passthrough){
		this.ownerType = ownerType;
		this.ownerId = ownerId;
		this.tags = tags;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetDocumentsRequest without super class members
	 * @param ownerType
	 * @param ownerId
	 * @param tags
	 */
	public GetDocumentsRequest(DocumentOwnerType ownerType, String ownerId, 
			List<String> tags){
		this.ownerType = ownerType;
		this.ownerId = ownerId;
		this.tags = tags;

	}

	/**
	 * Default Constructor for GetDocumentsRequest
	*/
	public GetDocumentsRequest(){

	}
}
/** @} */
