/**
 * @file DeleteMailRequest.java
 * @page classDeleteMailRequest DeleteMailRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteMailRequest: (id, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes a mail message from the authenticated player's inbox
 * 
*/
@JsonTypeName("DeleteMailMessageRequest")
public class DeleteMailRequest extends GluonRequest{

	/**
	 * Unique message ID of the mail message to be deleted
	 */
	public String id;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteMailResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteMailResponseDelegate){
                ((DeleteMailResponseDelegate)delegate).onDeleteMailResponse(this, (DeleteMailResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteMailResponseDelegate){
            ((DeleteMailResponseDelegate)delegate).onDeleteMailResponse(this, new DeleteMailResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteMailRequest that requires all members
	 * @param id
	 * @param passthrough
	 */
	public DeleteMailRequest(String id, String passthrough){
		this.id = id;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteMailRequest without super class members
	 * @param id
	 */
	public DeleteMailRequest(String id){
		this.id = id;

	}

	/**
	 * Default Constructor for DeleteMailRequest
	*/
	public DeleteMailRequest(){

	}
}
/** @} */
