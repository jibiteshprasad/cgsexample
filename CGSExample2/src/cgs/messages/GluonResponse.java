/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cgs.messages;

/**
 *
 * @author root
 */
public class GluonResponse extends GluonMessage{
    public ErrorType errorType;
    public GluonResponseStatus status;
    public String description;
    public ErrorAction action;
    
    public GluonResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
        this.status = status;
        this.errorType = errorType;
        this.action = action;
        this.description = description;
    }
    
    public GluonResponse(){

    }
}
