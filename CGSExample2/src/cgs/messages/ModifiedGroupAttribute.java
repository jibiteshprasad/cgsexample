/**
 * @file ModifiedGroupAttribute.java
 * @page classModifiedGroupAttribute ModifiedGroupAttribute
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ModifiedGroupAttribute: (name, value)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Information about a single Group Attribute that has been modified
 * 
*/
public class ModifiedGroupAttribute {

	/**
	 * Name description of the group attribute (e.g. Team Color)
	 */
	public String name;

	/**
	 * New value of the group attribute (e.g. Red)
	 */
	public Object value;

	/**
	 * Constructor for ModifiedGroupAttribute that requires all members
	 * @param name
	 * @param value
	 */
	public ModifiedGroupAttribute(String name, Object value){
		this.name = name;
		this.value = value;

	}

	/**
	 * Default Constructor for ModifiedGroupAttribute
	*/
	public ModifiedGroupAttribute(){

	}
}
/** @} */
