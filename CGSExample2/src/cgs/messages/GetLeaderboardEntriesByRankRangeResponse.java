/**
 * @file GetLeaderboardEntriesByRankRangeResponse.java
 * @page classGetLeaderboardEntriesByRankRangeResponse GetLeaderboardEntriesByRankRangeResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetLeaderboardEntriesByRankRangeResponse: (leaderboardName, entries, 
 * 		totalEntries, status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is successful, the entries of the leaderboard within the specified 
 * rank range were retrieved, populated with their name, value, and rank, and 
 * sorted in descending order.
*/
@JsonTypeName("GetLeaderboardEntriesByRankRangeResponse")
public class GetLeaderboardEntriesByRankRangeResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * The entries retrieved, each populated with its name, value and overall 
	 * rank (Optional)
	 */
	public List<RankedLeaderboardEntry> entries;

	/**
	 * The number of entries retrieved (maxRank-minRank + 1, unless the 
	 * leaderboard has less entries than the maxRank number) (Optional)
	 */
	public long totalEntries;

	/**
	 * Constructor for GetLeaderboardEntriesByRankRangeResponse that requires all members
	 * @param leaderboardName
	 * @param entries
	 * @param totalEntries
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetLeaderboardEntriesByRankRangeResponse(String leaderboardName, 
			List<RankedLeaderboardEntry> entries, long totalEntries, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.leaderboardName = leaderboardName;
		this.entries = entries;
		this.totalEntries = totalEntries;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetLeaderboardEntriesByRankRangeResponse without super class members
	 * @param leaderboardName
	 * @param entries
	 * @param totalEntries
	 */
	public GetLeaderboardEntriesByRankRangeResponse(String leaderboardName, 
			List<RankedLeaderboardEntry> entries, long totalEntries){
		this.leaderboardName = leaderboardName;
		this.entries = entries;
		this.totalEntries = totalEntries;

	}
	/**
	 * Constructor for GetLeaderboardEntriesByRankRangeResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetLeaderboardEntriesByRankRangeResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetLeaderboardEntriesByRankRangeResponse
	*/
	public GetLeaderboardEntriesByRankRangeResponse(){

	}
}
/** @} */
