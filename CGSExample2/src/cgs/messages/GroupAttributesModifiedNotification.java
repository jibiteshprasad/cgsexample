/**
 * @file GroupAttributesModifiedNotification.java
 * @page classGroupAttributesModifiedNotification GroupAttributesModifiedNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupAttributesModifiedNotification: (listGroupAndModifiedAttributes, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Notification to a player about changes to one or more group attributes if a 
 * player is subscribed using SubscribeToGroupNotificationsRequest. This is used 
 * for getting pushed updates about changes to one or more groups without having 
 * to frequently poll for these updates. Note that this notification is only 
 * triggered when attributes are modified, not when attributes are deleted.
*/
@JsonTypeName("GroupAttributesModifiedNotification")
public class GroupAttributesModifiedNotification extends GluonRequest{

	/**
	 * Collection of Groups and their attributes that were modified (Optional)
	 */
	public List<GroupAndModifiedAttributes> listGroupAndModifiedAttributes;

	/**
	 * Constructor for GroupAttributesModifiedNotification that requires all members
	 * @param listGroupAndModifiedAttributes
	 * @param passthrough
	 */
	public GroupAttributesModifiedNotification(List<GroupAndModifiedAttributes> listGroupAndModifiedAttributes, String passthrough){
		this.listGroupAndModifiedAttributes = listGroupAndModifiedAttributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GroupAttributesModifiedNotification without super class members
	 * @param listGroupAndModifiedAttributes
	 */
	public GroupAttributesModifiedNotification(List<GroupAndModifiedAttributes> listGroupAndModifiedAttributes){
		this.listGroupAndModifiedAttributes = listGroupAndModifiedAttributes;

	}

	/**
	 * Default Constructor for GroupAttributesModifiedNotification
	*/
	public GroupAttributesModifiedNotification(){

	}
}
/** @} */
