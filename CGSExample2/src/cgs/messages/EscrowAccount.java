/**
 * @file EscrowAccount.java
 * @page classEscrowAccount EscrowAccount
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class EscrowAccount: (ownerType, ownerId)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * An Escrow Account abstraction
 * 
*/
public class EscrowAccount {

	/**
	 * Type of the owner (Can either be PLAYER or GROUP)
	 */
	public String ownerType;

	/**
	 * PlayerId of the account owner
	 */
	public String ownerId;

	/**
	 * Constructor for EscrowAccount that requires all members
	 * @param ownerType
	 * @param ownerId
	 */
	public EscrowAccount(String ownerType, String ownerId){
		this.ownerType = ownerType;
		this.ownerId = ownerId;

	}

	/**
	 * Default Constructor for EscrowAccount
	*/
	public EscrowAccount(){

	}
}
/** @} */
