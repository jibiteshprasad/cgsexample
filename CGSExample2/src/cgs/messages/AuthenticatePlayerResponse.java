/**
 * @file AuthenticatePlayerResponse.java
 * @page classAuthenticatePlayerResponse AuthenticatePlayerResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AuthenticatePlayerResponse: (loginId, playerInfo, session, expireTime, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the player account was verified and you are now 
 * authorized to use %Gluon services
*/
@JsonTypeName("AuthenticatePlayerResponse")
public class AuthenticatePlayerResponse extends GluonResponse{

	/**
	 * Unique player identifier passed to future requests (Optional)
	 */
	public String loginId;

	/**
	 * The authenticated player (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Guid passed to future requests  (Optional)
	 */
	public String session;

	/**
	 * This is unused currently, so you can ignore. (Optional)
	 */
	public Calendar expireTime;

	/**
	 * Constructor for AuthenticatePlayerResponse that requires all members
	 * @param loginId
	 * @param playerInfo
	 * @param session
	 * @param expireTime
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public AuthenticatePlayerResponse(String loginId, PlayerInfo playerInfo, 
			String session, Calendar expireTime, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.loginId = loginId;
		this.playerInfo = playerInfo;
		this.session = session;
		this.expireTime = expireTime;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AuthenticatePlayerResponse without super class members
	 * @param loginId
	 * @param playerInfo
	 * @param session
	 * @param expireTime
	 */
	public AuthenticatePlayerResponse(String loginId, PlayerInfo playerInfo, 
			String session, Calendar expireTime){
		this.loginId = loginId;
		this.playerInfo = playerInfo;
		this.session = session;
		this.expireTime = expireTime;

	}
	/**
	 * Constructor for AuthenticatePlayerResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public AuthenticatePlayerResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for AuthenticatePlayerResponse
	*/
	public AuthenticatePlayerResponse(){

	}
}
/** @} */
