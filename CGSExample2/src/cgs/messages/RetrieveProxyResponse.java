/**
 * @file RetrieveProxyResponse.java
 * @page classRetrieveProxyResponse RetrieveProxyResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RetrieveProxyResponse: (proxyIp, proxyPort, password, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns IP/port information for proxy server if successful 
 * 
*/
@JsonTypeName("")
public class RetrieveProxyResponse extends GluonResponse{

	/**
	 * Public IP address of proxy server that is available to access (Optional)
	 */
	public String proxyIp;

	/**
	 * Public port of the proxy server (Optional)
	 */
	public String proxyPort;

	/**
	 * Password required to access proxy server (Optional)
	 */
	public String password;

	/**
	 * Constructor for RetrieveProxyResponse that requires all members
	 * @param proxyIp
	 * @param proxyPort
	 * @param password
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public RetrieveProxyResponse(String proxyIp, String proxyPort, 
			String password, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.proxyIp = proxyIp;
		this.proxyPort = proxyPort;
		this.password = password;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RetrieveProxyResponse without super class members
	 * @param proxyIp
	 * @param proxyPort
	 * @param password
	 */
	public RetrieveProxyResponse(String proxyIp, String proxyPort, 
			String password){
		this.proxyIp = proxyIp;
		this.proxyPort = proxyPort;
		this.password = password;

	}
	/**
	 * Constructor for RetrieveProxyResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public RetrieveProxyResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for RetrieveProxyResponse
	*/
	public RetrieveProxyResponse(){

	}
}
/** @} */
