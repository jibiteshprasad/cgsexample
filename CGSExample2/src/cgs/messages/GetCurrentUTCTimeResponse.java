/**
 * @file GetCurrentUTCTimeResponse.java
 * @page classGetCurrentUTCTimeResponse GetCurrentUTCTimeResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCurrentUTCTimeResponse: (time, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns current server time in UTC
 * 
*/
@JsonTypeName("GetCurrentUTCTimeResponse")
public class GetCurrentUTCTimeResponse extends GluonResponse{

	/**
	 * Current Server time in UTC
	 */
	public Date time;

	/**
	 * Constructor for GetCurrentUTCTimeResponse that requires all members
	 * @param time
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCurrentUTCTimeResponse(Date time, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.time = time;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCurrentUTCTimeResponse without super class members
	 * @param time
	 */
	public GetCurrentUTCTimeResponse(Date time){
		this.time = time;

	}
	/**
	 * Constructor for GetCurrentUTCTimeResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCurrentUTCTimeResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCurrentUTCTimeResponse
	*/
	public GetCurrentUTCTimeResponse(){

	}
}
/** @} */
