/**
 * @file RegisterPushNotificationTokenResponse.java
 * @page classRegisterPushNotificationTokenResponse RegisterPushNotificationTokenResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RegisterPushNotificationTokenResponse: (status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the supplied Push Notification Token will be associated 
 * with the player account currently logged into the %Gluon system.
*/
@JsonTypeName("RegisterPushNotificationTokenResponse")
public class RegisterPushNotificationTokenResponse extends GluonResponse{

	/**
	 * Constructor for RegisterPushNotificationTokenResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public RegisterPushNotificationTokenResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RegisterPushNotificationTokenResponse without super class members
	 */
	public RegisterPushNotificationTokenResponse(){

	}
	/**
	 * Constructor for RegisterPushNotificationTokenResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public RegisterPushNotificationTokenResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
