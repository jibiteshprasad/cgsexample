/**
 * @file SendJoinGroupRequestResponse.java
 * @page classSendJoinGroupRequestResponse SendJoinGroupRequestResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendJoinGroupRequestResponse: (groupName, groupType, requestStatus, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, the join request was submitted successfully. Check 
 * requestStatus to determine next course of action.
*/
@JsonTypeName("SendJoinGroupRequestResponse")
public class SendJoinGroupRequestResponse extends GluonResponse{

	/**
	 * Name of the group the join request was sent to - this name is unique
	 */
	public String groupName;

	/**
	 * Name of the group's type the join request was sent to
	 */
	public String groupType;

	/**
	 * Current status of the join request.
	 */
	public JoinGroupRequestStatus requestStatus;

	/**
	 * Constructor for SendJoinGroupRequestResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param requestStatus
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendJoinGroupRequestResponse(String groupName, String groupType, 
			JoinGroupRequestStatus requestStatus, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.requestStatus = requestStatus;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendJoinGroupRequestResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param requestStatus
	 */
	public SendJoinGroupRequestResponse(String groupName, String groupType, 
			JoinGroupRequestStatus requestStatus){
		this.groupName = groupName;
		this.groupType = groupType;
		this.requestStatus = requestStatus;

	}
	/**
	 * Constructor for SendJoinGroupRequestResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendJoinGroupRequestResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SendJoinGroupRequestResponse
	*/
	public SendJoinGroupRequestResponse(){

	}
}
/** @} */
