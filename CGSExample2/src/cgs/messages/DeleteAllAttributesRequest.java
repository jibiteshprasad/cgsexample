/**
 * @file DeleteAllAttributesRequest.java
 * @page classDeleteAllAttributesRequest DeleteAllAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllAttributesRequest: (playerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes all player attributes for a specified player
 * 
*/
@JsonTypeName("DeleteAllAttributesRequest")
public class DeleteAllAttributesRequest extends GluonRequest{

	/**
	 * ID of the player whose attributes will be deleted
	 */
	public String playerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteAllAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteAllAttributesResponseDelegate){
                ((DeleteAllAttributesResponseDelegate)delegate).onDeleteAllAttributesResponse(this, (DeleteAllAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteAllAttributesResponseDelegate){
            ((DeleteAllAttributesResponseDelegate)delegate).onDeleteAllAttributesResponse(this, new DeleteAllAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteAllAttributesRequest that requires all members
	 * @param playerId
	 * @param passthrough
	 */
	public DeleteAllAttributesRequest(String playerId, String passthrough){
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllAttributesRequest without super class members
	 * @param playerId
	 */
	public DeleteAllAttributesRequest(String playerId){
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for DeleteAllAttributesRequest
	*/
	public DeleteAllAttributesRequest(){

	}
}
/** @} */
