/**
 * @file GetAllGroupMemberAttributesRequest.java
 * @page classGetAllGroupMemberAttributesRequest GetAllGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllGroupMemberAttributesRequest: (groupName, groupType, playerIds, 
 * 		pageIndex, memberSize, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves ALL group member attributes for members specified in the playerIds 
 * list
*/
@JsonTypeName("GetAllGroupMemberAttributesRequest")
public class GetAllGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where attributes will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of playerIds for members whose attributes will be retrieved
	 */
	public ArrayList<String> playerIds;

	/**
	 * Page index for attributes to be retrieved (starts at 0) (Optional)
	 */
	public int pageIndex;

	/**
	 * The number of group members per page (Optional)
	 */
	public int memberSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetAllGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetAllGroupMemberAttributesResponseDelegate){
                ((GetAllGroupMemberAttributesResponseDelegate)delegate).onGetAllGroupMemberAttributesResponse(this, (GetAllGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetAllGroupMemberAttributesResponseDelegate){
            ((GetAllGroupMemberAttributesResponseDelegate)delegate).onGetAllGroupMemberAttributesResponse(this, new GetAllGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetAllGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param pageIndex
	 * @param memberSize
	 * @param passthrough
	 */
	public GetAllGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, int pageIndex, int memberSize, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param pageIndex
	 * @param memberSize
	 */
	public GetAllGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> playerIds, int pageIndex, int memberSize){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;

	}

	/**
	 * Default Constructor for GetAllGroupMemberAttributesRequest
	*/
	public GetAllGroupMemberAttributesRequest(){

	}
}
/** @} */
