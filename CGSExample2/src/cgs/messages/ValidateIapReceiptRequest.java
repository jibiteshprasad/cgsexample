/**
 * @file ValidateIapReceiptRequest.java
 * @page classValidateIapReceiptRequest ValidateIapReceiptRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ValidateIapReceiptRequest: (expectedBid, ticket, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to validate an IAP (in-app purchase) receipt.
 * 
*/
@JsonTypeName("ValidateIapReceiptRequest")
public class ValidateIapReceiptRequest extends GluonRequest{

	/**
	 * The requestor expects that this ticket relates to this Bundle Identifier 
	 * (i.e. game). If it doesn't, don't submit to Apple and fail the request.
	 */
	public String expectedBid;

	/**
	 * Unadulterated ticket from apple. Expected in base64.
	 */
	public String ticket;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ValidateIapReceiptResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ValidateIapReceiptResponseDelegate){
                ((ValidateIapReceiptResponseDelegate)delegate).onValidateIapReceiptResponse(this, (ValidateIapReceiptResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ValidateIapReceiptResponseDelegate){
            ((ValidateIapReceiptResponseDelegate)delegate).onValidateIapReceiptResponse(this, new ValidateIapReceiptResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ValidateIapReceiptRequest that requires all members
	 * @param expectedBid
	 * @param ticket
	 * @param passthrough
	 */
	public ValidateIapReceiptRequest(String expectedBid, String ticket, 
			String passthrough){
		this.expectedBid = expectedBid;
		this.ticket = ticket;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ValidateIapReceiptRequest without super class members
	 * @param expectedBid
	 * @param ticket
	 */
	public ValidateIapReceiptRequest(String expectedBid, String ticket){
		this.expectedBid = expectedBid;
		this.ticket = ticket;

	}

	/**
	 * Default Constructor for ValidateIapReceiptRequest
	*/
	public ValidateIapReceiptRequest(){

	}
}
/** @} */
