/**
 * @file GroupMembershipModifiedNotification.java
 * @page classGroupMembershipModifiedNotification GroupMembershipModifiedNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupMembershipModifiedNotification: (group, modificationType, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returned from the backend to a connected group member when a membership 
 * change has occurred.
*/
@JsonTypeName("GroupMembershipModifiedNotification")
public class GroupMembershipModifiedNotification extends GluonRequest{

	/**
	 * Detailed information (such as owner name, group size, attributes, etc.) 
	 * for the group that has changed
	 */
	public GroupSummary group;

	/**
	 * Specific change in group membership that has occurred (see GroupSummary 
	 * for details)
	 */
	public ModificationType modificationType;

	/**
	 * Constructor for GroupMembershipModifiedNotification that requires all members
	 * @param group
	 * @param modificationType
	 * @param passthrough
	 */
	public GroupMembershipModifiedNotification(GroupSummary group, 
			ModificationType modificationType, String passthrough){
		this.group = group;
		this.modificationType = modificationType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GroupMembershipModifiedNotification without super class members
	 * @param group
	 * @param modificationType
	 */
	public GroupMembershipModifiedNotification(GroupSummary group, 
			ModificationType modificationType){
		this.group = group;
		this.modificationType = modificationType;

	}

	/**
	 * Default Constructor for GroupMembershipModifiedNotification
	*/
	public GroupMembershipModifiedNotification(){

	}
}
/** @} */
