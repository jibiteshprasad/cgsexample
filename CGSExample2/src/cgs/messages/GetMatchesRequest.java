/**
 * @file GetMatchesRequest.java
 * @page classGetMatchesRequest GetMatchesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMatchesRequest: (queryName, presenceType, maxMatchesToReturn, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gathers a list of potential matches to play against based on the name query 
 * setup for your game.  * Use presenceType to filter between online, offline, 
 * or all players. Limit total matches with max parameter.
*/
@JsonTypeName("GetMatchesRequest")
public class GetMatchesRequest extends GluonRequest{

	/**
	 * The name of the query you have setup on the %Gluon web portal for your 
	 * game. Used to narrow down optimal results.
	 */
	public String queryName;

	/**
	 * The type of players you are looking for. 
	 */
	public PresenceType presenceType;

	/**
	 * Used to limit the number of matches returned
	 */
	public int maxMatchesToReturn;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMatchesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMatchesResponseDelegate){
                ((GetMatchesResponseDelegate)delegate).onGetMatchesResponse(this, (GetMatchesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMatchesResponseDelegate){
            ((GetMatchesResponseDelegate)delegate).onGetMatchesResponse(this, new GetMatchesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMatchesRequest that requires all members
	 * @param queryName
	 * @param presenceType
	 * @param maxMatchesToReturn
	 * @param passthrough
	 */
	public GetMatchesRequest(String queryName, PresenceType presenceType, 
			int maxMatchesToReturn, String passthrough){
		this.queryName = queryName;
		this.presenceType = presenceType;
		this.maxMatchesToReturn = maxMatchesToReturn;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMatchesRequest without super class members
	 * @param queryName
	 * @param presenceType
	 * @param maxMatchesToReturn
	 */
	public GetMatchesRequest(String queryName, PresenceType presenceType, 
			int maxMatchesToReturn){
		this.queryName = queryName;
		this.presenceType = presenceType;
		this.maxMatchesToReturn = maxMatchesToReturn;

	}

	/**
	 * Default Constructor for GetMatchesRequest
	*/
	public GetMatchesRequest(){

	}
}
/** @} */
