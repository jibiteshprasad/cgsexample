/**
 * @file GetCurrencyRequest.java
 * @page classGetCurrencyRequest GetCurrencyRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCurrencyRequest: (currencyType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to get the value of a currency in the player's wallet.
 * 
*/
@JsonTypeName("GetCurrencyRequest")
public class GetCurrencyRequest extends GluonRequest{

	/**
	 * The currency type (Optional)
	 */
	public int currencyType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCurrencyResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCurrencyResponseDelegate){
                ((GetCurrencyResponseDelegate)delegate).onGetCurrencyResponse(this, (GetCurrencyResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCurrencyResponseDelegate){
            ((GetCurrencyResponseDelegate)delegate).onGetCurrencyResponse(this, new GetCurrencyResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCurrencyRequest that requires all members
	 * @param currencyType
	 * @param passthrough
	 */
	public GetCurrencyRequest(int currencyType, String passthrough){
		this.currencyType = currencyType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCurrencyRequest without super class members
	 * @param currencyType
	 */
	public GetCurrencyRequest(int currencyType){
		this.currencyType = currencyType;

	}

	/**
	 * Default Constructor for GetCurrencyRequest
	*/
	public GetCurrencyRequest(){

	}
}
/** @} */
