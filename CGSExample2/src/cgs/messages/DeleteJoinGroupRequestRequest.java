/**
 * @file DeleteJoinGroupRequestRequest.java
 * @page classDeleteJoinGroupRequestRequest DeleteJoinGroupRequestRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteJoinGroupRequestRequest: (requestId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes a request to join a group created by the authenticated player
 * 
*/
@JsonTypeName("DeleteJoinGroupRequestRequest")
public class DeleteJoinGroupRequestRequest extends GluonRequest{

	/**
	 * Unique request ID of the join group request
	 */
	public String requestId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteJoinGroupRequestResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteJoinGroupRequestResponseDelegate){
                ((DeleteJoinGroupRequestResponseDelegate)delegate).onDeleteJoinGroupRequestResponse(this, (DeleteJoinGroupRequestResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteJoinGroupRequestResponseDelegate){
            ((DeleteJoinGroupRequestResponseDelegate)delegate).onDeleteJoinGroupRequestResponse(this, new DeleteJoinGroupRequestResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteJoinGroupRequestRequest that requires all members
	 * @param requestId
	 * @param passthrough
	 */
	public DeleteJoinGroupRequestRequest(String requestId, String passthrough){
		this.requestId = requestId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteJoinGroupRequestRequest without super class members
	 * @param requestId
	 */
	public DeleteJoinGroupRequestRequest(String requestId){
		this.requestId = requestId;

	}

	/**
	 * Default Constructor for DeleteJoinGroupRequestRequest
	*/
	public DeleteJoinGroupRequestRequest(){

	}
}
/** @} */
