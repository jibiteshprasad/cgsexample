/**
 * @file MailMessage.java
 * @page classMailMessage MailMessage
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class MailMessage: (id, fromPlayerInfo, toPlayerInfo, subject, body, isRead, 
 * 		createDate, tags)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A mail message that can be read from the inbox using the GetMailRequest 
 * message
*/
public class MailMessage {

	/**
	 * Message ID (Optional)
	 */
	public String id;

	/**
	 * Sender of the message
	 */
	public PlayerInfo fromPlayerInfo;

	/**
	 * Recipient of the message
	 */
	public PlayerInfo toPlayerInfo;

	/**
	 * Subject of the message (Optional)
	 */
	public String subject;

	/**
	 * Body of the message (Optional)
	 */
	public String body;

	/**
	 * Flag to determine whether the message is marked as read (Optional)
	 */
	public boolean isRead;

	/**
	 * Date when the mail message was created (Optional)
	 */
	public Date createDate;

	/**
	 * Arbitrary tags associated to this mail message (Optional)
	 */
	public List<String> tags;

	/**
	 * Constructor for MailMessage that requires all members
	 * @param id
	 * @param fromPlayerInfo
	 * @param toPlayerInfo
	 * @param subject
	 * @param body
	 * @param isRead
	 * @param createDate
	 * @param tags
	 */
	public MailMessage(String id, PlayerInfo fromPlayerInfo, 
			PlayerInfo toPlayerInfo, String subject, String body, boolean isRead, 
			Date createDate, List<String> tags){
		this.id = id;
		this.fromPlayerInfo = fromPlayerInfo;
		this.toPlayerInfo = toPlayerInfo;
		this.subject = subject;
		this.body = body;
		this.isRead = isRead;
		this.createDate = createDate;
		this.tags = tags;

	}

	/**
	 * Default Constructor for MailMessage
	*/
	public MailMessage(){

	}
}
/** @} */
