/**
 * @file ChangeNickNameRequest.java
 * @page classChangeNickNameRequest ChangeNickNameRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ChangeNickNameRequest: (currentName, proposedName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Changes the nickName.
 * 
*/
@JsonTypeName("ChangeNickNameRequest")
public class ChangeNickNameRequest extends GluonRequest{

	/**
	 * Current name being used
	 */
	public String currentName;

	/**
	 * Proposed change of nickName
	 */
	public String proposedName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ChangeNickNameResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ChangeNickNameResponseDelegate){
                ((ChangeNickNameResponseDelegate)delegate).onChangeNickNameResponse(this, (ChangeNickNameResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ChangeNickNameResponseDelegate){
            ((ChangeNickNameResponseDelegate)delegate).onChangeNickNameResponse(this, new ChangeNickNameResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ChangeNickNameRequest that requires all members
	 * @param currentName
	 * @param proposedName
	 * @param passthrough
	 */
	public ChangeNickNameRequest(String currentName, String proposedName, 
			String passthrough){
		this.currentName = currentName;
		this.proposedName = proposedName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ChangeNickNameRequest without super class members
	 * @param currentName
	 * @param proposedName
	 */
	public ChangeNickNameRequest(String currentName, String proposedName){
		this.currentName = currentName;
		this.proposedName = proposedName;

	}

	/**
	 * Default Constructor for ChangeNickNameRequest
	*/
	public ChangeNickNameRequest(){

	}
}
/** @} */
