/**
 * @file AuthorizeUserResponse.java
 * @page classAuthorizeUserResponse AuthorizeUserResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AuthorizeUserResponse: (serverHostname, serverPort, playerId, token, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response from an edge server to a game server with user authorization 
 * details.
*/
@JsonTypeName("AuthorizeUserResponse")
public class AuthorizeUserResponse extends GluonResponse{

	/**
	 *
	 */
	public String serverHostname;

	/**
	 * (Optional)
	 */
	public int serverPort;

	/**
	 *
	 */
	public String playerId;

	/**
	 *
	 */
	public String token;

	/**
	 * Constructor for AuthorizeUserResponse that requires all members
	 * @param serverHostname
	 * @param serverPort
	 * @param playerId
	 * @param token
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public AuthorizeUserResponse(String serverHostname, int serverPort, 
			String playerId, String token, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.serverHostname = serverHostname;
		this.serverPort = serverPort;
		this.playerId = playerId;
		this.token = token;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AuthorizeUserResponse without super class members
	 * @param serverHostname
	 * @param serverPort
	 * @param playerId
	 * @param token
	 */
	public AuthorizeUserResponse(String serverHostname, int serverPort, 
			String playerId, String token){
		this.serverHostname = serverHostname;
		this.serverPort = serverPort;
		this.playerId = playerId;
		this.token = token;

	}
	/**
	 * Constructor for AuthorizeUserResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public AuthorizeUserResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for AuthorizeUserResponse
	*/
	public AuthorizeUserResponse(){

	}
}
/** @} */
