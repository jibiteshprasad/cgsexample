package cgs.messages;


/**
 *
 * @author Trevor Dasch
 */
public class JsonWrapper {
    
    public class Header{
        public Header(){
            
        }
        public long utc_timestamp;
        public String messageId;
        public String playerId;
        public String gameId;
        public String messageType;
    }
    
    public int protocolVersion;
    public Header header;
    public GluonMessage body;
    
}
