/**
 * @file AcceptJoinGroupInviteRequest.java
 * @page classAcceptJoinGroupInviteRequest AcceptJoinGroupInviteRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AcceptJoinGroupInviteRequest: (invitiationId, maxGroupSize, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Accept an invitation that was sent to the specified player
 * 
*/
@JsonTypeName("AcceptJoinGroupInviteRequest")
public class AcceptJoinGroupInviteRequest extends GluonRequest{

	/**
	 * Unique inivitation ID of the invite to be accepted
	 */
	public String invitiationId;

	/**
	 * Maximum group size allowed for invite to be accepted (Optional)
	 */
	public int maxGroupSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(AcceptJoinGroupInviteResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof AcceptJoinGroupInviteResponseDelegate){
                ((AcceptJoinGroupInviteResponseDelegate)delegate).onAcceptJoinGroupInviteResponse(this, (AcceptJoinGroupInviteResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof AcceptJoinGroupInviteResponseDelegate){
            ((AcceptJoinGroupInviteResponseDelegate)delegate).onAcceptJoinGroupInviteResponse(this, new AcceptJoinGroupInviteResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for AcceptJoinGroupInviteRequest that requires all members
	 * @param invitiationId
	 * @param maxGroupSize
	 * @param passthrough
	 */
	public AcceptJoinGroupInviteRequest(String invitiationId, int maxGroupSize, 
			String passthrough){
		this.invitiationId = invitiationId;
		this.maxGroupSize = maxGroupSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AcceptJoinGroupInviteRequest without super class members
	 * @param invitiationId
	 * @param maxGroupSize
	 */
	public AcceptJoinGroupInviteRequest(String invitiationId, int maxGroupSize){
		this.invitiationId = invitiationId;
		this.maxGroupSize = maxGroupSize;

	}

	/**
	 * Default Constructor for AcceptJoinGroupInviteRequest
	*/
	public AcceptJoinGroupInviteRequest(){

	}
}
/** @} */
