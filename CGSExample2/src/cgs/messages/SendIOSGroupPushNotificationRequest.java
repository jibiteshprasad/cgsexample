/**
 * @file SendIOSGroupPushNotificationRequest.java
 * @page classSendIOSGroupPushNotificationRequest SendIOSGroupPushNotificationRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendIOSGroupPushNotificationRequest: (groups, notification, 
 * 		displayNumber, soundFileName, tags, customData, scheduledTime, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to send IOS group push notifications.
*/
@JsonTypeName("SendIOSGroupPushNotificationRequest")
public class SendIOSGroupPushNotificationRequest extends GluonRequest{

	/**
	 *
	 */
	public List<GroupDescriptor> groups;

	/**
	 *
	 */
	public IOSNotification notification;

	/**
	 * (Optional)
	 */
	public Integer displayNumber;

	/**
	 * (Optional)
	 */
	public String soundFileName;

	/**
	 * (Optional)
	 */
	public List<String> tags;

	/**
	 * (Optional)
	 */
	public Map<String, Object> customData;

	/**
	 * (Optional)
	 */
	public List<Date> scheduledTime;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendIOSGroupPushNotificationResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendIOSGroupPushNotificationResponseDelegate){
                ((SendIOSGroupPushNotificationResponseDelegate)delegate).onSendIOSGroupPushNotificationResponse(this, (SendIOSGroupPushNotificationResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendIOSGroupPushNotificationResponseDelegate){
            ((SendIOSGroupPushNotificationResponseDelegate)delegate).onSendIOSGroupPushNotificationResponse(this, new SendIOSGroupPushNotificationResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendIOSGroupPushNotificationRequest that requires all members
	 * @param groups
	 * @param notification
	 * @param displayNumber
	 * @param soundFileName
	 * @param tags
	 * @param customData
	 * @param scheduledTime
	 * @param passthrough
	 */
	public SendIOSGroupPushNotificationRequest(List<GroupDescriptor> groups, 
			IOSNotification notification, Integer displayNumber, 
			String soundFileName, List<String> tags, Map<String, 
			Object> customData, List<Date> scheduledTime, String passthrough){
		this.groups = groups;
		this.notification = notification;
		this.displayNumber = displayNumber;
		this.soundFileName = soundFileName;
		this.tags = tags;
		this.customData = customData;
		this.scheduledTime = scheduledTime;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendIOSGroupPushNotificationRequest without super class members
	 * @param groups
	 * @param notification
	 * @param displayNumber
	 * @param soundFileName
	 * @param tags
	 * @param customData
	 * @param scheduledTime
	 */
	public SendIOSGroupPushNotificationRequest(List<GroupDescriptor> groups, 
			IOSNotification notification, Integer displayNumber, 
			String soundFileName, List<String> tags, Map<String, 
			Object> customData, List<Date> scheduledTime){
		this.groups = groups;
		this.notification = notification;
		this.displayNumber = displayNumber;
		this.soundFileName = soundFileName;
		this.tags = tags;
		this.customData = customData;
		this.scheduledTime = scheduledTime;

	}

	/**
	 * Default Constructor for SendIOSGroupPushNotificationRequest
	*/
	public SendIOSGroupPushNotificationRequest(){

	}
}
/** @} */
