/**
 * @file GroupAndModifiedAttributes.java
 * @page classGroupAndModifiedAttributes GroupAndModifiedAttributes
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupAndModifiedAttributes: (group, currentMemberCount, maxMemberLimit, 
 * 		modifiedAttributes)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Collection of Groups and their modified attributes (so we can batch updates 
 * in notification)
*/
public class GroupAndModifiedAttributes {

	/**
	 * The group whose attributes were just modified
	 */
	public GroupDescriptor group;

	/**
	 * Number of players currently in this group
	 */
	public Integer currentMemberCount;

	/**
	 * Maximum number of players allowed in this group (Optional)
	 */
	public Integer maxMemberLimit;

	/**
	 * Collection of Group Attributes that were modified (contains old/new for 
	 * comparison) (Optional)
	 */
	public List<ModifiedGroupAttribute> modifiedAttributes;

	/**
	 * Constructor for GroupAndModifiedAttributes that requires all members
	 * @param group
	 * @param currentMemberCount
	 * @param maxMemberLimit
	 * @param modifiedAttributes
	 */
	public GroupAndModifiedAttributes(GroupDescriptor group, 
			Integer currentMemberCount, Integer maxMemberLimit, 
			List<ModifiedGroupAttribute> modifiedAttributes){
		this.group = group;
		this.currentMemberCount = currentMemberCount;
		this.maxMemberLimit = maxMemberLimit;
		this.modifiedAttributes = modifiedAttributes;

	}

	/**
	 * Default Constructor for GroupAndModifiedAttributes
	*/
	public GroupAndModifiedAttributes(){

	}
}
/** @} */
