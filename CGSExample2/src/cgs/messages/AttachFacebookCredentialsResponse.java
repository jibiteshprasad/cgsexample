/**
 * @file AttachFacebookCredentialsResponse.java
 * @page classAttachFacebookCredentialsResponse AttachFacebookCredentialsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AttachFacebookCredentialsResponse: (playerInfo, playerAndAttributesList, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the Facebook account has been associated with the 
 * current player. (See AttachFacebookCredentialsRequest for details on how to 
 * handle the case of a Facebook account attached to multiple players.)
*/
@JsonTypeName("AttachFacebookCredentialsResponse")
public class AttachFacebookCredentialsResponse extends GluonResponse{

	/**
	 * The currently authenticated player, now associated with the Facebook 
	 * account (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * If status is FAILURE and errorCode is 
	 * CREDENTIAL_ATTACHED_TO_DIFFERENT_PLAYER, playerAndAttributesList will be 
	 * populated with existing attachments associated with this account. 
	 * (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributesList;

	/**
	 * Constructor for AttachFacebookCredentialsResponse that requires all members
	 * @param playerInfo
	 * @param playerAndAttributesList
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public AttachFacebookCredentialsResponse(PlayerInfo playerInfo, 
			List<PlayerAndAttributes> playerAndAttributesList, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.playerAndAttributesList = playerAndAttributesList;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AttachFacebookCredentialsResponse without super class members
	 * @param playerInfo
	 * @param playerAndAttributesList
	 */
	public AttachFacebookCredentialsResponse(PlayerInfo playerInfo, 
			List<PlayerAndAttributes> playerAndAttributesList){
		this.playerInfo = playerInfo;
		this.playerAndAttributesList = playerAndAttributesList;

	}
	/**
	 * Constructor for AttachFacebookCredentialsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public AttachFacebookCredentialsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for AttachFacebookCredentialsResponse
	*/
	public AttachFacebookCredentialsResponse(){

	}
}
/** @} */
