package cgs.messages;

public enum EntityType
	{
	/// Indicates that the ownerId is associated to a playerId. Responses will return the player NickName in the extraInfo property of the LeaderboardEntry class.
	PLAYER,
	/// Indicates that the ownerId is associated to an "accumulated" entry created by the SetLeaderboardEntryAsAccumulation message. The extraInfo parameter will be "null".
	GROUP
	
}
