/**
 * @file GetDocumentsResponse.java
 * @page classGetDocumentsResponse GetDocumentsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetDocumentsResponse: (documents, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the player's documents with any of the tags passed were 
 * retrieved
*/
@JsonTypeName("GetDocumentsResponse")
public class GetDocumentsResponse extends GluonResponse{

	/**
	 * All of the documents retrieved (Optional)
	 */
	public List<Document> documents;

	/**
	 * Constructor for GetDocumentsResponse that requires all members
	 * @param documents
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetDocumentsResponse(List<Document> documents, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.documents = documents;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetDocumentsResponse without super class members
	 * @param documents
	 */
	public GetDocumentsResponse(List<Document> documents){
		this.documents = documents;

	}
	/**
	 * Constructor for GetDocumentsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetDocumentsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetDocumentsResponse
	*/
	public GetDocumentsResponse(){

	}
}
/** @} */
