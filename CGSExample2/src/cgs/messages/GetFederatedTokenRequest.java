/**
 * @file GetFederatedTokenRequest.java
 * @page classGetFederatedTokenRequest GetFederatedTokenRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetFederatedTokenRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * This message must be called after authenication of the player in order to use 
 * non-Gluon services such as the Relay Service. * After successfully retrieving 
 * the federated token, the signature is automatically saved in the EdgeClient. 
 * There is no need to pass this information to each non-Gluon service, the 
 * EdgeClient will perform this task for the developer.
*/
@JsonTypeName("GetFederatedTokenRequest")
public class GetFederatedTokenRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetFederatedTokenResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetFederatedTokenResponseDelegate){
                ((GetFederatedTokenResponseDelegate)delegate).onGetFederatedTokenResponse(this, (GetFederatedTokenResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetFederatedTokenResponseDelegate){
            ((GetFederatedTokenResponseDelegate)delegate).onGetFederatedTokenResponse(this, new GetFederatedTokenResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetFederatedTokenRequest that requires all members
	 * @param passthrough
	 */
	public GetFederatedTokenRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetFederatedTokenRequest without super class members
	 */
	public GetFederatedTokenRequest(){

	}
}
/** @} */
