/**
 * @file GetMySentGroupInvitesRequest.java
 * @page classGetMySentGroupInvitesRequest GetMySentGroupInvitesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMySentGroupInvitesRequest: (groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all invites that the authenticated player had sent out
 * 
*/
@JsonTypeName("GetMySentGroupInvitesRequest")
public class GetMySentGroupInvitesRequest extends GluonRequest{

	/**
	 * Invites received will be from specified group type 
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMySentGroupInvitesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMySentGroupInvitesResponseDelegate){
                ((GetMySentGroupInvitesResponseDelegate)delegate).onGetMySentGroupInvitesResponse(this, (GetMySentGroupInvitesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMySentGroupInvitesResponseDelegate){
            ((GetMySentGroupInvitesResponseDelegate)delegate).onGetMySentGroupInvitesResponse(this, new GetMySentGroupInvitesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMySentGroupInvitesRequest that requires all members
	 * @param groupType
	 * @param passthrough
	 */
	public GetMySentGroupInvitesRequest(String groupType, String passthrough){
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMySentGroupInvitesRequest without super class members
	 * @param groupType
	 */
	public GetMySentGroupInvitesRequest(String groupType){
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetMySentGroupInvitesRequest
	*/
	public GetMySentGroupInvitesRequest(){

	}
}
/** @} */
