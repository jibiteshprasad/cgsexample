/**
 * @file RemoveBaseCatalogProductsRequest.java
 * @page classRemoveBaseCatalogProductsRequest RemoveBaseCatalogProductsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemoveBaseCatalogProductsRequest: (baseCatalogName, productNames, 
 * 		userName, endDate, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Removes the base products for a catalog.
*/
@JsonTypeName("RemoveBaseCatalogProductsRequest")
public class RemoveBaseCatalogProductsRequest extends GluonRequest{

	/**
	 *
	 */
	public String baseCatalogName;

	/**
	 *
	 */
	public List<String> productNames;

	/**
	 *
	 */
	public String userName;

	/**
	 * (Optional)
	 */
	public Date endDate;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(RemoveBaseCatalogProductsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof RemoveBaseCatalogProductsResponseDelegate){
                ((RemoveBaseCatalogProductsResponseDelegate)delegate).onRemoveBaseCatalogProductsResponse(this, (RemoveBaseCatalogProductsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof RemoveBaseCatalogProductsResponseDelegate){
            ((RemoveBaseCatalogProductsResponseDelegate)delegate).onRemoveBaseCatalogProductsResponse(this, new RemoveBaseCatalogProductsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for RemoveBaseCatalogProductsRequest that requires all members
	 * @param baseCatalogName
	 * @param productNames
	 * @param userName
	 * @param endDate
	 * @param passthrough
	 */
	public RemoveBaseCatalogProductsRequest(String baseCatalogName, 
			List<String> productNames, String userName, Date endDate, 
			String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.productNames = productNames;
		this.userName = userName;
		this.endDate = endDate;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemoveBaseCatalogProductsRequest without super class members
	 * @param baseCatalogName
	 * @param productNames
	 * @param userName
	 * @param endDate
	 */
	public RemoveBaseCatalogProductsRequest(String baseCatalogName, 
			List<String> productNames, String userName, Date endDate){
		this.baseCatalogName = baseCatalogName;
		this.productNames = productNames;
		this.userName = userName;
		this.endDate = endDate;

	}

	/**
	 * Default Constructor for RemoveBaseCatalogProductsRequest
	*/
	public RemoveBaseCatalogProductsRequest(){

	}
}
/** @} */
