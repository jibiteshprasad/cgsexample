/**
 * @file GetMyGroupMemberAttributesRequest.java
 * @page classGetMyGroupMemberAttributesRequest GetMyGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyGroupMemberAttributesRequest: (groupName, groupType, 
 * 		attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves group member attributes, which are specified in the attributeNames 
 * list, for the authenticated player
*/
@JsonTypeName("GetMyGroupMemberAttributesRequest")
public class GetMyGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where member attributes will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of the attribute names for the attributes that will be retrieved
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMyGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMyGroupMemberAttributesResponseDelegate){
                ((GetMyGroupMemberAttributesResponseDelegate)delegate).onGetMyGroupMemberAttributesResponse(this, (GetMyGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMyGroupMemberAttributesResponseDelegate){
            ((GetMyGroupMemberAttributesResponseDelegate)delegate).onGetMyGroupMemberAttributesResponse(this, new GetMyGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMyGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param passthrough
	 */
	public GetMyGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> attributeNames, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 */
	public GetMyGroupMemberAttributesRequest(String groupName, String groupType, 
			ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for GetMyGroupMemberAttributesRequest
	*/
	public GetMyGroupMemberAttributesRequest(){

	}
}
/** @} */
