/**
 * @file DeleteGroupAttributesResponse.java
 * @page classDeleteGroupAttributesResponse DeleteGroupAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupAttributesResponse: (groupName, groupType, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether the specified group 
 * attributes were deleted properly 
*/
@JsonTypeName("DeleteGroupAttributesResponse")
public class DeleteGroupAttributesResponse extends GluonResponse{

	/**
	 * Name of the group whose attributes were deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Constructor for DeleteGroupAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteGroupAttributesResponse(String groupName, String groupType, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteGroupAttributesResponse(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}
	/**
	 * Constructor for DeleteGroupAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteGroupAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteGroupAttributesResponse
	*/
	public DeleteGroupAttributesResponse(){

	}
}
/** @} */
