/**
 * @file GetCalendarEventResponse.java
 * @page classGetCalendarEventResponse GetCalendarEventResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCalendarEventResponse: (calendarEvent, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a calandar event object as specified in the request.
 * 
*/
@JsonTypeName("GetCalendarEventResponse")
public class GetCalendarEventResponse extends GluonResponse{

	/**
	 * The calender event requested (Optional)
	 */
	public CalendarEvent calendarEvent;

	/**
	 * Constructor for GetCalendarEventResponse that requires all members
	 * @param calendarEvent
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCalendarEventResponse(CalendarEvent calendarEvent, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.calendarEvent = calendarEvent;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCalendarEventResponse without super class members
	 * @param calendarEvent
	 */
	public GetCalendarEventResponse(CalendarEvent calendarEvent){
		this.calendarEvent = calendarEvent;

	}
	/**
	 * Constructor for GetCalendarEventResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCalendarEventResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCalendarEventResponse
	*/
	public GetCalendarEventResponse(){

	}
}
/** @} */
