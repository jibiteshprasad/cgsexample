/**
 * @file SendRoomMessageRequest.java
 * @page classSendRoomMessageRequest SendRoomMessageRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendRoomMessageRequest: (room, message, timestamp, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a message to everyone in the room. In addition to the corresponding 
 * response (SendRoomMessageResponse), this  will trigger a 
 * ReceiveRoomMessageNotification, as you would receive when anyone else in the 
 * room sends a message.
*/
@JsonTypeName("SendRoomMessageRequest")
public class SendRoomMessageRequest extends GluonRequest{

	/**
	 * Name of the room where the message will be sent
	 */
	public String room;

	/**
	 * Message that will be sent to the room (Optional)
	 */
	public String message;

	/**
	 * (Optional)
	 */
	public long timestamp;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendRoomMessageResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendRoomMessageResponseDelegate){
                ((SendRoomMessageResponseDelegate)delegate).onSendRoomMessageResponse(this, (SendRoomMessageResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendRoomMessageResponseDelegate){
            ((SendRoomMessageResponseDelegate)delegate).onSendRoomMessageResponse(this, new SendRoomMessageResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendRoomMessageRequest that requires all members
	 * @param room
	 * @param message
	 * @param timestamp
	 * @param passthrough
	 */
	public SendRoomMessageRequest(String room, String message, long timestamp, 
			String passthrough){
		this.room = room;
		this.message = message;
		this.timestamp = timestamp;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendRoomMessageRequest without super class members
	 * @param room
	 * @param message
	 * @param timestamp
	 */
	public SendRoomMessageRequest(String room, String message, long timestamp){
		this.room = room;
		this.message = message;
		this.timestamp = timestamp;

	}

	/**
	 * Default Constructor for SendRoomMessageRequest
	*/
	public SendRoomMessageRequest(){

	}
}
/** @} */
