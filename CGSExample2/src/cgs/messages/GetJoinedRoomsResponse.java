/**
 * @file GetJoinedRoomsResponse.java
 * @page classGetJoinedRoomsResponse GetJoinedRoomsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetJoinedRoomsResponse: (rooms, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of chat rooms the authenticated player has joined
 * 
*/
@JsonTypeName("GetJoinedRoomsResponse")
public class GetJoinedRoomsResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public Collection<String> rooms;

	/**
	 * Constructor for GetJoinedRoomsResponse that requires all members
	 * @param rooms
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetJoinedRoomsResponse(Collection<String> rooms, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.rooms = rooms;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetJoinedRoomsResponse without super class members
	 * @param rooms
	 */
	public GetJoinedRoomsResponse(Collection<String> rooms){
		this.rooms = rooms;

	}
	/**
	 * Constructor for GetJoinedRoomsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetJoinedRoomsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetJoinedRoomsResponse
	*/
	public GetJoinedRoomsResponse(){

	}
}
/** @} */
