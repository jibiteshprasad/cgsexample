/**
 * @file ApplyDeltaToLeaderboardEntryResponse.java
 * @page classApplyDeltaToLeaderboardEntryResponse ApplyDeltaToLeaderboardEntryResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApplyDeltaToLeaderboardEntryResponse: (leaderboardName, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the leaderboard entry's value has been updated with the 
 * added delta.
*/
@JsonTypeName("ApplyDeltaToLeaderboardEntryResponse")
public class ApplyDeltaToLeaderboardEntryResponse extends GluonResponse{

	/**
	 * The name of the leaderboard from which the entries were retrieved
	 */
	public String leaderboardName;

	/**
	 * Constructor for ApplyDeltaToLeaderboardEntryResponse that requires all members
	 * @param leaderboardName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ApplyDeltaToLeaderboardEntryResponse(String leaderboardName, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.leaderboardName = leaderboardName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApplyDeltaToLeaderboardEntryResponse without super class members
	 * @param leaderboardName
	 */
	public ApplyDeltaToLeaderboardEntryResponse(String leaderboardName){
		this.leaderboardName = leaderboardName;

	}
	/**
	 * Constructor for ApplyDeltaToLeaderboardEntryResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ApplyDeltaToLeaderboardEntryResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ApplyDeltaToLeaderboardEntryResponse
	*/
	public ApplyDeltaToLeaderboardEntryResponse(){

	}
}
/** @} */
