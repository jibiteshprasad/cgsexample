/**
 * @file CreateFacebookPlayerRequest.java
 * @page classCreateFacebookPlayerRequest CreateFacebookPlayerRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreateFacebookPlayerRequest: (nickName, accessToken, clientId, 
 * 		clientSecret, gameId, protocolVersion, clientSdkVersion, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Create a new player, whose Facebook credentials can then be used to 
 * authenticate for this game (based on the gameId).  * The Facebook account 
 * must not already be in use for this game. If this is successful, you will 
 * also be 'logged in'  to use %Gluon services (and thus do not need to call 
 * AuthenticatePlayer for this session). 
*/
@JsonTypeName("")
public class CreateFacebookPlayerRequest extends GluonRequest{

	/**
	 * The nickName of the nickName associated with the Facebook account 
	 * (Optional)
	 */
	public String nickName;

	/**
	 * Returned from Facebook in the login response (specifically the 
	 * "accessToken" within the "authResponse") (Optional)
	 */
	public String accessToken;

	/**
	 * Returned from Facebook in the login response (specifically the "userID" 
	 * within the "authResponse") (Optional)
	 */
	public String clientId;

	/**
	 * Any non-empty string (Optional)
	 */
	public String clientSecret;

	/**
	 * The game identifier string provided by the %Gluon support team 
	 * (Optional)
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public int protocolVersion;

	/**
	 * (Optional)
	 */
	public String clientSdkVersion;

	/**
	 * Constructor for CreateFacebookPlayerRequest that requires all members
	 * @param nickName
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 * @param passthrough
	 */
	public CreateFacebookPlayerRequest(String nickName, String accessToken, 
			String clientId, String clientSecret, String gameId, 
			int protocolVersion, String clientSdkVersion, String passthrough){
		this.nickName = nickName;
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreateFacebookPlayerRequest without super class members
	 * @param nickName
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 */
	public CreateFacebookPlayerRequest(String nickName, String accessToken, 
			String clientId, String clientSecret, String gameId, 
			int protocolVersion, String clientSdkVersion){
		this.nickName = nickName;
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;

	}

	/**
	 * Default Constructor for CreateFacebookPlayerRequest
	*/
	public CreateFacebookPlayerRequest(){

	}
}
/** @} */
