/**
 * @file JoinRoomResponse.java
 * @page classJoinRoomResponse JoinRoomResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class JoinRoomResponse: (roomName, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message confirming that the player joined the specified chat room
 * 
*/
@JsonTypeName("JoinedRoom")
public class JoinRoomResponse extends GluonResponse{

	/**
	 * Name of the room that the player joined
	 */
	public String roomName;

	/**
	 * Constructor for JoinRoomResponse that requires all members
	 * @param roomName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public JoinRoomResponse(String roomName, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.roomName = roomName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for JoinRoomResponse without super class members
	 * @param roomName
	 */
	public JoinRoomResponse(String roomName){
		this.roomName = roomName;

	}
	/**
	 * Constructor for JoinRoomResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public JoinRoomResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for JoinRoomResponse
	*/
	public JoinRoomResponse(){

	}
}
/** @} */
