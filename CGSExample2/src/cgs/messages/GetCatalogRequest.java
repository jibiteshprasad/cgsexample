/**
 * @file GetCatalogRequest.java
 * @page classGetCatalogRequest GetCatalogRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCatalogRequest: (baseCatalogName, variantCatalogNames, sorting, 
 * 		pageIndex, productsPerPage, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request from the client asking for a list of products offered to customers 
 * in the requested catalogs
*/
@JsonTypeName("GetCatalogRequest")
public class GetCatalogRequest extends GluonRequest{

	/**
	 * Base catalog name. A base catalog refers to a catalog at it's standard 
	 * price.
	 */
	public String baseCatalogName;

	/**
	 * List of variant catalog names. A variant catalog refers to a catalog 
	 * which is the same as the base, but with different pricing (i.e. sale). 
	 * (Optional)
	 */
	public List<String> variantCatalogNames;

	/**
	 * Custom sorting defined by CatalogSort entity. List is used to prioritize 
	 * sorting order amongst different product properties. (Optional)
	 */
	public List<CatalogSort> sorting;

	/**
	 * Current page index (Optional)
	 */
	public int pageIndex;

	/**
	 * Number of products per page (Optional)
	 */
	public int productsPerPage;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetCatalogResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetCatalogResponseDelegate){
                ((GetCatalogResponseDelegate)delegate).onGetCatalogResponse(this, (GetCatalogResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetCatalogResponseDelegate){
            ((GetCatalogResponseDelegate)delegate).onGetCatalogResponse(this, new GetCatalogResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetCatalogRequest that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogNames
	 * @param sorting
	 * @param pageIndex
	 * @param productsPerPage
	 * @param passthrough
	 */
	public GetCatalogRequest(String baseCatalogName, 
			List<String> variantCatalogNames, List<CatalogSort> sorting, 
			int pageIndex, int productsPerPage, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogNames = variantCatalogNames;
		this.sorting = sorting;
		this.pageIndex = pageIndex;
		this.productsPerPage = productsPerPage;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCatalogRequest without super class members
	 * @param baseCatalogName
	 * @param variantCatalogNames
	 * @param sorting
	 * @param pageIndex
	 * @param productsPerPage
	 */
	public GetCatalogRequest(String baseCatalogName, 
			List<String> variantCatalogNames, List<CatalogSort> sorting, 
			int pageIndex, int productsPerPage){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogNames = variantCatalogNames;
		this.sorting = sorting;
		this.pageIndex = pageIndex;
		this.productsPerPage = productsPerPage;

	}

	/**
	 * Default Constructor for GetCatalogRequest
	*/
	public GetCatalogRequest(){

	}
}
/** @} */
