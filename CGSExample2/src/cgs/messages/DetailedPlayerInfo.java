/**
 * @file DetailedPlayerInfo.java
 * @page classDetailedPlayerInfo DetailedPlayerInfo
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DetailedPlayerInfo: (credentials, playerId, nickName)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * An extension of PlayerInfo that contains additional information about the 
 * player's attachment to external 3rd party credentials (e.g. Facebook)
*/
public class DetailedPlayerInfo {

	/**
	 * Collection of external credentials linked to this gluon account (e.g. 
	 * FACEBOOK) (Optional)
	 */
	public List<ExternalCredential> credentials;

	/**
	 * (Optional)
	 */
	public String playerId;

	/**
	 * (Optional)
	 */
	public String nickName;

	/**
	 * Constructor for DetailedPlayerInfo that requires all members
	 * @param credentials
	 * @param playerId
	 * @param nickName
	 */
	public DetailedPlayerInfo(List<ExternalCredential> credentials, 
			String playerId, String nickName){
		this.credentials = credentials;
		this.playerId = playerId;
		this.nickName = nickName;

	}

	/**
	 * Default Constructor for DetailedPlayerInfo
	*/
	public DetailedPlayerInfo(){

	}
}
/** @} */
