/**
 * @file SetAttributesForPlayersRequest.java
 * @page classSetAttributesForPlayersRequest SetAttributesForPlayersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAttributesForPlayersRequest: (playerAndAttributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets attributes for a list of players specified
 * 
*/
@JsonTypeName("SetAttributesForPlayersRequest")
public class SetAttributesForPlayersRequest extends GluonRequest{

	/**
	 *	List of players and their attribute collection you wish to set. Can set 
	 * different attributes for each player.
	 */
	public List<PlayerAndAttributes> playerAndAttributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetAttributesForPlayersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetAttributesForPlayersResponseDelegate){
                ((SetAttributesForPlayersResponseDelegate)delegate).onSetAttributesForPlayersResponse(this, (SetAttributesForPlayersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetAttributesForPlayersResponseDelegate){
            ((SetAttributesForPlayersResponseDelegate)delegate).onSetAttributesForPlayersResponse(this, new SetAttributesForPlayersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetAttributesForPlayersRequest that requires all members
	 * @param playerAndAttributes
	 * @param passthrough
	 */
	public SetAttributesForPlayersRequest(List<PlayerAndAttributes> playerAndAttributes, String passthrough){
		this.playerAndAttributes = playerAndAttributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAttributesForPlayersRequest without super class members
	 * @param playerAndAttributes
	 */
	public SetAttributesForPlayersRequest(List<PlayerAndAttributes> playerAndAttributes){
		this.playerAndAttributes = playerAndAttributes;

	}

	/**
	 * Default Constructor for SetAttributesForPlayersRequest
	*/
	public SetAttributesForPlayersRequest(){

	}
}
/** @} */
