package cgs.messages;

public enum ExistingDocumentMode
	{
	/// Replace the document with the existing tag, generating a new documentId.
	REPLACE,
	/// Replace the document with the existing tag, maintaining the same documentId.
	OVERWRITE,
	/// Do not allow setting a document with a tag that's already in-use.
	ABORT,
	/// Do not treat tags as unique - multiple files can exist under the same tag.
	IGNORE
	
}
