/**
 * @file LockAndGetAttributesResponse.java
 * @page classLockAndGetAttributesResponse LockAndGetAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LockAndGetAttributesResponse: (playerAndAttributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, returns a list of players and their attributes that 
 * have been successfully locked
*/
@JsonTypeName("LockAndGetAttributesResponse")
public class LockAndGetAttributesResponse extends GluonResponse{

	/**
	 * List of players and their attributes that have been successfully locked 
	 * (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributes;

	/**
	 * Constructor for LockAndGetAttributesResponse that requires all members
	 * @param playerAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public LockAndGetAttributesResponse(List<PlayerAndAttributes> playerAndAttributes, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.playerAndAttributes = playerAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for LockAndGetAttributesResponse without super class members
	 * @param playerAndAttributes
	 */
	public LockAndGetAttributesResponse(List<PlayerAndAttributes> playerAndAttributes){
		this.playerAndAttributes = playerAndAttributes;

	}
	/**
	 * Constructor for LockAndGetAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public LockAndGetAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for LockAndGetAttributesResponse
	*/
	public LockAndGetAttributesResponse(){

	}
}
/** @} */
