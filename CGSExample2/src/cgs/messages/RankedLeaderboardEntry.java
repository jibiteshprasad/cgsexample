/**
 * @file RankedLeaderboardEntry.java
 * @page classRankedLeaderboardEntry RankedLeaderboardEntry
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RankedLeaderboardEntry: (entryValueRank)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A LeaderboardEntry with a ranking value
 * 
*/
public class RankedLeaderboardEntry {

	/**
	 * Specifies what place the entry's value has on the sorted leaderboard 
	 * (Optional)
	 */
	public long entryValueRank;

	/**
	 * Constructor for RankedLeaderboardEntry that requires all members
	 * @param entryValueRank
	 */
	public RankedLeaderboardEntry(long entryValueRank){
		this.entryValueRank = entryValueRank;

	}

	/**
	 * Default Constructor for RankedLeaderboardEntry
	*/
	public RankedLeaderboardEntry(){

	}
}
/** @} */
