/**
 * @file PlayerAuthorized.java
 * @page classPlayerAuthorized PlayerAuthorized
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class PlayerAuthorized: (playerId, edgeServerAddress, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * 
*/
@JsonTypeName("PlayerAuthorized")
public class PlayerAuthorized extends GluonRequest{

	/**
	 *
	 */
	public String playerId;

	/**
	 *
	 */
	public String edgeServerAddress;

	/**
	 * Constructor for PlayerAuthorized that requires all members
	 * @param playerId
	 * @param edgeServerAddress
	 * @param passthrough
	 */
	public PlayerAuthorized(String playerId, String edgeServerAddress, 
			String passthrough){
		this.playerId = playerId;
		this.edgeServerAddress = edgeServerAddress;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for PlayerAuthorized without super class members
	 * @param playerId
	 * @param edgeServerAddress
	 */
	public PlayerAuthorized(String playerId, String edgeServerAddress){
		this.playerId = playerId;
		this.edgeServerAddress = edgeServerAddress;

	}

	/**
	 * Default Constructor for PlayerAuthorized
	*/
	public PlayerAuthorized(){

	}
}
/** @} */
