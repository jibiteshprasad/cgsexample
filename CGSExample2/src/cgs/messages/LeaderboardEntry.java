/**
 * @file LeaderboardEntry.java
 * @page classLeaderboardEntry LeaderboardEntry
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LeaderboardEntry: (leaderboardName, ownerId, value, extraInfo)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A 'record' of the leaderboard table, typically associated with a single 
 * player (though it could also be group-based or some other game entity like a 
 * faction or region e.g.).
*/
public class LeaderboardEntry {

	/**
	 * Name of the value that this leaderboard is ranked (sorted) on - 
	 * 'Headshots', for instance
	 */
	public String leaderboardName;

	/**
	 * In the case of a player-based leaderboard, the ownerId will be set to 
	 * the 'playerId' or unique player identifier to reference the player; for 
	 * group this can be group name.
	 */
	public String ownerId;

	/**
	 * The player's (or group's etc.) value for the ranked (sorted) stat  
	 * (Optional)
	 */
	public long value;

	/**
	 * For a player-based leaderboard, this can be set to the nickname of the 
	 * player so that it can be displayed when navigating the leaderboard. 
	 * (Optional)
	 */
	public String extraInfo;

	/**
	 * Constructor for LeaderboardEntry that requires all members
	 * @param leaderboardName
	 * @param ownerId
	 * @param value
	 * @param extraInfo
	 */
	public LeaderboardEntry(String leaderboardName, String ownerId, long value, 
			String extraInfo){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;
		this.value = value;
		this.extraInfo = extraInfo;

	}

	/**
	 * Default Constructor for LeaderboardEntry
	*/
	public LeaderboardEntry(){

	}
}
/** @} */
