/**
 * @file ClaimQueuedItemsRollbackResponse.java
 * @page classClaimQueuedItemsRollbackResponse ClaimQueuedItemsRollbackResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClaimQueuedItemsRollbackResponse: (status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, Items have been rolled back and reset to unclaimed.
 * 
*/
@JsonTypeName("ClaimQueuedItemsRollbackResponse")
public class ClaimQueuedItemsRollbackResponse extends GluonResponse{

	/**
	 * Constructor for ClaimQueuedItemsRollbackResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ClaimQueuedItemsRollbackResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClaimQueuedItemsRollbackResponse without super class members
	 */
	public ClaimQueuedItemsRollbackResponse(){

	}
	/**
	 * Constructor for ClaimQueuedItemsRollbackResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ClaimQueuedItemsRollbackResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
