/**
 * @file GetQueuedItemsRequest.java
 * @page classGetQueuedItemsRequest GetQueuedItemsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetQueuedItemsRequest: (accountId, filter, queueName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Get list of items waiting to be claimed by the user
 * 
*/
@JsonTypeName("GetQueuedItemsRequest")
public class GetQueuedItemsRequest extends GluonRequest{

	/**
	 * Id of escrow account you wish to retreive items from
	 */
	public EscrowAccount accountId;

	/**
	 * Narrows the type of items being retrieved (ALL, UNCLAIMED, or PENDING)
	 */
	public Filter filter;

	/**
	 * Name of the queue to get list of items from (Optional)
	 */
	public String queueName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetQueuedItemsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetQueuedItemsResponseDelegate){
                ((GetQueuedItemsResponseDelegate)delegate).onGetQueuedItemsResponse(this, (GetQueuedItemsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetQueuedItemsResponseDelegate){
            ((GetQueuedItemsResponseDelegate)delegate).onGetQueuedItemsResponse(this, new GetQueuedItemsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetQueuedItemsRequest that requires all members
	 * @param accountId
	 * @param filter
	 * @param queueName
	 * @param passthrough
	 */
	public GetQueuedItemsRequest(EscrowAccount accountId, Filter filter, 
			String queueName, String passthrough){
		this.accountId = accountId;
		this.filter = filter;
		this.queueName = queueName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetQueuedItemsRequest without super class members
	 * @param accountId
	 * @param filter
	 * @param queueName
	 */
	public GetQueuedItemsRequest(EscrowAccount accountId, Filter filter, 
			String queueName){
		this.accountId = accountId;
		this.filter = filter;
		this.queueName = queueName;

	}

	/**
	 * Default Constructor for GetQueuedItemsRequest
	*/
	public GetQueuedItemsRequest(){

	}
}
/** @} */
