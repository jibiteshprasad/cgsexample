/**
 * @file DeleteAllMyGroupMemberAttributesRequest.java
 * @page classDeleteAllMyGroupMemberAttributesRequest DeleteAllMyGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllMyGroupMemberAttributesRequest: (groupName, groupType, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes ALL group member attributes for the authenticated player
 * 
*/
@JsonTypeName("DeleteAllMyGroupMemberAttributesRequest")
public class DeleteAllMyGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where group attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteAllMyGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteAllMyGroupMemberAttributesResponseDelegate){
                ((DeleteAllMyGroupMemberAttributesResponseDelegate)delegate).onDeleteAllMyGroupMemberAttributesResponse(this, (DeleteAllMyGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteAllMyGroupMemberAttributesResponseDelegate){
            ((DeleteAllMyGroupMemberAttributesResponseDelegate)delegate).onDeleteAllMyGroupMemberAttributesResponse(this, new DeleteAllMyGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteAllMyGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public DeleteAllMyGroupMemberAttributesRequest(String groupName, 
			String groupType, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllMyGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteAllMyGroupMemberAttributesRequest(String groupName, 
			String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for DeleteAllMyGroupMemberAttributesRequest
	*/
	public DeleteAllMyGroupMemberAttributesRequest(){

	}
}
/** @} */
