/**
 * @file GetPlayersWhoLockedMeResponse.java
 * @page classGetPlayersWhoLockedMeResponse GetPlayersWhoLockedMeResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPlayersWhoLockedMeResponse: (lockDetails, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, returns a list of players and their details who have 
 * locked the player who submitted the request.
*/
@JsonTypeName("GetPlayersWhoLockedMeResponse")
public class GetPlayersWhoLockedMeResponse extends GluonResponse{

	/**
	 * A list of players (and their respective information) who have locked the 
	 * player who submitted the request (Optional)
	 */
	public List<LockDetail> lockDetails;

	/**
	 * Constructor for GetPlayersWhoLockedMeResponse that requires all members
	 * @param lockDetails
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetPlayersWhoLockedMeResponse(List<LockDetail> lockDetails, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.lockDetails = lockDetails;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPlayersWhoLockedMeResponse without super class members
	 * @param lockDetails
	 */
	public GetPlayersWhoLockedMeResponse(List<LockDetail> lockDetails){
		this.lockDetails = lockDetails;

	}
	/**
	 * Constructor for GetPlayersWhoLockedMeResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetPlayersWhoLockedMeResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetPlayersWhoLockedMeResponse
	*/
	public GetPlayersWhoLockedMeResponse(){

	}
}
/** @} */
