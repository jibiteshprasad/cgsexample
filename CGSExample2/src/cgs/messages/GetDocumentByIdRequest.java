/**
 * @file GetDocumentByIdRequest.java
 * @page classGetDocumentByIdRequest GetDocumentByIdRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetDocumentByIdRequest: (documentId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve (download) a specific document that was previously stored (by this 
 * player)  via SetDocument.
*/
@JsonTypeName("GetDocumentByIdRequest")
public class GetDocumentByIdRequest extends GluonRequest{

	/**
	 * The unique identifier for the document, returned from the SetDocument 
	 * call
	 */
	public String documentId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetDocumentByIdResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetDocumentByIdResponseDelegate){
                ((GetDocumentByIdResponseDelegate)delegate).onGetDocumentByIdResponse(this, (GetDocumentByIdResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetDocumentByIdResponseDelegate){
            ((GetDocumentByIdResponseDelegate)delegate).onGetDocumentByIdResponse(this, new GetDocumentByIdResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetDocumentByIdRequest that requires all members
	 * @param documentId
	 * @param passthrough
	 */
	public GetDocumentByIdRequest(String documentId, String passthrough){
		this.documentId = documentId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetDocumentByIdRequest without super class members
	 * @param documentId
	 */
	public GetDocumentByIdRequest(String documentId){
		this.documentId = documentId;

	}

	/**
	 * Default Constructor for GetDocumentByIdRequest
	*/
	public GetDocumentByIdRequest(){

	}
}
/** @} */
