/**
 * @file GetMyReceivedGroupInvitesRequest.java
 * @page classGetMyReceivedGroupInvitesRequest GetMyReceivedGroupInvitesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyReceivedGroupInvitesRequest: (groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all incoming invites that were sent to the authenticated player's 
 * group
*/
@JsonTypeName("GetMyReceivedGroupInvitesRequest")
public class GetMyReceivedGroupInvitesRequest extends GluonRequest{

	/**
	 * Group Type specified on the invitation
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMyReceivedGroupInvitesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMyReceivedGroupInvitesResponseDelegate){
                ((GetMyReceivedGroupInvitesResponseDelegate)delegate).onGetMyReceivedGroupInvitesResponse(this, (GetMyReceivedGroupInvitesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMyReceivedGroupInvitesResponseDelegate){
            ((GetMyReceivedGroupInvitesResponseDelegate)delegate).onGetMyReceivedGroupInvitesResponse(this, new GetMyReceivedGroupInvitesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMyReceivedGroupInvitesRequest that requires all members
	 * @param groupType
	 * @param passthrough
	 */
	public GetMyReceivedGroupInvitesRequest(String groupType, String passthrough){
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyReceivedGroupInvitesRequest without super class members
	 * @param groupType
	 */
	public GetMyReceivedGroupInvitesRequest(String groupType){
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetMyReceivedGroupInvitesRequest
	*/
	public GetMyReceivedGroupInvitesRequest(){

	}
}
/** @} */
