/**
 * @file GetGroupsAttributesRequest.java
 * @page classGetGroupsAttributesRequest GetGroupsAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupsAttributesRequest: (groups, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all the group attributes for a list of groups specified
 * 
*/
@JsonTypeName("GetGroupsAttributesRequest")
public class GetGroupsAttributesRequest extends GluonRequest{

	/**
	 * A list of groups bundled in the GroupDescriptor, which is the group name 
	 * and type 
	 */
	public ArrayList<GroupDescriptor> groups;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetGroupsAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetGroupsAttributesResponseDelegate){
                ((GetGroupsAttributesResponseDelegate)delegate).onGetGroupsAttributesResponse(this, (GetGroupsAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetGroupsAttributesResponseDelegate){
            ((GetGroupsAttributesResponseDelegate)delegate).onGetGroupsAttributesResponse(this, new GetGroupsAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetGroupsAttributesRequest that requires all members
	 * @param groups
	 * @param passthrough
	 */
	public GetGroupsAttributesRequest(ArrayList<GroupDescriptor> groups, 
			String passthrough){
		this.groups = groups;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupsAttributesRequest without super class members
	 * @param groups
	 */
	public GetGroupsAttributesRequest(ArrayList<GroupDescriptor> groups){
		this.groups = groups;

	}

	/**
	 * Default Constructor for GetGroupsAttributesRequest
	*/
	public GetGroupsAttributesRequest(){

	}
}
/** @} */
