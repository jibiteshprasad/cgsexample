/**
 * @file SetGroupAttributesForGroupsRequest.java
 * @page classSetGroupAttributesForGroupsRequest SetGroupAttributesForGroupsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetGroupAttributesForGroupsRequest: (groupsAndAttributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets attributes for a list of groups specified
 * 
*/
@JsonTypeName("SetGroupAttributesForGroupsRequest")
public class SetGroupAttributesForGroupsRequest extends GluonRequest{

	/**
	 * List of groups and the collection of attributes you wish to set for each 
	 * group
	 */
	public List<GroupAttributes> groupsAndAttributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetGroupAttributesForGroupsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetGroupAttributesForGroupsResponseDelegate){
                ((SetGroupAttributesForGroupsResponseDelegate)delegate).onSetGroupAttributesForGroupsResponse(this, (SetGroupAttributesForGroupsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetGroupAttributesForGroupsResponseDelegate){
            ((SetGroupAttributesForGroupsResponseDelegate)delegate).onSetGroupAttributesForGroupsResponse(this, new SetGroupAttributesForGroupsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetGroupAttributesForGroupsRequest that requires all members
	 * @param groupsAndAttributes
	 * @param passthrough
	 */
	public SetGroupAttributesForGroupsRequest(List<GroupAttributes> groupsAndAttributes, String passthrough){
		this.groupsAndAttributes = groupsAndAttributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetGroupAttributesForGroupsRequest without super class members
	 * @param groupsAndAttributes
	 */
	public SetGroupAttributesForGroupsRequest(List<GroupAttributes> groupsAndAttributes){
		this.groupsAndAttributes = groupsAndAttributes;

	}

	/**
	 * Default Constructor for SetGroupAttributesForGroupsRequest
	*/
	public SetGroupAttributesForGroupsRequest(){

	}
}
/** @} */
