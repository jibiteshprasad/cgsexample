package cgs.messages;

public enum DocumentOwnerType
	{
	/// The document belongs to a specific player, so that only he can overwrite or replace.
	PLAYER,
	/// The document is intended to be retrieved by all players game-wide, for generic game config data
	/// or settings e.g.
	GAME,
	/// The document belongs to a group, so that only a member can overwrite or replace
	GROUP,
	/// The document has been flagged by a CSR for further review, and will not be retrieved via GetDocuments
	ADMINISTRATOR
	
}
