/**
 * @file GetVariantCatalogProductsResponse.java
 * @page classGetVariantCatalogProductsResponse GetVariantCatalogProductsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetVariantCatalogProductsResponse: (baseCatalogName, variantCatalogName, 
 * 		products, pageIndex, totalNumberOfPages, totalNumberOfProducts, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gets the variant products for a catalog.
*/
@JsonTypeName("GetVariantCatalogProductsResponse")
public class GetVariantCatalogProductsResponse extends GluonResponse{

	/**
	 * (Optional) (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional) (Optional)
	 */
	public String variantCatalogName;

	/**
	 * (Optional) (Optional)
	 */
	public List<CatalogProduct> products;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public int pageIndex;

	/**
	 * (Optional) (Optional)
	 */
	public int totalNumberOfPages;

	/**
	 * (Optional) (Optional)
	 */
	public int totalNumberOfProducts;

	/**
	 * Constructor for GetVariantCatalogProductsResponse that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param products
	 * @param pageIndex
	 * @param totalNumberOfPages
	 * @param totalNumberOfProducts
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetVariantCatalogProductsResponse(String baseCatalogName, 
			String variantCatalogName, List<CatalogProduct> products, 
			int pageIndex, int totalNumberOfPages, int totalNumberOfProducts, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.products = products;
		this.pageIndex = pageIndex;
		this.totalNumberOfPages = totalNumberOfPages;
		this.totalNumberOfProducts = totalNumberOfProducts;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetVariantCatalogProductsResponse without super class members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param products
	 * @param pageIndex
	 * @param totalNumberOfPages
	 * @param totalNumberOfProducts
	 */
	public GetVariantCatalogProductsResponse(String baseCatalogName, 
			String variantCatalogName, List<CatalogProduct> products, 
			int pageIndex, int totalNumberOfPages, int totalNumberOfProducts){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.products = products;
		this.pageIndex = pageIndex;
		this.totalNumberOfPages = totalNumberOfPages;
		this.totalNumberOfProducts = totalNumberOfProducts;

	}
	/**
	 * Constructor for GetVariantCatalogProductsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetVariantCatalogProductsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetVariantCatalogProductsResponse
	*/
	public GetVariantCatalogProductsResponse(){

	}
}
/** @} */
