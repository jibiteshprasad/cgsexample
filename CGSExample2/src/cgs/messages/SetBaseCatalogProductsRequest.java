/**
 * @file SetBaseCatalogProductsRequest.java
 * @page classSetBaseCatalogProductsRequest SetBaseCatalogProductsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetBaseCatalogProductsRequest: (baseCatalogName, products, userName, 
 * 		effectiveDate, endDate, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets the base products for a catalog.
*/
@JsonTypeName("SetBaseCatalogProductsRequest")
public class SetBaseCatalogProductsRequest extends GluonRequest{

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public List<CatalogProduct> products;

	/**
	 *
	 */
	public String userName;

	/**
	 * (Optional) (Optional)
	 */
	public Date effectiveDate;

	/**
	 * (Optional) (Optional) (Optional) (Optional)
	 */
	public Date endDate;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetBaseCatalogProductsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetBaseCatalogProductsResponseDelegate){
                ((SetBaseCatalogProductsResponseDelegate)delegate).onSetBaseCatalogProductsResponse(this, (SetBaseCatalogProductsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetBaseCatalogProductsResponseDelegate){
            ((SetBaseCatalogProductsResponseDelegate)delegate).onSetBaseCatalogProductsResponse(this, new SetBaseCatalogProductsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetBaseCatalogProductsRequest that requires all members
	 * @param baseCatalogName
	 * @param products
	 * @param userName
	 * @param effectiveDate
	 * @param endDate
	 * @param passthrough
	 */
	public SetBaseCatalogProductsRequest(String baseCatalogName, 
			List<CatalogProduct> products, String userName, Date effectiveDate, 
			Date endDate, String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.products = products;
		this.userName = userName;
		this.effectiveDate = effectiveDate;
		this.endDate = endDate;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetBaseCatalogProductsRequest without super class members
	 * @param baseCatalogName
	 * @param products
	 * @param userName
	 * @param effectiveDate
	 * @param endDate
	 */
	public SetBaseCatalogProductsRequest(String baseCatalogName, 
			List<CatalogProduct> products, String userName, Date effectiveDate, 
			Date endDate){
		this.baseCatalogName = baseCatalogName;
		this.products = products;
		this.userName = userName;
		this.effectiveDate = effectiveDate;
		this.endDate = endDate;

	}

	/**
	 * Default Constructor for SetBaseCatalogProductsRequest
	*/
	public SetBaseCatalogProductsRequest(){

	}
}
/** @} */
