/**
 * @file GetGroupMemberAttributesForPlayersResponse.java
 * @page classGetGroupMemberAttributesForPlayersResponse GetGroupMemberAttributesForPlayersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupMemberAttributesForPlayersResponse: (groupName, groupType, 
 * 		playersAndAttributes, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information and group member attributes for the 
 * requested playerIds will be returned
*/
@JsonTypeName("GetGroupMemberAttributesForPlayersResponse")
public class GetGroupMemberAttributesForPlayersResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Contains player information and a list of the member attributes that was 
	 * requested (Optional)
	 */
	public List<PlayerAndAttributes> playersAndAttributes;

	/**
	 * Constructor for GetGroupMemberAttributesForPlayersResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playersAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetGroupMemberAttributesForPlayersResponse(String groupName, 
			String groupType, List<PlayerAndAttributes> playersAndAttributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playersAndAttributes = playersAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupMemberAttributesForPlayersResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param playersAndAttributes
	 */
	public GetGroupMemberAttributesForPlayersResponse(String groupName, 
			String groupType, List<PlayerAndAttributes> playersAndAttributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playersAndAttributes = playersAndAttributes;

	}
	/**
	 * Constructor for GetGroupMemberAttributesForPlayersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetGroupMemberAttributesForPlayersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetGroupMemberAttributesForPlayersResponse
	*/
	public GetGroupMemberAttributesForPlayersResponse(){

	}
}
/** @} */
