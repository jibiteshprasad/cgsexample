/**
 * @file GetSentJoinGroupRequestsRequest.java
 * @page classGetSentJoinGroupRequestsRequest GetSentJoinGroupRequestsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetSentJoinGroupRequestsRequest: (groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieve all join group requests that the authenticated player has sent
 * 
*/
@JsonTypeName("GetSentJoinGroupRequestsRequest")
public class GetSentJoinGroupRequestsRequest extends GluonRequest{

	/**
	 * Name of the group's type associated to the join group request
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetSentJoinGroupRequestsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetSentJoinGroupRequestsResponseDelegate){
                ((GetSentJoinGroupRequestsResponseDelegate)delegate).onGetSentJoinGroupRequestsResponse(this, (GetSentJoinGroupRequestsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetSentJoinGroupRequestsResponseDelegate){
            ((GetSentJoinGroupRequestsResponseDelegate)delegate).onGetSentJoinGroupRequestsResponse(this, new GetSentJoinGroupRequestsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetSentJoinGroupRequestsRequest that requires all members
	 * @param groupType
	 * @param passthrough
	 */
	public GetSentJoinGroupRequestsRequest(String groupType, String passthrough){
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetSentJoinGroupRequestsRequest without super class members
	 * @param groupType
	 */
	public GetSentJoinGroupRequestsRequest(String groupType){
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetSentJoinGroupRequestsRequest
	*/
	public GetSentJoinGroupRequestsRequest(){

	}
}
/** @} */
