/**
 * @file DeleteAllGroupMemberAttributesRequest.java
 * @page classDeleteAllGroupMemberAttributesRequest DeleteAllGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllGroupMemberAttributesRequest: (groupName, groupType, playerIds, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Deletes ALL group member attributes for the members identified in the 
 * playerIds list
*/
@JsonTypeName("DeleteAllGroupMemberAttributesRequest")
public class DeleteAllGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where group attributes will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * A list of playerIds for group members whose attributes will be deleted
	 */
	public ArrayList<String> playerIds;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteAllGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteAllGroupMemberAttributesResponseDelegate){
                ((DeleteAllGroupMemberAttributesResponseDelegate)delegate).onDeleteAllGroupMemberAttributesResponse(this, (DeleteAllGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteAllGroupMemberAttributesResponseDelegate){
            ((DeleteAllGroupMemberAttributesResponseDelegate)delegate).onDeleteAllGroupMemberAttributesResponse(this, new DeleteAllGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteAllGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param passthrough
	 */
	public DeleteAllGroupMemberAttributesRequest(String groupName, 
			String groupType, ArrayList<String> playerIds, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 */
	public DeleteAllGroupMemberAttributesRequest(String groupName, 
			String groupType, ArrayList<String> playerIds){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;

	}

	/**
	 * Default Constructor for DeleteAllGroupMemberAttributesRequest
	*/
	public DeleteAllGroupMemberAttributesRequest(){

	}
}
/** @} */
