/**
 * @file LeaveRoomRequest.java
 * @page classLeaveRoomRequest LeaveRoomRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class LeaveRoomRequest: (roomName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a request to leave the specified chat room
 * 
*/
@JsonTypeName("LeaveRoom")
public class LeaveRoomRequest extends GluonRequest{

	/**
	 * Name of the room that will be left
	 */
	public String roomName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(LeaveRoomResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof LeaveRoomResponseDelegate){
                ((LeaveRoomResponseDelegate)delegate).onLeaveRoomResponse(this, (LeaveRoomResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof LeaveRoomResponseDelegate){
            ((LeaveRoomResponseDelegate)delegate).onLeaveRoomResponse(this, new LeaveRoomResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for LeaveRoomRequest that requires all members
	 * @param roomName
	 * @param passthrough
	 */
	public LeaveRoomRequest(String roomName, String passthrough){
		this.roomName = roomName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for LeaveRoomRequest without super class members
	 * @param roomName
	 */
	public LeaveRoomRequest(String roomName){
		this.roomName = roomName;

	}

	/**
	 * Default Constructor for LeaveRoomRequest
	*/
	public LeaveRoomRequest(){

	}
}
/** @} */
