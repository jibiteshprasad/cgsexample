/**
 * @file GetAllGroupAttributesResponse.java
 * @page classGetAllGroupAttributesResponse GetAllGroupAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllGroupAttributesResponse: (groupName, groupType, attributes, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all group attributes for a specified group
 * 
*/
@JsonTypeName("GetAllGroupAttributesResponse")
public class GetAllGroupAttributesResponse extends GluonResponse{

	/**
	 * Name of the group specified
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * A list of all group attributes for specified group (Optional)
	 */
	public List<GroupAttribute> attributes;

	/**
	 * Constructor for GetAllGroupAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetAllGroupAttributesResponse(String groupName, String groupType, 
			List<GroupAttribute> attributes, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllGroupAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 */
	public GetAllGroupAttributesResponse(String groupName, String groupType, 
			List<GroupAttribute> attributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;

	}
	/**
	 * Constructor for GetAllGroupAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetAllGroupAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetAllGroupAttributesResponse
	*/
	public GetAllGroupAttributesResponse(){

	}
}
/** @} */
