/**
 * @file GetJoinedRoomsRequest.java
 * @page classGetJoinedRoomsRequest GetJoinedRoomsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetJoinedRoomsRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all the room names the authenticated player has joined
 * 
*/
@JsonTypeName("GetJoinedRoomsRequest")
public class GetJoinedRoomsRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetJoinedRoomsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetJoinedRoomsResponseDelegate){
                ((GetJoinedRoomsResponseDelegate)delegate).onGetJoinedRoomsResponse(this, (GetJoinedRoomsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetJoinedRoomsResponseDelegate){
            ((GetJoinedRoomsResponseDelegate)delegate).onGetJoinedRoomsResponse(this, new GetJoinedRoomsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetJoinedRoomsRequest that requires all members
	 * @param passthrough
	 */
	public GetJoinedRoomsRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetJoinedRoomsRequest without super class members
	 */
	public GetJoinedRoomsRequest(){

	}
}
/** @} */
