/**
 * @file SubscribeToGroupNotificationsRequest.java
 * @page classSubscribeToGroupNotificationsRequest SubscribeToGroupNotificationsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SubscribeToGroupNotificationsRequest: (groups, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Subscribe to receive notifications for one or more groups when an attribute's 
 * value is updated.
*/
@JsonTypeName("SubscribeToGroupNotificationsRequest")
public class SubscribeToGroupNotificationsRequest extends GluonRequest{

	/**
	 * Collection of groups to subscribe to (Optional)
	 */
	public List<GroupDescriptor> groups;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SubscribeToGroupNotificationsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SubscribeToGroupNotificationsResponseDelegate){
                ((SubscribeToGroupNotificationsResponseDelegate)delegate).onSubscribeToGroupNotificationsResponse(this, (SubscribeToGroupNotificationsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SubscribeToGroupNotificationsResponseDelegate){
            ((SubscribeToGroupNotificationsResponseDelegate)delegate).onSubscribeToGroupNotificationsResponse(this, new SubscribeToGroupNotificationsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SubscribeToGroupNotificationsRequest that requires all members
	 * @param groups
	 * @param passthrough
	 */
	public SubscribeToGroupNotificationsRequest(List<GroupDescriptor> groups, 
			String passthrough){
		this.groups = groups;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SubscribeToGroupNotificationsRequest without super class members
	 * @param groups
	 */
	public SubscribeToGroupNotificationsRequest(List<GroupDescriptor> groups){
		this.groups = groups;

	}

	/**
	 * Default Constructor for SubscribeToGroupNotificationsRequest
	*/
	public SubscribeToGroupNotificationsRequest(){

	}
}
/** @} */
