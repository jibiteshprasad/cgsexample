/**
 * @file GetReceivedJoinGroupRequestsResponse.java
 * @page classGetReceivedJoinGroupRequestsResponse GetReceivedJoinGroupRequestsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetReceivedJoinGroupRequestsResponse: (requests, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns all the join group requests for authenticated player
 * 
*/
@JsonTypeName("GetReceivedJoinGroupRequestsResponse")
public class GetReceivedJoinGroupRequestsResponse extends GluonResponse{

	/**
	 * Requests to join a group from non-member players (Optional)
	 */
	public List<ReceivedJoinGroupRequest> requests;

	/**
	 * Constructor for GetReceivedJoinGroupRequestsResponse that requires all members
	 * @param requests
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetReceivedJoinGroupRequestsResponse(List<ReceivedJoinGroupRequest> requests, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.requests = requests;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetReceivedJoinGroupRequestsResponse without super class members
	 * @param requests
	 */
	public GetReceivedJoinGroupRequestsResponse(List<ReceivedJoinGroupRequest> requests){
		this.requests = requests;

	}
	/**
	 * Constructor for GetReceivedJoinGroupRequestsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetReceivedJoinGroupRequestsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetReceivedJoinGroupRequestsResponse
	*/
	public GetReceivedJoinGroupRequestsResponse(){

	}
}
/** @} */
