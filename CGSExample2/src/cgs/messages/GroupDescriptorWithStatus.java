/**
 * @file GroupDescriptorWithStatus.java
 * @page classGroupDescriptorWithStatus GroupDescriptorWithStatus
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupDescriptorWithStatus: (subscriptionStatus, groupName, groupType)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * GroupDescriptor + GroupSubscriptionStatus to indicate if subscribed or not.
 * 
*/
public class GroupDescriptorWithStatus {

	/**
	 * Name of the group (Optional)
	 */
	public GroupSubscriptionStatus subscriptionStatus;

	/**
	 *
	 */
	public String groupName;

	/**
	 *
	 */
	public String groupType;

	/**
	 * Constructor for GroupDescriptorWithStatus that requires all members
	 * @param subscriptionStatus
	 * @param groupName
	 * @param groupType
	 */
	public GroupDescriptorWithStatus(GroupSubscriptionStatus subscriptionStatus, 
			String groupName, String groupType){
		this.subscriptionStatus = subscriptionStatus;
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GroupDescriptorWithStatus
	*/
	public GroupDescriptorWithStatus(){

	}
}
/** @} */
