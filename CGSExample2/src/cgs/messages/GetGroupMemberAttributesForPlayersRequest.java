/**
 * @file GetGroupMemberAttributesForPlayersRequest.java
 * @page classGetGroupMemberAttributesForPlayersRequest GetGroupMemberAttributesForPlayersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupMemberAttributesForPlayersRequest: (groupName, groupType, 
 * 		playerIds, attributeNames, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves a list of attributes for members specified in the playerIds list.
 * 
*/
@JsonTypeName("GetGroupMemberAttributesForPlayersRequest")
public class GetGroupMemberAttributesForPlayersRequest extends GluonRequest{

	/**
	 * Name of the group where member attributes will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * The list of playerIds for which to return the list of attributes
	 */
	public ArrayList<String> playerIds;

	/**
	 * A list of the attribute names to return or NULL for all attributes 
	 * (Optional)
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetGroupMemberAttributesForPlayersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetGroupMemberAttributesForPlayersResponseDelegate){
                ((GetGroupMemberAttributesForPlayersResponseDelegate)delegate).onGetGroupMemberAttributesForPlayersResponse(this, (GetGroupMemberAttributesForPlayersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetGroupMemberAttributesForPlayersResponseDelegate){
            ((GetGroupMemberAttributesForPlayersResponseDelegate)delegate).onGetGroupMemberAttributesForPlayersResponse(this, new GetGroupMemberAttributesForPlayersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetGroupMemberAttributesForPlayersRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributeNames
	 * @param passthrough
	 */
	public GetGroupMemberAttributesForPlayersRequest(String groupName, 
			String groupType, ArrayList<String> playerIds, 
			ArrayList<String> attributeNames, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupMemberAttributesForPlayersRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param playerIds
	 * @param attributeNames
	 */
	public GetGroupMemberAttributesForPlayersRequest(String groupName, 
			String groupType, ArrayList<String> playerIds, 
			ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.playerIds = playerIds;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for GetGroupMemberAttributesForPlayersRequest
	*/
	public GetGroupMemberAttributesForPlayersRequest(){

	}
}
/** @} */
