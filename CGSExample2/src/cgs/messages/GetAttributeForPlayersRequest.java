/**
 * @file GetAttributeForPlayersRequest.java
 * @page classGetAttributeForPlayersRequest GetAttributeForPlayersRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAttributeForPlayersRequest: (playerIds, attributeName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves a specific attribute for a list of players specified
 * 
*/
@JsonTypeName("GetAttributeForPlayersRequest")
public class GetAttributeForPlayersRequest extends GluonRequest{

	/**
	 * A list of players in question (Optional)
	 */
	public HashSet<String> playerIds;

	/**
	 * Name of the attribute in question
	 */
	public String attributeName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetAttributeForPlayersResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetAttributeForPlayersResponseDelegate){
                ((GetAttributeForPlayersResponseDelegate)delegate).onGetAttributeForPlayersResponse(this, (GetAttributeForPlayersResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetAttributeForPlayersResponseDelegate){
            ((GetAttributeForPlayersResponseDelegate)delegate).onGetAttributeForPlayersResponse(this, new GetAttributeForPlayersResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetAttributeForPlayersRequest that requires all members
	 * @param playerIds
	 * @param attributeName
	 * @param passthrough
	 */
	public GetAttributeForPlayersRequest(HashSet<String> playerIds, 
			String attributeName, String passthrough){
		this.playerIds = playerIds;
		this.attributeName = attributeName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAttributeForPlayersRequest without super class members
	 * @param playerIds
	 * @param attributeName
	 */
	public GetAttributeForPlayersRequest(HashSet<String> playerIds, 
			String attributeName){
		this.playerIds = playerIds;
		this.attributeName = attributeName;

	}

	/**
	 * Default Constructor for GetAttributeForPlayersRequest
	*/
	public GetAttributeForPlayersRequest(){

	}
}
/** @} */
