/**
 * @file DeleteAllGroupAttributesResponse.java
 * @page classDeleteAllGroupAttributesResponse DeleteAllGroupAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllGroupAttributesResponse: (groupName, groupType, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether all group attributes were 
 * deleted properly
*/
@JsonTypeName("DeleteAllGroupAttributesResponse")
public class DeleteAllGroupAttributesResponse extends GluonResponse{

	/**
	 * Name of the group whose attributes were deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Constructor for DeleteAllGroupAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteAllGroupAttributesResponse(String groupName, String groupType, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllGroupAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteAllGroupAttributesResponse(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}
	/**
	 * Constructor for DeleteAllGroupAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteAllGroupAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteAllGroupAttributesResponse
	*/
	public DeleteAllGroupAttributesResponse(){

	}
}
/** @} */
