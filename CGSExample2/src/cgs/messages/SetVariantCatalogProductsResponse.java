/**
 * @file SetVariantCatalogProductsResponse.java
 * @page classSetVariantCatalogProductsResponse SetVariantCatalogProductsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetVariantCatalogProductsResponse: (baseCatalogName, variantCatalogName, 
 * 		totalNumberOfProductsAdded, totalNumberOfProductsUpdated, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets the variant products for a catalog.
*/
@JsonTypeName("SetVariantCatalogProductsResponse")
public class SetVariantCatalogProductsResponse extends GluonResponse{

	/**
	 * (Optional) (Optional) (Optional) (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public String variantCatalogName;

	/**
	 * (Optional) (Optional)
	 */
	public int totalNumberOfProductsAdded;

	/**
	 * (Optional) (Optional)
	 */
	public int totalNumberOfProductsUpdated;

	/**
	 * Constructor for SetVariantCatalogProductsResponse that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param totalNumberOfProductsAdded
	 * @param totalNumberOfProductsUpdated
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetVariantCatalogProductsResponse(String baseCatalogName, 
			String variantCatalogName, int totalNumberOfProductsAdded, 
			int totalNumberOfProductsUpdated, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.totalNumberOfProductsAdded = totalNumberOfProductsAdded;
		this.totalNumberOfProductsUpdated = totalNumberOfProductsUpdated;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetVariantCatalogProductsResponse without super class members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param totalNumberOfProductsAdded
	 * @param totalNumberOfProductsUpdated
	 */
	public SetVariantCatalogProductsResponse(String baseCatalogName, 
			String variantCatalogName, int totalNumberOfProductsAdded, 
			int totalNumberOfProductsUpdated){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.totalNumberOfProductsAdded = totalNumberOfProductsAdded;
		this.totalNumberOfProductsUpdated = totalNumberOfProductsUpdated;

	}
	/**
	 * Constructor for SetVariantCatalogProductsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetVariantCatalogProductsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetVariantCatalogProductsResponse
	*/
	public SetVariantCatalogProductsResponse(){

	}
}
/** @} */
