/**
 * @file GetSentMailMessagesRequest.java
 * @page classGetSentMailMessagesRequest GetSentMailMessagesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetSentMailMessagesRequest: (page, size, tags, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gets the requested number of sent mail messages sent by the authenticated 
 * player
*/
@JsonTypeName("GetSentMailMessagesRequest")
public class GetSentMailMessagesRequest extends GluonRequest{

	/**
	 * Number of the page that will be returned to player (page is 0 indexed) 
	 * (Optional)
	 */
	public int page;

	/**
	 * Number of messages to be returned on one page (max size is 20) 
	 * (Optional)
	 */
	public int size;

	/**
	 * Optional list of tags to associate to this mail message for easier 
	 * searching (Optional)
	 */
	public List<String> tags;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetSentMailMessagesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetSentMailMessagesResponseDelegate){
                ((GetSentMailMessagesResponseDelegate)delegate).onGetSentMailMessagesResponse(this, (GetSentMailMessagesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetSentMailMessagesResponseDelegate){
            ((GetSentMailMessagesResponseDelegate)delegate).onGetSentMailMessagesResponse(this, new GetSentMailMessagesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetSentMailMessagesRequest that requires all members
	 * @param page
	 * @param size
	 * @param tags
	 * @param passthrough
	 */
	public GetSentMailMessagesRequest(int page, int size, List<String> tags, 
			String passthrough){
		this.page = page;
		this.size = size;
		this.tags = tags;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetSentMailMessagesRequest without super class members
	 * @param page
	 * @param size
	 * @param tags
	 */
	public GetSentMailMessagesRequest(int page, int size, List<String> tags){
		this.page = page;
		this.size = size;
		this.tags = tags;

	}

	/**
	 * Default Constructor for GetSentMailMessagesRequest
	*/
	public GetSentMailMessagesRequest(){

	}
}
/** @} */
