/**
 * @file GetAllMyGroupMemberAttributesRequest.java
 * @page classGetAllMyGroupMemberAttributesRequest GetAllMyGroupMemberAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllMyGroupMemberAttributesRequest: (groupName, groupType, pageIndex, 
 * 		attributeSize, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves ALL group member attributes for authenticated player
 * 
*/
@JsonTypeName("GetAllMyGroupMemberAttributesRequest")
public class GetAllMyGroupMemberAttributesRequest extends GluonRequest{

	/**
	 * Name of the group where attributes will be retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * Page index for attributes to be retrieved (starts at 0) (Optional)
	 */
	public int pageIndex;

	/**
	 * The number of attributes per page (Optional)
	 */
	public int attributeSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetAllMyGroupMemberAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetAllMyGroupMemberAttributesResponseDelegate){
                ((GetAllMyGroupMemberAttributesResponseDelegate)delegate).onGetAllMyGroupMemberAttributesResponse(this, (GetAllMyGroupMemberAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetAllMyGroupMemberAttributesResponseDelegate){
            ((GetAllMyGroupMemberAttributesResponseDelegate)delegate).onGetAllMyGroupMemberAttributesResponse(this, new GetAllMyGroupMemberAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetAllMyGroupMemberAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param attributeSize
	 * @param passthrough
	 */
	public GetAllMyGroupMemberAttributesRequest(String groupName, 
			String groupType, int pageIndex, int attributeSize, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.attributeSize = attributeSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllMyGroupMemberAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param attributeSize
	 */
	public GetAllMyGroupMemberAttributesRequest(String groupName, 
			String groupType, int pageIndex, int attributeSize){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.attributeSize = attributeSize;

	}

	/**
	 * Default Constructor for GetAllMyGroupMemberAttributesRequest
	*/
	public GetAllMyGroupMemberAttributesRequest(){

	}
}
/** @} */
