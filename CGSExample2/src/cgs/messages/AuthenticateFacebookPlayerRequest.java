/**
 * @file AuthenticateFacebookPlayerRequest.java
 * @page classAuthenticateFacebookPlayerRequest AuthenticateFacebookPlayerRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AuthenticateFacebookPlayerRequest: (accessToken, clientId, clientSecret, 
 * 		gameId, protocolVersion, clientSdkVersion, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Login to the %Gluon system using Facebook credentials; the Facebook account 
 * must already exist in the system, which happens via 
 * AttachFacebookCredentialsRequest or CreateFacebookPlayerRequest. 
*/
@JsonTypeName("")
public class AuthenticateFacebookPlayerRequest extends GluonRequest{

	/**
	 * Returned from Facebook in the login response (specifically the 
	 * "accessToken" within the "authResponse") (Optional)
	 */
	public String accessToken;

	/**
	 * Returned from Facebook in the login response (specifically the "userID" 
	 * within the "authResponse") (Optional)
	 */
	public String clientId;

	/**
	 * Any non-empty string (Optional)
	 */
	public String clientSecret;

	/**
	 * The game identifier string provided by the %Gluon support team 
	 * (Optional)
	 */
	public String gameId;

	/**
	 * (Optional)
	 */
	public int protocolVersion;

	/**
	 * (Optional)
	 */
	public String clientSdkVersion;

	/**
	 * Constructor for AuthenticateFacebookPlayerRequest that requires all members
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 * @param passthrough
	 */
	public AuthenticateFacebookPlayerRequest(String accessToken, String clientId, 
			String clientSecret, String gameId, int protocolVersion, 
			String clientSdkVersion, String passthrough){
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AuthenticateFacebookPlayerRequest without super class members
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param gameId
	 * @param protocolVersion
	 * @param clientSdkVersion
	 */
	public AuthenticateFacebookPlayerRequest(String accessToken, String clientId, 
			String clientSecret, String gameId, int protocolVersion, 
			String clientSdkVersion){
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.gameId = gameId;
		this.protocolVersion = protocolVersion;
		this.clientSdkVersion = clientSdkVersion;

	}

	/**
	 * Default Constructor for AuthenticateFacebookPlayerRequest
	*/
	public AuthenticateFacebookPlayerRequest(){

	}
}
/** @} */
