/**
 * @file GetGroupMatchesRequest.java
 * @page classGetGroupMatchesRequest GetGroupMatchesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupMatchesRequest: (queryName, maxMatchesToReturn, 
 * 		referenceGroupName, referenceGroupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Asks for a list of groups to be potentially matched against following the 
 * criteria set in  the request parameters
*/
@JsonTypeName("GetGroupMatchesRequest")
public class GetGroupMatchesRequest extends GluonRequest{

	/**
	 * Name of the matchmaking algorithm that will be used. This is set on the 
	 * %Gluon backend.
	 */
	public String queryName;

	/**
	 * The number of matches requested. The server will try to fulfill this 
	 * request, but cannot guarantee it.
	 */
	public int maxMatchesToReturn;

	/**
	 * To perform a query based off of a specific group's attribute, the 
	 * reference group's identifier, which is group name and type, is required 
	 * (Optional)
	 */
	public String referenceGroupName;

	/**
	 * To perform a query based off of a specific group's attribute, the 
	 * reference group's identifier, which is group name and type, is required 
	 * (Optional)
	 */
	public String referenceGroupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetGroupMatchesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetGroupMatchesResponseDelegate){
                ((GetGroupMatchesResponseDelegate)delegate).onGetGroupMatchesResponse(this, (GetGroupMatchesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetGroupMatchesResponseDelegate){
            ((GetGroupMatchesResponseDelegate)delegate).onGetGroupMatchesResponse(this, new GetGroupMatchesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetGroupMatchesRequest that requires all members
	 * @param queryName
	 * @param maxMatchesToReturn
	 * @param referenceGroupName
	 * @param referenceGroupType
	 * @param passthrough
	 */
	public GetGroupMatchesRequest(String queryName, int maxMatchesToReturn, 
			String referenceGroupName, String referenceGroupType, 
			String passthrough){
		this.queryName = queryName;
		this.maxMatchesToReturn = maxMatchesToReturn;
		this.referenceGroupName = referenceGroupName;
		this.referenceGroupType = referenceGroupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupMatchesRequest without super class members
	 * @param queryName
	 * @param maxMatchesToReturn
	 * @param referenceGroupName
	 * @param referenceGroupType
	 */
	public GetGroupMatchesRequest(String queryName, int maxMatchesToReturn, 
			String referenceGroupName, String referenceGroupType){
		this.queryName = queryName;
		this.maxMatchesToReturn = maxMatchesToReturn;
		this.referenceGroupName = referenceGroupName;
		this.referenceGroupType = referenceGroupType;

	}

	/**
	 * Default Constructor for GetGroupMatchesRequest
	*/
	public GetGroupMatchesRequest(){

	}
}
/** @} */
