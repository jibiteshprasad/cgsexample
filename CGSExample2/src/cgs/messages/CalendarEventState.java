package cgs.messages;

public enum CalendarEventState
	{
	/// Event is currently active.
	ACTIVE,
	/// Event is currently in the process of being finished, such as prizes are being computed to be awarded, etc.
	CONSUMING,
	/// Event is complete. All prizes are awarded, etc.
	CONSUMED
	
}
