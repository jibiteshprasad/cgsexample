/**
 * @file GetRandomGroupsResponse.java
 * @page classGetRandomGroupsResponse GetRandomGroupsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetRandomGroupsResponse: (groups, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns random groups according to parameters specified in the request
 * 
*/
@JsonTypeName("GetRandomGroupsResponse")
public class GetRandomGroupsResponse extends GluonResponse{

	/**
	 * Detailed information (such as owner name, group size, attributes, etc.) 
	 * for all groups returned (Optional)
	 */
	public List<GroupSummary> groups;

	/**
	 * Constructor for GetRandomGroupsResponse that requires all members
	 * @param groups
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetRandomGroupsResponse(List<GroupSummary> groups, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groups = groups;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetRandomGroupsResponse without super class members
	 * @param groups
	 */
	public GetRandomGroupsResponse(List<GroupSummary> groups){
		this.groups = groups;

	}
	/**
	 * Constructor for GetRandomGroupsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetRandomGroupsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetRandomGroupsResponse
	*/
	public GetRandomGroupsResponse(){

	}
}
/** @} */
