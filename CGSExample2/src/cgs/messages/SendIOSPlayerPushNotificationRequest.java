/**
 * @file SendIOSPlayerPushNotificationRequest.java
 * @page classSendIOSPlayerPushNotificationRequest SendIOSPlayerPushNotificationRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendIOSPlayerPushNotificationRequest: (playerIds, notification, 
 * 		displayNumber, soundFileName, tags, customData, scheduledTime, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to send IOS player push notifications.
*/
@JsonTypeName("SendIOSPlayerPushNotificationRequest")
public class SendIOSPlayerPushNotificationRequest extends GluonRequest{

	/**
	 *
	 */
	public List<String> playerIds;

	/**
	 *
	 */
	public IOSNotification notification;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public Integer displayNumber;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public String soundFileName;

	/**
	 * (Optional) (Optional) (Optional) (Optional)
	 */
	public List<String> tags;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public Map<String, Object> customData;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public List<Date> scheduledTime;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendIOSPlayerPushNotificationResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendIOSPlayerPushNotificationResponseDelegate){
                ((SendIOSPlayerPushNotificationResponseDelegate)delegate).onSendIOSPlayerPushNotificationResponse(this, (SendIOSPlayerPushNotificationResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendIOSPlayerPushNotificationResponseDelegate){
            ((SendIOSPlayerPushNotificationResponseDelegate)delegate).onSendIOSPlayerPushNotificationResponse(this, new SendIOSPlayerPushNotificationResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendIOSPlayerPushNotificationRequest that requires all members
	 * @param playerIds
	 * @param notification
	 * @param displayNumber
	 * @param soundFileName
	 * @param tags
	 * @param customData
	 * @param scheduledTime
	 * @param passthrough
	 */
	public SendIOSPlayerPushNotificationRequest(List<String> playerIds, 
			IOSNotification notification, Integer displayNumber, 
			String soundFileName, List<String> tags, Map<String, 
			Object> customData, List<Date> scheduledTime, String passthrough){
		this.playerIds = playerIds;
		this.notification = notification;
		this.displayNumber = displayNumber;
		this.soundFileName = soundFileName;
		this.tags = tags;
		this.customData = customData;
		this.scheduledTime = scheduledTime;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendIOSPlayerPushNotificationRequest without super class members
	 * @param playerIds
	 * @param notification
	 * @param displayNumber
	 * @param soundFileName
	 * @param tags
	 * @param customData
	 * @param scheduledTime
	 */
	public SendIOSPlayerPushNotificationRequest(List<String> playerIds, 
			IOSNotification notification, Integer displayNumber, 
			String soundFileName, List<String> tags, Map<String, 
			Object> customData, List<Date> scheduledTime){
		this.playerIds = playerIds;
		this.notification = notification;
		this.displayNumber = displayNumber;
		this.soundFileName = soundFileName;
		this.tags = tags;
		this.customData = customData;
		this.scheduledTime = scheduledTime;

	}

	/**
	 * Default Constructor for SendIOSPlayerPushNotificationRequest
	*/
	public SendIOSPlayerPushNotificationRequest(){

	}
}
/** @} */
