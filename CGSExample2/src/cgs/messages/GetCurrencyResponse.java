/**
 * @file GetCurrencyResponse.java
 * @page classGetCurrencyResponse GetCurrencyResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCurrencyResponse: (playerInfo, currency, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response for getting the value of a currency in the player's wallet.
 * 
*/
@JsonTypeName("GetCurrencyResponse")
public class GetCurrencyResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * The currency with its current value
	 */
	public Currency currency;

	/**
	 * Constructor for GetCurrencyResponse that requires all members
	 * @param playerInfo
	 * @param currency
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCurrencyResponse(PlayerInfo playerInfo, Currency currency, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.currency = currency;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCurrencyResponse without super class members
	 * @param playerInfo
	 * @param currency
	 */
	public GetCurrencyResponse(PlayerInfo playerInfo, Currency currency){
		this.playerInfo = playerInfo;
		this.currency = currency;

	}
	/**
	 * Constructor for GetCurrencyResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCurrencyResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCurrencyResponse
	*/
	public GetCurrencyResponse(){

	}
}
/** @} */
