/**
 * @file ConsumeIapReceiptRequest.java
 * @page classConsumeIapReceiptRequest ConsumeIapReceiptRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ConsumeIapReceiptRequest: (transactionId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to mark an IAP (in-app purchase) as consumed (not reusable).
 * 
*/
@JsonTypeName("ConsumeIapReceiptRequest")
public class ConsumeIapReceiptRequest extends GluonRequest{

	/**
	 * The transaction ID of the IAP (Optional)
	 */
	public String transactionId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ConsumeIapReceiptResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ConsumeIapReceiptResponseDelegate){
                ((ConsumeIapReceiptResponseDelegate)delegate).onConsumeIapReceiptResponse(this, (ConsumeIapReceiptResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ConsumeIapReceiptResponseDelegate){
            ((ConsumeIapReceiptResponseDelegate)delegate).onConsumeIapReceiptResponse(this, new ConsumeIapReceiptResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ConsumeIapReceiptRequest that requires all members
	 * @param transactionId
	 * @param passthrough
	 */
	public ConsumeIapReceiptRequest(String transactionId, String passthrough){
		this.transactionId = transactionId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ConsumeIapReceiptRequest without super class members
	 * @param transactionId
	 */
	public ConsumeIapReceiptRequest(String transactionId){
		this.transactionId = transactionId;

	}

	/**
	 * Default Constructor for ConsumeIapReceiptRequest
	*/
	public ConsumeIapReceiptRequest(){

	}
}
/** @} */
