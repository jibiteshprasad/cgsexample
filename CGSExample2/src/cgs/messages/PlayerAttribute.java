/**
 * @file PlayerAttribute.java
 * @page classPlayerAttribute PlayerAttribute
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class PlayerAttribute: (name, value, type)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A key name/value pair used for attributes
 * 
*/
public class PlayerAttribute {

	/**
	 * Name description of the attribute (e.g. Hair Color)
	 */
	public String name;

	/**
	 * Value of the attribute (e.g. Black) (Optional)
	 */
	public Object value;

	/**
	 * (Optional)
	 */
	public String type;

	/**
	 * Constructor for PlayerAttribute that requires all members
	 * @param name
	 * @param value
	 * @param type
	 */
	public PlayerAttribute(String name, Object value, String type){
		this.name = name;
		this.value = value;
		this.type = type;

	}

	/**
	 * Default Constructor for PlayerAttribute
	*/
	public PlayerAttribute(){

	}
}
/** @} */
