/**
 * @file GetMyGroupMembersResponse.java
 * @page classGetMyGroupMembersResponse GetMyGroupMembersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMyGroupMembersResponse: (group, pageIndex, memberSize, totalPages, 
 * 		totalMemberSize, members, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns group and member information for specified group
 * 
*/
@JsonTypeName("GetMyGroupMembersResponse")
public class GetMyGroupMembersResponse extends GluonResponse{

	/**
	 * Detailed information on the specified group such as owner name, group 
	 * size, attributes, etc. (Optional)
	 */
	public GroupSummary group;

	/**
	 * Page index returned for members list (Optional)
	 */
	public int pageIndex;

	/**
	 * Number of members per page (Optional)
	 */
	public int memberSize;

	/**
	 * Total number of pages for entire member list (Optional)
	 */
	public int totalPages;

	/**
	 * Total number of members in group sepcified (Optional)
	 */
	public long totalMemberSize;

	/**
	 * Detailed information on all members in the group such as name, role, and 
	 * permissions (Optional)
	 */
	public List<MyGroupMember> members;

	/**
	 * Constructor for GetMyGroupMembersResponse that requires all members
	 * @param group
	 * @param pageIndex
	 * @param memberSize
	 * @param totalPages
	 * @param totalMemberSize
	 * @param members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetMyGroupMembersResponse(GroupSummary group, int pageIndex, 
			int memberSize, int totalPages, long totalMemberSize, 
			List<MyGroupMember> members, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.group = group;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;
		this.totalPages = totalPages;
		this.totalMemberSize = totalMemberSize;
		this.members = members;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMyGroupMembersResponse without super class members
	 * @param group
	 * @param pageIndex
	 * @param memberSize
	 * @param totalPages
	 * @param totalMemberSize
	 * @param members
	 */
	public GetMyGroupMembersResponse(GroupSummary group, int pageIndex, 
			int memberSize, int totalPages, long totalMemberSize, 
			List<MyGroupMember> members){
		this.group = group;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;
		this.totalPages = totalPages;
		this.totalMemberSize = totalMemberSize;
		this.members = members;

	}
	/**
	 * Constructor for GetMyGroupMembersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetMyGroupMembersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetMyGroupMembersResponse
	*/
	public GetMyGroupMembersResponse(){

	}
}
/** @} */
