/**
 * @file ScheduleCalendarEventRequest.java
 * @page classScheduleCalendarEventRequest ScheduleCalendarEventRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ScheduleCalendarEventRequest: (calendarName, eventName, startTime, 
 * 		endTime, timezone, tags, data, userName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Schedules an event for a calendar.
*/
@JsonTypeName("ScheduleCalendarEventRequest")
public class ScheduleCalendarEventRequest extends GluonRequest{

	/**
	 *
	 */
	public String calendarName;

	/**
	 *
	 */
	public String eventName;

	/**
	 * (Optional)
	 */
	public long startTime;

	/**
	 * (Optional)
	 */
	public long endTime;

	/**
	 * (Optional)
	 */
	public int timezone;

	/**
	 * (Optional) (Optional)
	 */
	public Set<String> tags;

	/**
	 * (Optional)
	 */
	public byte[] data;

	/**
	 *
	 */
	public String userName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ScheduleCalendarEventResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ScheduleCalendarEventResponseDelegate){
                ((ScheduleCalendarEventResponseDelegate)delegate).onScheduleCalendarEventResponse(this, (ScheduleCalendarEventResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ScheduleCalendarEventResponseDelegate){
            ((ScheduleCalendarEventResponseDelegate)delegate).onScheduleCalendarEventResponse(this, new ScheduleCalendarEventResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ScheduleCalendarEventRequest that requires all members
	 * @param calendarName
	 * @param eventName
	 * @param startTime
	 * @param endTime
	 * @param timezone
	 * @param tags
	 * @param data
	 * @param userName
	 * @param passthrough
	 */
	public ScheduleCalendarEventRequest(String calendarName, String eventName, 
			long startTime, long endTime, int timezone, Set<String> tags, 
			byte[] data, String userName, String passthrough){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timezone = timezone;
		this.tags = tags;
		this.data = data;
		this.userName = userName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ScheduleCalendarEventRequest without super class members
	 * @param calendarName
	 * @param eventName
	 * @param startTime
	 * @param endTime
	 * @param timezone
	 * @param tags
	 * @param data
	 * @param userName
	 */
	public ScheduleCalendarEventRequest(String calendarName, String eventName, 
			long startTime, long endTime, int timezone, Set<String> tags, 
			byte[] data, String userName){
		this.calendarName = calendarName;
		this.eventName = eventName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timezone = timezone;
		this.tags = tags;
		this.data = data;
		this.userName = userName;

	}

	/**
	 * Default Constructor for ScheduleCalendarEventRequest
	*/
	public ScheduleCalendarEventRequest(){

	}
}
/** @} */
