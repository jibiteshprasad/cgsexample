/**
 * @file SendJoinGroupInviteResponse.java
 * @page classSendJoinGroupInviteResponse SendJoinGroupInviteResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendJoinGroupInviteResponse: (invitationId, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether the invite was sent 
 * properly
*/
@JsonTypeName("SendJoinGroupInviteResponse")
public class SendJoinGroupInviteResponse extends GluonResponse{

	/**
	 * Unique invitiation ID if invite was sent successfully (Optional)
	 */
	public String invitationId;

	/**
	 * Constructor for SendJoinGroupInviteResponse that requires all members
	 * @param invitationId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendJoinGroupInviteResponse(String invitationId, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.invitationId = invitationId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendJoinGroupInviteResponse without super class members
	 * @param invitationId
	 */
	public SendJoinGroupInviteResponse(String invitationId){
		this.invitationId = invitationId;

	}
	/**
	 * Constructor for SendJoinGroupInviteResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendJoinGroupInviteResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SendJoinGroupInviteResponse
	*/
	public SendJoinGroupInviteResponse(){

	}
}
/** @} */
