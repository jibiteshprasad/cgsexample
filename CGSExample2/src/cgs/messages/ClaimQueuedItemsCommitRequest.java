/**
 * @file ClaimQueuedItemsCommitRequest.java
 * @page classClaimQueuedItemsCommitRequest ClaimQueuedItemsCommitRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClaimQueuedItemsCommitRequest: (accountId, itemIds, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Commits and closes a claim transaction for queued items. This removes the 
 * items from the escrow service.
*/
@JsonTypeName("ClaimQueuedItemsCommitRequest")
public class ClaimQueuedItemsCommitRequest extends GluonRequest{

	/**
	 * Account of the player who will receive the item
	 */
	public EscrowAccount accountId;

	/**
	 * List of item ids to close out the claim transaction. This removes the 
	 * items from the escrow service.
	 */
	public List<String> itemIds;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ClaimQueuedItemsCommitResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ClaimQueuedItemsCommitResponseDelegate){
                ((ClaimQueuedItemsCommitResponseDelegate)delegate).onClaimQueuedItemsCommitResponse(this, (ClaimQueuedItemsCommitResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ClaimQueuedItemsCommitResponseDelegate){
            ((ClaimQueuedItemsCommitResponseDelegate)delegate).onClaimQueuedItemsCommitResponse(this, new ClaimQueuedItemsCommitResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ClaimQueuedItemsCommitRequest that requires all members
	 * @param accountId
	 * @param itemIds
	 * @param passthrough
	 */
	public ClaimQueuedItemsCommitRequest(EscrowAccount accountId, 
			List<String> itemIds, String passthrough){
		this.accountId = accountId;
		this.itemIds = itemIds;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClaimQueuedItemsCommitRequest without super class members
	 * @param accountId
	 * @param itemIds
	 */
	public ClaimQueuedItemsCommitRequest(EscrowAccount accountId, 
			List<String> itemIds){
		this.accountId = accountId;
		this.itemIds = itemIds;

	}

	/**
	 * Default Constructor for ClaimQueuedItemsCommitRequest
	*/
	public ClaimQueuedItemsCommitRequest(){

	}
}
/** @} */
