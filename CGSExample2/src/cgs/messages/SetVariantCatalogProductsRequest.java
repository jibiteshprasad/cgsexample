/**
 * @file SetVariantCatalogProductsRequest.java
 * @page classSetVariantCatalogProductsRequest SetVariantCatalogProductsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetVariantCatalogProductsRequest: (baseCatalogName, variantCatalogName, 
 * 		products, userName, effectiveDate, endDate, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets the variant products for a catalog.
*/
@JsonTypeName("SetVariantCatalogProductsRequest")
public class SetVariantCatalogProductsRequest extends GluonRequest{

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public String variantCatalogName;

	/**
	 * (Optional)
	 */
	public List<CatalogProduct> products;

	/**
	 *
	 */
	public String userName;

	/**
	 * (Optional)
	 */
	public Date effectiveDate;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public Date endDate;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetVariantCatalogProductsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetVariantCatalogProductsResponseDelegate){
                ((SetVariantCatalogProductsResponseDelegate)delegate).onSetVariantCatalogProductsResponse(this, (SetVariantCatalogProductsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetVariantCatalogProductsResponseDelegate){
            ((SetVariantCatalogProductsResponseDelegate)delegate).onSetVariantCatalogProductsResponse(this, new SetVariantCatalogProductsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetVariantCatalogProductsRequest that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param products
	 * @param userName
	 * @param effectiveDate
	 * @param endDate
	 * @param passthrough
	 */
	public SetVariantCatalogProductsRequest(String baseCatalogName, 
			String variantCatalogName, List<CatalogProduct> products, 
			String userName, Date effectiveDate, Date endDate, 
			String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.products = products;
		this.userName = userName;
		this.effectiveDate = effectiveDate;
		this.endDate = endDate;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetVariantCatalogProductsRequest without super class members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param products
	 * @param userName
	 * @param effectiveDate
	 * @param endDate
	 */
	public SetVariantCatalogProductsRequest(String baseCatalogName, 
			String variantCatalogName, List<CatalogProduct> products, 
			String userName, Date effectiveDate, Date endDate){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.products = products;
		this.userName = userName;
		this.effectiveDate = effectiveDate;
		this.endDate = endDate;

	}

	/**
	 * Default Constructor for SetVariantCatalogProductsRequest
	*/
	public SetVariantCatalogProductsRequest(){

	}
}
/** @} */
