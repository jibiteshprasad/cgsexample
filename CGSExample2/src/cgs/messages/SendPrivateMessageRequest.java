/**
 * @file SendPrivateMessageRequest.java
 * @page classSendPrivateMessageRequest SendPrivateMessageRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendPrivateMessageRequest: (to, message, timestamp, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a private message to a specific player. In addition to the 
 * corresponding response (SendPrivateMessageResponse),  this will trigger a 
 * ReceivePrivateMessageNotification for the recipient.
*/
@JsonTypeName("SendPrivateMessageRequest")
public class SendPrivateMessageRequest extends GluonRequest{

	/**
	 * PlayerId of the recipient of the message
	 */
	public String to;

	/**
	 * Message that will be sent to the named player (Optional)
	 */
	public String message;

	/**
	 * (Optional)
	 */
	public long timestamp;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendPrivateMessageResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendPrivateMessageResponseDelegate){
                ((SendPrivateMessageResponseDelegate)delegate).onSendPrivateMessageResponse(this, (SendPrivateMessageResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendPrivateMessageResponseDelegate){
            ((SendPrivateMessageResponseDelegate)delegate).onSendPrivateMessageResponse(this, new SendPrivateMessageResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendPrivateMessageRequest that requires all members
	 * @param to
	 * @param message
	 * @param timestamp
	 * @param passthrough
	 */
	public SendPrivateMessageRequest(String to, String message, long timestamp, 
			String passthrough){
		this.to = to;
		this.message = message;
		this.timestamp = timestamp;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendPrivateMessageRequest without super class members
	 * @param to
	 * @param message
	 * @param timestamp
	 */
	public SendPrivateMessageRequest(String to, String message, long timestamp){
		this.to = to;
		this.message = message;
		this.timestamp = timestamp;

	}

	/**
	 * Default Constructor for SendPrivateMessageRequest
	*/
	public SendPrivateMessageRequest(){

	}
}
/** @} */
