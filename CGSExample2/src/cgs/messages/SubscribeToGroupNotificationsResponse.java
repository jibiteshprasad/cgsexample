/**
 * @file SubscribeToGroupNotificationsResponse.java
 * @page classSubscribeToGroupNotificationsResponse SubscribeToGroupNotificationsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SubscribeToGroupNotificationsResponse: (groupsWithStatus, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether attributes were set 
 * properly
*/
@JsonTypeName("SubscribeToGroupNotificationsResponse")
public class SubscribeToGroupNotificationsResponse extends GluonResponse{

	/**
	 * Name of the group whose attributes were set (Optional)
	 */
	public List<GroupDescriptorWithStatus> groupsWithStatus;

	/**
	 * Constructor for SubscribeToGroupNotificationsResponse that requires all members
	 * @param groupsWithStatus
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SubscribeToGroupNotificationsResponse(List<GroupDescriptorWithStatus> groupsWithStatus, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.groupsWithStatus = groupsWithStatus;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SubscribeToGroupNotificationsResponse without super class members
	 * @param groupsWithStatus
	 */
	public SubscribeToGroupNotificationsResponse(List<GroupDescriptorWithStatus> groupsWithStatus){
		this.groupsWithStatus = groupsWithStatus;

	}
	/**
	 * Constructor for SubscribeToGroupNotificationsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SubscribeToGroupNotificationsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SubscribeToGroupNotificationsResponse
	*/
	public SubscribeToGroupNotificationsResponse(){

	}
}
/** @} */
