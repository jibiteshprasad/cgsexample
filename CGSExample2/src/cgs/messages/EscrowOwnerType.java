package cgs.messages;

public enum EscrowOwnerType
	{
	/// Player-related escrow account (used to pass items from player-to-player)
	PLAYER,
	/// Group-related escrow account (used to pass items between group memebers)
	GROUP
	
}
