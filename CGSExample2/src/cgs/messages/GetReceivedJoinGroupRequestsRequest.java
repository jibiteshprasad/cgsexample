/**
 * @file GetReceivedJoinGroupRequestsRequest.java
 * @page classGetReceivedJoinGroupRequestsRequest GetReceivedJoinGroupRequestsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetReceivedJoinGroupRequestsRequest: (groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves all the join group requests sent to the group(s) of the authorized 
 * player
*/
@JsonTypeName("GetReceivedJoinGroupRequestsRequest")
public class GetReceivedJoinGroupRequestsRequest extends GluonRequest{

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetReceivedJoinGroupRequestsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetReceivedJoinGroupRequestsResponseDelegate){
                ((GetReceivedJoinGroupRequestsResponseDelegate)delegate).onGetReceivedJoinGroupRequestsResponse(this, (GetReceivedJoinGroupRequestsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetReceivedJoinGroupRequestsResponseDelegate){
            ((GetReceivedJoinGroupRequestsResponseDelegate)delegate).onGetReceivedJoinGroupRequestsResponse(this, new GetReceivedJoinGroupRequestsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetReceivedJoinGroupRequestsRequest that requires all members
	 * @param groupType
	 * @param passthrough
	 */
	public GetReceivedJoinGroupRequestsRequest(String groupType, 
			String passthrough){
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetReceivedJoinGroupRequestsRequest without super class members
	 * @param groupType
	 */
	public GetReceivedJoinGroupRequestsRequest(String groupType){
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for GetReceivedJoinGroupRequestsRequest
	*/
	public GetReceivedJoinGroupRequestsRequest(){

	}
}
/** @} */
