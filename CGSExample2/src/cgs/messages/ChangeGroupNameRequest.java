/**
 * @file ChangeGroupNameRequest.java
 * @page classChangeGroupNameRequest ChangeGroupNameRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ChangeGroupNameRequest: (groupName, groupType, newGroupName, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Changes a group name. Only owners can change the group name. Name must be 
 * unique and not already taken.
*/
@JsonTypeName("ChangeGroupNameRequest")
public class ChangeGroupNameRequest extends GluonRequest{

	/**
	 * Name of the group to change - this name is unique
	 */
	public String groupName;

	/**
	 * Name of the group's type for the group you wish to change. Group type 
	 * cannot be changed once a group is created.
	 */
	public String groupType;

	/**
	 * New name for the group. Name must be not already be taken.
	 */
	public String newGroupName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ChangeGroupNameResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ChangeGroupNameResponseDelegate){
                ((ChangeGroupNameResponseDelegate)delegate).onChangeGroupNameResponse(this, (ChangeGroupNameResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ChangeGroupNameResponseDelegate){
            ((ChangeGroupNameResponseDelegate)delegate).onChangeGroupNameResponse(this, new ChangeGroupNameResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ChangeGroupNameRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param newGroupName
	 * @param passthrough
	 */
	public ChangeGroupNameRequest(String groupName, String groupType, 
			String newGroupName, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.newGroupName = newGroupName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ChangeGroupNameRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param newGroupName
	 */
	public ChangeGroupNameRequest(String groupName, String groupType, 
			String newGroupName){
		this.groupName = groupName;
		this.groupType = groupType;
		this.newGroupName = newGroupName;

	}

	/**
	 * Default Constructor for ChangeGroupNameRequest
	*/
	public ChangeGroupNameRequest(){

	}
}
/** @} */
