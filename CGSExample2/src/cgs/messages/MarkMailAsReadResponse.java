/**
 * @file MarkMailAsReadResponse.java
 * @page classMarkMailAsReadResponse MarkMailAsReadResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class MarkMailAsReadResponse: (id, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming that the mail message specified 
 * has been marked as read
*/
@JsonTypeName("MarkMailMessageAsReadResponse")
public class MarkMailAsReadResponse extends GluonResponse{

	/**
	 * Message ID of the message marked as read
	 */
	public String id;

	/**
	 * Constructor for MarkMailAsReadResponse that requires all members
	 * @param id
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public MarkMailAsReadResponse(String id, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.id = id;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for MarkMailAsReadResponse without super class members
	 * @param id
	 */
	public MarkMailAsReadResponse(String id){
		this.id = id;

	}
	/**
	 * Constructor for MarkMailAsReadResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public MarkMailAsReadResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for MarkMailAsReadResponse
	*/
	public MarkMailAsReadResponse(){

	}
}
/** @} */
