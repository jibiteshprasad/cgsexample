/**
 * @file SetAttributesForPlayersResponse.java
 * @page classSetAttributesForPlayersResponse SetAttributesForPlayersResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAttributesForPlayersResponse: (playerAndAttributes, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, Returns a list of players and the attributes that were 
 * set for each
*/
@JsonTypeName("SetAttributesForPlayersResponse")
public class SetAttributesForPlayersResponse extends GluonResponse{

	/**
	 *	The list of players and their attributes that have been successfully set 
	 * - only difference is it will be now properly filled in with player 
	 * nicknames (Optional)
	 */
	public List<PlayerAndAttributes> playerAndAttributes;

	/**
	 * Constructor for SetAttributesForPlayersResponse that requires all members
	 * @param playerAndAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetAttributesForPlayersResponse(List<PlayerAndAttributes> playerAndAttributes, GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action, String passthrough){
		this.playerAndAttributes = playerAndAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAttributesForPlayersResponse without super class members
	 * @param playerAndAttributes
	 */
	public SetAttributesForPlayersResponse(List<PlayerAndAttributes> playerAndAttributes){
		this.playerAndAttributes = playerAndAttributes;

	}
	/**
	 * Constructor for SetAttributesForPlayersResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetAttributesForPlayersResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetAttributesForPlayersResponse
	*/
	public SetAttributesForPlayersResponse(){

	}
}
/** @} */
