/**
 * @file GetGroupAttributesRequest.java
 * @page classGetGroupAttributesRequest GetGroupAttributesRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupAttributesRequest: (groupName, groupType, attributeNames, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Retrieves multiple group attributes for a specified group
 * 
*/
@JsonTypeName("GetGroupAttributesRequest")
public class GetGroupAttributesRequest extends GluonRequest{

	/**
	 * Name of group for the attributes to be received
	 */
	public String groupName;

	/**
	 * Name of group's type
	 */
	public String groupType;

	/**
	 * A list of group attribute names of the attributes to be received
	 */
	public ArrayList<String> attributeNames;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetGroupAttributesResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetGroupAttributesResponseDelegate){
                ((GetGroupAttributesResponseDelegate)delegate).onGetGroupAttributesResponse(this, (GetGroupAttributesResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetGroupAttributesResponseDelegate){
            ((GetGroupAttributesResponseDelegate)delegate).onGetGroupAttributesResponse(this, new GetGroupAttributesResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetGroupAttributesRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 * @param passthrough
	 */
	public GetGroupAttributesRequest(String groupName, String groupType, 
			ArrayList<String> attributeNames, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupAttributesRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeNames
	 */
	public GetGroupAttributesRequest(String groupName, String groupType, 
			ArrayList<String> attributeNames){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeNames = attributeNames;

	}

	/**
	 * Default Constructor for GetGroupAttributesRequest
	*/
	public GetGroupAttributesRequest(){

	}
}
/** @} */
