/**
 * @file ApplyDeltaToGroupAttributeRequest.java
 * @page classApplyDeltaToGroupAttributeRequest ApplyDeltaToGroupAttributeRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ApplyDeltaToGroupAttributeRequest: (groupName, groupType, attributeName, 
 * 		delta, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Adds the 'delta' value to the group attribute value specified; this can be 
 * used to avoid the need to first retrieve the existing value before updating. 
 * A negative delta will decrement, a positive delta will increment. This only 
 * works on numeric values.
*/
@JsonTypeName("ApplyDeltaToGroupAttributeRequest")
public class ApplyDeltaToGroupAttributeRequest extends GluonRequest{

	/**
	 * Name of the group whose attribute will be updated
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Name of the attribute to apply delta to
	 */
	public String attributeName;

	/**
	 * The delta value to add to the requested attribute. Negative values will 
	 * decrement, positive values will increment.
	 */
	public int delta;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ApplyDeltaToGroupAttributeResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ApplyDeltaToGroupAttributeResponseDelegate){
                ((ApplyDeltaToGroupAttributeResponseDelegate)delegate).onApplyDeltaToGroupAttributeResponse(this, (ApplyDeltaToGroupAttributeResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ApplyDeltaToGroupAttributeResponseDelegate){
            ((ApplyDeltaToGroupAttributeResponseDelegate)delegate).onApplyDeltaToGroupAttributeResponse(this, new ApplyDeltaToGroupAttributeResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ApplyDeltaToGroupAttributeRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributeName
	 * @param delta
	 * @param passthrough
	 */
	public ApplyDeltaToGroupAttributeRequest(String groupName, String groupType, 
			String attributeName, int delta, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeName = attributeName;
		this.delta = delta;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ApplyDeltaToGroupAttributeRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributeName
	 * @param delta
	 */
	public ApplyDeltaToGroupAttributeRequest(String groupName, String groupType, 
			String attributeName, int delta){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributeName = attributeName;
		this.delta = delta;

	}

	/**
	 * Default Constructor for ApplyDeltaToGroupAttributeRequest
	*/
	public ApplyDeltaToGroupAttributeRequest(){

	}
}
/** @} */
