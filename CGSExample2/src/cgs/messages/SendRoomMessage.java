/**
 * @file SendRoomMessage.java
 * @page classSendRoomMessage SendRoomMessage
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendRoomMessage: (room, message, timestamp, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;

/**
 * Sends a message to everyone in the room. This will trigger a 
 * ReceiveRoomMessageNotification as its corresponding response.
*/
@JsonTypeName("SendRoomMessageRequest")
public class SendRoomMessage extends GluonRequest{

	/**
	 * Name of the room where the message will be sent
	 */
	public String room;

	/**
	 * Message that will be sent to the room (Optional)
	 */
	public String message;

	/**
	 * (Optional)
	 */
	public long timestamp;

	/**
	 * Constructor for SendRoomMessage that requires all members
	 * @param room
	 * @param message
	 * @param timestamp
	 * @param passthrough
	 */
	public SendRoomMessage(String room, String message, long timestamp, 
			String passthrough){
		this.room = room;
		this.message = message;
		this.timestamp = timestamp;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendRoomMessage without super class members
	 * @param room
	 * @param message
	 * @param timestamp
	 */
	public SendRoomMessage(String room, String message, long timestamp){
		this.room = room;
		this.message = message;
		this.timestamp = timestamp;

	}

	/**
	 * Default Constructor for SendRoomMessage
	*/
	public SendRoomMessage(){

	}
}
/** @} */
