package cgs.messages;

public enum CatalogSort
	{
	/// order by product name ascending
	NAME_ASC,
	/// order by product name descending
	NAME_DESC,
	/// order by product price ascending
	PRICE_ASC,
	/// order by product price descending
	PRICE_DESC,
	/// order by product currency ascending
	CURRENCY_ASC,
	/// order by product currency descending
	CURRENCY_DESC,
	/// order by product custom order ascending
	CUSTOM_ASC,
	/// order by product custom order descending
	CUSTOM_DESC
	
}
