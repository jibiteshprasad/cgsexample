/**
 * @file MarkMailAsReadRequest.java
 * @page classMarkMailAsReadRequest MarkMailAsReadRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class MarkMailAsReadRequest: (id, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Marks the authenticated player's message as read 
 * 
*/
@JsonTypeName("MarkMailMessageAsReadRequest")
public class MarkMailAsReadRequest extends GluonRequest{

	/**
	 * Message ID of the message that will be marked as read
	 */
	public String id;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(MarkMailAsReadResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof MarkMailAsReadResponseDelegate){
                ((MarkMailAsReadResponseDelegate)delegate).onMarkMailAsReadResponse(this, (MarkMailAsReadResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof MarkMailAsReadResponseDelegate){
            ((MarkMailAsReadResponseDelegate)delegate).onMarkMailAsReadResponse(this, new MarkMailAsReadResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for MarkMailAsReadRequest that requires all members
	 * @param id
	 * @param passthrough
	 */
	public MarkMailAsReadRequest(String id, String passthrough){
		this.id = id;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for MarkMailAsReadRequest without super class members
	 * @param id
	 */
	public MarkMailAsReadRequest(String id){
		this.id = id;

	}

	/**
	 * Default Constructor for MarkMailAsReadRequest
	*/
	public MarkMailAsReadRequest(){

	}
}
/** @} */
