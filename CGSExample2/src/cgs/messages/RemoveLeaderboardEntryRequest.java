/**
 * @file RemoveLeaderboardEntryRequest.java
 * @page classRemoveLeaderboardEntryRequest RemoveLeaderboardEntryRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class RemoveLeaderboardEntryRequest: (leaderboardName, ownerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Delete the specified entry from the specified leaderboard
 * 
*/
@JsonTypeName("RemoveLeaderboardEntryRequest")
public class RemoveLeaderboardEntryRequest extends GluonRequest{

	/**
	 * The name of the leaderboard from which the entry will be removed
	 */
	public String leaderboardName;

	/**
	 * The name of the entry that will be removed (playerId for a player-based 
	 * leaderboard)
	 */
	public String ownerId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(RemoveLeaderboardEntryResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof RemoveLeaderboardEntryResponseDelegate){
                ((RemoveLeaderboardEntryResponseDelegate)delegate).onRemoveLeaderboardEntryResponse(this, (RemoveLeaderboardEntryResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof RemoveLeaderboardEntryResponseDelegate){
            ((RemoveLeaderboardEntryResponseDelegate)delegate).onRemoveLeaderboardEntryResponse(this, new RemoveLeaderboardEntryResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for RemoveLeaderboardEntryRequest that requires all members
	 * @param leaderboardName
	 * @param ownerId
	 * @param passthrough
	 */
	public RemoveLeaderboardEntryRequest(String leaderboardName, String ownerId, 
			String passthrough){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for RemoveLeaderboardEntryRequest without super class members
	 * @param leaderboardName
	 * @param ownerId
	 */
	public RemoveLeaderboardEntryRequest(String leaderboardName, String ownerId){
		this.leaderboardName = leaderboardName;
		this.ownerId = ownerId;

	}

	/**
	 * Default Constructor for RemoveLeaderboardEntryRequest
	*/
	public RemoveLeaderboardEntryRequest(){

	}
}
/** @} */
