/**
 * @file GetCustomGameServerIdResponse.java
 * @page classGetCustomGameServerIdResponse GetCustomGameServerIdResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCustomGameServerIdResponse: (cgsId, enableStickySession, status, 
 * 		errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to a cmrs server containing the game identification information.
*/
@JsonTypeName("GetCustomGameServerIdResponse")
public class GetCustomGameServerIdResponse extends GluonResponse{

	/**
	 *
	 */
	public String cgsId;

	/**
	 *
	 */
	public boolean enableStickySession;

	/**
	 * Constructor for GetCustomGameServerIdResponse that requires all members
	 * @param cgsId
	 * @param enableStickySession
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCustomGameServerIdResponse(String cgsId, 
			boolean enableStickySession, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.cgsId = cgsId;
		this.enableStickySession = enableStickySession;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCustomGameServerIdResponse without super class members
	 * @param cgsId
	 * @param enableStickySession
	 */
	public GetCustomGameServerIdResponse(String cgsId, 
			boolean enableStickySession){
		this.cgsId = cgsId;
		this.enableStickySession = enableStickySession;

	}
	/**
	 * Constructor for GetCustomGameServerIdResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCustomGameServerIdResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCustomGameServerIdResponse
	*/
	public GetCustomGameServerIdResponse(){

	}
}
/** @} */
