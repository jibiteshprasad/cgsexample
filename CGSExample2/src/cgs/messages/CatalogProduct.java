/**
 * @file CatalogProduct.java
 * @page classCatalogProduct CatalogProduct
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CatalogProduct: (name, basePrice, basePriceCurrency, currentPrice, 
 * 		currentPriceCurrency, data, customOrder)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A product that is stored in a catalog
 * 
*/
public class CatalogProduct {

	/**
	 * Product name (Optional)
	 */
	public String name;

	/**
	 * This is the original price of a product (price is smallest unit for 
	 * currency. For example, US Dollar would be pennies) (Optional)
	 */
	public int basePrice;

	/**
	 * Base price currency type (i.e. USD, JPY, etc.) (Optional)
	 */
	public String basePriceCurrency;

	/**
	 * This is the current actual price of a product. (i.e. price change due to 
	 * sale) (Optional)
	 */
	public int currentPrice;

	/**
	 * Current price currency type  (i.e. USD, JPY, etc.) (Optional)
	 */
	public String currentPriceCurrency;

	/**
	 * Some data associated with this product. This could be actual JSON that 
	 * define product details or the product image. (Optional)
	 */
	public byte[] data;

	/**
	 * Used for custom sorting. For example, setting this to 1 will make this 
	 * product appear first in the catalog when CatalogSort is set to the 
	 * CUSTOM_ASC enum. (Optional)
	 */
	public int customOrder;

	/**
	 * Constructor for CatalogProduct that requires all members
	 * @param name
	 * @param basePrice
	 * @param basePriceCurrency
	 * @param currentPrice
	 * @param currentPriceCurrency
	 * @param data
	 * @param customOrder
	 */
	public CatalogProduct(String name, int basePrice, String basePriceCurrency, 
			int currentPrice, String currentPriceCurrency, byte[] data, 
			int customOrder){
		this.name = name;
		this.basePrice = basePrice;
		this.basePriceCurrency = basePriceCurrency;
		this.currentPrice = currentPrice;
		this.currentPriceCurrency = currentPriceCurrency;
		this.data = data;
		this.customOrder = customOrder;

	}

	/**
	 * Default Constructor for CatalogProduct
	*/
	public CatalogProduct(){

	}
}
/** @} */
