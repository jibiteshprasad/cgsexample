/**
 * @file CreateGroupRequest.java
 * @page classCreateGroupRequest CreateGroupRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CreateGroupRequest: (groupName, groupType, attributes, 
 * 		ownerAbandonBehaviour, isGroupOpen, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Creates a group with the name and type specified in the request
 * 
*/
@JsonTypeName("CreateGroupRequest")
public class CreateGroupRequest extends GluonRequest{

	/**
	 * Name of the group that will be created
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Group attributes that will be associated to the group when the group is 
	 * created
	 */
	public ArrayList<GroupAttribute> attributes;

	/**
	 * Sets how the group will exist after the owner leaves the group
	 */
	public OwnerAbandonBehaviour ownerAbandonBehaviour;

	/**
	 * Flag that determines whether a group is open to the public (Optional)
	 */
	public boolean isGroupOpen;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(CreateGroupResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof CreateGroupResponseDelegate){
                ((CreateGroupResponseDelegate)delegate).onCreateGroupResponse(this, (CreateGroupResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof CreateGroupResponseDelegate){
            ((CreateGroupResponseDelegate)delegate).onCreateGroupResponse(this, new CreateGroupResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for CreateGroupRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param ownerAbandonBehaviour
	 * @param isGroupOpen
	 * @param passthrough
	 */
	public CreateGroupRequest(String groupName, String groupType, 
			ArrayList<GroupAttribute> attributes, 
			OwnerAbandonBehaviour ownerAbandonBehaviour, boolean isGroupOpen, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.ownerAbandonBehaviour = ownerAbandonBehaviour;
		this.isGroupOpen = isGroupOpen;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CreateGroupRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param attributes
	 * @param ownerAbandonBehaviour
	 * @param isGroupOpen
	 */
	public CreateGroupRequest(String groupName, String groupType, 
			ArrayList<GroupAttribute> attributes, 
			OwnerAbandonBehaviour ownerAbandonBehaviour, boolean isGroupOpen){
		this.groupName = groupName;
		this.groupType = groupType;
		this.attributes = attributes;
		this.ownerAbandonBehaviour = ownerAbandonBehaviour;
		this.isGroupOpen = isGroupOpen;

	}

	/**
	 * Default Constructor for CreateGroupRequest
	*/
	public CreateGroupRequest(){

	}
}
/** @} */
