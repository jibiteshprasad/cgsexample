/**
 * @file SendIOSPlayerPushNotificationResponse.java
 * @page classSendIOSPlayerPushNotificationResponse SendIOSPlayerPushNotificationResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendIOSPlayerPushNotificationResponse: (status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to send IOS player push notifications.
*/
@JsonTypeName("SendIOSPlayerPushNotificationResponse")
public class SendIOSPlayerPushNotificationResponse extends GluonResponse{

	/**
	 * Constructor for SendIOSPlayerPushNotificationResponse that requires all members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendIOSPlayerPushNotificationResponse(GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendIOSPlayerPushNotificationResponse without super class members
	 */
	public SendIOSPlayerPushNotificationResponse(){

	}
	/**
	 * Constructor for SendIOSPlayerPushNotificationResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendIOSPlayerPushNotificationResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}
}
/** @} */
