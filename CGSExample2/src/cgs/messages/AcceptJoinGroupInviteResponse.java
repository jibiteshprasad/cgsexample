/**
 * @file AcceptJoinGroupInviteResponse.java
 * @page classAcceptJoinGroupInviteResponse AcceptJoinGroupInviteResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AcceptJoinGroupInviteResponse: (groupName, groupType, invitationId, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a confirmation message to the player whether the invite was 
 * successfully accepted
*/
@JsonTypeName("AcceptJoinGroupInviteResponse")
public class AcceptJoinGroupInviteResponse extends GluonResponse{

	/**
	 * Name of the group whose invitation was accepted (Optional)
	 */
	public String groupName;

	/**
	 * Name of the group's type (Optional)
	 */
	public String groupType;

	/**
	 * Unique invitation ID of invitation accepted
	 */
	public String invitationId;

	/**
	 * Constructor for AcceptJoinGroupInviteResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param invitationId
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public AcceptJoinGroupInviteResponse(String groupName, String groupType, 
			String invitationId, GluonResponseStatus status, ErrorType errorType, 
			String description, ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.invitationId = invitationId;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AcceptJoinGroupInviteResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param invitationId
	 */
	public AcceptJoinGroupInviteResponse(String groupName, String groupType, 
			String invitationId){
		this.groupName = groupName;
		this.groupType = groupType;
		this.invitationId = invitationId;

	}
	/**
	 * Constructor for AcceptJoinGroupInviteResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public AcceptJoinGroupInviteResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for AcceptJoinGroupInviteResponse
	*/
	public AcceptJoinGroupInviteResponse(){

	}
}
/** @} */
