/**
 * @file AttachFacebookCredentialsRequest.java
 * @page classAttachFacebookCredentialsRequest AttachFacebookCredentialsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class AttachFacebookCredentialsRequest: (accessToken, clientId, clientSecret, 
 * 		attributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Associate a Facebook account with the current player. Once attached, you can 
 * use either AuthenticateFacebook or AuthenticatePlayer for future logins. * If 
 * the Facebook account has already been attached to a different player, you 
 * will receive a  MULTIPLE_PLAYERS_FOR_CREDENTIAL error in the response, 
 * implying that the Facebook account is now attached to two  players. At this 
 * point you can use ListPlayersAndAttributesByCredentialRequest to retrieve 
 * attributes for each player  attached, then based on which player the user 
 * chooses to keep, SetAsPrimaryPlayer can be called to remove all attached 
 * players and keep one player account (after which the Facebook account will 
 * only be attached to the chosen player).
*/
@JsonTypeName("AttachFacebookCredentialsRequest")
public class AttachFacebookCredentialsRequest extends GluonRequest{

	/**
	 * Returned from Facebook in the login response (specifically the 
	 * "accessToken" within the "authResponse")
	 */
	public String accessToken;

	/**
	 * Returned from Facebook in the login response (specifically the "userID" 
	 * within the "authResponse")
	 */
	public String clientId;

	/**
	 * Any non-empty string
	 */
	public String clientSecret;

	/**
	 * (Optional)
	 */
	public List<String> attributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(AttachFacebookCredentialsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof AttachFacebookCredentialsResponseDelegate){
                ((AttachFacebookCredentialsResponseDelegate)delegate).onAttachFacebookCredentialsResponse(this, (AttachFacebookCredentialsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof AttachFacebookCredentialsResponseDelegate){
            ((AttachFacebookCredentialsResponseDelegate)delegate).onAttachFacebookCredentialsResponse(this, new AttachFacebookCredentialsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for AttachFacebookCredentialsRequest that requires all members
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param attributes
	 * @param passthrough
	 */
	public AttachFacebookCredentialsRequest(String accessToken, String clientId, 
			String clientSecret, List<String> attributes, String passthrough){
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.attributes = attributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for AttachFacebookCredentialsRequest without super class members
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param attributes
	 */
	public AttachFacebookCredentialsRequest(String accessToken, String clientId, 
			String clientSecret, List<String> attributes){
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.attributes = attributes;

	}

	/**
	 * Default Constructor for AttachFacebookCredentialsRequest
	*/
	public AttachFacebookCredentialsRequest(){

	}
}
/** @} */
