/**
 * @file GetGroupMatchesResponse.java
 * @page classGetGroupMatchesResponse GetGroupMatchesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetGroupMatchesResponse: (groupAttributes, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of GroupAttributes, which consists of group information and 
 * attributes, that match the  search query algorithm for groups to matchmake 
 * with.
*/
@JsonTypeName("GetGroupMatchesResponse")
public class GetGroupMatchesResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public List<GroupAttributes> groupAttributes;

	/**
	 * Constructor for GetGroupMatchesResponse that requires all members
	 * @param groupAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetGroupMatchesResponse(List<GroupAttributes> groupAttributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupAttributes = groupAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetGroupMatchesResponse without super class members
	 * @param groupAttributes
	 */
	public GetGroupMatchesResponse(List<GroupAttributes> groupAttributes){
		this.groupAttributes = groupAttributes;

	}
	/**
	 * Constructor for GetGroupMatchesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetGroupMatchesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetGroupMatchesResponse
	*/
	public GetGroupMatchesResponse(){

	}
}
/** @} */
