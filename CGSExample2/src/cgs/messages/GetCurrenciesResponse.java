/**
 * @file GetCurrenciesResponse.java
 * @page classGetCurrenciesResponse GetCurrenciesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCurrenciesResponse: (playerInfo, currencies, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response for getting all the currencies in a player's wallet and their 
 * values for the player.
*/
@JsonTypeName("GetCurrenciesResponse")
public class GetCurrenciesResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * The list of currencies (Optional)
	 */
	public List<Currency> currencies;

	/**
	 * Constructor for GetCurrenciesResponse that requires all members
	 * @param playerInfo
	 * @param currencies
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCurrenciesResponse(PlayerInfo playerInfo, 
			List<Currency> currencies, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.playerInfo = playerInfo;
		this.currencies = currencies;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCurrenciesResponse without super class members
	 * @param playerInfo
	 * @param currencies
	 */
	public GetCurrenciesResponse(PlayerInfo playerInfo, 
			List<Currency> currencies){
		this.playerInfo = playerInfo;
		this.currencies = currencies;

	}
	/**
	 * Constructor for GetCurrenciesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCurrenciesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCurrenciesResponse
	*/
	public GetCurrenciesResponse(){

	}
}
/** @} */
