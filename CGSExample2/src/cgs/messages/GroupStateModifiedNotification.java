/**
 * @file GroupStateModifiedNotification.java
 * @page classGroupStateModifiedNotification GroupStateModifiedNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GroupStateModifiedNotification: (currentValue, oldValue, group, 
 * 		modificationType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returned from the backend to a connected group member when a change has 
 * occured for the group
*/
@JsonTypeName("GroupStateModifiedNotification")
public class GroupStateModifiedNotification extends GluonRequest{

	/**
	 * Specific string value that has changed/added for the group (e.g. player 
	 * name of new member that joined) (Optional)
	 */
	public String currentValue;

	/**
	 * The string value that was previously set. (Optional)
	 */
	public String oldValue;

	/**
	 * Detailed information (such as owner name, group size, attributes, etc.) 
	 * for all groups returned
	 */
	public GroupSummary group;

	/**
	 * Type of modification that was made to the group
	 */
	public ModificationType modificationType;

	/**
	 * Constructor for GroupStateModifiedNotification that requires all members
	 * @param currentValue
	 * @param oldValue
	 * @param group
	 * @param modificationType
	 * @param passthrough
	 */
	public GroupStateModifiedNotification(String currentValue, String oldValue, 
			GroupSummary group, ModificationType modificationType, 
			String passthrough){
		this.currentValue = currentValue;
		this.oldValue = oldValue;
		this.group = group;
		this.modificationType = modificationType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GroupStateModifiedNotification without super class members
	 * @param currentValue
	 * @param oldValue
	 * @param group
	 * @param modificationType
	 */
	public GroupStateModifiedNotification(String currentValue, String oldValue, 
			GroupSummary group, ModificationType modificationType){
		this.currentValue = currentValue;
		this.oldValue = oldValue;
		this.group = group;
		this.modificationType = modificationType;

	}

	/**
	 * Default Constructor for GroupStateModifiedNotification
	*/
	public GroupStateModifiedNotification(){

	}
}
/** @} */
