/**
 * @file ResolveFacebookConflictRequest.java
 * @page classResolveFacebookConflictRequest ResolveFacebookConflictRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ResolveFacebookConflictRequest: (playerId, accessToken, clientId, 
 * 		clientSecret, loginId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Associates the specified player (and all associated attributes) or a new 
 * player with the provided facebook account and retires all other attached 
 * players.  This is intended to be used after a Facebook account has been 
 * attached to multiple players (devices e.g.) to consolidate down to one saved  
 * game for the playerId. 
*/
@JsonTypeName("ResolveFacebookConflictRequest")
public class ResolveFacebookConflictRequest extends GluonRequest{

	/**
	 * The player id to associate this facebook credential with. Older player 
	 * associations with this credential are deleted. If this is null, a new 
	 * player is created and returned back to the client. (Optional)
	 */
	public String playerId;

	/**
	 * Returned from Facebook in the login response (specifically the 
	 * "accessToken" within the "authResponse")
	 */
	public String accessToken;

	/**
	 * Returned from Facebook in the login response (specifically the "userID" 
	 * within the "authResponse")
	 */
	public String clientId;

	/**
	 * Any non-empty string
	 */
	public String clientSecret;

	/**
	 * The local udid
	 */
	public String loginId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ResolveFacebookConflictResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ResolveFacebookConflictResponseDelegate){
                ((ResolveFacebookConflictResponseDelegate)delegate).onResolveFacebookConflictResponse(this, (ResolveFacebookConflictResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ResolveFacebookConflictResponseDelegate){
            ((ResolveFacebookConflictResponseDelegate)delegate).onResolveFacebookConflictResponse(this, new ResolveFacebookConflictResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ResolveFacebookConflictRequest that requires all members
	 * @param playerId
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param loginId
	 * @param passthrough
	 */
	public ResolveFacebookConflictRequest(String playerId, String accessToken, 
			String clientId, String clientSecret, String loginId, 
			String passthrough){
		this.playerId = playerId;
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.loginId = loginId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ResolveFacebookConflictRequest without super class members
	 * @param playerId
	 * @param accessToken
	 * @param clientId
	 * @param clientSecret
	 * @param loginId
	 */
	public ResolveFacebookConflictRequest(String playerId, String accessToken, 
			String clientId, String clientSecret, String loginId){
		this.playerId = playerId;
		this.accessToken = accessToken;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.loginId = loginId;

	}

	/**
	 * Default Constructor for ResolveFacebookConflictRequest
	*/
	public ResolveFacebookConflictRequest(){

	}
}
/** @} */
