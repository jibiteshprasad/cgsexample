/**
 * @file ConsumeIapReceiptResponse.java
 * @page classConsumeIapReceiptResponse ConsumeIapReceiptResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ConsumeIapReceiptResponse: (playerInfo, transactionId, quantity, 
 * 		productId, pod, signingStatus, environment, bid, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to marking an IAP (in-app purhcase) as consumed (not reusable).
 * 
*/
@JsonTypeName("ConsumeIapReceiptResponse")
public class ConsumeIapReceiptResponse extends GluonResponse{

	/**
	 * The player identifier (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * The transaction ID of the IAP (Optional)
	 */
	public String transactionId;

	/**
	 * The quantity of the given product in this IAP (Optional)
	 */
	public String quantity;

	/**
	 * The product identifier in the IAP store (Optional)
	 */
	public String productId;

	/**
	 * The "pod" value in the IAP receipt (Optional)
	 */
	public String pod;

	/**
	 * The "signingStatus" value in the IAP receipt (Optional)
	 */
	public String signingStatus;

	/**
	 * The "environment" value in the IAP receipt (Optional)
	 */
	public String environment;

	/**
	 * The "bid" value in the IAP receipt (Optional)
	 */
	public String bid;

	/**
	 * Constructor for ConsumeIapReceiptResponse that requires all members
	 * @param playerInfo
	 * @param transactionId
	 * @param quantity
	 * @param productId
	 * @param pod
	 * @param signingStatus
	 * @param environment
	 * @param bid
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ConsumeIapReceiptResponse(PlayerInfo playerInfo, String transactionId, 
			String quantity, String productId, String pod, String signingStatus, 
			String environment, String bid, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.playerInfo = playerInfo;
		this.transactionId = transactionId;
		this.quantity = quantity;
		this.productId = productId;
		this.pod = pod;
		this.signingStatus = signingStatus;
		this.environment = environment;
		this.bid = bid;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ConsumeIapReceiptResponse without super class members
	 * @param playerInfo
	 * @param transactionId
	 * @param quantity
	 * @param productId
	 * @param pod
	 * @param signingStatus
	 * @param environment
	 * @param bid
	 */
	public ConsumeIapReceiptResponse(PlayerInfo playerInfo, String transactionId, 
			String quantity, String productId, String pod, String signingStatus, 
			String environment, String bid){
		this.playerInfo = playerInfo;
		this.transactionId = transactionId;
		this.quantity = quantity;
		this.productId = productId;
		this.pod = pod;
		this.signingStatus = signingStatus;
		this.environment = environment;
		this.bid = bid;

	}
	/**
	 * Constructor for ConsumeIapReceiptResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ConsumeIapReceiptResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ConsumeIapReceiptResponse
	*/
	public ConsumeIapReceiptResponse(){

	}
}
/** @} */
