/**
 * @file SetAttributesAndUnlockRequest.java
 * @page classSetAttributesAndUnlockRequest SetAttributesAndUnlockRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAttributesAndUnlockRequest: (playerAndAttributes, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sets the attributes for a group of players and unlocks their attribute 
 * records.  * Used in asynchronous matchmaking to remove race conditions in 
 * conjunction with LockAndGetAttributesRequest.
*/
@JsonTypeName("SetAttributesAndUnlockRequest")
public class SetAttributesAndUnlockRequest extends GluonRequest{

	/**
	 * Collection of players and their attributes you wish to set and unlock
	 */
	public List<PlayerAndAttributes> playerAndAttributes;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetAttributesAndUnlockResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetAttributesAndUnlockResponseDelegate){
                ((SetAttributesAndUnlockResponseDelegate)delegate).onSetAttributesAndUnlockResponse(this, (SetAttributesAndUnlockResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetAttributesAndUnlockResponseDelegate){
            ((SetAttributesAndUnlockResponseDelegate)delegate).onSetAttributesAndUnlockResponse(this, new SetAttributesAndUnlockResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetAttributesAndUnlockRequest that requires all members
	 * @param playerAndAttributes
	 * @param passthrough
	 */
	public SetAttributesAndUnlockRequest(List<PlayerAndAttributes> playerAndAttributes, String passthrough){
		this.playerAndAttributes = playerAndAttributes;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAttributesAndUnlockRequest without super class members
	 * @param playerAndAttributes
	 */
	public SetAttributesAndUnlockRequest(List<PlayerAndAttributes> playerAndAttributes){
		this.playerAndAttributes = playerAndAttributes;

	}

	/**
	 * Default Constructor for SetAttributesAndUnlockRequest
	*/
	public SetAttributesAndUnlockRequest(){

	}
}
/** @} */
