/**
 * @file GetFederatedTokenResponse.java
 * @page classGetFederatedTokenResponse GetFederatedTokenResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetFederatedTokenResponse: (playerInfo, gameId, expireTime, signature, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns the federated token, which can be obtained after authenticating, and 
 * used to access non-Gluon services such as the Relay Service 
*/
@JsonTypeName("GetFederatedTokenResponse")
public class GetFederatedTokenResponse extends GluonResponse{

	/**
	 * The player that was authenicated (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Assigned %Gluon game identifier (e.g. com.glu.game) (Optional)
	 */
	public String gameId;

	/**
	 * The time when the token will expire and no longer be valid for use 
	 * (Optional)
	 */
	public String expireTime;

	/**
	 * The federated token signature (Optional)
	 */
	public String signature;

	/**
	 * Constructor for GetFederatedTokenResponse that requires all members
	 * @param playerInfo
	 * @param gameId
	 * @param expireTime
	 * @param signature
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetFederatedTokenResponse(PlayerInfo playerInfo, String gameId, 
			String expireTime, String signature, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.playerInfo = playerInfo;
		this.gameId = gameId;
		this.expireTime = expireTime;
		this.signature = signature;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetFederatedTokenResponse without super class members
	 * @param playerInfo
	 * @param gameId
	 * @param expireTime
	 * @param signature
	 */
	public GetFederatedTokenResponse(PlayerInfo playerInfo, String gameId, 
			String expireTime, String signature){
		this.playerInfo = playerInfo;
		this.gameId = gameId;
		this.expireTime = expireTime;
		this.signature = signature;

	}
	/**
	 * Constructor for GetFederatedTokenResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetFederatedTokenResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetFederatedTokenResponse
	*/
	public GetFederatedTokenResponse(){

	}
}
/** @} */
