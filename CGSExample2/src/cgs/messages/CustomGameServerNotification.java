/**
 * @file CustomGameServerNotification.java
 * @page classCustomGameServerNotification CustomGameServerNotification
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CustomGameServerNotification: (payload, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A pushed message from a Custom Game Server (CGS) down to the game client 
 * without a corresponding request message.     
*/
@JsonTypeName("CustomGameServerNotification")
public class CustomGameServerNotification extends GluonResponse{

	/**
	 * The response data payload containing the data for the game (Optional)
	 */
	public Object payload;

	/**
	 * Constructor for CustomGameServerNotification that requires all members
	 * @param payload
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CustomGameServerNotification(Object payload, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.payload = payload;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CustomGameServerNotification without super class members
	 * @param payload
	 */
	public CustomGameServerNotification(Object payload){
		this.payload = payload;

	}
	/**
	 * Constructor for CustomGameServerNotification with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CustomGameServerNotification(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CustomGameServerNotification
	*/
	public CustomGameServerNotification(){

	}
}
/** @} */
