/**
 * @file SearchForGroupsRequest.java
 * @page classSearchForGroupsRequest SearchForGroupsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SearchForGroupsRequest: (searchType, searchString, groupType, pageSize, 
 * 		pageIndex, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Searches for the name of groups with the specified parameters
 * 
*/
@JsonTypeName("SearchForGroupsRequest")
public class SearchForGroupsRequest extends GluonRequest{

	/**
	 * The type of search requested when searching through group names
	 */
	public SearchType searchType;

	/**
	 * The specific string that is being searched within group names
	 */
	public String searchString;

	/**
	 * The type the group must be in for the returned search
	 */
	public String groupType;

	/**
	 * Number of results to return (Optional)
	 */
	public int pageSize;

	/**
	 * The 0 based index of the page to return (for pagination with large 
	 * result sets) (Optional)
	 */
	public int pageIndex;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SearchForGroupsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SearchForGroupsResponseDelegate){
                ((SearchForGroupsResponseDelegate)delegate).onSearchForGroupsResponse(this, (SearchForGroupsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SearchForGroupsResponseDelegate){
            ((SearchForGroupsResponseDelegate)delegate).onSearchForGroupsResponse(this, new SearchForGroupsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SearchForGroupsRequest that requires all members
	 * @param searchType
	 * @param searchString
	 * @param groupType
	 * @param pageSize
	 * @param pageIndex
	 * @param passthrough
	 */
	public SearchForGroupsRequest(SearchType searchType, String searchString, 
			String groupType, int pageSize, int pageIndex, String passthrough){
		this.searchType = searchType;
		this.searchString = searchString;
		this.groupType = groupType;
		this.pageSize = pageSize;
		this.pageIndex = pageIndex;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SearchForGroupsRequest without super class members
	 * @param searchType
	 * @param searchString
	 * @param groupType
	 * @param pageSize
	 * @param pageIndex
	 */
	public SearchForGroupsRequest(SearchType searchType, String searchString, 
			String groupType, int pageSize, int pageIndex){
		this.searchType = searchType;
		this.searchString = searchString;
		this.groupType = groupType;
		this.pageSize = pageSize;
		this.pageIndex = pageIndex;

	}

	/**
	 * Default Constructor for SearchForGroupsRequest
	*/
	public SearchForGroupsRequest(){

	}
}
/** @} */
