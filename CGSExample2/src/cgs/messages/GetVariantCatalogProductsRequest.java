/**
 * @file GetVariantCatalogProductsRequest.java
 * @page classGetVariantCatalogProductsRequest GetVariantCatalogProductsRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetVariantCatalogProductsRequest: (baseCatalogName, variantCatalogName, 
 * 		sorting, pageIndex, productsPerPage, showActiveProductsOnly, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gets the variant products for a catalog.
*/
@JsonTypeName("GetVariantCatalogProductsRequest")
public class GetVariantCatalogProductsRequest extends GluonRequest{

	/**
	 * (Optional)
	 */
	public String baseCatalogName;

	/**
	 *
	 */
	public String variantCatalogName;

	/**
	 * (Optional)
	 */
	public List<CatalogSort> sorting;

	/**
	 * (Optional) (Optional)
	 */
	public int pageIndex;

	/**
	 * (Optional)
	 */
	public int productsPerPage;

	/**
	 * (Optional)
	 */
	public boolean showActiveProductsOnly;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetVariantCatalogProductsResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetVariantCatalogProductsResponseDelegate){
                ((GetVariantCatalogProductsResponseDelegate)delegate).onGetVariantCatalogProductsResponse(this, (GetVariantCatalogProductsResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetVariantCatalogProductsResponseDelegate){
            ((GetVariantCatalogProductsResponseDelegate)delegate).onGetVariantCatalogProductsResponse(this, new GetVariantCatalogProductsResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetVariantCatalogProductsRequest that requires all members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param sorting
	 * @param pageIndex
	 * @param productsPerPage
	 * @param showActiveProductsOnly
	 * @param passthrough
	 */
	public GetVariantCatalogProductsRequest(String baseCatalogName, 
			String variantCatalogName, List<CatalogSort> sorting, int pageIndex, 
			int productsPerPage, boolean showActiveProductsOnly, 
			String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.sorting = sorting;
		this.pageIndex = pageIndex;
		this.productsPerPage = productsPerPage;
		this.showActiveProductsOnly = showActiveProductsOnly;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetVariantCatalogProductsRequest without super class members
	 * @param baseCatalogName
	 * @param variantCatalogName
	 * @param sorting
	 * @param pageIndex
	 * @param productsPerPage
	 * @param showActiveProductsOnly
	 */
	public GetVariantCatalogProductsRequest(String baseCatalogName, 
			String variantCatalogName, List<CatalogSort> sorting, int pageIndex, 
			int productsPerPage, boolean showActiveProductsOnly){
		this.baseCatalogName = baseCatalogName;
		this.variantCatalogName = variantCatalogName;
		this.sorting = sorting;
		this.pageIndex = pageIndex;
		this.productsPerPage = productsPerPage;
		this.showActiveProductsOnly = showActiveProductsOnly;

	}

	/**
	 * Default Constructor for GetVariantCatalogProductsRequest
	*/
	public GetVariantCatalogProductsRequest(){

	}
}
/** @} */
