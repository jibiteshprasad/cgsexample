/**
 * @file SetCurrencyRequest.java
 * @page classSetCurrencyRequest SetCurrencyRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetCurrencyRequest: (currencyType, value, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to set a currency value in a player's wallet.
 * 
*/
@JsonTypeName("SetCurrencyRequest")
public class SetCurrencyRequest extends GluonRequest{

	/**
	 * The currency type (Optional)
	 */
	public int currencyType;

	/**
	 * The currency value to set (Optional)
	 */
	public long value;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SetCurrencyResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SetCurrencyResponseDelegate){
                ((SetCurrencyResponseDelegate)delegate).onSetCurrencyResponse(this, (SetCurrencyResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SetCurrencyResponseDelegate){
            ((SetCurrencyResponseDelegate)delegate).onSetCurrencyResponse(this, new SetCurrencyResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SetCurrencyRequest that requires all members
	 * @param currencyType
	 * @param value
	 * @param passthrough
	 */
	public SetCurrencyRequest(int currencyType, long value, String passthrough){
		this.currencyType = currencyType;
		this.value = value;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetCurrencyRequest without super class members
	 * @param currencyType
	 * @param value
	 */
	public SetCurrencyRequest(int currencyType, long value){
		this.currencyType = currencyType;
		this.value = value;

	}

	/**
	 * Default Constructor for SetCurrencyRequest
	*/
	public SetCurrencyRequest(){

	}
}
/** @} */
