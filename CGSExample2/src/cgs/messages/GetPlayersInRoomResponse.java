/**
 * @file GetPlayersInRoomResponse.java
 * @page classGetPlayersInRoomResponse GetPlayersInRoomResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPlayersInRoomResponse: (roomName, players, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of players who are in the specified chat room
 * 
*/
@JsonTypeName("GetPlayersInRoomResponse")
public class GetPlayersInRoomResponse extends GluonResponse{

	/**
	 *
	 */
	public String roomName;

	/**
	 * (Optional)
	 */
	public Collection<PlayerInfo> players;

	/**
	 * Constructor for GetPlayersInRoomResponse that requires all members
	 * @param roomName
	 * @param players
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetPlayersInRoomResponse(String roomName, 
			Collection<PlayerInfo> players, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.roomName = roomName;
		this.players = players;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPlayersInRoomResponse without super class members
	 * @param roomName
	 * @param players
	 */
	public GetPlayersInRoomResponse(String roomName, 
			Collection<PlayerInfo> players){
		this.roomName = roomName;
		this.players = players;

	}
	/**
	 * Constructor for GetPlayersInRoomResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetPlayersInRoomResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetPlayersInRoomResponse
	*/
	public GetPlayersInRoomResponse(){

	}
}
/** @} */
