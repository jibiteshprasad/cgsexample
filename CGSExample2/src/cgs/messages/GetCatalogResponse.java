/**
 * @file GetCatalogResponse.java
 * @page classGetCatalogResponse GetCatalogResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetCatalogResponse: (products, pageIndex, totalNumberOfPages, 
 * 		totalNumberOfProducts, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of products offered to customers in the requested catalogs
 * 
*/
@JsonTypeName("GetCatalogResponse")
public class GetCatalogResponse extends GluonResponse{

	/**
	 * List of catalog products (Optional)
	 */
	public List<CatalogProduct> products;

	/**
	 * Current page index (Optional)
	 */
	public int pageIndex;

	/**
	 * Total number of pages in reference to products per page (set in the 
	 * request) (Optional)
	 */
	public int totalNumberOfPages;

	/**
	 * Total number of products in the catalog (Optional)
	 */
	public int totalNumberOfProducts;

	/**
	 * Constructor for GetCatalogResponse that requires all members
	 * @param products
	 * @param pageIndex
	 * @param totalNumberOfPages
	 * @param totalNumberOfProducts
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetCatalogResponse(List<CatalogProduct> products, int pageIndex, 
			int totalNumberOfPages, int totalNumberOfProducts, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.products = products;
		this.pageIndex = pageIndex;
		this.totalNumberOfPages = totalNumberOfPages;
		this.totalNumberOfProducts = totalNumberOfProducts;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetCatalogResponse without super class members
	 * @param products
	 * @param pageIndex
	 * @param totalNumberOfPages
	 * @param totalNumberOfProducts
	 */
	public GetCatalogResponse(List<CatalogProduct> products, int pageIndex, 
			int totalNumberOfPages, int totalNumberOfProducts){
		this.products = products;
		this.pageIndex = pageIndex;
		this.totalNumberOfPages = totalNumberOfPages;
		this.totalNumberOfProducts = totalNumberOfProducts;

	}
	/**
	 * Constructor for GetCatalogResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetCatalogResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetCatalogResponse
	*/
	public GetCatalogResponse(){

	}
}
/** @} */
