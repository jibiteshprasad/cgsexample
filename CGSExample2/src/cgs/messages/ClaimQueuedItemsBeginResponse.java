/**
 * @file ClaimQueuedItemsBeginResponse.java
 * @page classClaimQueuedItemsBeginResponse ClaimQueuedItemsBeginResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClaimQueuedItemsBeginResponse: (pendingItemIds, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is SUCCESS, Returns a list of pending items ready to claim. Items 
 * are now in a pending state.
*/
@JsonTypeName("ClaimQueuedItemsBeginResponse")
public class ClaimQueuedItemsBeginResponse extends GluonResponse{

	/**
	 * Pending items ready to be claimed (Optional)
	 */
	public List<EscrowItem> pendingItemIds;

	/**
	 * Constructor for ClaimQueuedItemsBeginResponse that requires all members
	 * @param pendingItemIds
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public ClaimQueuedItemsBeginResponse(List<EscrowItem> pendingItemIds, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.pendingItemIds = pendingItemIds;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClaimQueuedItemsBeginResponse without super class members
	 * @param pendingItemIds
	 */
	public ClaimQueuedItemsBeginResponse(List<EscrowItem> pendingItemIds){
		this.pendingItemIds = pendingItemIds;

	}
	/**
	 * Constructor for ClaimQueuedItemsBeginResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public ClaimQueuedItemsBeginResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for ClaimQueuedItemsBeginResponse
	*/
	public ClaimQueuedItemsBeginResponse(){

	}
}
/** @} */
