package cgs.messages;

public enum ValidateIapReceiptResult
	{
	/// Is Valid
	IS_VALID,
	/// Apple says this isn't any good
	NOT_VALID,
	/// Can't convert from base64 or into json - invalid ticket string.
	DECODE_FAILED,
	/// Request and ticket don't match
	BAD_BID,
	/// Retrieved version from DB has mismatch
	RETRIEVED_BID_MISMATCH,
	/// Apple service threw an exception or was down or...something.
	VERIFY_EXCEPTION,
	/// Retrieved version from DB has mismatch
	RETRIEVED_PLAYER_MISMATCH
	
}
