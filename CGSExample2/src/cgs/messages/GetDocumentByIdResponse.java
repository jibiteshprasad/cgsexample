/**
 * @file GetDocumentByIdResponse.java
 * @page classGetDocumentByIdResponse GetDocumentByIdResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetDocumentByIdResponse: (document, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the document was retrieved and is returned within the 
 * Document object.
*/
@JsonTypeName("GetDocumentByIdResponse")
public class GetDocumentByIdResponse extends GluonResponse{

	/**
	 * Contains the raw data of the document, along with some associated info 
	 * (Optional)
	 */
	public Document document;

	/**
	 * Constructor for GetDocumentByIdResponse that requires all members
	 * @param document
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetDocumentByIdResponse(Document document, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.document = document;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetDocumentByIdResponse without super class members
	 * @param document
	 */
	public GetDocumentByIdResponse(Document document){
		this.document = document;

	}
	/**
	 * Constructor for GetDocumentByIdResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetDocumentByIdResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetDocumentByIdResponse
	*/
	public GetDocumentByIdResponse(){

	}
}
/** @} */
