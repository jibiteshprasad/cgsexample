/**
 * @file GetMailRequest.java
 * @page classGetMailRequest GetMailRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetMailRequest: (page, size, unreadMessagesOnly, tags, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gets all the mail that was sent to the authenticated player
 * 
*/
@JsonTypeName("GetMailMessagesRequest")
public class GetMailRequest extends GluonRequest{

	/**
	 * Number of the page that will be returned to player (page is 0 indexed) 
	 * (Optional)
	 */
	public int page;

	/**
	 * Number of messages to be returned on one page (max size is 20) 
	 * (Optional)
	 */
	public int size;

	/**
	 * Set to true if you only want to return unread emails (Optional)
	 */
	public boolean unreadMessagesOnly;

	/**
	 * Optional list of tags to associate to this mail message for easier 
	 * searching (Optional)
	 */
	public List<String> tags;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(GetMailResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof GetMailResponseDelegate){
                ((GetMailResponseDelegate)delegate).onGetMailResponse(this, (GetMailResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof GetMailResponseDelegate){
            ((GetMailResponseDelegate)delegate).onGetMailResponse(this, new GetMailResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for GetMailRequest that requires all members
	 * @param page
	 * @param size
	 * @param unreadMessagesOnly
	 * @param tags
	 * @param passthrough
	 */
	public GetMailRequest(int page, int size, boolean unreadMessagesOnly, 
			List<String> tags, String passthrough){
		this.page = page;
		this.size = size;
		this.unreadMessagesOnly = unreadMessagesOnly;
		this.tags = tags;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetMailRequest without super class members
	 * @param page
	 * @param size
	 * @param unreadMessagesOnly
	 * @param tags
	 */
	public GetMailRequest(int page, int size, boolean unreadMessagesOnly, 
			List<String> tags){
		this.page = page;
		this.size = size;
		this.unreadMessagesOnly = unreadMessagesOnly;
		this.tags = tags;

	}

	/**
	 * Default Constructor for GetMailRequest
	*/
	public GetMailRequest(){

	}
}
/** @} */
