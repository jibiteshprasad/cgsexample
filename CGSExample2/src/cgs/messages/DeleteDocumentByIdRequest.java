/**
 * @file DeleteDocumentByIdRequest.java
 * @page classDeleteDocumentByIdRequest DeleteDocumentByIdRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteDocumentByIdRequest: (documentId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Delete the specific document that was previously stored (by this player) via 
 * SetDocument
*/
@JsonTypeName("DeleteDocumentByIdRequest")
public class DeleteDocumentByIdRequest extends GluonRequest{

	/**
	 * The unique identifier for the document, returned from the SetDocument 
	 * call
	 */
	public String documentId;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteDocumentByIdResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteDocumentByIdResponseDelegate){
                ((DeleteDocumentByIdResponseDelegate)delegate).onDeleteDocumentByIdResponse(this, (DeleteDocumentByIdResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteDocumentByIdResponseDelegate){
            ((DeleteDocumentByIdResponseDelegate)delegate).onDeleteDocumentByIdResponse(this, new DeleteDocumentByIdResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteDocumentByIdRequest that requires all members
	 * @param documentId
	 * @param passthrough
	 */
	public DeleteDocumentByIdRequest(String documentId, String passthrough){
		this.documentId = documentId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteDocumentByIdRequest without super class members
	 * @param documentId
	 */
	public DeleteDocumentByIdRequest(String documentId){
		this.documentId = documentId;

	}

	/**
	 * Default Constructor for DeleteDocumentByIdRequest
	*/
	public DeleteDocumentByIdRequest(){

	}
}
/** @} */
