/**
 * @file GetBaseCatalogProductsResponse.java
 * @page classGetBaseCatalogProductsResponse GetBaseCatalogProductsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetBaseCatalogProductsResponse: (baseCatalogName, products, pageIndex, 
 * 		totalNumberOfPages, totalNumberOfProducts, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Gets the base products for a catalog.
*/
@JsonTypeName("GetBaseCatalogProductsResponse")
public class GetBaseCatalogProductsResponse extends GluonResponse{

	/**
	 * (Optional)
	 */
	public String baseCatalogName;

	/**
	 * (Optional)
	 */
	public List<CatalogProduct> products;

	/**
	 * (Optional)
	 */
	public int pageIndex;

	/**
	 * (Optional)
	 */
	public int totalNumberOfPages;

	/**
	 * (Optional)
	 */
	public int totalNumberOfProducts;

	/**
	 * Constructor for GetBaseCatalogProductsResponse that requires all members
	 * @param baseCatalogName
	 * @param products
	 * @param pageIndex
	 * @param totalNumberOfPages
	 * @param totalNumberOfProducts
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetBaseCatalogProductsResponse(String baseCatalogName, 
			List<CatalogProduct> products, int pageIndex, int totalNumberOfPages, 
			int totalNumberOfProducts, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.baseCatalogName = baseCatalogName;
		this.products = products;
		this.pageIndex = pageIndex;
		this.totalNumberOfPages = totalNumberOfPages;
		this.totalNumberOfProducts = totalNumberOfProducts;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetBaseCatalogProductsResponse without super class members
	 * @param baseCatalogName
	 * @param products
	 * @param pageIndex
	 * @param totalNumberOfPages
	 * @param totalNumberOfProducts
	 */
	public GetBaseCatalogProductsResponse(String baseCatalogName, 
			List<CatalogProduct> products, int pageIndex, int totalNumberOfPages, 
			int totalNumberOfProducts){
		this.baseCatalogName = baseCatalogName;
		this.products = products;
		this.pageIndex = pageIndex;
		this.totalNumberOfPages = totalNumberOfPages;
		this.totalNumberOfProducts = totalNumberOfProducts;

	}
	/**
	 * Constructor for GetBaseCatalogProductsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetBaseCatalogProductsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetBaseCatalogProductsResponse
	*/
	public GetBaseCatalogProductsResponse(){

	}
}
/** @} */
