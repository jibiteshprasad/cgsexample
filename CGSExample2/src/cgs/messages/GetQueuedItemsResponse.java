/**
 * @file GetQueuedItemsResponse.java
 * @page classGetQueuedItemsResponse GetQueuedItemsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetQueuedItemsResponse: (items, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of items from the escrow queue and account specified
 * 
*/
@JsonTypeName("GetQueuedItemsResponse")
public class GetQueuedItemsResponse extends GluonResponse{

	/**
	 * List of the item ids you wish to begin a claim request for (Optional)
	 */
	public List<EscrowItem> items;

	/**
	 * Constructor for GetQueuedItemsResponse that requires all members
	 * @param items
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetQueuedItemsResponse(List<EscrowItem> items, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.items = items;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetQueuedItemsResponse without super class members
	 * @param items
	 */
	public GetQueuedItemsResponse(List<EscrowItem> items){
		this.items = items;

	}
	/**
	 * Constructor for GetQueuedItemsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetQueuedItemsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetQueuedItemsResponse
	*/
	public GetQueuedItemsResponse(){

	}
}
/** @} */
