/**
 * @file GetPlayersOnlineResponse.java
 * @page classGetPlayersOnlineResponse GetPlayersOnlineResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetPlayersOnlineResponse: (players, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a list of players in the gluon system matching the identifiers 
 * requested
*/
@JsonTypeName("")
public class GetPlayersOnlineResponse extends GluonResponse{

	/**
	 * A list of players that match the corresponding search criteria 
	 * (Optional)
	 */
	public ArrayList<PlayerInfo> players;

	/**
	 * Constructor for GetPlayersOnlineResponse that requires all members
	 * @param players
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetPlayersOnlineResponse(ArrayList<PlayerInfo> players, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.players = players;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetPlayersOnlineResponse without super class members
	 * @param players
	 */
	public GetPlayersOnlineResponse(ArrayList<PlayerInfo> players){
		this.players = players;

	}
	/**
	 * Constructor for GetPlayersOnlineResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetPlayersOnlineResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetPlayersOnlineResponse
	*/
	public GetPlayersOnlineResponse(){

	}
}
/** @} */
