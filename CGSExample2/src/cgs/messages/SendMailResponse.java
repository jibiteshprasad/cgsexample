/**
 * @file SendMailResponse.java
 * @page classSendMailResponse SendMailResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendMailResponse: (id, status, errorType, description, action, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the sender confirming whether the mail message has been 
 * sent
*/
@JsonTypeName("SendMailMessageResponse")
public class SendMailResponse extends GluonResponse{

	/**
	 * Message ID (Optional)
	 */
	public String id;

	/**
	 * Constructor for SendMailResponse that requires all members
	 * @param id
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SendMailResponse(String id, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.id = id;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendMailResponse without super class members
	 * @param id
	 */
	public SendMailResponse(String id){
		this.id = id;

	}
	/**
	 * Constructor for SendMailResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SendMailResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SendMailResponse
	*/
	public SendMailResponse(){

	}
}
/** @} */
