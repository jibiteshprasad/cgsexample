/**
 * @file SendIOSBroadcastNotificationRequest.java
 * @page classSendIOSBroadcastNotificationRequest SendIOSBroadcastNotificationRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendIOSBroadcastNotificationRequest: (notification, displayNumber, 
 * 		soundFileName, tags, customData, scheduledTime, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A request to send IOS broadcast notifications.
*/
@JsonTypeName("SendIOSBroadcastNotificationRequest")
public class SendIOSBroadcastNotificationRequest extends GluonRequest{

	/**
	 *
	 */
	public IOSNotification notification;

	/**
	 * (Optional) (Optional)
	 */
	public Integer displayNumber;

	/**
	 * (Optional) (Optional)
	 */
	public String soundFileName;

	/**
	 * (Optional) (Optional) (Optional)
	 */
	public List<String> tags;

	/**
	 * (Optional) (Optional)
	 */
	public Map<String, Object> customData;

	/**
	 * (Optional) (Optional)
	 */
	public List<Date> scheduledTime;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendIOSBroadcastNotificationResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendIOSBroadcastNotificationResponseDelegate){
                ((SendIOSBroadcastNotificationResponseDelegate)delegate).onSendIOSBroadcastNotificationResponse(this, (SendIOSBroadcastNotificationResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendIOSBroadcastNotificationResponseDelegate){
            ((SendIOSBroadcastNotificationResponseDelegate)delegate).onSendIOSBroadcastNotificationResponse(this, new SendIOSBroadcastNotificationResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendIOSBroadcastNotificationRequest that requires all members
	 * @param notification
	 * @param displayNumber
	 * @param soundFileName
	 * @param tags
	 * @param customData
	 * @param scheduledTime
	 * @param passthrough
	 */
	public SendIOSBroadcastNotificationRequest(IOSNotification notification, 
			Integer displayNumber, String soundFileName, List<String> tags, 
			Map<String, Object> customData, List<Date> scheduledTime, 
			String passthrough){
		this.notification = notification;
		this.displayNumber = displayNumber;
		this.soundFileName = soundFileName;
		this.tags = tags;
		this.customData = customData;
		this.scheduledTime = scheduledTime;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendIOSBroadcastNotificationRequest without super class members
	 * @param notification
	 * @param displayNumber
	 * @param soundFileName
	 * @param tags
	 * @param customData
	 * @param scheduledTime
	 */
	public SendIOSBroadcastNotificationRequest(IOSNotification notification, 
			Integer displayNumber, String soundFileName, List<String> tags, 
			Map<String, Object> customData, List<Date> scheduledTime){
		this.notification = notification;
		this.displayNumber = displayNumber;
		this.soundFileName = soundFileName;
		this.tags = tags;
		this.customData = customData;
		this.scheduledTime = scheduledTime;

	}

	/**
	 * Default Constructor for SendIOSBroadcastNotificationRequest
	*/
	public SendIOSBroadcastNotificationRequest(){

	}
}
/** @} */
