/**
 * @file SearchForGroupsResponse.java
 * @page classSearchForGroupsResponse SearchForGroupsResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SearchForGroupsResponse: (groups, totalGroupsFound, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * 
*/
@JsonTypeName("SearchForGroupsResponse")
public class SearchForGroupsResponse extends GluonResponse{

	/**
	 * Detailed information (such as owner name, group size, attributes, etc.) 
	 * on the groups that were returned from the search (Optional)
	 */
	public List<GroupSummary> groups;

	/**
	 * Total number of groups that were found according to the search 
	 * parameters sent in the request (Optional)
	 */
	public long totalGroupsFound;

	/**
	 * Constructor for SearchForGroupsResponse that requires all members
	 * @param groups
	 * @param totalGroupsFound
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SearchForGroupsResponse(List<GroupSummary> groups, 
			long totalGroupsFound, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groups = groups;
		this.totalGroupsFound = totalGroupsFound;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SearchForGroupsResponse without super class members
	 * @param groups
	 * @param totalGroupsFound
	 */
	public SearchForGroupsResponse(List<GroupSummary> groups, 
			long totalGroupsFound){
		this.groups = groups;
		this.totalGroupsFound = totalGroupsFound;

	}
	/**
	 * Constructor for SearchForGroupsResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SearchForGroupsResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SearchForGroupsResponse
	*/
	public SearchForGroupsResponse(){

	}
}
/** @} */
