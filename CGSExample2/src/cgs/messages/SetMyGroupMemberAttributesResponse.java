/**
 * @file SetMyGroupMemberAttributesResponse.java
 * @page classSetMyGroupMemberAttributesResponse SetMyGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetMyGroupMemberAttributesResponse: (groupName, groupType, memberInfo, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information for the authenticated player will be 
 * returned
*/
@JsonTypeName("SetMyGroupMemberAttributesResponse")
public class SetMyGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were set
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Player information for the member whose attributes were set (Optional)
	 */
	public PlayerInfo memberInfo;

	/**
	 * Constructor for SetMyGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param memberInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public SetMyGroupMemberAttributesResponse(String groupName, String groupType, 
			PlayerInfo memberInfo, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.memberInfo = memberInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetMyGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param memberInfo
	 */
	public SetMyGroupMemberAttributesResponse(String groupName, String groupType, 
			PlayerInfo memberInfo){
		this.groupName = groupName;
		this.groupType = groupType;
		this.memberInfo = memberInfo;

	}
	/**
	 * Constructor for SetMyGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public SetMyGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for SetMyGroupMemberAttributesResponse
	*/
	public SetMyGroupMemberAttributesResponse(){

	}
}
/** @} */
