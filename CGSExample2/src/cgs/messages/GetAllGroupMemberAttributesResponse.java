/**
 * @file GetAllGroupMemberAttributesResponse.java
 * @page classGetAllGroupMemberAttributesResponse GetAllGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetAllGroupMemberAttributesResponse: (groupName, groupType, pageIndex, 
 * 		memberSize, totalPages, totalMemberSize, membersWithAllAttributes, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon success, the player information and all group attributes for each member 
 * requested will be returned
*/
@JsonTypeName("GetAllGroupMemberAttributesResponse")
public class GetAllGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where member attributes were retrieved
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Page index for attributes that were retrieved (starts at 0) (Optional)
	 */
	public int pageIndex;

	/**
	 * Group member size of the current page (Optional)
	 */
	public int memberSize;

	/**
	 * Total Number of pages (Optional)
	 */
	public int totalPages;

	/**
	 * Total Number of group members (Optional)
	 */
	public long totalMemberSize;

	/**
	 * Contains a list of members' player information and a list of member 
	 * attributes for each member as specified in the request (Optional)
	 */
	public ArrayList<PlayerAndAttributes> membersWithAllAttributes;

	/**
	 * Constructor for GetAllGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param memberSize
	 * @param totalPages
	 * @param totalMemberSize
	 * @param membersWithAllAttributes
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetAllGroupMemberAttributesResponse(String groupName, 
			String groupType, int pageIndex, int memberSize, int totalPages, 
			long totalMemberSize, 
			ArrayList<PlayerAndAttributes> membersWithAllAttributes, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;
		this.totalPages = totalPages;
		this.totalMemberSize = totalMemberSize;
		this.membersWithAllAttributes = membersWithAllAttributes;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetAllGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param pageIndex
	 * @param memberSize
	 * @param totalPages
	 * @param totalMemberSize
	 * @param membersWithAllAttributes
	 */
	public GetAllGroupMemberAttributesResponse(String groupName, 
			String groupType, int pageIndex, int memberSize, int totalPages, 
			long totalMemberSize, 
			ArrayList<PlayerAndAttributes> membersWithAllAttributes){
		this.groupName = groupName;
		this.groupType = groupType;
		this.pageIndex = pageIndex;
		this.memberSize = memberSize;
		this.totalPages = totalPages;
		this.totalMemberSize = totalMemberSize;
		this.membersWithAllAttributes = membersWithAllAttributes;

	}
	/**
	 * Constructor for GetAllGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetAllGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetAllGroupMemberAttributesResponse
	*/
	public GetAllGroupMemberAttributesResponse(){

	}
}
/** @} */
