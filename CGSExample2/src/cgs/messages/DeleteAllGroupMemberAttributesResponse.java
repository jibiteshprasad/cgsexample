/**
 * @file DeleteAllGroupMemberAttributesResponse.java
 * @page classDeleteAllGroupMemberAttributesResponse DeleteAllGroupMemberAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllGroupMemberAttributesResponse: (groupName, groupType, members, 
 * 		status, errorType, description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Upon successful deletion, the player information for members whose attributes 
 * were deleted will be returned
*/
@JsonTypeName("DeleteAllGroupMemberAttributesResponse")
public class DeleteAllGroupMemberAttributesResponse extends GluonResponse{

	/**
	 * Name of the group where group attributes were deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;

	/**
	 * Player information for the members whose attributes were deleted 
	 * (Optional)
	 */
	public ArrayList<PlayerInfo> members;

	/**
	 * Constructor for DeleteAllGroupMemberAttributesResponse that requires all members
	 * @param groupName
	 * @param groupType
	 * @param members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteAllGroupMemberAttributesResponse(String groupName, 
			String groupType, ArrayList<PlayerInfo> members, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllGroupMemberAttributesResponse without super class members
	 * @param groupName
	 * @param groupType
	 * @param members
	 */
	public DeleteAllGroupMemberAttributesResponse(String groupName, 
			String groupType, ArrayList<PlayerInfo> members){
		this.groupName = groupName;
		this.groupType = groupType;
		this.members = members;

	}
	/**
	 * Constructor for DeleteAllGroupMemberAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteAllGroupMemberAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteAllGroupMemberAttributesResponse
	*/
	public DeleteAllGroupMemberAttributesResponse(){

	}
}
/** @} */
