/**
 * @file SendJoinGroupRequestRequest.java
 * @page classSendJoinGroupRequestRequest SendJoinGroupRequestRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SendJoinGroupRequestRequest: (groupName, groupType, maxGroupSize, 
 * 		passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Sends a request to join a group. groupName and groupType must match exactly 
 * to the group you wish to join.
*/
@JsonTypeName("SendJoinGroupRequestRequest")
public class SendJoinGroupRequestRequest extends GluonRequest{

	/**
	 * Name of the group to join - this name is unique
	 */
	public String groupName;

	/**
	 * Name of the group's type to join
	 */
	public String groupType;

	/**
	 * Filters request to only send if current group size is less than 
	 * maxGroupSize specified (Optional)
	 */
	public int maxGroupSize;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(SendJoinGroupRequestResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof SendJoinGroupRequestResponseDelegate){
                ((SendJoinGroupRequestResponseDelegate)delegate).onSendJoinGroupRequestResponse(this, (SendJoinGroupRequestResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof SendJoinGroupRequestResponseDelegate){
            ((SendJoinGroupRequestResponseDelegate)delegate).onSendJoinGroupRequestResponse(this, new SendJoinGroupRequestResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for SendJoinGroupRequestRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param maxGroupSize
	 * @param passthrough
	 */
	public SendJoinGroupRequestRequest(String groupName, String groupType, 
			int maxGroupSize, String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.maxGroupSize = maxGroupSize;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SendJoinGroupRequestRequest without super class members
	 * @param groupName
	 * @param groupType
	 * @param maxGroupSize
	 */
	public SendJoinGroupRequestRequest(String groupName, String groupType, 
			int maxGroupSize){
		this.groupName = groupName;
		this.groupType = groupType;
		this.maxGroupSize = maxGroupSize;

	}

	/**
	 * Default Constructor for SendJoinGroupRequestRequest
	*/
	public SendJoinGroupRequestRequest(){

	}
}
/** @} */
