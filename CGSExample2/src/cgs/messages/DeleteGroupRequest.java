/**
 * @file DeleteGroupRequest.java
 * @page classDeleteGroupRequest DeleteGroupRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteGroupRequest: (groupName, groupType, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Removes the group specifed and all members are disbanded
 * 
*/
@JsonTypeName("DeleteGroupRequest")
public class DeleteGroupRequest extends GluonRequest{

	/**
	 * Name of the group that will be deleted
	 */
	public String groupName;

	/**
	 * Name of the group's type
	 */
	public String groupType;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(DeleteGroupResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof DeleteGroupResponseDelegate){
                ((DeleteGroupResponseDelegate)delegate).onDeleteGroupResponse(this, (DeleteGroupResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof DeleteGroupResponseDelegate){
            ((DeleteGroupResponseDelegate)delegate).onDeleteGroupResponse(this, new DeleteGroupResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for DeleteGroupRequest that requires all members
	 * @param groupName
	 * @param groupType
	 * @param passthrough
	 */
	public DeleteGroupRequest(String groupName, String groupType, 
			String passthrough){
		this.groupName = groupName;
		this.groupType = groupType;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteGroupRequest without super class members
	 * @param groupName
	 * @param groupType
	 */
	public DeleteGroupRequest(String groupName, String groupType){
		this.groupName = groupName;
		this.groupType = groupType;

	}

	/**
	 * Default Constructor for DeleteGroupRequest
	*/
	public DeleteGroupRequest(){

	}
}
/** @} */
