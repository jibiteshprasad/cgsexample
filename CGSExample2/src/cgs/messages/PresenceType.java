/**
 * @file PresenceType.java
 * @page classPresenceType PresenceType
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class PresenceType: (ONLINE, OFFLINE, EITHER)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If matched players to return are online or offline at time of making request
*/
public class PresenceType {

	/**
	 * (Optional)
	 */
	public PresenceType ONLINE;

	/**
	 * (Optional)
	 */
	public PresenceType OFFLINE;

	/**
	 * (Optional)
	 */
	public PresenceType EITHER;

	/**
	 * Constructor for PresenceType that requires all members
	 * @param ONLINE
	 * @param OFFLINE
	 * @param EITHER
	 */
	public PresenceType(PresenceType ONLINE, PresenceType OFFLINE, 
			PresenceType EITHER){
		this.ONLINE = ONLINE;
		this.OFFLINE = OFFLINE;
		this.EITHER = EITHER;

	}

	/**
	 * Default Constructor for PresenceType
	*/
	public PresenceType(){

	}
}
/** @} */
