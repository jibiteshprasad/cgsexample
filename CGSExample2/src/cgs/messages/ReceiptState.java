package cgs.messages;

public enum ReceiptState
	{
	/// Receipt Received
	RECEIVED,
	/// Receipt Sent
	SENT,
	/// Receipt Denied
	DENIED,
	/// Reciept Accepted
	ACCEPTED
	
}
