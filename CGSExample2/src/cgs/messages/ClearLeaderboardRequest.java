/**
 * @file ClearLeaderboardRequest.java
 * @page classClearLeaderboardRequest ClearLeaderboardRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ClearLeaderboardRequest: (leaderboardName, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Remove all entries from the specified leaderboard table
 * 
*/
@JsonTypeName("ClearLeaderboardRequest")
public class ClearLeaderboardRequest extends GluonRequest{

	/**
	 * The name of the specific leaderboard to wipe 
	 */
	public String leaderboardName;


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ClearLeaderboardResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ClearLeaderboardResponseDelegate){
                ((ClearLeaderboardResponseDelegate)delegate).onClearLeaderboardResponse(this, (ClearLeaderboardResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ClearLeaderboardResponseDelegate){
            ((ClearLeaderboardResponseDelegate)delegate).onClearLeaderboardResponse(this, new ClearLeaderboardResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ClearLeaderboardRequest that requires all members
	 * @param leaderboardName
	 * @param passthrough
	 */
	public ClearLeaderboardRequest(String leaderboardName, String passthrough){
		this.leaderboardName = leaderboardName;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ClearLeaderboardRequest without super class members
	 * @param leaderboardName
	 */
	public ClearLeaderboardRequest(String leaderboardName){
		this.leaderboardName = leaderboardName;

	}

	/**
	 * Default Constructor for ClearLeaderboardRequest
	*/
	public ClearLeaderboardRequest(){

	}
}
/** @} */
