/**
 * @file DeleteAllAttributesResponse.java
 * @page classDeleteAllAttributesResponse DeleteAllAttributesResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class DeleteAllAttributesResponse: (playerInfo, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Returns a message to the player confirming whether all attributes have been 
 * deleted for a specified player
*/
@JsonTypeName("DeleteAllAttributesResponse")
public class DeleteAllAttributesResponse extends GluonResponse{

	/**
	 * The player whose attributes were deleted (Optional)
	 */
	public PlayerInfo playerInfo;

	/**
	 * Constructor for DeleteAllAttributesResponse that requires all members
	 * @param playerInfo
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public DeleteAllAttributesResponse(PlayerInfo playerInfo, 
			GluonResponseStatus status, ErrorType errorType, String description, 
			ErrorAction action, String passthrough){
		this.playerInfo = playerInfo;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for DeleteAllAttributesResponse without super class members
	 * @param playerInfo
	 */
	public DeleteAllAttributesResponse(PlayerInfo playerInfo){
		this.playerInfo = playerInfo;

	}
	/**
	 * Constructor for DeleteAllAttributesResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public DeleteAllAttributesResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for DeleteAllAttributesResponse
	*/
	public DeleteAllAttributesResponse(){

	}
}
/** @} */
