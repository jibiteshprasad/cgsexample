/**
 * @file CheckNickNameResponse.java
 * @page classCheckNickNameResponse CheckNickNameResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class CheckNickNameResponse: (nickName, status, errorType, description, 
 * 		action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * If status is success, the nickName was verified to be available. Otherwise 
 * check the errorType -  ACCOUNT_NAME_TAKEN will be set if the name is already 
 * in use. 
*/
@JsonTypeName("CheckNickNameResponse")
public class CheckNickNameResponse extends GluonResponse{

	/**
	 * The name that was checked for availability
	 */
	public String nickName;

	/**
	 * Constructor for CheckNickNameResponse that requires all members
	 * @param nickName
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public CheckNickNameResponse(String nickName, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.nickName = nickName;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for CheckNickNameResponse without super class members
	 * @param nickName
	 */
	public CheckNickNameResponse(String nickName){
		this.nickName = nickName;

	}
	/**
	 * Constructor for CheckNickNameResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public CheckNickNameResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for CheckNickNameResponse
	*/
	public CheckNickNameResponse(){

	}
}
/** @} */
