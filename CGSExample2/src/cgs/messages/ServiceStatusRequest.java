/**
 * @file ServiceStatusRequest.java
 * @page classServiceStatusRequest ServiceStatusRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class ServiceStatusRequest: (passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A message sent to edge server requesting service status.
*/
@JsonTypeName("ServiceStatusRequest")
public class ServiceStatusRequest extends GluonRequest{


    public void triggerDelegate(GluonResponse response, GluonResponseDelegate delegate){
        //if(ServiceStatusResponse.class.isAssignableFrom(response.getClass())){
           
            //if(delegate instanceof ServiceStatusResponseDelegate){
                ((ServiceStatusResponseDelegate)delegate).onServiceStatusResponse(this, (ServiceStatusResponse)response);
            //}
        //}
    }


    public void triggerDelegateTimeout(GluonResponseDelegate delegate){
        //if(delegate instanceof ServiceStatusResponseDelegate){
            ((ServiceStatusResponseDelegate)delegate).onServiceStatusResponse(this, new ServiceStatusResponse(GluonResponseStatus.FAILURE, ErrorType.TIMEOUT, "Request timed out waiting on server response", ErrorAction.HOLD_AND_RESEND));
        //}
    }

	/**
	 * Constructor for ServiceStatusRequest that requires all members
	 * @param passthrough
	 */
	public ServiceStatusRequest(String passthrough){
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for ServiceStatusRequest without super class members
	 */
	public ServiceStatusRequest(){

	}
}
/** @} */
