/**
 * @file SetAsPrimaryPlayerRequest.java
 * @page classSetAsPrimaryPlayerRequest SetAsPrimaryPlayerRequest
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class SetAsPrimaryPlayerRequest: (playerId, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * Keeps the specified player (and all associated attributes) attached to its 
 * facebook account and retires all other attached players.  This is intended to 
 * be used after a Facebook account has been attached to multiple players 
 * (devices e.g.) to consolidate down to one saved  game for the playerId. (See 
 * AttachFacebookCredentialRequest description for further details on the flow.)
*/
@JsonTypeName("")
public class SetAsPrimaryPlayerRequest extends GluonRequest{

	/**
	 * The ID of the player to keep attached to a facebook account (Optional)
	 */
	public String playerId;

	/**
	 * Constructor for SetAsPrimaryPlayerRequest that requires all members
	 * @param playerId
	 * @param passthrough
	 */
	public SetAsPrimaryPlayerRequest(String playerId, String passthrough){
		this.playerId = playerId;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for SetAsPrimaryPlayerRequest without super class members
	 * @param playerId
	 */
	public SetAsPrimaryPlayerRequest(String playerId){
		this.playerId = playerId;

	}

	/**
	 * Default Constructor for SetAsPrimaryPlayerRequest
	*/
	public SetAsPrimaryPlayerRequest(){

	}
}
/** @} */
