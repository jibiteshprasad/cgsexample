/**
 * @file GetLeaderboardEntryResponse.java
 * @page classGetLeaderboardEntryResponse GetLeaderboardEntryResponse
 * @{
 * 
 *
 * Gluon Class Reference
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
 * class GetLeaderboardEntryResponse: (leaderboardName, entry, status, errorType, 
 * 		description, action, passthrough)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


package cgs.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.*;
import cgs.delegates.*;

/**
 * A response to get the value for a leaderboard entry.
*/
@JsonTypeName("GetLeaderboardEntryResponse")
public class GetLeaderboardEntryResponse extends GluonResponse{

	/**
	 *
	 */
	public String leaderboardName;

	/**
	 * (Optional)
	 */
	public RankedLeaderboardEntry entry;

	/**
	 * Constructor for GetLeaderboardEntryResponse that requires all members
	 * @param leaderboardName
	 * @param entry
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 * @param passthrough
	 */
	public GetLeaderboardEntryResponse(String leaderboardName, 
			RankedLeaderboardEntry entry, GluonResponseStatus status, 
			ErrorType errorType, String description, ErrorAction action, 
			String passthrough){
		this.leaderboardName = leaderboardName;
		this.entry = entry;
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;
		this.passthrough = passthrough;

	}

	/**
	 * Constructor for GetLeaderboardEntryResponse without super class members
	 * @param leaderboardName
	 * @param entry
	 */
	public GetLeaderboardEntryResponse(String leaderboardName, 
			RankedLeaderboardEntry entry){
		this.leaderboardName = leaderboardName;
		this.entry = entry;

	}
	/**
	 * Constructor for GetLeaderboardEntryResponse with GluonResponse members
	 * @param status
	 * @param errorType
	 * @param description
	 * @param action
	 */
	public GetLeaderboardEntryResponse(GluonResponseStatus status, ErrorType errorType, String description, ErrorAction action){
		this.status = status;
		this.errorType = errorType;
		this.description = description;
		this.action = action;

	}

	/**
	 * Default Constructor for GetLeaderboardEntryResponse
	*/
	public GetLeaderboardEntryResponse(){

	}
}
/** @} */
