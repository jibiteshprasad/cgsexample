package cgs.tests;

import cgs.core.CustomGameServer;
import cgs.delegates.AuthenticatePlayerResponseDelegate;
import cgs.messages.AuthenticatePlayerRequest;
import cgs.messages.AuthenticatePlayerResponse;
import cgs.messages.CreatePlayerRequest;
import cgs.delegates.CreatePlayerResponseDelegate;
import cgs.Config;
import cgs.TestServer;
import cgs.messages.CreatePlayerResponse;

/**
 * Created with IntelliJ IDEA.
 * User: stephen.diteljan
 * Date: 6/24/13
 * Time: 6:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestAuthenticate extends AbstractTest implements AuthenticatePlayerResponseDelegate, CreatePlayerResponseDelegate
{
    public TestAuthenticate( TestServer cgs )
    {
        super( cgs );
    }

    public void runTest()
    {
        System.out.println( "in TestAuthenticate::runTest" );
        this.testState = TestState.TestRunning;

        // Create a new player. This will only work once, subsequent attempts return duplicate player error.
        String loginId = Config.testPlayerPreprod;
        String password = "noPassword";
        String gameId = Config.gameId;
        int protocolVersion = 1;
        String clientSdkVersion = "1.00";
        String email = "whatever@email.com";
        cgs.sendRequestAsTestPlayer( ( new CreatePlayerRequest( loginId, password, gameId, protocolVersion, clientSdkVersion, email )), this );
    }

    public void onCreatePlayerResponse(CreatePlayerRequest createPlayerRequest, CreatePlayerResponse createPlayerResponse)
    {
        System.out.println( "in TestAuthenticate::onCreatePlayerResponse" );

        // Now authenticate an existing player
        String loginId = Config.testPlayerPreprod;
        String password = "noPassword";
        String gameId = Config.gameId;
        int protocolVersion = 1;
        String clientSdkVersion = "1.00";
        cgs.sendRequestAsTestPlayer( ( new AuthenticatePlayerRequest( loginId, password, gameId, protocolVersion, clientSdkVersion )), this );
    }

    public void onAuthenticatePlayerResponse(AuthenticatePlayerRequest authenticatePlayerRequest, AuthenticatePlayerResponse authenticatePlayerResponse)
    {
        System.out.println( "in TestAuthenticate::onAuthenticatePlayerResponse" );

        this.cgs.playerInfo = authenticatePlayerResponse.playerInfo;

        this.testState = TestState.TestDoneOk;
    }
}
