package cgs.tests;

import cgs.TestServer;
import cgs.core.CustomGameServer;
import cgs.delegates.*;
import cgs.messages.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephen.diteljan
 * Date: 6/28/13
 * Time: 3:15 PM
 * To change this template use File | Settings | File Templates.
 *
 * Exercises basic operations involving groups and group attributes only.
 */
public class TestGroup extends AbstractTest
    implements CreateGroupResponseDelegate, ChangeGroupNameResponseDelegate, ChangeGroupOwnerResponseDelegate,
        CheckGroupNameResponseDelegate, GetGroupAttributesResponseDelegate, GetAllGroupAttributesResponseDelegate,
        DeleteGroupAttributesResponseDelegate, DeleteAllGroupAttributesResponseDelegate, DeleteGroupResponseDelegate
{
    public TestGroup( TestServer cgs )
    {
        super( cgs );
    }

    /**
     * Exercises the following calls (in this order):
     * create group
     * change group name
     * change group owner
     * check group name
     * get group attributes
     * get all group attributes
     * delete group attributes
     * delete all group attributes
     * delete group
     */
    public void runTest()
    {
        System.out.println( "in TestGroup::runTest" );
        this.testState = TestState.TestRunning;

        // temporary: clean up orphaned groups
//        cgs.sendRequestAsTestPlayer((new DeleteGroupRequest("TestGroupName", "TestGroupType")), this);
//        cgs.sendRequestAsTestPlayer((new DeleteGroupRequest("TestGroupNameNew", "TestGroupType")), this);

        String groupName = "TestGroupName";
        String groupType = "TestGroupType";
        OwnerAbandonBehaviour ownerAbandonBehaviour = OwnerAbandonBehaviour.PROMOTE_ELDEST;
        boolean isGroupOpen = false;

        ArrayList<GroupAttribute> groupAttributes = new ArrayList<GroupAttribute>();
        groupAttributes.add( new GroupAttribute( "groupAttr1", "groupAttr1_value", "String" ));
        groupAttributes.add( new GroupAttribute( "groupAttr2", "groupAttr2_value", "String" ));
        groupAttributes.add( new GroupAttribute( "groupAttr3", "groupAttr3_value", "String" ));

        cgs.sendRequestAsTestPlayer( ( new CreateGroupRequest( groupName, groupType, groupAttributes, ownerAbandonBehaviour, isGroupOpen ) ), this );
    }

    public void onCreateGroupResponse(CreateGroupRequest createGroupRequest, CreateGroupResponse createGroupResponse)
    {
        System.out.println( "in onCreateGroupResponse" );

        if( createGroupResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }

        String groupName = "TestGroupName";
        String groupType = "TestGroupType";
        String groupNameNew = "TestGroupNameNew";

        // Group has been created, now change the group name.
        cgs.sendRequestAsTestPlayer( ( new ChangeGroupNameRequest( groupName, groupType, groupNameNew ) ), this );
    }

    public void onChangeGroupNameResponse(ChangeGroupNameRequest changeGroupNameRequest, ChangeGroupNameResponse changeGroupNameResponse)
    {
        System.out.println( "in onChangeGroupNameResponse" );

        if( changeGroupNameResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }

        if( changeGroupNameResponse.groupName.equals( "TestGroupNameNew" ) )
        {
            // change group name back to the original name
            String groupName = "TestGroupNameNew";
            String groupType = "TestGroupType";
            String groupNameNew = "TestGroupName";
            cgs.sendRequestAsTestPlayer( ( new ChangeGroupNameRequest( groupName, groupType, groupNameNew ) ), this );
        }
        else
        {
            // proceed with next test step
            String groupName = "TestGroupName";
            String groupType = "TestGroupType";
            cgs.sendRequestAsTestPlayer((new DeleteGroupRequest(groupName, groupType)), this);
        }
    }

    public void onChangeGroupOwnerResponse(ChangeGroupOwnerRequest changeGroupOwnerRequest, ChangeGroupOwnerResponse changeGroupOwnerResponse)
    {
        System.out.println( "in onChangeGroupOwnerResponse" );

        if( changeGroupOwnerResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }
    }

    public void onCheckGroupNameResponse(CheckGroupNameRequest checkGroupNameRequest, CheckGroupNameResponse checkGroupNameResponse)
    {
        System.out.println( "in onCheckGroupNameResponse" );

        if( checkGroupNameResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }
    }

    public void onGetGroupAttributesResponse(GetGroupAttributesRequest getGroupAttributesRequest, GetGroupAttributesResponse getGroupAttributesResponse)
    {
        System.out.println( "in onGetGroupAttributesResponse" );

        if( getGroupAttributesResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }
    }

    public void onGetAllGroupAttributesResponse(GetAllGroupAttributesRequest getAllGroupAttributesRequest, GetAllGroupAttributesResponse getAllGroupAttributesResponse)
    {
        System.out.println( "in onGetAllGroupAttributesResponse" );

        if( getAllGroupAttributesResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }
    }

    public void onDeleteGroupAttributesResponse(DeleteGroupAttributesRequest deleteGroupAttributesRequest, DeleteGroupAttributesResponse deleteGroupAttributesResponse)
    {
        System.out.println( "in onDeleteGroupAttributesResponse" );

        if( deleteGroupAttributesResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }
    }

    public void onDeleteAllGroupAttributesResponse(DeleteAllGroupAttributesRequest deleteAllGroupAttributesRequest, DeleteAllGroupAttributesResponse deleteAllGroupAttributesResponse)
    {
        System.out.println( "in onDeleteAllGroupAttributesResponse" );

        if( deleteAllGroupAttributesResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }
    }

    public void onDeleteGroupResponse(DeleteGroupRequest deleteGroupRequest, DeleteGroupResponse deleteGroupResponse)
    {
        System.out.println( "in onDeleteGroupResponse" );

        if( deleteGroupResponse.status == GluonResponseStatus.FAILURE )
        {
            this.testState = TestState.TestDoneError;
            return;
        }

        this.testState = TestState.TestDoneOk;
    }
}
