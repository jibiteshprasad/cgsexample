package cgs.tests;

import cgs.TestServer;
import cgs.core.CustomGameServer;
import cgs.delegates.GetCurrentUTCTimeResponseDelegate;
import cgs.messages.GetCurrentUTCTimeRequest;
import cgs.messages.GetCurrentUTCTimeResponse;

/**
 * Created with IntelliJ IDEA.
 * User: stephen.diteljan
 * Date: 6/26/13
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestUTCTime extends AbstractTest implements GetCurrentUTCTimeResponseDelegate
{
    public TestUTCTime( TestServer cgs )
    {
        super( cgs );
    }

    public void runTest()
    {
        System.out.println( "in TestUTCTime::runTest" );
        this.testState = TestState.TestRunning;
        cgs.sendRequestAsTestPlayer( ( new GetCurrentUTCTimeRequest() ), this );
    }

    public void onGetCurrentUTCTimeResponse(GetCurrentUTCTimeRequest getCurrentUTCTimeRequest, GetCurrentUTCTimeResponse getCurrentUTCTimeResponse)
    {
        System.out.println( "in TestUTCTime::onAuthenticatePlayerResponse" );
        this.testState = TestState.TestDoneOk;
    }
}
