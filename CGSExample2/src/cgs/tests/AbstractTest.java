package cgs.tests;

import cgs.TestServer;

/**
 * Created with IntelliJ IDEA.
 * User: stephen.diteljan
 * Date: 6/24/13
 * Time: 6:40 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractTest
{
    public enum TestState
    {
        TestNotStarted,
        TestRunning,
        TestDoneOk,
        TestDoneError
    };

    protected TestServer cgs;
    public  TestState testState;

    private AbstractTest() {}

    public AbstractTest( TestServer cgs )
    {
        System.out.println( "base class AbstractTest constructor" );
        this.cgs = cgs;
        this.testState = TestState.TestNotStarted;
    }

    public abstract void runTest();
}
