package cgs;

import cgs.core.CustomGameServer;
import cgs.delegates.GluonResponseDelegate;
import cgs.messages.GluonRequest;
import cgs.messages.PlayerInfo;
import cgs.tests.TestAuthenticate;
import cgs.tests.AbstractTest;
import cgs.tests.TestGroup;
import cgs.tests.TestUTCTime;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: stephen.diteljan
 * Date: 6/24/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestServer extends CustomGameServer
{
    public PlayerInfo playerInfo; // this is set after "TestAuthenticate" runs

    private boolean waitingForHandshake;
    private ArrayList<AbstractTest> tests;

    public TestServer( String host, int port ) throws URISyntaxException
    {
        super( host, port );

        // comment this out if you want to reduce logging (or just change the log level)
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel( Level.ALL);
        this.logger.setLevel( Level.ALL );
        this.logger.addHandler( handler );

        createTests();
        playerInfo = null;
        waitingForHandshake = true;
    }

    private void createTests()
    {
        synchronized( this )
        {
            tests = new ArrayList<AbstractTest>();
            tests.add( new TestAuthenticate( this ) );
            tests.add( new TestUTCTime( this ) );
            tests.add( new TestGroup( this ) );
        }
    }

    public void sendRequestAsAdminPlayer( GluonRequest req, GluonResponseDelegate delegate )
    {
        try
        {
            sendGluonRequest( Config.admin, req, delegate );
        }
        catch( TimeoutException ex )
        {
            System.out.println( "timeout exception: "+ ex );
        }
    }

    public void sendRequestAsTestPlayer( GluonRequest req, GluonResponseDelegate delegate )
    {
        try
        {
            // use "Config.testPlayerPreprod" when authenticating, otherwise use "playerInfo.playerId" once authentication has been established.
            String playerId = (playerInfo == null) ? Config.testPlayerPreprod : playerInfo.playerId;
            sendGluonRequest( playerId, req, delegate );
        }
        catch( TimeoutException ex )
        {
            System.out.println( "timeout exception: "+ ex );
        }
    }

    public boolean finishedRunning()
    {
        synchronized( this )
        {
            return( (!waitingForHandshake) && (tests.size() == 0) );
        }
    }

    public void update()
    {
        if( !waitingForHandshake )
        {
            synchronized( this )
            {
                if( tests.size() > 0 )
                {
                    switch( tests.get( 0 ).testState )
                    {
                        case TestNotStarted:
                        {
                            tests.get( 0 ).runTest();
                            break;
                        }
                        case TestRunning:
                        {
                            // just wait for test to complete
                            break;
                        }
                        case TestDoneOk:
                        {
                            tests.remove( 0 );
                            break;
                        }
                        case TestDoneError:
                        {
                            System.out.println( "A test failed! Stopping execution" );
                            tests.clear(); // this will stop execution
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            System.out.println( "waiting for CMRS handshake to complete..." );
        }
    }

    /**
     * The extension point for CGS
     */
    public void onConnected()
    {
        System.out.println( "got CMRS handshake, ready to run tests" );
        waitingForHandshake = false;
    }

    /**
     * The extension point for CGS
     */
    public void onDisconnected()
    {
        System.out.println( "in onDisconnected()" );
    }

    /**
     * The extension point for CGS
     */
    public void onLockTimeout()
    {
        System.out.println( "in onLockTimeout()" );
    }
    /**
     * The extension point for CGS
     */
    public void onLockInterrupted()
    {
        System.out.println( "in onLockInterrupted()" );
    }
}
